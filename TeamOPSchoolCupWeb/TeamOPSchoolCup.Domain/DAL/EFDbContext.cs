﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Data.Entity.ModelConfiguration.Conventions;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using System.Data.Entity.Infrastructure;

namespace TeamOPSchoolCup.Domain.DAL
{
  public class EFDbContext : DbContext
  {
    //Although we might want the specific types instead...
    public DbSet<Member> Members { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<Region> Regions { get; set; }
    public DbSet<Team> Teams { get; set; }
    public DbSet<TeamMembership> TeamMemberships { get; set; }
    public DbSet<TeamCoach> TeamCoaches { get; set; }
    public DbSet<Result> Results { get; set; }
    public DbSet<Event> Events { get; set; }
    public DbSet<NSSF> NSSFs { get; set; }
    public DbSet<Sport> Sports { get; set; }
    public DbSet<Topic> Topics { get; set; }
    public DbSet<Message> Messages { get; set; }
    public DbSet<School> Schools { get; set; }
    public DbSet<Advertisement> Advertisements { get; set; }
    public DbSet<AdvertisementStatistic> AdvertisementStatistics { get; set; }
    public DbSet<CompetitionYear> CompetitionYears { get; set; }
    public DbSet<Record> Records { get; set; }
    public DbSet<RecordHolder> RecordHolders { get; set; }
    //Users DataTables
    public DbSet<Coach> Coaches { get; set; }
    public DbSet<NSSFRep> NSSFReps { get; set; }
    public DbSet<SCC> SCCs { get; set; }
    public DbSet<Student> Students { get; set; }
    public DbSet<ISFAdmin> ISFAdmins { get; set; }

    //Ranking-stuff
    public DbSet<BestStudentResult> BestStudentResults { get; set; }
    public DbSet<Point> Points { get; set; }
    public DbSet<TeamResult> TeamResults { get; set; }
    public DbSet<BestTeamResult> BestTeamResults { get; set; }
    public DbSet<StudentRanking> StudentRankings { get; set; }
    public DbSet<TeamRanking> TeamRankings { get; set; }

    //Meeting
    public DbSet<Meeting> Meetings { get; set; }
    public DbSet<ISFMeeting> ISFMeetings { get; set; }
    public DbSet<RegionalMeeting> RegionalMeetings { get; set; }
    public DbSet<CoachMeeting> CoachMeetings { get; set; }
    
    //Constructor
    public EFDbContext() : base("TeamOPSchoolCup")
    {
      this.Configuration.LazyLoadingEnabled = true;
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      //1-1 relations
      //Member class with his subs
      modelBuilder.Entity<ISFAdmin>().HasKey(m => m.MemberId).HasRequired(m => m.Member).WithOptional(s => s.ISFAdmin);
      modelBuilder.Entity<NSSFRep>().HasKey(m => m.MemberId).HasRequired(m => m.Member).WithOptional(s => s.NSSFRep);
      modelBuilder.Entity<SCC>().HasKey(m => m.MemberId).HasRequired(m => m.Member).WithOptional(s => s.SCC);
      modelBuilder.Entity<Coach>().HasKey(m => m.MemberId).HasRequired(m => m.Member).WithOptional(s => s.Coach);
      modelBuilder.Entity<Student>().HasKey(m => m.MemberId).HasRequired(m => m.Member).WithOptional(s => s.Student);

     
      //modelBuilder.Entity<SCC>().HasRequired(s => s.School).WithOptional(s => s.SCC).Map(x => x.MapKey("SchoolId"));
      
      modelBuilder.Entity<TeamMembership>().HasKey(tm => new { tm.MemberId,tm.TeamId });
      modelBuilder.Entity<TeamCoach>().HasKey(tc => new { tc.MemberId, tc.TeamId });
      modelBuilder.Entity<AdvertisementStatistic>().HasKey(adstat => new { adstat.AdvertisementId, adstat.RegionId, adstat.Gender_Value, adstat.SportId });
      
      //Rankings
      modelBuilder.Entity<BestStudentResult>().HasKey(bsr => new { bsr.MemberId, bsr.EventId, bsr.CompetitionYear, bsr.IsOfficial });
      modelBuilder.Entity<TeamResult>().HasKey(tr => new { tr.MeetingId, tr.TeamId });
      modelBuilder.Entity<BestTeamResult>().HasKey(btr => new { btr.TeamId, btr.MeetingId, btr.CompetitionYear });
      modelBuilder.Entity<StudentRanking>().HasKey(tr => new { tr.MemberId, tr.EventId });
      modelBuilder.Entity<TeamRanking>().HasKey(tr => new { tr.TeamId, tr.EventId });

      //Region class
      modelBuilder.Entity<Region>().HasOptional(r => r.Parent);
      modelBuilder.Entity<NSSF>().HasRequired(n => n.Region).WithOptional(r => r.NSSF).Map(x => x.MapKey("RegionId"));
      //modelBuilder.Entity<Region>().HasOptional(r => r.NSSF).WithRequired(r => r.Region);
     
      //Meetings
      modelBuilder.Entity<ISFMeeting>().HasKey(im => new { im.MeetingId });
      modelBuilder.Entity<ISFMeeting>().HasRequired(im => im.Meeting).WithOptional(m => m.ISFMeeting);
      modelBuilder.Entity<RegionalMeeting>().HasKey(im => new { im.MeetingId });
      modelBuilder.Entity<RegionalMeeting>().HasRequired(im => im.Meeting).WithOptional(m => m.RegionalMeeting);
      modelBuilder.Entity<CoachMeeting>().HasKey(im => new { im.MeetingId });
      modelBuilder.Entity<CoachMeeting>().HasRequired(im => im.Meeting).WithOptional(m => m.CoachMeeting);

      modelBuilder.Entity<Topic>().HasOptional(t => t.Coach).WithMany(c => c.Topics).Map(y => y.MapKey("CoachId"));
      modelBuilder.Entity<Topic>().HasOptional(t => t.Student).WithMany(c => c.Topics).Map(y => y.MapKey("StudentId"));
      modelBuilder.Entity<Message>().HasOptional(t => t.Coach).WithMany(c => c.Messages).Map(y => y.MapKey("CoachId"));
      modelBuilder.Entity<Message>().HasOptional(t => t.Student).WithMany(c => c.Messages).Map(y => y.MapKey("StudentId"));
      //Conventions
      modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
      modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
      modelBuilder.Conventions.Remove<IncludeMetadataConvention>();
      
    }
  }
}
