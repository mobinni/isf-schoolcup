﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using System.Diagnostics;

namespace TeamOPSchoolCup.Domain.DAL
{
  //TODO While we're in development mode... Remove from deployment situations!
  //After this, service methods with EFDBContext have to be made protected
  public class EFDbContextInitializer : DropCreateDatabaseAlways<EFDbContext>
  {
    protected override void Seed(EFDbContext context)
    {
      base.Seed(context);
      DateTime startDataTime = DateTime.Now;
      //Creating a unique key on events, because events keep getting added 5 times. This should find me the error.
      context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX UN_Event_Name ON Event (Name)");
      context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX UN_Member_Email ON Member (Email)");
      context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX UN_School_Email ON School (Email)");
      context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX UN_NSSF_Email ON NSSF (Email)");
      context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX UN_RECORDHOLDER_NAME ON RecordHolder (Name)");
      CompetitionYear cy2013 = new CompetitionYear { CompetitionYearValue = 2013 };
      CompetitionYearService.AddCompetitionYear(context, cy2013);
      CompetitionYear cy2014 = new CompetitionYear { CompetitionYearValue = 2014 };
      CompetitionYearService.AddCompetitionYear(context, cy2014);

      List<String> roles = new List<String>() { 
         "ISFAdmin" ,
         "NSSFRep" ,
         "SCC" ,
         "Coach" ,
         "Student",
         "NotConfirmed"
      };

      foreach (String role in roles) RoleService.AddRole(role);

      //As for user, testdata should be made for the subentities
      //Add Regions
      RegionService.AddRegion(context, new Region { Name = "World", DateJoined = DateTime.Today });
      RegionService.AddRegion(context, new Region { Name = "North-America", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "World") });
      RegionService.AddRegion(context, new Region { Name = "USA", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "North-America") });
      RegionService.AddRegion(context, new Region { Name = "Florida", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "USA") });
      RegionService.AddRegion(context, new Region { Name = "New York", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "USA") });
      RegionService.AddRegion(context, new Region { Name = "Europe", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "World") });
      RegionService.AddRegion(context, new Region { Name = "Belgium", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "Europe") });
      RegionService.AddRegion(context, new Region { Name = "Flanders", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "Belgium") });
      RegionService.AddRegion(context, new Region { Name = "Wallonia", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "Belgium") });
      RegionService.AddRegion(context, new Region { Name = "Asia", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "World") });
      RegionService.AddRegion(context, new Region { Name = "Japan", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "Asia") });
      RegionService.AddRegion(context, new Region { Name = "Hokkaido", DateJoined = DateTime.Today, Parent = RegionService.GetRegion(context, "Japan") });

      //Add NSSFs
      NSSFService.AddNSSF(context, new NSSF { Name = "SNS", Street = "Atkins Avenue 613", City = "Brooklyn", Zipcode = "USA-5586", Country = "USA", PhoneNumber = "+32925473658", FaxNumber = "+32925473658", Email = "nssf1@sns.com", DateAdded = new DateTime(2012, 5, 5), Region = RegionService.GetRegion(context, "New York") });
      NSSFService.AddNSSF(context, new NSSF { Name = "SVS", Street = "Lombardenvest 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", FaxNumber = "+32925473658", Email = "nssf2@svs.be", DateAdded = new DateTime(2012, 5, 5), Region = RegionService.GetRegion(context, "Flanders") });
      NSSFService.AddNSSF(context, new NSSF { Name = "SBS", Street = "Bird Road 6520", City = "Miami", Zipcode = "USA-5586", Country = "USA", PhoneNumber = "+32925473658", FaxNumber = "+32925473658", Email = "nssf3@sbs.com", DateAdded = new DateTime(2012, 5, 5), Region = RegionService.GetRegion(context, "Florida") });
      NSSFService.AddNSSF(context, new NSSF { Name = "チーケーチー", Street = "網走市字呼人190番地の1", City = "網走市", Zipcode = "JP-5586", Country = "日本", PhoneNumber = "+32925473658", FaxNumber = "+32925473658", Email = "nssf4@tkt.jp", DateAdded = new DateTime(2012, 5, 5), Region = RegionService.GetRegion(context, "Hokkaido") });
      NSSFService.AddNSSF(context, new NSSF { Name = "LKL", Street = "Rue Verte 61", City = "Mons", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", FaxNumber = "+32925473658", Email = "nssf5@lkl.be", DateAdded = new DateTime(2012, 5, 5), Region = RegionService.GetRegion(context, "Wallonia") });

      //Add Schools
      SchoolService.AddSchool(context, new School { Name = "Brooklyn High", Street = "Zottendijk 5", City = "Brooklyn", Zipcode = "BE-5586", Email = "isfschool1@wimtibackx.be", AbbrName = "BH", NSSF = NSSFService.GetNSSF(context, "SNS") }, "http://example.com");
      SchoolService.AddSchool(context, new School { Name = "Sint-Lodewijk", Street = "Zottendijk 6", City = "Antwerpen", Zipcode = "BE-5586", Email = "isfschool2@wimtibackx.be", AbbrName = "SL", NSSF = NSSFService.GetNSSF(context, "SVS") }, "http://example.com");
      SchoolService.AddSchool(context, new School { Name = "Miami High", Street = "Zottendijk 7", City = "Miami", Zipcode = "BE-5586", Email = "isfschool3@wimtibackx.be", AbbrName = "MH", NSSF = NSSFService.GetNSSF(context, "SBS") }, "http://example.com");
      SchoolService.AddSchool(context, new School { Name = "北海道札幌南高等学校", Street = "Zottendijk 8", City = "網走市", Zipcode = "BE-5586", Email = "isfschool4@wimtibackx.be", AbbrName = "HSMHS", NSSF = NSSFService.GetNSSF(context, "チーケーチー") }, "http://example.com");
      SchoolService.AddSchool(context, new School { Name = "Athénée Royal M Bervoets", Street = "Zottendijk 8", City = "Mons", Zipcode = "BE-5586", Email = "isfschool5@wimtibackx.be", AbbrName = "ARMB", NSSF = NSSFService.GetNSSF(context, "LKL") }, "http://example.com");

      //Add Coaches
      Role coachRole = RoleService.GetRole(context, "Coach");
      MemberService.AddCoach(context, new Coach { Member = new Member { Name = "Joe Bell", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "jan@coach.be", Gender = Gender.MALE, BirthDate = new DateTime(1974, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("8fbb8e07-6261-47d6-9517-79b8b9d0b353"), Password = "$2a$12$rNXYj.REuEepCNC/6Usl7.3RZjie5iqYicgSyJzoX0fu6N/PWtKlm", Roles = new List<Role>() { coachRole } }, Photo = "photo", School = SchoolService.GetSchool(context, abbr: "SL"), MemberId = new Guid("8fbb8e07-6261-47d6-9517-79b8b9d0b353") });
      MemberService.AddCoach(context, new Coach { Member = new Member { Name = "Joske Vermeulen", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "joske@coach.be", Gender = Gender.MALE, BirthDate = new DateTime(1972, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("702e5244-5453-4c36-bdb8-56700710caf8"), Password = /*"$2a$12$7CKB.GqnHo3Ex/mnFmwwpeL5tlJTWYEVckmx0a9.vTJsnxTZoTUOW"*/ "$2a$12$fAAtDErhermoG9Gz3T2kuOLkM/78nTrspC4Xap6LdnXpA7A8a7Ws6", Roles = new List<Role>() { coachRole } }, Photo = "photo", School = SchoolService.GetSchool(context, abbr: "BH"), MemberId = new Guid("702e5244-5453-4c36-bdb8-56700710caf8") });
      MemberService.AddCoach(context, new Coach { Member = new Member { Name = "Sophie Gates", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "anneke@coach.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1973, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("f2e1221c-58a7-4460-868f-487555718763"), Password = "$2a$12$h0/X4nMoI66qO2ljd4pqNeOITDogu4kEArjhlpPbIKq0Zf1gzXzAG", Roles = new List<Role>() { coachRole } }, Photo = "photo", School = SchoolService.GetSchool(context, abbr: "MH"), MemberId = new Guid("f2e1221c-58a7-4460-868f-487555718763") });
      MemberService.AddCoach(context, new Coach { Member = new Member { Name = "浜崎あゆみ", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "catherine.horvat@coach.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1974, 5, 29), PhoneNumber = "+32925473658", MemberId = new Guid("bb048bbb-e006-42a4-8bc2-cb207930a794"), Password = "$2a$12$7NcNxq4K9POyVcQaKJxobeLo33gDDYPqv43PjdLPFtZhHy736Bpu6", Roles = new List<Role>() { coachRole } }, Photo = "photo", School = SchoolService.GetSchool(context, abbr: "HSMHS"), MemberId = new Guid("bb048bbb-e006-42a4-8bc2-cb207930a794") });
      MemberService.AddCoach(context, new Coach { Member = new Member { Name = "Yves Rideau", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "yves@schoolcup.org", Gender = Gender.FEMALE, BirthDate = new DateTime(1974, 5, 29), PhoneNumber = "+32925473658", MemberId = Guid.NewGuid(), Password = BCrypt.HashPassword(BCrypt.GenerateSalt(4), BCrypt.GenerateSalt(12)), Roles = new List<Role>() { coachRole } }, Photo = "photo", School = SchoolService.GetSchool(context, abbr: "ARMB"), MemberId = Guid.NewGuid() });

      //Add NSSFReps
      Role nssfRepRole = RoleService.GetRole(context, "NSSFRep");
      MemberService.AddNSSFRep(context, new NSSFRep { Member = new Member { Name = "James Smith", Street = "Zottendijk 5", City = "Brooklyn", Zipcode = "BE-5586", Country = "USA", Email = "peter@nssfrep.be", Gender = Gender.MALE, BirthDate = new DateTime(1972, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("ada84fde-5903-4a67-a41f-770ec591b60f"), Password = /*"$2a$12$0joe/T7mQ/nfxQMOOmoOJeMxjg8m/84hSH9/FQwn1xX8CLXNgn9Da"*/ "$2a$12$fAAtDErhermoG9Gz3T2kuOLkM/78nTrspC4Xap6LdnXpA7A8a7Ws6", Roles = new List<Role>() { nssfRepRole } }, MemberId = new Guid("ada84fde-5903-4a67-a41f-770ec591b60f") }, NSSFService.GetNSSF("SVS").NSSFId);
      MemberService.AddNSSFRep(context, new NSSFRep { Member = new Member { Name = "Wim Segers", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "snow@nssfrep.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1973, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("f136d189-c305-4afc-9b41-61a626760385"), Password = "$2a$12$xWNiYKtnGJVCt46tfBFM/.JNlHcn2fcwdEjRe85aCpfqUxwapbsSe", Roles = new List<Role>() { nssfRepRole } }, MemberId = new Guid("f136d189-c305-4afc-9b41-61a626760385") }, NSSFService.GetNSSF("SBS").NSSFId);
      MemberService.AddNSSFRep(context, new NSSFRep { Member = new Member { Name = "John Johnson", Street = "Zottendijk 5", City = "Miami", Country = "USA", Zipcode = "BE-5586", Email = "klein@nssfrep.be", Gender = Gender.MALE, BirthDate = new DateTime(1972, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("fc4eb004-bc05-41fc-9f4a-ad84e1d6e39a"), Password = "$2a$12$6w9fbZBJgysvNiMCGjxuS.r9RrTQQdCK4UqB5WvE2kJIwF9OCShha", Roles = new List<Role>() { nssfRepRole } }, MemberId = new Guid("fc4eb004-bc05-41fc-9f4a-ad84e1d6e39a") }, NSSFService.GetNSSF("SNS").NSSFId);
      MemberService.AddNSSFRep(context, new NSSFRep { Member = new Member { Name = "小川貴博", Street = "Zottendijk 5", City = "網走市", Country = "日本", Zipcode = "BE-5586", Email = "bruce@nssfrep.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1973, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("9ef24e4c-cdf7-4dc8-8f0e-71a1f79119d2"), Password = "$2a$12$yncakOSzU5SoYZOVbC8c/.Z336vN1xph4fRquX5wmg5l8XojSoY/q", Roles = new List<Role>() { nssfRepRole } }, MemberId = new Guid("9ef24e4c-cdf7-4dc8-8f0e-71a1f79119d2") }, NSSFService.GetNSSF("チーケーチー").NSSFId);
      MemberService.AddNSSFRep(context, new NSSFRep { Member = new Member { Name = "Jacques-Yves Cousteau", Street = "Zottendijk 5", City = "Mons", Country = "Belgium", Zipcode = "BE-5586", Email = "jacques@schoolcup.org", Gender = Gender.FEMALE, BirthDate = new DateTime(1973, 4, 29), PhoneNumber = "+32925473658", MemberId = Guid.NewGuid(), Password = BCrypt.HashPassword(BCrypt.GenerateSalt(4), BCrypt.GenerateSalt(12)), Roles = new List<Role>() { nssfRepRole } } }, NSSFService.GetNSSF("LKL").NSSFId);

      //Add SCCs
      Role sccRole = RoleService.GetRole(context, "SCC");
      MemberService.AddSCC(context, new SCC { Member = new Member { Name = "John Mcain", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s1@scc.be", Gender = Gender.MALE, BirthDate = new DateTime(1972, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("7b6bb214-a054-4148-ae45-96f6a88b2fb7"), Password = /*"$2a$12$4aC.tVgpa4Pm0TsoUYRlvOOQTQlBLNCJg8wgQyBbxY.vtkxGQ7EmW"*/ "$2a$12$fAAtDErhermoG9Gz3T2kuOLkM/78nTrspC4Xap6LdnXpA7A8a7Ws6", Roles = new List<Role>() { sccRole } }, MemberId = new Guid("7b6bb214-a054-4148-ae45-96f6a88b2fb7") }, SchoolService.GetSchool(context, abbr: "BH").SchoolId);
      MemberService.AddSCC(context, new SCC { Member = new Member { Name = "Thomas Van Dyck", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s2@scc.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1973, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("0adf3c6f-bf92-4d82-8e6d-e4a15ca4a0f8"), Password = "$2a$12$duDCxibhyhmjaY3C9iKIS.jWH.oVCKTTrwT66LBuNbY1dRU805oZ6", Roles = new List<Role>() { sccRole } }, MemberId = new Guid("0adf3c6f-bf92-4d82-8e6d-e4a15ca4a0f8") }, SchoolService.GetSchool(context, abbr: "SL").SchoolId);
      MemberService.AddSCC(context, new SCC { Member = new Member { Name = "Will Adams", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s3@scc.be", Gender = Gender.MALE, BirthDate = new DateTime(1972, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("461a92c9-0c85-4cf3-92bb-4bc7454e2ede"), Password = "$2a$12$ZDEm1Z5ZW38XtyWNOD.OuuilDJ8VOLYVTzJNzPhuZoRgCg375tC9m", Roles = new List<Role>() { sccRole } }, MemberId = new Guid("461a92c9-0c85-4cf3-92bb-4bc7454e2ede") }, SchoolService.GetSchool(context, abbr: "MH").SchoolId);
      MemberService.AddSCC(context, new SCC { Member = new Member { Name = "山田涼介", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s4@scc.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1973, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("fc1ce184-f79a-4f73-9ad1-2d0687918bea"), Password = "$2a$12$bK1JnOqBoyJTadPsWkZTDuy3bq0Qhf3eTV0siOO81hF1jZeC6FXfy", Roles = new List<Role>() { sccRole } }, MemberId = new Guid("fc1ce184-f79a-4f73-9ad1-2d0687918bea") }, SchoolService.GetSchool(context, abbr: "HSMHS").SchoolId);
      MemberService.AddSCC(context, new SCC { Member = new Member { Name = "Guillaume François", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s5@schoolcup.org", Gender = Gender.FEMALE, BirthDate = new DateTime(1973, 4, 29), PhoneNumber = "+32925473658", MemberId = Guid.NewGuid(), Password = BCrypt.HashPassword(BCrypt.GenerateSalt(4), BCrypt.GenerateSalt(12)), Roles = new List<Role>() { sccRole } } }, SchoolService.GetSchool(context, abbr: "ARMB").SchoolId);

      //Add Students
      Role studentRole = RoleService.GetRole(context, "Student");
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Alva Theo", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s1@student.be", Gender = Gender.MALE, BirthDate = new DateTime(1995, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("63fa4ea2-b2f6-40f9-8762-009c72dcc09e"), Password = "$2a$12$ZiGN5ntE/4EqvhsCi6D6s.g1qUD9KRaJB.uoGATPMdtpXIjjmpTU2", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("63fa4ea2-b2f6-40f9-8762-009c72dcc09e") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Darcey Rosalynne", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s2@studente.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1996, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("b10f1908-0c62-4cd4-83fd-8639f672015b"), Password = "$2a$12$2ZWMxdx/TTRbFT0kvT6TG.WHq58rhtQ1MB6S4lCBE8hJ9Y1Sv4Mdu", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("b10f1908-0c62-4cd4-83fd-8639f672015b") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Raven Roswell", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s3@student.be", Gender = Gender.MALE, BirthDate = new DateTime(1997, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("bc2132f3-ba91-4ab5-a16e-e153c1f2bf00"), Password = "$2a$12$XRnWF9P0px3q6VFeCvMchulbN9mxp/aSN8wjTtgfahfE588KRFnqu", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("bc2132f3-ba91-4ab5-a16e-e153c1f2bf00") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Rylee Wenona", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s4@student.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1998, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("53ff2529-8793-4271-b86a-5035c12ec3b5"), Password = "$2a$12$9e6ph2DSXRkwgyR1S80v4O4jd/YS67o6Qy957Cv8Qg/WyJQ3dKtOG" }, Photo = "photo", MemberId = new Guid("53ff2529-8793-4271-b86a-5035c12ec3b5") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Bertie Graham", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s5@student.be", Gender = Gender.MALE, BirthDate = new DateTime(1995, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("1c9f755b-04bf-422a-94da-37245e40b282"), Password = "$2a$12$mXpP6Xk0MFVupVNyrX4WXevK4yVfOnMfsEfpHtc9qoYcKVXmWuD6W", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("1c9f755b-04bf-422a-94da-37245e40b282") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Shae Magdalene", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s6@student.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1996, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("39be4192-a5e2-45d7-9ed9-bd5b35a7c946"), Password = "$2a$12$oixkMbL2/P5WW3J9y.cdjuwnsE9xTiV7KnIhuxOJtdr/s4lr1bbxK", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("39be4192-a5e2-45d7-9ed9-bd5b35a7c946") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Cletis Jeffrey", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s7@student.be", Gender = Gender.MALE, BirthDate = new DateTime(1997, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("1038d147-f30c-4ec3-b836-dd4ae2520d77"), Password = "$2a$12$I1n35jqbph70mYjyZcvtkOpZOoRXDgHUgFBLwQBzJkxyHAX0Mmzpm", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("1038d147-f30c-4ec3-b836-dd4ae2520d77") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Eleanore Brittania", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s8@student.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1998, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("b6f5e520-032f-486d-b566-473274cab327"), Password = "$2a$12$D4mjoRhfw6CuGjTlD/OhXu/O4rRj/XBVPpxakspPpB0ag16JE/ENG", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("b6f5e520-032f-486d-b566-473274cab327") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Vance Daley", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s9@student.be", Gender = Gender.MALE, BirthDate = new DateTime(1995, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("9d0e8cb8-3352-42af-92f8-636fc8e7adff"), Password = "$2a$12$Ey84eUB82M348u1Pbwyp5Op7V/WERnoir3xyBbfZ2KgmM.FkR7haC", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("9d0e8cb8-3352-42af-92f8-636fc8e7adff") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Roxanna Odetta", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s10@student.be", Gender = Gender.FEMALE, BirthDate = new DateTime(1996, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("c97a4c7b-bd68-466e-9be3-dea6f08b9a07"), Password = /*"$2a$12$qy5.9VMNSKLgUCk6LhRIUunMeq0KWVEySUXV/K/OmI0OOjySmWWiy"*/ "$2a$12$fAAtDErhermoG9Gz3T2kuOLkM/78nTrspC4Xap6LdnXpA7A8a7Ws6", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("c97a4c7b-bd68-466e-9be3-dea6f08b9a07") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Emery Jules", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "s11@student.be", Gender = Gender.MALE, BirthDate = new DateTime(1997, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("99832e41-a9f6-4fcb-b175-39eec1767546"), Password = "$2a$12$Ae6/ZgEEhXC1c4Kim16qsO8rrfBO97yHdM5OungNujGQqjgjTnnj2", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("99832e41-a9f6-4fcb-b175-39eec1767546") });
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "Omar Joe", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "thomas.ducheyne@student.kdg.be", Gender = Gender.MALE, BirthDate = new DateTime(1998, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("5be72ddf-b13b-4937-82eb-6eb551b71821"), Password = "$2a$12$5g7D6FfEid.ojkpwDmwnieS6CIbgynF2qLGwAeuqSklnFxfsWZDXa", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("5be72ddf-b13b-4937-82eb-6eb551b71821") });

      SportService.AddSport(context, new Sport { Name = "Swimming", Abbreviation = "SWI" });
      SportService.AddSport(context, new Sport { Name = "Athletics", Abbreviation = "ATH" });

      TeamService.AddTeam(context, new Team { Name = "Les françois vite", Photo = "photo", Logo = "logo", Gender = Gender.MALE, School = SchoolService.GetSchool(context, abbr: "ARMB"), Sport = SportService.GetSport(context, abbr: "SWI") });
      TeamService.AddTeam(context, new Team { Name = "Streamliners", Photo = "photo", Logo = "logo", Gender = Gender.FEMALE, School = SchoolService.GetSchool(context, abbr: "BH"), Sport = SportService.GetSport(context, abbr: "SWI") });
      TeamService.AddTeam(context, new Team { Name = "Prototypes", Photo = "photo", Logo = "logo", Gender = Gender.MALE, School = SchoolService.GetSchool(context, abbr: "MH"), Sport = SportService.GetSport(context, abbr: "SWI") });

      TeamService.AddTeamCoach(context, TeamService.GetTeam(context, "Les françois vite").TeamId, MemberService.GetMember(context,email:"yves@schoolcup.org").MemberId);
      TeamService.AddTeamCoach(context, TeamService.GetTeam(context, "Streamliners").TeamId, MemberService.GetMember(context,email:"joske@coach.be").MemberId);
      TeamService.AddTeamCoach(context, TeamService.GetTeam(context, "Prototypes").TeamId, MemberService.GetMember(context, email: "anneke@coach.be").MemberId);

      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Les françois vite").TeamId, MemberService.GetMember(context, email: "s1@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Streamliners").TeamId, MemberService.GetMember(context, email: "s2@studente.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Les françois vite").TeamId, MemberService.GetMember(context, email: "s3@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Streamliners").TeamId, MemberService.GetMember(context, email: "s4@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Les françois vite").TeamId, MemberService.GetMember(context, email: "s5@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Streamliners").TeamId, MemberService.GetMember(context, email: "s6@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Prototypes").TeamId, MemberService.GetMember(context, email: "s7@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Streamliners").TeamId, MemberService.GetMember(context, email: "s8@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Prototypes").TeamId, MemberService.GetMember(context, email: "s9@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Streamliners").TeamId, MemberService.GetMember(context, email: "s10@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Prototypes").TeamId, MemberService.GetMember(context, email: "s11@student.be").MemberId);
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Prototypes").TeamId, MemberService.GetMember(context, email: "thomas.ducheyne@student.kdg.be").MemberId);

      //Add Events

      EventService.AddEvent(context, new Event { Name = "100m fly short", IsOfficial = true, Unit = Event.EUnit.TIME, Directionality = Event.EDirectionality.DESC, MinimumValue = 0, MaximumValue = 3, Sport = SportService.GetSport(context, abbr: "SWI"), Gender_Value = 0 });
      EventService.AddEvent(context, new Event { Name = "100m baksteenslag", IsOfficial = false, Unit = Event.EUnit.TIME, Directionality = Event.EDirectionality.DESC, MinimumValue = 0, MaximumValue = 3, Sport = SportService.GetSport(context, abbr: "SWI"), Gender_Value = 0 });
      EventService.AddEvent(context, new Event { Name = "100m fly long", IsOfficial = true, Unit = Event.EUnit.TIME, Directionality = Event.EDirectionality.DESC, MinimumValue = 0, MaximumValue = 3, Sport = SportService.GetSport(context, abbr: "SWI"), Gender_Value = 0 });
      EventService.AddEvent(context, new Event { Name = "close-jumping", IsOfficial = false, Unit = Event.EUnit.DISTANCE, Directionality = Event.EDirectionality.ASC, MinimumValue = 0, MaximumValue = 3, Sport = SportService.GetSport(context, abbr: "ATH"), Gender_Value = 0 });
      EventService.AddEvent(context, new Event { Name = "100m sprint", IsOfficial = true, Unit = Event.EUnit.TIME, Directionality = Event.EDirectionality.DESC, MinimumValue = 0, MaximumValue = 3, Sport = SportService.GetSport(context, abbr: "ATH"), Gender_Value = 0 });
      EventService.AddEvent(context, new Event { Name = "100m breaststroke short", IsOfficial = true, Unit = Event.EUnit.TIME, Directionality = Event.EDirectionality.DESC, MinimumValue = 0, MaximumValue = 3, Sport = SportService.GetSport(context, abbr: "SWI"), Gender_Value = 0 });
      EventService.AddEvent(context, new Event { Name = "100m breaststroke long", IsOfficial = true, Unit = Event.EUnit.TIME, Directionality = Event.EDirectionality.DESC, MinimumValue = 0, MaximumValue = 3, Sport = SportService.GetSport(context, abbr: "SWI"), Gender_Value = 0 });

      //MemberService.AddISFAdmin(context, new ISFAdmin { Member = new Member { Name = "IA1", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Email = "ia1@isfsports.org", Country = "Belgium", Gender = Gender.MALE, BirthDate = new DateTime(1975, 4, 29), PhoneNumber = "+32925473658", UserId = "3258a995-5db0-4d6d-a4af-24d2e8990768" } });

      Role isfAdminRole = RoleService.GetRole(context, "ISFAdmin");
      MemberService.AddISFAdmin(context, new ISFAdmin { Member = new Member { Name = "IA1", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Email = "ia1@isfsports.org", Country = "Belgium", Gender = Gender.MALE, BirthDate = new DateTime(1975, 4, 29), PhoneNumber = "+32925473658", MemberId = new Guid("3258a995-5db0-4d6d-a4af-24d2e8990768"), Password = /*"$2a$12$1Bwp.xKthiUkoMMhrWAuVeYqKXgIau0QvaxhhSRdDXZQccFbR0TtC"*/"$2a$12$fAAtDErhermoG9Gz3T2kuOLkM/78nTrspC4Xap6LdnXpA7A8a7Ws6", Roles = new List<Role>() { isfAdminRole } }, MemberId = new Guid("3258a995-5db0-4d6d-a4af-24d2e8990768") });

      MeetingService.AddISFMeeting(context, new ISFMeeting { ISFAdmin = MemberService.GetISFAdmins(context).First(), Meeting = new Meeting { Date = DateTime.Today, IsOfficial = true, Name = "WC 2013", Location = "Foo", Events = SportService.GetSport(context, abbr: "ATH").Events }, Regions = new List<Region> { RegionService.GetWorld(context) } });

      MeetingService.AddCoachMeeting(context, new CoachMeeting { Coach = MemberService.GetMember(context, email: "joske@coach.be").Coach, Meeting = new Meeting { Date = DateTime.Today, IsOfficial = false, Name = "Some Meeting", Location = "Bar", Events = SportService.GetSport(context, abbr: "SWI").Events }, TeamId = MemberService.GetMember(context, email: "joske@coach.be").Coach.TeamCoaches.First().TeamId });
      MeetingService.AddCoachMeeting(context, new CoachMeeting { Coach = MemberService.GetMember(context, email: "joske@coach.be").Coach, Meeting = new Meeting { Date = new DateTime(2013, 01, 05), IsOfficial = false, Name = "Some Other Meeting", Location = "Bar", Events = SportService.GetSport(context, abbr: "SWI").Events }, TeamId = MemberService.GetMember(context, email: "joske@coach.be").Coach.TeamCoaches.First().TeamId });

      //context.SaveChanges();

      //this.InsertResults(context);
      this.InsertDataForRanking(context);

      new List<TeamRanking> {
        new TeamRanking { TeamId=4, EventId=5,CurrentWorldRanking=2,BestWorldRankingThisYear=2 },
        new TeamRanking { TeamId=4, EventId=6,CurrentWorldRanking=1,BestWorldRankingThisYear=1 },
        new TeamRanking { TeamId=5, EventId=5, CurrentWorldRanking=3, BestWorldRankingThisYear=3 },
        new TeamRanking { TeamId=5, EventId=6, CurrentWorldRanking=3, BestWorldRankingThisYear=3 },
        new TeamRanking { TeamId=6, EventId=5, CurrentWorldRanking=1, BestWorldRankingThisYear=1 },
        new TeamRanking { TeamId=6, EventId=6, CurrentWorldRanking=2, BestWorldRankingThisYear=2 },
        new TeamRanking { TeamId=7, EventId=5, CurrentWorldRanking=4, BestWorldRankingThisYear=4 },
        new TeamRanking { TeamId=7, EventId=6, CurrentWorldRanking=4, BestWorldRankingThisYear=4 },
      }.ForEach(x => context.TeamRankings.Add(x));
      context.SaveChanges();

      new List<StudentRanking>
      {
        new StudentRanking { MemberId=new Guid("d1c38da8-0bd4-4736-9461-0f58a459617f"), EventId=5, CurrentWorldRanking=6, BestWorldRankingThisYear=1 },
        new StudentRanking { MemberId=new Guid("d1c38da8-0bd4-4736-9461-0f58a459617f"), EventId=6, CurrentWorldRanking=2, BestWorldRankingThisYear=2 },
        new StudentRanking { MemberId=new Guid("2f3b8da1-db70-4865-8f10-3e5c48d32631"), EventId=5, CurrentWorldRanking=2, BestWorldRankingThisYear=2 },
        new StudentRanking { MemberId=new Guid("2f3b8da1-db70-4865-8f10-3e5c48d32631"), EventId=6, CurrentWorldRanking=5, BestWorldRankingThisYear=5 },
        new StudentRanking { MemberId=new Guid("1133d3a2-919b-4c7b-9603-2c30e6601f0a"), EventId=5, CurrentWorldRanking=4, BestWorldRankingThisYear=4 },
        new StudentRanking { MemberId=new Guid("1133d3a2-919b-4c7b-9603-2c30e6601f0a"), EventId=6, CurrentWorldRanking=7, BestWorldRankingThisYear=7 },
        new StudentRanking { MemberId=new Guid("f58b3465-470c-4053-b503-a010e509bdde"), EventId=5, CurrentWorldRanking=11, BestWorldRankingThisYear=11 },
        new StudentRanking { MemberId=new Guid("f58b3465-470c-4053-b503-a010e509bdde"), EventId=6, CurrentWorldRanking=6, BestWorldRankingThisYear=6 },
        new StudentRanking { MemberId=new Guid("449457a0-d138-4fdd-a24b-d479d0a55db6"), EventId=5, CurrentWorldRanking=12, BestWorldRankingThisYear=12 },
        new StudentRanking { MemberId=new Guid("449457a0-d138-4fdd-a24b-d479d0a55db6"), EventId=6, CurrentWorldRanking=10, BestWorldRankingThisYear=10 },
        new StudentRanking { MemberId=new Guid("4319b3c4-805a-4e10-932a-6b7da00696c3"), EventId=5, CurrentWorldRanking=5, BestWorldRankingThisYear=5 },
        new StudentRanking { MemberId=new Guid("4319b3c4-805a-4e10-932a-6b7da00696c3"), EventId=6, CurrentWorldRanking=8, BestWorldRankingThisYear=8 },
        new StudentRanking { MemberId=new Guid("c58c3584-0c29-43da-b555-a7047844a0d0"), EventId=5, CurrentWorldRanking=1, BestWorldRankingThisYear=1 },
        new StudentRanking { MemberId=new Guid("c58c3584-0c29-43da-b555-a7047844a0d0"), EventId=6, CurrentWorldRanking=3, BestWorldRankingThisYear=3 },
        new StudentRanking { MemberId=new Guid("d4c5a68d-1753-4f65-937f-9860f1e90431"), EventId=5, CurrentWorldRanking=3, BestWorldRankingThisYear=3 },
        new StudentRanking { MemberId=new Guid("d4c5a68d-1753-4f65-937f-9860f1e90431"), EventId=6, CurrentWorldRanking=4, BestWorldRankingThisYear=4 },
        new StudentRanking { MemberId=new Guid("8ec1957b-d753-4798-a7e0-2c96eda88cad"), EventId=5, CurrentWorldRanking=10, BestWorldRankingThisYear=10 },
        new StudentRanking { MemberId=new Guid("8ec1957b-d753-4798-a7e0-2c96eda88cad"), EventId=6, CurrentWorldRanking=1, BestWorldRankingThisYear=1 },
        new StudentRanking { MemberId=new Guid("810a42ee-c118-4d8f-80c6-dfab63050555"), EventId=5, CurrentWorldRanking=6, BestWorldRankingThisYear=6 },
        new StudentRanking { MemberId=new Guid("810a42ee-c118-4d8f-80c6-dfab63050555"), EventId=6, CurrentWorldRanking=12, BestWorldRankingThisYear=12 },
        new StudentRanking { MemberId=new Guid("2e3c5aab-c2db-498e-af92-eb7f6172e5c5"), EventId=5, CurrentWorldRanking=9, BestWorldRankingThisYear=9 },
        new StudentRanking { MemberId=new Guid("2e3c5aab-c2db-498e-af92-eb7f6172e5c5"), EventId=6, CurrentWorldRanking=9, BestWorldRankingThisYear=9 },
        new StudentRanking { MemberId=new Guid("6eeaa239-025b-4ad5-881a-24109cd50d1b"), EventId=5, CurrentWorldRanking=8, BestWorldRankingThisYear=8 },
        new StudentRanking { MemberId=new Guid("6eeaa239-025b-4ad5-881a-24109cd50d1b"), EventId=6, CurrentWorldRanking=11, BestWorldRankingThisYear=11 },
      }.ForEach(x => context.StudentRankings.Add(x));
      context.SaveChanges();

      new List<Advertisement>
      {
        new Advertisement { AdvertisementId=1, Company="Coca-Cola Light", Begin=new DateTime(2013,01,01), End=new DateTime(2013,12,31), Regions=new List<Region> { RegionService.GetWorld(context) }, Sports=SportService.GetSports(context,false),Url="http://www.google.com",ForGenderFemale=true, ForGenderMale=false },
        new Advertisement { AdvertisementId=2, Company="Lays Belgium", Begin=new DateTime(2013,01,01), End=new DateTime(2013,12,31), Regions=new List<Region> { RegionService.GetRegion(context,"Florida") },  Sports=SportService.GetSports(context,false),Url="http://www.google.com", ForGenderMale=true, ForGenderFemale=true },
        new Advertisement { AdvertisementId=3, Company="Mobistar Flanders", Begin=new DateTime(2013,01,01), End=new DateTime(2013,12,31), Regions=new List<Region> { RegionService.GetRegion(context,"Florida") }, Sports=SportService.GetSports(context,false),Url="http://www.google.com", ForGenderFemale=true, ForGenderMale=true },
        new Advertisement { AdvertisementId=4, Company="Nike", Begin=new DateTime(2013,01,01), End=new DateTime(2013,12,31), Regions=new List<Region> { RegionService.GetWorld(context) }, Sports=SportService.GetSports(context,false),Url="http://www.google.com",ForGenderMale=true,ForGenderFemale=true },
        new Advertisement { AdvertisementId=5, Company="Jupiler", Begin=new DateTime(2013,01,01), End=new DateTime(2013,12,31), Regions=new List<Region> { RegionService.GetWorld(context) }, Sports=SportService.GetSports(context,false),Url="http://www.google.com",ForGenderMale=true,ForGenderFemale=false },
        new Advertisement { AdvertisementId=6, Company="Google", Begin=new DateTime(2013,01,01), End=new DateTime(2013,12,31), Regions=new List<Region> { RegionService.GetWorld(context) }, Sports=SportService.GetSports(context,false),Url="http://www.google.com",ForGenderMale=true,ForGenderFemale=true },
        new Advertisement { AdvertisementId=7, Company="VL-F-SWI", Begin=new DateTime(2013,01,01), End=new DateTime(2013,12,31), Regions=new List<Region> { RegionService.GetRegion(context,"Florida") }, Sports=new List<Sport> { SportService.GetSport(context,abbr:"SWI") },Url="http://www.google.com", ForGenderFemale=true, ForGenderMale=false },
        new Advertisement { AdvertisementId=8, Company="VL-F-ATH", Begin=new DateTime(2013,01,01), End=new DateTime(2013,12,31), Regions=new List<Region> { RegionService.GetRegion(context,"Florida") }, Sports=new List<Sport> { SportService.GetSport(context,abbr:"ATH") },Url="http://www.google.com", ForGenderFemale=true, ForGenderMale=false },
      }.ForEach(x => context.Advertisements.Add(x));
      context.SaveChanges();
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "S13", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "jerry-lee.boonen@student.kdg.be", Gender = Gender.MALE, BirthDate = new DateTime(1997, 4, 19), PhoneNumber = "+32925473658", MemberId = new Guid("90f4c84c-d070-44eb-84a0-fff456300374"), Password = "$2a$12$VH69jrbfFVp36dWikMt.g.FZ/9UGQSWqjgQEoTrQ/bj1hKx3yBjJS", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("90f4c84c-d070-44eb-84a0-fff456300374") });
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Les françois vite").TeamId, MemberService.GetMember(context, email: "jerry-lee.boonen@student.kdg.be").MemberId);
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "S14", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "wim.tibackx@student.kdg.be", Gender = Gender.MALE, BirthDate = new DateTime(1997, 4, 19), PhoneNumber = "+32925473658", MemberId = new Guid("96a5f6bc-43d2-4926-a22f-61e2fb720dc6"), Password = "$2a$12$34BwBfKzZZoVCSQ30kXz3eS52v3T76FnXt9SJTIlbk.71p9N2GC/S", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("96a5f6bc-43d2-4926-a22f-61e2fb720dc6") });
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Les françois vite").TeamId, MemberService.GetMember(context, email: "wim.tibackx@student.kdg.be").MemberId);
      MemberService.AddStudent(context, new Student { Member = new Member { Name = "S15", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", Email = "tibackx.wim@gmail.com", Gender = Gender.MALE, BirthDate = new DateTime(1997, 4, 19), PhoneNumber = "+32925473658", MemberId = new Guid("292a8c83-6071-49da-bb2f-b2c74e9b7dff"), Password = "$2a$12$2GfHRYfagRHU5rulsPXdo.YWPj8Y22NBiAYLLPKnQOlezPJGCOFUe", Roles = new List<Role>() { studentRole } }, Photo = "photo", MemberId = new Guid("292a8c83-6071-49da-bb2f-b2c74e9b7dff") });
      TeamService.AddTeamMembership(context, TeamService.GetTeam(context, "Les françois vite").TeamId, MemberService.GetMember(context, email: "tibackx.wim@gmail.com").MemberId);


      this.InsertForConversation(context);

      DateTime endDataTime = DateTime.Now;
      Debug.WriteLine("Alle data Vergane tijd: " + (endDataTime - startDataTime).TotalMilliseconds);
    }

    /*private void InsertResults(EFDbContext db)
    {
      Event eLJ = EventService.GetEvent(db, "long-jumping");
      Event eCF = EventService.GetEvent(db, "100m fly");
      Event eCB = EventService.GetEvent(db, "100m baksteenslag");
      Event eCJ = EventService.GetEvent(db, "close-jumping");
      ResultService.AddResult(db, new Result { ResultScore = 1.00, Event = eLJ, TeamMembership = MemberService.GetStudent(db, 0).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.59, Event = eCF, TeamMembership = MemberService.GetStudent(db, 1).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 1.30, Event = eCB, TeamMembership = MemberService.GetStudent(db, 2).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.50, Event = eCJ, TeamMembership = MemberService.GetStudent(db, 3).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 3.00, Event = eLJ, TeamMembership = MemberService.GetStudent(db, 4).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.50, Event = eCF, TeamMembership = MemberService.GetStudent(db, 5).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 1.25, Event = eCB, TeamMembership = MemberService.GetStudent(db, 6).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 1.00, Event = eCJ, TeamMembership = MemberService.GetStudent(db, 7).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 2.50, Event = eLJ, TeamMembership = MemberService.GetStudent(db, 8).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.55, Event = eCF, TeamMembership = MemberService.GetStudent(db, 9).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 2.75, Event = eCB, TeamMembership = MemberService.GetStudent(db, 10).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.25, Event = eCJ, TeamMembership = MemberService.GetStudent(db, 11).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 2.43, Event = eLJ, TeamMembership = MemberService.GetStudent(db, 0).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.57, Event = eCF, TeamMembership = MemberService.GetStudent(db, 1).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 1.80, Event = eCB, TeamMembership = MemberService.GetStudent(db, 2).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.33, Event = eCJ, TeamMembership = MemberService.GetStudent(db, 3).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 2.47, Event = eLJ, TeamMembership = MemberService.GetStudent(db, 4).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.47, Event = eCF, TeamMembership = MemberService.GetStudent(db, 5).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 1.99, Event = eCB, TeamMembership = MemberService.GetStudent(db, 6).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.44, Event = eCJ, TeamMembership = MemberService.GetStudent(db, 7).TeamMemberShips.First() });
      ResultService.AddResult(db, new Result { ResultScore = 0.44, Event = eCJ, TeamMembership = MemberService.GetStudent(db, 8).TeamMemberShips.First() });
    }*/

    //See the TestdataRankings-excelsheet in the docs folder
    private void InsertDataForRanking(EFDbContext db)
    {
      Role studentRole = RoleService.GetRole(db, "Student");
      Student s101 = new Student { Member = new Member { Name = "John Smith", Email = "S101@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.MALE, BirthDate = new DateTime(1995, 4, 29), MemberId = new Guid("d1c38da8-0bd4-4736-9461-0f58a459617f"), Password = "$2a$12$8Ev/vE4imVZcJH0z65VOFeC6VMNdrdN25Xd9H.xiuSyL/En3k6P.6", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("d1c38da8-0bd4-4736-9461-0f58a459617f") };
      Student s102 = new Student { Member = new Member { Name = "Jack Russell", Email = "S102@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.MALE, BirthDate = new DateTime(1996, 4, 29), MemberId = new Guid("2f3b8da1-db70-4865-8f10-3e5c48d32631"), Password = "$2a$12$9ZCiTCehxYygxRSN0MojH.klqN8r6wwcCcbwMiFWs0H8mGR4u5Ve.", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("2f3b8da1-db70-4865-8f10-3e5c48d32631") };
      Student s103 = new Student { Member = new Member { Name = "Bill Gates", Email = "S103@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.MALE, BirthDate = new DateTime(1997, 4, 29), MemberId = new Guid("1133d3a2-919b-4c7b-9603-2c30e6601f0a"), Password = "$2a$12$rvMVWXJC6vVdKmkmdyF4B.rxuSGiDDhRfQAUs3.hK94gFUEsBd6IO", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("1133d3a2-919b-4c7b-9603-2c30e6601f0a") };
      Student s104 = new Student { Member = new Member { Name = "河辺千恵子", Email = "S104@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.FEMALE, BirthDate = new DateTime(1998, 4, 29), MemberId = new Guid("f58b3465-470c-4053-b503-a010e509bdde"), Password = "$2a$12$qchWpENBUEHXG5UkjQQOaursuIArPZQbQc0mzvzlIRNIKB/i3D0fG", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("f58b3465-470c-4053-b503-a010e509bdde") };
      Student s105 = new Student { Member = new Member { Name = "山田葵", Email = "S105@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.FEMALE, BirthDate = new DateTime(1995, 4, 29), MemberId = new Guid("449457a0-d138-4fdd-a24b-d479d0a55db6"), Password = "$2a$12$qE.hNkLZrKOY1VkjTEDfR.Lq2EBDsPaM5r9Tu3EHxbhzU/A.ZUnue", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("449457a0-d138-4fdd-a24b-d479d0a55db6") };
      Student s106 = new Student { Member = new Member { Name = "立華奏", Email = "S106@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.FEMALE, BirthDate = new DateTime(1996, 4, 29), MemberId = new Guid("4319b3c4-805a-4e10-932a-6b7da00696c3"), Password = "$2a$12$c1xbBZq3mghzNE2jkdMKPeNcFqnO4kNAUT8oM3PBdioLEZSkuN8QC", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("4319b3c4-805a-4e10-932a-6b7da00696c3") };
      Student s107 = new Student { Member = new Member { Name = "Jan de Smet", Email = "S107@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.MALE, BirthDate = new DateTime(1997, 4, 29), MemberId = new Guid("c58c3584-0c29-43da-b555-a7047844a0d0"), Password = "$2a$12$o7GnUWaZ0VyLdXf7DC54y.lBoRFsiqVxdaCHIPsAsInw0ih.h7sGe", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("c58c3584-0c29-43da-b555-a7047844a0d0") };
      Student s108 = new Student { Member = new Member { Name = "Jos van de Putte", Email = "S108@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.MALE, BirthDate = new DateTime(1998, 4, 29), MemberId = new Guid("d4c5a68d-1753-4f65-937f-9860f1e90431"), Password = "$2a$12$itFUEhWq9Y.bUnPx2mQ/YOWvsXj/93OyfGVaCGOlNzJyFFa3PNY/.", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("d4c5a68d-1753-4f65-937f-9860f1e90431") };
      Student s109 = new Student { Member = new Member { Name = "Dries van Hoof", Email = "S109@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.MALE, BirthDate = new DateTime(1995, 4, 29), MemberId = new Guid("8ec1957b-d753-4798-a7e0-2c96eda88cad"), Password = "$2a$12$wPlJRHVSqrRXZv2IQzELMOXFrNbjlqunZSYHYAX0.uYdoZ0SxQGTq", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("8ec1957b-d753-4798-a7e0-2c96eda88cad") };
      Student s110 = new Student { Member = new Member { Name = "Justine Léopoldine", Email = "S110@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.FEMALE, BirthDate = new DateTime(1996, 4, 29), MemberId = new Guid("810a42ee-c118-4d8f-80c6-dfab63050555"), Password ="$2a$12$REpe5X.32amkqMY8AmRiHO19DYVBgSuy1IU58tVziOcN.k.Rc.Nue", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("810a42ee-c118-4d8f-80c6-dfab63050555") };
      Student s111 = new Student { Member = new Member { Name = "Philomène Ninon", Email = "S111@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.FEMALE, BirthDate = new DateTime(1997, 4, 29), MemberId = new Guid("2e3c5aab-c2db-498e-af92-eb7f6172e5c5"), Password = "$2a$12$/w4uTPdA3LzSe4sztw1.o.4KFQOCmoUdGviWUBsndtXrEuU6h4CVm", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("2e3c5aab-c2db-498e-af92-eb7f6172e5c5") };
      Student s112 = new Student { Member = new Member { Name = "Isabelle Yolande", Email = "S112@student.com", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = Gender.FEMALE, BirthDate = new DateTime(1998, 4, 29), MemberId = new Guid("6eeaa239-025b-4ad5-881a-24109cd50d1b"), Password = "$2a$12$KohQa8.rx6bzS2NlpuP49OBCkykIj7o/QPCKkE2rSpFAW6.loPq5u", Roles = new List<Role>() { studentRole } }, MemberId = new Guid("6eeaa239-025b-4ad5-881a-24109cd50d1b") };
      new List<Student> { s101, s102, s103, s104, s105, s106, s107, s108, s109, s110, s111, s112 }.ForEach(x => MemberService.AddStudent(db, x));

      Team tAthM1 = new Team { Name = "The Giants", Sport = SportService.GetSport(db, abbr: "ATH"), Gender = Gender.MALE, School = SchoolService.GetSchool(db, abbr: "BH") };
      Team tAthF1 = new Team { Name = "竜の力", Sport = SportService.GetSport(db, abbr: "ATH"), Gender = Gender.FEMALE, School = SchoolService.GetSchool(db, abbr: "HSMHS") };
      Team tAthM2 = new Team { Name = "De lopers", Sport = SportService.GetSport(db, abbr: "ATH"), Gender = Gender.MALE, School = SchoolService.GetSchool(db, abbr: "SL") };
      Team tAthF2 = new Team { Name = "Les Demoiselles", Sport = SportService.GetSport(db, abbr: "ATH"), Gender = Gender.FEMALE, School = SchoolService.GetSchool(db, abbr: "ARMB") };
      new List<Team> { tAthM1, tAthF1, tAthM2, tAthF2 }.ForEach(x => TeamService.AddTeam(db, x));

      TeamService.AddTeamCoach(db, tAthM1.TeamId, MemberService.GetMember(db, email: "joske@coach.be").MemberId);
      TeamService.AddTeamCoach(db, tAthF1.TeamId, MemberService.GetMember(db, email: "catherine.horvat@coach.be").MemberId);
      TeamService.AddTeamCoach(db, tAthM2.TeamId, MemberService.GetMember(db, email: "jan@coach.be").MemberId);
      TeamService.AddTeamCoach(db, tAthF2.TeamId, MemberService.GetMember(db, email: "yves@schoolcup.org").MemberId);

      TeamService.AddTeamMembership(db, tAthM1.TeamId, s101.MemberId);
      TeamService.AddTeamMembership(db, tAthM1.TeamId, s102.MemberId);
      TeamService.AddTeamMembership(db, tAthM1.TeamId, s103.MemberId);
      TeamService.AddTeamMembership(db, tAthF1.TeamId, s104.MemberId);
      TeamService.AddTeamMembership(db, tAthF1.TeamId, s105.MemberId);
      TeamService.AddTeamMembership(db, tAthF1.TeamId, s106.MemberId);
      TeamService.AddTeamMembership(db, tAthM2.TeamId, s107.MemberId);
      TeamService.AddTeamMembership(db, tAthM2.TeamId, s108.MemberId);
      TeamService.AddTeamMembership(db, tAthM2.TeamId, s109.MemberId);
      TeamService.AddTeamMembership(db, tAthF2.TeamId, s110.MemberId);
      TeamService.AddTeamMembership(db, tAthF2.TeamId, s111.MemberId);
      TeamService.AddTeamMembership(db, tAthF2.TeamId, s112.MemberId);

      Event athEv1 = new Event { Name = "High Jumping", Directionality = Event.EDirectionality.ASC, Unit = Event.EUnit.DISTANCE, IsOfficial = true, Sport = SportService.GetSport(db, abbr: "ATH"), MaximumValue = 3, MinimumValue = 0, Gender_Value = 0 };
      Event athEv2 = new Event { Name = "100m Hordes", Directionality = Event.EDirectionality.DESC, Unit = Event.EUnit.TIME, IsOfficial = true, Sport = SportService.GetSport(db, abbr: "ATH"), MaximumValue = 3, MinimumValue = 0, Gender_Value = 2 };
      Event athEv3 = new Event { Name = "110m Hordes", Directionality = Event.EDirectionality.DESC, Unit = Event.EUnit.TIME, IsOfficial = true, Sport = SportService.GetSport(db, abbr: "ATH"), MaximumValue = 3, MinimumValue = 0, Gender_Value = 1 };

      EventService.AddEvent(db, athEv1);
      EventService.AddEvent(db, athEv2);
      EventService.AddEvent(db, athEv3);

      RegionalMeeting meet1 = new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 16), IsOfficial = true, Name = "世界選手権", Location = "網走市字呼人170番地の1網走市" }, NSSFRep = NSSFService.GetNSSFs(db, RegionService.GetRegion(db, "Hokkaido")).First().NSSFReps.First(), Region = RegionService.GetRegion(db, "Hokkaido") };
      RegionalMeeting meet2 = new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 19), IsOfficial = true, Name = "Vlaams Kampioenschap", Location = "Lombardenvest 25 Antwerpen" }, NSSFRep = NSSFService.GetNSSFs(db, RegionService.GetRegion(db, "Flanders")).First().NSSFReps.First(), Region = RegionService.GetRegion(db, "Flanders") };
      RegionalMeeting meet3 = new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 25), IsOfficial = true, Name = "Regional Championship Athletics Brooklyn", Location = "Atkins Avenue 600 Brooklyn" }, NSSFRep = NSSFService.GetNSSFs(db, RegionService.GetRegion(db, "New York")).First().NSSFReps.First(), Region = RegionService.GetRegion(db, "New York") };
      RegionalMeeting meet4 = new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 05), IsOfficial = true, Name = "Championnats de Wallonie", Location = "Rue Verte 55 Mons" }, NSSFRep = NSSFService.GetNSSFs(db, RegionService.GetRegion(db, "Wallonia")).First().NSSFReps.First(), Region = RegionService.GetRegion(db, "Wallonia") };


      meet1.Meeting.Events = EventService.GetEventsOfSport(db, 2);
      meet2.Meeting.Events = EventService.GetEventsOfSport(db, 2);
      meet3.Meeting.Events = EventService.GetEventsOfSport(db, 2);
      meet4.Meeting.Events = EventService.GetEventsOfSport(db, 2);
      MeetingService.AddRegionalMeeting(db, meet1);
      MeetingService.AddRegionalMeeting(db, meet2);
      MeetingService.AddRegionalMeeting(db, meet3);
      MeetingService.AddRegionalMeeting(db, meet4);
      db.SaveChanges();

      PointsImport piAthEv1 = new PointsImport
      {
        CompetitionYear = 2013,
        Event = athEv1,
        Lines = new List<PointsImport.ImportLine>
        {
					new PointsImport.ImportLine { PointValue=100, BoysMeasure=2.21, GirlsMeasure=1.85 },
					new PointsImport.ImportLine { PointValue=98, BoysMeasure=2.2, GirlsMeasure=1.84 },
					new PointsImport.ImportLine { PointValue=96, BoysMeasure=2.19, GirlsMeasure=1.83 },
					new PointsImport.ImportLine { PointValue=94, BoysMeasure=2.18, GirlsMeasure=1.82 },
					new PointsImport.ImportLine { PointValue=92, BoysMeasure=2.17, GirlsMeasure=1.81 },
					new PointsImport.ImportLine { PointValue=90, BoysMeasure=2.16, GirlsMeasure=1.8 },
					new PointsImport.ImportLine { PointValue=89, BoysMeasure=2.15, GirlsMeasure=1.79 },
					new PointsImport.ImportLine { PointValue=88, BoysMeasure=2.14, GirlsMeasure=1.78 },
					new PointsImport.ImportLine { PointValue=87, BoysMeasure=2.13, GirlsMeasure=1.77 },
					new PointsImport.ImportLine { PointValue=86, BoysMeasure=2.12, GirlsMeasure=1.76 },
					new PointsImport.ImportLine { PointValue=85, BoysMeasure=2.11, GirlsMeasure=1.75 },
					new PointsImport.ImportLine { PointValue=84, BoysMeasure=2.1, GirlsMeasure=1.74 },
					new PointsImport.ImportLine { PointValue=83, BoysMeasure=2.09, GirlsMeasure=1.73 },
					new PointsImport.ImportLine { PointValue=82, BoysMeasure=2.08, GirlsMeasure=1.72 },
					new PointsImport.ImportLine { PointValue=81, BoysMeasure=2.07, GirlsMeasure=1.71 },
					new PointsImport.ImportLine { PointValue=80, BoysMeasure=2.06, GirlsMeasure=1.7 },
					new PointsImport.ImportLine { PointValue=79, BoysMeasure=2.05, GirlsMeasure=1.69 },
					new PointsImport.ImportLine { PointValue=78, BoysMeasure=2.04, GirlsMeasure=1.68 },
					new PointsImport.ImportLine { PointValue=77, BoysMeasure=2.03, GirlsMeasure=1.67 },
					new PointsImport.ImportLine { PointValue=76, BoysMeasure=2.02, GirlsMeasure=1.66 },
					new PointsImport.ImportLine { PointValue=75, BoysMeasure=2.01, GirlsMeasure=1.65 },
					new PointsImport.ImportLine { PointValue=74, BoysMeasure=2, GirlsMeasure=1.64 },
					new PointsImport.ImportLine { PointValue=73, BoysMeasure=1.99, GirlsMeasure=1.63 },
					new PointsImport.ImportLine { PointValue=72, BoysMeasure=1.98, GirlsMeasure=1.62 },
					new PointsImport.ImportLine { PointValue=71, BoysMeasure=1.97, GirlsMeasure=1.61 },
					new PointsImport.ImportLine { PointValue=70, BoysMeasure=1.96, GirlsMeasure=1.6 },
					new PointsImport.ImportLine { PointValue=69, BoysMeasure=1.95, GirlsMeasure=1.59 },
					new PointsImport.ImportLine { PointValue=68, BoysMeasure=1.94, GirlsMeasure=1.58 },
					new PointsImport.ImportLine { PointValue=67, BoysMeasure=1.93, GirlsMeasure=1.57 },
					new PointsImport.ImportLine { PointValue=66, BoysMeasure=1.92, GirlsMeasure=1.56 },
					new PointsImport.ImportLine { PointValue=65, BoysMeasure=1.91, GirlsMeasure=1.55 },
					new PointsImport.ImportLine { PointValue=64, BoysMeasure=1.9, GirlsMeasure=1.54 },
					new PointsImport.ImportLine { PointValue=63, BoysMeasure=1.89, GirlsMeasure=1.53 },
					new PointsImport.ImportLine { PointValue=62, BoysMeasure=1.88, GirlsMeasure=1.52 },
					new PointsImport.ImportLine { PointValue=61, BoysMeasure=1.87, GirlsMeasure=1.51 },
					new PointsImport.ImportLine { PointValue=60, BoysMeasure=1.86, GirlsMeasure=1.5 },
					new PointsImport.ImportLine { PointValue=59, BoysMeasure=1.85, GirlsMeasure=1.49 },
					new PointsImport.ImportLine { PointValue=58, BoysMeasure=1.84, GirlsMeasure=1.48 },
					new PointsImport.ImportLine { PointValue=57, BoysMeasure=1.83, GirlsMeasure=1.47 },
					new PointsImport.ImportLine { PointValue=56, BoysMeasure=1.82, GirlsMeasure=1.46 },
					new PointsImport.ImportLine { PointValue=55, BoysMeasure=1.81, GirlsMeasure=1.45 },
					new PointsImport.ImportLine { PointValue=54, BoysMeasure=1.8, GirlsMeasure=1.44 },
					new PointsImport.ImportLine { PointValue=53, BoysMeasure=1.79, GirlsMeasure=1.43 },
					new PointsImport.ImportLine { PointValue=52, BoysMeasure=1.78, GirlsMeasure=1.42 },
					new PointsImport.ImportLine { PointValue=51, BoysMeasure=1.77, GirlsMeasure=1.41 },
					new PointsImport.ImportLine { PointValue=50, BoysMeasure=1.76, GirlsMeasure=1.4 },
					new PointsImport.ImportLine { PointValue=49, BoysMeasure=1.75, GirlsMeasure=1.39 },
					new PointsImport.ImportLine { PointValue=48, BoysMeasure=1.74, GirlsMeasure=1.38 },
					new PointsImport.ImportLine { PointValue=47, BoysMeasure=1.73, GirlsMeasure=1.37 },
					new PointsImport.ImportLine { PointValue=46, BoysMeasure=1.72, GirlsMeasure=1.36 },
					new PointsImport.ImportLine { PointValue=45, BoysMeasure=1.71, GirlsMeasure=1.35 },
					new PointsImport.ImportLine { PointValue=44, BoysMeasure=1.7, GirlsMeasure=1.34 },
					new PointsImport.ImportLine { PointValue=43, BoysMeasure=1.69, GirlsMeasure=1.33 },
					new PointsImport.ImportLine { PointValue=42, BoysMeasure=1.68, GirlsMeasure=1.32 },
					new PointsImport.ImportLine { PointValue=41, BoysMeasure=1.67, GirlsMeasure=1.31 },
					new PointsImport.ImportLine { PointValue=40, BoysMeasure=1.66, GirlsMeasure=1.3 },
					new PointsImport.ImportLine { PointValue=39, BoysMeasure=1.65, GirlsMeasure=1.29 },
					new PointsImport.ImportLine { PointValue=38, BoysMeasure=1.64, GirlsMeasure=1.28 },
					new PointsImport.ImportLine { PointValue=37, BoysMeasure=1.63, GirlsMeasure=1.27 },
					new PointsImport.ImportLine { PointValue=36, BoysMeasure=1.62, GirlsMeasure=1.26 },
					new PointsImport.ImportLine { PointValue=35, BoysMeasure=1.61, GirlsMeasure=1.25 },
					new PointsImport.ImportLine { PointValue=34, BoysMeasure=1.6, GirlsMeasure=1.24 },
					new PointsImport.ImportLine { PointValue=33, BoysMeasure=1.59, GirlsMeasure=1.23 },
					new PointsImport.ImportLine { PointValue=32, BoysMeasure=1.58, GirlsMeasure=1.22 },
					new PointsImport.ImportLine { PointValue=31, BoysMeasure=1.57, GirlsMeasure=1.2 },
					new PointsImport.ImportLine { PointValue=30, BoysMeasure=1.56, GirlsMeasure=1.18 },
					new PointsImport.ImportLine { PointValue=29, BoysMeasure=1.54, GirlsMeasure=1.16 },
					new PointsImport.ImportLine { PointValue=28, BoysMeasure=1.52, GirlsMeasure=1.14 },
					new PointsImport.ImportLine { PointValue=27, BoysMeasure=1.5, GirlsMeasure=1.12 },
					new PointsImport.ImportLine { PointValue=26, BoysMeasure=1.48, GirlsMeasure=1.1 },
					new PointsImport.ImportLine { PointValue=25, BoysMeasure=1.46, GirlsMeasure=1.08 },
					new PointsImport.ImportLine { PointValue=24, BoysMeasure=1.44, GirlsMeasure=1.06 },
					new PointsImport.ImportLine { PointValue=23, BoysMeasure=1.42, GirlsMeasure=1.04 },
					new PointsImport.ImportLine { PointValue=22, BoysMeasure=1.4, GirlsMeasure=1.02 },
					new PointsImport.ImportLine { PointValue=21, BoysMeasure=1.38, GirlsMeasure=1 },
					new PointsImport.ImportLine { PointValue=20, BoysMeasure=1.36, GirlsMeasure=0.98 },
					new PointsImport.ImportLine { PointValue=19, BoysMeasure=1.34, GirlsMeasure=0.96 },
					new PointsImport.ImportLine { PointValue=18, BoysMeasure=1.32, GirlsMeasure=0.94 },
					new PointsImport.ImportLine { PointValue=17, BoysMeasure=1.3, GirlsMeasure=0.92 },
					new PointsImport.ImportLine { PointValue=16, BoysMeasure=1.28, GirlsMeasure=0.9 },
					new PointsImport.ImportLine { PointValue=15, BoysMeasure=1.26, GirlsMeasure=0.88 },
					new PointsImport.ImportLine { PointValue=14, BoysMeasure=1.24, GirlsMeasure=0.86 },
					new PointsImport.ImportLine { PointValue=13, BoysMeasure=1.22, GirlsMeasure=0.84 },
					new PointsImport.ImportLine { PointValue=12, BoysMeasure=1.2, GirlsMeasure=0.82 },
					new PointsImport.ImportLine { PointValue=11, BoysMeasure=1.18, GirlsMeasure=0.8 },
					new PointsImport.ImportLine { PointValue=10, BoysMeasure=1.16, GirlsMeasure=0.78 },
					new PointsImport.ImportLine { PointValue=9, BoysMeasure=1.14, GirlsMeasure=0.76 },
					new PointsImport.ImportLine { PointValue=8, BoysMeasure=1.12, GirlsMeasure=0.74 },
					new PointsImport.ImportLine { PointValue=7, BoysMeasure=1.1, GirlsMeasure=0.72 },
					new PointsImport.ImportLine { PointValue=6, BoysMeasure=1.08, GirlsMeasure=0.7 },
					new PointsImport.ImportLine { PointValue=5, BoysMeasure=1.06, GirlsMeasure=0.68 },
					new PointsImport.ImportLine { PointValue=4, BoysMeasure=1.04, GirlsMeasure=0.66 },
					new PointsImport.ImportLine { PointValue=3, BoysMeasure=1.02, GirlsMeasure=0.64 },
					new PointsImport.ImportLine { PointValue=2, BoysMeasure=1, GirlsMeasure=0.62 },
					new PointsImport.ImportLine { PointValue=1, BoysMeasure=0.98, GirlsMeasure=0.6 },
        }
      };

      RankingService.ImportPoints(db, piAthEv1);

      PointsImport piAthEv2 = new PointsImport
      {
        CompetitionYear = 2013,
        Event = athEv2,
        Lines = new List<PointsImport.ImportLine>
        {
					new PointsImport.ImportLine { PointValue=100, BoysMeasure=0.98, GirlsMeasure=0.6 },
					new PointsImport.ImportLine { PointValue=98, BoysMeasure=1, GirlsMeasure=0.62 },
					new PointsImport.ImportLine { PointValue=96, BoysMeasure=1.02, GirlsMeasure=0.64 },
					new PointsImport.ImportLine { PointValue=94, BoysMeasure=1.04, GirlsMeasure=0.66 },
					new PointsImport.ImportLine { PointValue=92, BoysMeasure=1.06, GirlsMeasure=0.68 },
					new PointsImport.ImportLine { PointValue=90, BoysMeasure=1.08, GirlsMeasure=0.7 },
					new PointsImport.ImportLine { PointValue=89, BoysMeasure=1.1, GirlsMeasure=0.72 },
					new PointsImport.ImportLine { PointValue=88, BoysMeasure=1.12, GirlsMeasure=0.74 },
					new PointsImport.ImportLine { PointValue=87, BoysMeasure=1.14, GirlsMeasure=0.76 },
					new PointsImport.ImportLine { PointValue=86, BoysMeasure=1.16, GirlsMeasure=0.78 },
					new PointsImport.ImportLine { PointValue=85, BoysMeasure=1.18, GirlsMeasure=0.8 },
					new PointsImport.ImportLine { PointValue=84, BoysMeasure=1.2, GirlsMeasure=0.82 },
					new PointsImport.ImportLine { PointValue=83, BoysMeasure=1.22, GirlsMeasure=0.84 },
					new PointsImport.ImportLine { PointValue=82, BoysMeasure=1.24, GirlsMeasure=0.86 },
					new PointsImport.ImportLine { PointValue=81, BoysMeasure=1.26, GirlsMeasure=0.88 },
					new PointsImport.ImportLine { PointValue=80, BoysMeasure=1.28, GirlsMeasure=0.9 },
					new PointsImport.ImportLine { PointValue=79, BoysMeasure=1.3, GirlsMeasure=0.92 },
					new PointsImport.ImportLine { PointValue=78, BoysMeasure=1.32, GirlsMeasure=0.94 },
					new PointsImport.ImportLine { PointValue=77, BoysMeasure=1.34, GirlsMeasure=0.96 },
					new PointsImport.ImportLine { PointValue=76, BoysMeasure=1.36, GirlsMeasure=0.98 },
					new PointsImport.ImportLine { PointValue=75, BoysMeasure=1.38, GirlsMeasure=1 },
					new PointsImport.ImportLine { PointValue=74, BoysMeasure=1.4, GirlsMeasure=1.02 },
					new PointsImport.ImportLine { PointValue=73, BoysMeasure=1.42, GirlsMeasure=1.04 },
					new PointsImport.ImportLine { PointValue=72, BoysMeasure=1.44, GirlsMeasure=1.06 },
					new PointsImport.ImportLine { PointValue=71, BoysMeasure=1.46, GirlsMeasure=1.08 },
					new PointsImport.ImportLine { PointValue=70, BoysMeasure=1.48, GirlsMeasure=1.1 },
					new PointsImport.ImportLine { PointValue=69, BoysMeasure=1.5, GirlsMeasure=1.12 },
					new PointsImport.ImportLine { PointValue=68, BoysMeasure=1.52, GirlsMeasure=1.14 },
					new PointsImport.ImportLine { PointValue=67, BoysMeasure=1.54, GirlsMeasure=1.16 },
					new PointsImport.ImportLine { PointValue=66, BoysMeasure=1.56, GirlsMeasure=1.18 },
					new PointsImport.ImportLine { PointValue=65, BoysMeasure=1.57, GirlsMeasure=1.2 },
					new PointsImport.ImportLine { PointValue=64, BoysMeasure=1.58, GirlsMeasure=1.22 },
					new PointsImport.ImportLine { PointValue=63, BoysMeasure=1.59, GirlsMeasure=1.23 },
					new PointsImport.ImportLine { PointValue=62, BoysMeasure=1.6, GirlsMeasure=1.24 },
					new PointsImport.ImportLine { PointValue=61, BoysMeasure=1.61, GirlsMeasure=1.25 },
					new PointsImport.ImportLine { PointValue=60, BoysMeasure=1.62, GirlsMeasure=1.26 },
					new PointsImport.ImportLine { PointValue=59, BoysMeasure=1.63, GirlsMeasure=1.27 },
					new PointsImport.ImportLine { PointValue=58, BoysMeasure=1.64, GirlsMeasure=1.28 },
					new PointsImport.ImportLine { PointValue=57, BoysMeasure=1.65, GirlsMeasure=1.29 },
					new PointsImport.ImportLine { PointValue=56, BoysMeasure=1.66, GirlsMeasure=1.3 },
					new PointsImport.ImportLine { PointValue=55, BoysMeasure=1.67, GirlsMeasure=1.31 },
					new PointsImport.ImportLine { PointValue=54, BoysMeasure=1.68, GirlsMeasure=1.32 },
					new PointsImport.ImportLine { PointValue=53, BoysMeasure=1.69, GirlsMeasure=1.33 },
					new PointsImport.ImportLine { PointValue=52, BoysMeasure=1.7, GirlsMeasure=1.34 },
					new PointsImport.ImportLine { PointValue=51, BoysMeasure=1.71, GirlsMeasure=1.35 },
					new PointsImport.ImportLine { PointValue=50, BoysMeasure=1.72, GirlsMeasure=1.36 },
					new PointsImport.ImportLine { PointValue=49, BoysMeasure=1.73, GirlsMeasure=1.37 },
					new PointsImport.ImportLine { PointValue=48, BoysMeasure=1.74, GirlsMeasure=1.38 },
					new PointsImport.ImportLine { PointValue=47, BoysMeasure=1.75, GirlsMeasure=1.39 },
					new PointsImport.ImportLine { PointValue=46, BoysMeasure=1.76, GirlsMeasure=1.4 },
					new PointsImport.ImportLine { PointValue=45, BoysMeasure=1.77, GirlsMeasure=1.41 },
					new PointsImport.ImportLine { PointValue=44, BoysMeasure=1.78, GirlsMeasure=1.42 },
					new PointsImport.ImportLine { PointValue=43, BoysMeasure=1.79, GirlsMeasure=1.43 },
					new PointsImport.ImportLine { PointValue=42, BoysMeasure=1.8, GirlsMeasure=1.44 },
					new PointsImport.ImportLine { PointValue=41, BoysMeasure=1.81, GirlsMeasure=1.45 },
					new PointsImport.ImportLine { PointValue=40, BoysMeasure=1.82, GirlsMeasure=1.46 },
					new PointsImport.ImportLine { PointValue=39, BoysMeasure=1.83, GirlsMeasure=1.47 },
					new PointsImport.ImportLine { PointValue=38, BoysMeasure=1.84, GirlsMeasure=1.48 },
					new PointsImport.ImportLine { PointValue=37, BoysMeasure=1.85, GirlsMeasure=1.49 },
					new PointsImport.ImportLine { PointValue=36, BoysMeasure=1.86, GirlsMeasure=1.5 },
					new PointsImport.ImportLine { PointValue=35, BoysMeasure=1.87, GirlsMeasure=1.51 },
					new PointsImport.ImportLine { PointValue=34, BoysMeasure=1.88, GirlsMeasure=1.52 },
					new PointsImport.ImportLine { PointValue=33, BoysMeasure=1.89, GirlsMeasure=1.53 },
					new PointsImport.ImportLine { PointValue=32, BoysMeasure=1.9, GirlsMeasure=1.54 },
					new PointsImport.ImportLine { PointValue=31, BoysMeasure=1.91, GirlsMeasure=1.55 },
					new PointsImport.ImportLine { PointValue=30, BoysMeasure=1.92, GirlsMeasure=1.56 },
					new PointsImport.ImportLine { PointValue=29, BoysMeasure=1.93, GirlsMeasure=1.57 },
					new PointsImport.ImportLine { PointValue=28, BoysMeasure=1.94, GirlsMeasure=1.58 },
					new PointsImport.ImportLine { PointValue=27, BoysMeasure=1.95, GirlsMeasure=1.59 },
					new PointsImport.ImportLine { PointValue=26, BoysMeasure=1.96, GirlsMeasure=1.6 },
					new PointsImport.ImportLine { PointValue=25, BoysMeasure=1.97, GirlsMeasure=1.61 },
					new PointsImport.ImportLine { PointValue=24, BoysMeasure=1.98, GirlsMeasure=1.62 },
					new PointsImport.ImportLine { PointValue=23, BoysMeasure=1.99, GirlsMeasure=1.63 },
					new PointsImport.ImportLine { PointValue=22, BoysMeasure=2, GirlsMeasure=1.64 },
					new PointsImport.ImportLine { PointValue=21, BoysMeasure=2.01, GirlsMeasure=1.65 },
					new PointsImport.ImportLine { PointValue=20, BoysMeasure=2.02, GirlsMeasure=1.66 },
					new PointsImport.ImportLine { PointValue=19, BoysMeasure=2.03, GirlsMeasure=1.67 },
					new PointsImport.ImportLine { PointValue=18, BoysMeasure=2.04, GirlsMeasure=1.68 },
					new PointsImport.ImportLine { PointValue=17, BoysMeasure=2.05, GirlsMeasure=1.69 },
					new PointsImport.ImportLine { PointValue=16, BoysMeasure=2.06, GirlsMeasure=1.7 },
					new PointsImport.ImportLine { PointValue=15, BoysMeasure=2.07, GirlsMeasure=1.71 },
					new PointsImport.ImportLine { PointValue=14, BoysMeasure=2.08, GirlsMeasure=1.72 },
					new PointsImport.ImportLine { PointValue=13, BoysMeasure=2.09, GirlsMeasure=1.73 },
					new PointsImport.ImportLine { PointValue=12, BoysMeasure=2.1, GirlsMeasure=1.74 },
					new PointsImport.ImportLine { PointValue=11, BoysMeasure=2.11, GirlsMeasure=1.75 },
					new PointsImport.ImportLine { PointValue=10, BoysMeasure=2.12, GirlsMeasure=1.76 },
					new PointsImport.ImportLine { PointValue=9, BoysMeasure=2.13, GirlsMeasure=1.77 },
					new PointsImport.ImportLine { PointValue=8, BoysMeasure=2.14, GirlsMeasure=1.78 },
					new PointsImport.ImportLine { PointValue=7, BoysMeasure=2.15, GirlsMeasure=1.79 },
					new PointsImport.ImportLine { PointValue=6, BoysMeasure=2.16, GirlsMeasure=1.8 },
					new PointsImport.ImportLine { PointValue=5, BoysMeasure=2.17, GirlsMeasure=1.81 },
					new PointsImport.ImportLine { PointValue=4, BoysMeasure=2.18, GirlsMeasure=1.82 },
					new PointsImport.ImportLine { PointValue=3, BoysMeasure=2.19, GirlsMeasure=1.83 },
					new PointsImport.ImportLine { PointValue=2, BoysMeasure=2.2, GirlsMeasure=1.84 },
					new PointsImport.ImportLine { PointValue=1, BoysMeasure=2.21, GirlsMeasure=1.85 },
        }
      };

      RankingService.ImportPoints(db, piAthEv2);

      new List<Result>
      {
        new Result { MeetingId=meet1.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s101).First(), ResultScore=1.77 },
        new Result { MeetingId=meet1.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s102).First(), ResultScore=1.36 },
        new Result { MeetingId=meet1.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s103).First(), ResultScore=1.82 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s104).First(), ResultScore=0.84 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s105).First(), ResultScore=0.82 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s106).First(), ResultScore=1.81 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s107).First(), ResultScore=2.21 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s108).First(), ResultScore=1.57 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s109).First(), ResultScore=1.12 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s110).First(), ResultScore=1.77 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s111).First(), ResultScore=1.37 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s112).First(), ResultScore=0.96 },

        new Result { MeetingId=meet1.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s101).First(), ResultScore=1.92 },
        new Result { MeetingId=meet1.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s102).First(), ResultScore=2.05 },
        new Result { MeetingId=meet1.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s103).First(), ResultScore=1.20 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s104).First(), ResultScore=1.29 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s105).First(), ResultScore=1.64 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s106).First(), ResultScore=1.14 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s107).First(), ResultScore=1.70 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s108).First(), ResultScore=1.36 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s109).First(), ResultScore=2.00 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s110).First(), ResultScore=0.42 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s111).First(), ResultScore=1.07 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s112).First(), ResultScore=0.70 },
      }.ForEach(x => db.Results.Add(x));


      new List<Result>
      {
        new Result { MeetingId=meet1.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s101).First(), ResultScore=1.44 },
        new Result { MeetingId=meet1.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s102).First(), ResultScore=2.19 },
        new Result { MeetingId=meet1.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s103).First(), ResultScore=1.18 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s104).First(), ResultScore=1.12 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s105).First(), ResultScore=0.94 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s106).First(), ResultScore=1.50 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s107).First(), ResultScore=2.16 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s108).First(), ResultScore=2.01 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s109).First(), ResultScore=1.14 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s110).First(), ResultScore=1.30 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s111).First(), ResultScore=1.02 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv1, TeamMembership=TeamService.GetTeamMemberships(db,student:s112).First(), ResultScore=1.73 },

        new Result { MeetingId=meet1.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s101).First(), ResultScore=2.14 },
        new Result { MeetingId=meet1.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s102).First(), ResultScore=1.30 },
        new Result { MeetingId=meet1.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s103).First(), ResultScore=2.10 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s104).First(), ResultScore=1.52 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s105).First(), ResultScore=0.88 },
        new Result { MeetingId=meet2.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s106).First(), ResultScore=1.82 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s107).First(), ResultScore=1.99 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s108).First(), ResultScore=1.63 },
        new Result { MeetingId=meet3.MeetingId, Event=athEv3, TeamMembership=TeamService.GetTeamMemberships(db,student:s109).First(), ResultScore=2.03 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s110).First(), ResultScore=0.78 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s111).First(), ResultScore=1.41 },
        new Result { MeetingId=meet4.MeetingId, Event=athEv2, TeamMembership=TeamService.GetTeamMemberships(db,student:s112).First(), ResultScore=1.57 },
      }.ForEach(x => db.Results.Add(x));

      RecordService.AddRecordHolder(db, new RecordHolder { Name = "Usain Bolt", Gender_Value = 1 });
      RecordService.AddRecordHolder(db, new RecordHolder { Name = "Michael Phelps", Gender_Value = 1 });
      RecordService.AddRecordHolder(db, new RecordHolder { Name = "Jordanka Donkova", Gender_Value = 2 });

      RecordService.AddRecord(db, new Record { Result = 9.58, Event = EventService.GetEvent(db, "100m sprint"), Age = 25, Meeting = "World Athletics Champ", RecordDate = new DateTime(2012, 07, 15), Sport = SportService.GetSport(db, abbr: "ATH"), RecordHolder = RecordService.GetRecordHolder(db, "Usain Bolt") });
      RecordService.AddRecord(db, new Record { Result = 49.82, Event = EventService.GetEvent(db, "100m fly short"), Age = 25, Meeting = "World Swimmming Champ", RecordDate = new DateTime(2012, 04, 15), Sport = SportService.GetSport(db, abbr: "SWI"), RecordHolder = RecordService.GetRecordHolder(db, "Michael Phelps") });
      RecordService.AddRecord(db, new Record { Result = 12.21, Event = EventService.GetEvent(db, "100m hordes"), Age = 27, Meeting = "Olympic Games 1988", RecordDate = new DateTime(1988, 08, 20), Sport = SportService.GetSport(db, abbr: "ATH"), RecordHolder = RecordService.GetRecordHolder(db, "Jordanka Donkova") });

      addTestData(db);

    }
    private void InsertForConversation(EFDbContext context)
    {
      new List<Topic>
      {
        new Topic{Title = "Topic1",Spotlight= true,Team = TeamService.GetTeam(context,1),Coach = MemberService.GetCoach(context,new Guid("8fbb8e07-6261-47d6-9517-79b8b9d0b353"))},
        new Topic{Title = "Topic2",Spotlight= false,Team = TeamService.GetTeam(context,1),Student = MemberService.GetStudent(context,new Guid("63fa4ea2-b2f6-40f9-8762-009c72dcc09e"))},
        new Topic{Title = "Topic3",Spotlight= true,Team = TeamService.GetTeam(context,2),Coach = MemberService.GetCoach(context,new Guid("702e5244-5453-4c36-bdb8-56700710caf8"))},
        new Topic{Title = "Topic4",Spotlight= false,Team = TeamService.GetTeam(context,2),Student = MemberService.GetStudent(context,new Guid("b10f1908-0c62-4cd4-83fd-8639f672015b"))},
        new Topic{Title = "Topic5",Spotlight= true,Team = TeamService.GetTeam(context,3),Coach = MemberService.GetCoach(context,new Guid("f2e1221c-58a7-4460-868f-487555718763"))},
        new Topic{Title = "Topic6",Spotlight= false,Team = TeamService.GetTeam(context,3),Student = MemberService.GetStudent(context,new Guid("1038d147-f30c-4ec3-b836-dd4ae2520d77"))},
        new Topic{Title = "Topic7",Spotlight= true,Team = TeamService.GetTeam(context,4),Coach = MemberService.GetCoach(context,new Guid("8fbb8e07-6261-47d6-9517-79b8b9d0b353"))},
        new Topic{Title = "Topic8",Spotlight= false,Team = TeamService.GetTeam(context,4),Student = MemberService.GetStudent(context,new Guid("d1c38da8-0bd4-4736-9461-0f58a459617f"))},
        new Topic{Title = "Topic9",Spotlight= true,Team = TeamService.GetTeam(context,5),Coach = MemberService.GetCoach(context,new Guid("702e5244-5453-4c36-bdb8-56700710caf8"))},
        new Topic{Title = "Topic10",Spotlight= false,Team = TeamService.GetTeam(context,5),Student = MemberService.GetStudent(context,new Guid("f58b3465-470c-4053-b503-a010e509bdde"))},
        new Topic{Title = "Topic11",Spotlight= true,Team = TeamService.GetTeam(context,6),Coach = MemberService.GetCoach(context,new Guid("8fbb8e07-6261-47d6-9517-79b8b9d0b353"))},
        new Topic{Title = "Topic12",Spotlight= false,Team = TeamService.GetTeam(context,6),Student = MemberService.GetStudent(context,new Guid("c58c3584-0c29-43da-b555-a7047844a0d0"))},
        new Topic{Title = "Topic13",Spotlight= true,Team = TeamService.GetTeam(context,7),Coach = MemberService.GetCoach(context,new Guid("702e5244-5453-4c36-bdb8-56700710caf8"))},
        new Topic{Title = "Topic14",Spotlight= false,Team = TeamService.GetTeam(context,7),Student = MemberService.GetStudent(context,new Guid("810a42ee-c118-4d8f-80c6-dfab63050555"))}
      }.ForEach(t => ConversationService.AddTopic(context, topic: t));
      context.SaveChanges();
      for (int i = 0; i < context.Topics.ToList().Count; i++)
      {

        for (int j = 0; j < 5; j++)
        {
          switch (i)
          {
            case 1:
            case 2:
              ConversationService.AddMessage(context, new Message { Content = "This is message" + j, DatePosted = DateTime.Now }, coachId: new Guid("8fbb8e07-6261-47d6-9517-79b8b9d0b353"), topicId: i);
              break;
            case 3:
            case 4:
              ConversationService.AddMessage(context, new Message { Content = "This is message" + j, DatePosted = DateTime.Now }, coachId: new Guid("702e5244-5453-4c36-bdb8-56700710caf8"), topicId: i);
              break;
            case 5:
            case 6:
              ConversationService.AddMessage(context, new Message { Content = "This is message" + j, DatePosted = DateTime.Now }, coachId: new Guid("f2e1221c-58a7-4460-868f-487555718763"), topicId: i);
              break;
            case 7:
            case 8:
              ConversationService.AddMessage(context, new Message { Content = "This is message" + j, DatePosted = DateTime.Now }, studentId: new Guid("d1c38da8-0bd4-4736-9461-0f58a459617f"), topicId: i);
              break;
            case 9:
            case 10:
              ConversationService.AddMessage(context, new Message { Content = "This is message" + j, DatePosted = DateTime.Now }, studentId: new Guid("f58b3465-470c-4053-b503-a010e509bdde"), topicId: i);
              break;
            case 11:
            case 12:
              ConversationService.AddMessage(context, new Message { Content = "This is message" + j, DatePosted = DateTime.Now }, studentId: new Guid("c58c3584-0c29-43da-b555-a7047844a0d0"), topicId: i);
              break;
            case 13:
            case 14:
              ConversationService.AddMessage(context, new Message { Content = "This is message" + j, DatePosted = DateTime.Now }, studentId: new Guid("810a42ee-c118-4d8f-80c6-dfab63050555"), topicId: i);
              break;

          }

        }



      }
      context.SaveChanges();
    }
    private void addTestData(EFDbContext db)
    {
      DateTime dt1 = DateTime.Now;
      Sport Sport_ATH = SportService.GetSport(db, abbr: "ATH");
      Sport Sport_SWI = SportService.GetSport(db, abbr: "SWI");
      Role Role_Coach = RoleService.GetRole(db, "Coach");
      List<Role> coachRoles = new List<Role> { Role_Coach };
      Role role_Student = RoleService.GetRole(db, "Student");
      List<Role> studentRoles = new List<Role> { role_Student };

      Region region_Florida = RegionService.GetRegion(db, "Florida");
      Region region_Hokkaido = RegionService.GetRegion(db, "Hokkaido");
      Region region_Flanders = RegionService.GetRegion(db, "Flanders");
      Region region_NY = RegionService.GetRegion(db, "New York");
      Region region_Wallonia = RegionService.GetRegion(db, "Wallonia");

      Dictionary<String, Guid> schoolCoaches = new Dictionary<String, Guid>();
      schoolCoaches.Add("BH", MemberService.GetMember(db,email:"joske@coach.be").MemberId);
      schoolCoaches.Add("SL", MemberService.GetMember(db,email:"jan@coach.be").MemberId);
      schoolCoaches.Add("MH", MemberService.GetMember(db,email: "anneke@coach.be").MemberId);
      schoolCoaches.Add("HSMHS", MemberService.GetMember(db,email: "catherine.horvat@coach.be").MemberId);
      schoolCoaches.Add("ARMB", MemberService.GetMember(db,email: "yves@schoolcup.org").MemberId);

      SchoolService.AddSchool(db, new School { Name = "Queens High", Street = "Zottendijk 5", City = "Queens", Zipcode = "BE-5586", Email = "isfschool6@wimtibackx.be", AbbrName = "QH", NSSF = NSSFService.GetNSSF(db, "SNS") }, "http://example.com");
      SchoolService.AddSchool(db, new School { Name = "Sint-Barbaracollege", Street = "Zottendijk 6", City = "Gent", Zipcode = "BE-5586", Email = "isfschool7@wimtibackx.be", AbbrName = "SBC", NSSF = NSSFService.GetNSSF(db, "SVS") }, "http://example.com");
      SchoolService.AddSchool(db, new School { Name = "Orlando High", Street = "Zottendijk 7", City = "Orlando", Zipcode = "BE-5586", Email = "isfschool8@wimtibackx.be", AbbrName = "OH", NSSF = NSSFService.GetNSSF(db, "SBS") }, "http://example.com");
      SchoolService.AddSchool(db, new School { Name = "千歳中学校", Street = "Zottendijk 8", City = "千歳市", Zipcode = "BE-5586", Email = "isfschool9@wimtibackx.be", AbbrName = "CJHS", NSSF = NSSFService.GetNSSF(db, "チーケーチー") }, "http://example.com");
      SchoolService.AddSchool(db, new School { Name = "Athénée Royal François Bovesse", Street = "Zottendijk 8", City = "Namur", Zipcode = "BE-5586", Email = "isfschool10@wimtibackx.be", AbbrName = "ARFB", NSSF = NSSFService.GetNSSF(db, "LKL") }, "http://example.com");

      Team tSwiF1 = new Team { Name = "De snelle mariekes", Sport = Sport_SWI, Gender = Gender.FEMALE, School = SchoolService.GetSchool(db, abbr: "SL") };
      Team tSwiM1 = new Team { Name = "竜の体", Sport = Sport_SWI, Gender = Gender.MALE, School = SchoolService.GetSchool(db, abbr: "HSMHS") };
      Team tAthF1 = new Team { Name = "The Davids", Sport = Sport_ATH, Gender = Gender.FEMALE, School = SchoolService.GetSchool(db, abbr: "MH") };
      new List<Team> { tSwiM1, tSwiF1, tAthF1 }.ForEach(x => TeamService.AddTeam(db, x));

      TeamService.AddTeamCoach(db, tSwiF1.TeamId, MemberService.GetMember(db, email: "jan@coach.be").MemberId);
      TeamService.AddTeamCoach(db, tSwiM1.TeamId, MemberService.GetMember(db, email: "catherine.horvat@coach.be").MemberId);
      TeamService.AddTeamCoach(db, tAthF1.TeamId, MemberService.GetMember(db, email: "anneke@coach.be").MemberId);
      Guid guid = Guid.NewGuid();
      MemberService.AddCoach(db, new Coach { Member = new Member { Name = "James Adams", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "james@schoolcup.org", Gender = Gender.MALE, BirthDate = new DateTime(1974, 4, 29), PhoneNumber = "+32925473658", MemberId = guid, Password = "nopassword", Roles = coachRoles }, Photo = "photo", School = SchoolService.GetSchool(db, abbr: "QH"), MemberId = guid });
      schoolCoaches.Add("QH", guid);
      MemberService.AddCoach(db, new Coach { Member = new Member { Name = "Jan Janssens", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "jan@schoolcup.org", Gender = Gender.MALE, BirthDate = new DateTime(1972, 4, 29), PhoneNumber = "+32925473658", MemberId = (guid = Guid.NewGuid()), Password = "nopassword", Roles = coachRoles }, Photo = "photo", School = SchoolService.GetSchool(db, abbr: "SBC"), MemberId = guid });
      schoolCoaches.Add("SBC", guid);
      MemberService.AddCoach(db, new Coach { Member = new Member { Name = "Catherine Horvat", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "catherine@schoolcup.org", Gender = Gender.FEMALE, BirthDate = new DateTime(1973, 4, 29), PhoneNumber = "+32925473658", MemberId = (guid = Guid.NewGuid()), Password = "nopassword", Roles = coachRoles }, Photo = "photo", School = SchoolService.GetSchool(db, abbr: "OH"), MemberId = guid });
      schoolCoaches.Add("OH", guid);
      MemberService.AddCoach(db, new Coach { Member = new Member { Name = "浜崎あゆめ", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "ayume@schoolcup.org", Gender = Gender.FEMALE, BirthDate = new DateTime(1974, 5, 29), PhoneNumber = "+32925473658", MemberId = (guid = Guid.NewGuid()), Password = "nopassword", Roles = coachRoles }, Photo = "photo", School = SchoolService.GetSchool(db, abbr: "CJHS"), MemberId = guid });
      schoolCoaches.Add("CJHS", guid);
      MemberService.AddCoach(db, new Coach { Member = new Member { Name = "Elio Rideau", Street = "Zottendijk 5", City = "Antwerpen", Country = "Belgium", Zipcode = "BE-5586", Email = "elio@schoolcup.org", Gender = Gender.FEMALE, BirthDate = new DateTime(1974, 5, 29), PhoneNumber = "+32925473658", MemberId = (guid = Guid.NewGuid()), Password = "nopassword", Roles = coachRoles }, Photo = "photo", School = SchoolService.GetSchool(db, abbr: "ARFB"), MemberId = guid });
      schoolCoaches.Add("ARFB", guid);

      MeetingService.AddRegionalMeeting(db, new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 16), IsOfficial = true, Name = "Regional Championship Athletics Florida", Location = "Bird Road 6500 Florida", Events=Sport_ATH.Events }, NSSFRep = NSSFService.GetNSSFs(db, region_Florida).First().NSSFReps.First(), Region = region_Florida });
      MeetingService.AddRegionalMeeting(db, new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 17), IsOfficial = true, Name = "世界選手権", Location = "網走市字呼人170番地の1網走市", Events=Sport_SWI.Events }, NSSFRep = NSSFService.GetNSSFs(db, region_Hokkaido).First().NSSFReps.First(), Region = region_Hokkaido });
      MeetingService.AddRegionalMeeting(db, new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 18), IsOfficial = true, Name = "Regional Championship Swimming Florida", Location = "Bird Road 6500 Florida", Events = Sport_SWI.Events }, NSSFRep = NSSFService.GetNSSFs(db, region_Florida).First().NSSFReps.First(), Region =region_Florida });
      MeetingService.AddRegionalMeeting(db, new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 19), IsOfficial = true, Name = "Vlaams Kampioenschap Swimming", Location = "Lombardenvest 25 Antwerpen", Events = Sport_SWI.Events }, NSSFRep = NSSFService.GetNSSFs(db, region_Flanders).First().NSSFReps.First(), Region = region_Flanders });
      MeetingService.AddRegionalMeeting(db, new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 25), IsOfficial = true, Name = "Regional Championship Swimming Brooklyn", Location = "Atkins Avenue 600 Brooklyn", Events = Sport_SWI.Events }, NSSFRep = NSSFService.GetNSSFs(db, region_NY).First().NSSFReps.First(), Region = region_NY });
      MeetingService.AddRegionalMeeting(db, new RegionalMeeting { Meeting = new Meeting { Date = new DateTime(2013, 04, 05), IsOfficial = true, Name = "Championnats de Wallonie Natation", Location = "Rue Verte 55 Mons", Events = Sport_SWI.Events }, NSSFRep = NSSFService.GetNSSFs(db, region_Wallonia).First().NSSFReps.First(), Region = region_Wallonia });

      foreach (School s in db.Schools.Include("Teams.TeamMemberships").ToList())
      {
        int maleAth = 0;
        int femaleAth = 0;
        int maleSwi = 0;
        int femaleSwi = 0;
        for (int i = 0; i < 4; i++)
        {

          if (i < s.Teams.Count && s.Teams[i] != null)
          {
            if (s.Teams[i].Gender_Value == 1 && s.Teams[i].SportId == 1) { maleSwi++; }
            if (s.Teams[i].Gender_Value == 2 && s.Teams[i].SportId == 1) { femaleSwi++; }
            if (s.Teams[i].Gender_Value == 1 && s.Teams[i].SportId == 2) { maleAth++; }
            if (s.Teams[i].Gender_Value == 2 && s.Teams[i].SportId == 2) { femaleAth++; }
          }
          else
          {
            Team t = null;
            if (maleAth == 0)
            {
              t = new Team { Name = "t" + s.SchoolId + s.NSSFId + (i + 1), Sport = Sport_ATH, Gender = Gender.MALE, School = s };
              maleAth++;
            }
            else if (femaleAth == 0)
            {
              t = new Team { Name = "t" + s.SchoolId + s.NSSFId + (i + 1), Sport = Sport_ATH, Gender = Gender.FEMALE, School = s };
              femaleAth++;
            }
            else if (maleSwi == 0)
            {
              t = new Team { Name = "t" + s.SchoolId + s.NSSFId + (i + 1), Sport = Sport_SWI, Gender = Gender.MALE, School = s };
              maleSwi++;
            }
            else if (femaleSwi == 0) 
            {
              t = new Team { Name = "t" + s.SchoolId + s.NSSFId + (i + 1), Sport = Sport_SWI, Gender = Gender.FEMALE, School = s };
              femaleSwi++;
            }
            TeamService.AddTeam(db, t);
            TeamService.AddTeamCoach(db, t.TeamId,schoolCoaches[s.AbbrName]);
          }
        }

        foreach (Team t in s.Teams.ToList())
        {
          int count = (t.TeamMemberships == null ? 0 : t.TeamMemberships.Count);

          for (int i = count; i < 5; i++)
          {
            guid = Guid.NewGuid();
            Student stud = new Student { Member = new Member { Name = "s" + (i + 1) + t.TeamId + t.SportId + t.SchoolId, Email = "s" + (i + 1) + t.TeamId + t.SportId + t.SchoolId + "@schoolcup.org", Street = "Zottendijk 5", City = "Antwerpen", Zipcode = "BE-5586", Country = "Belgium", PhoneNumber = "+32925473658", Gender = t.Gender, BirthDate = new DateTime(1995, 4, 29), MemberId = guid, Password = "nopassword", Roles=studentRoles }, MemberId = guid };

            MemberService.AddStudent(db,stud);
            TeamService.AddTeamMembership(db, t.TeamId, stud.MemberId);

          }
        }
      }
      DateTime resultsBegin = DateTime.Now;
      //Back in the function itself :) -- Let's see if we can make this faster
      List<RegionalMeeting> regionalMeetings =MeetingService.GetRegionalMeetings(db);
      List<TeamMembership> allTeamMemberships = TeamService.GetTeamMemberships(db);
      Random rand = new Random();
      foreach (RegionalMeeting meet in regionalMeetings)
      {
        //Get the teammemberships in this region
        //List<TeamMembership> teamMemberships = TeamService.GetTeamMembershipsOfRegion(db, meet.Region);
        List<TeamMembership> teamMemberships = allTeamMemberships.Where(x => x.Team.School.NSSF.Region.RegionId == meet.RegionId).ToList();
        List<Result> results = new List<Result>();
        foreach (TeamMembership tm in teamMemberships)
        {
          foreach (Event e in meet.Meeting.Events.Where(x => (tm.Team.SportId == x.SportId && (x.Gender_Value == 0 || tm.Team.Gender_Value == x.Gender_Value))).ToList())
          {
            double result = 0;
            switch (tm.Team.Gender_Value)
            {
              case 1: result = Math.Round(rand.NextDouble() * (2.3 - 0.9) + 0.9, 2); break;
              case 2: result = Math.Round(rand.NextDouble() * (1.9 - 0.6) + 0.6, 2); break;
            }
            results.Add(new Result { MeetingId = meet.MeetingId, EventId = e.EventId, TeamId = tm.TeamId, MemberId = tm.MemberId, ResultScore = result });
          }
        }
        ResultService.AddResults(db, results, meet.Meeting);
      }

      DateTime dt2 = DateTime.Now;
      Debug.WriteLine("AddTestData-Results Vergane tijd: " + (dt2 - resultsBegin).TotalMilliseconds);
      Debug.WriteLine("AddTestData Vergane tijd: " + (dt2-dt1).TotalMilliseconds);
    }
  }
}