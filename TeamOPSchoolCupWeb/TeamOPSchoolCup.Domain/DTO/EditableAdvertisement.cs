﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Services;
using System.ComponentModel.DataAnnotations;
using System.Web;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Domain.DTO
{
  //We have to copy everything not directly EF-related from Advertisement because:
  //  -Regionmultiselect needs a string of ids
  //  -This class can't simply extend advertisement, because EF will go and ad columns to the advertisement table
  public class EditableAdvertisement
  {
    public int AdvertisementId { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorCompReq", ErrorMessageResourceType = typeof(AdvertisementStrings))]
    [LocalizedDisplayName("ModelCompany", NameResourceType = typeof(AdvertisementStrings))]
    public String Company { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorBeDaReq", ErrorMessageResourceType = typeof(AdvertisementStrings))]
    [LocalizedDisplayName("ModelBegin", NameResourceType = typeof(AdvertisementStrings))]
    public DateTime Begin { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorEnDaReq", ErrorMessageResourceType = typeof(AdvertisementStrings))]
    [LocalizedDisplayName("ModelEnd", NameResourceType = typeof(AdvertisementStrings))]
    public DateTime End { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorUrlXReq", ErrorMessageResourceType = typeof(AdvertisementStrings))]
    [LocalizedDisplayName("ModelUrl", NameResourceType = typeof(AdvertisementStrings))]
    public String Url { get; set; }
    public bool ForGenderMale { get; set; }
    public bool ForGenderFemale { get; set; }
    public string RegionsStr { get; set; }
    public string SportsStr { get; set; }
    public HttpPostedFileBase File { get; set; }

    public EditableAdvertisement(Advertisement ad)
    {
      this.AdvertisementId = ad.AdvertisementId;
      this.Company = ad.Company;
      this.Begin = ad.Begin;
      this.End = ad.End;
      this.Url = ad.Url;
      this.ForGenderMale = ad.ForGenderMale;
      this.ForGenderFemale = ad.ForGenderFemale;
      this.RegionsStr = (ad.Regions == null || ad.Regions.Count < 1 ? "" : String.Join(",", ad.Regions.Select(x => x.RegionId).ToList()));
      this.SportsStr = (ad.Sports == null || ad.Sports.Count < 1 ? "" : String.Join(",", ad.Sports.Select(x => x.SportId).ToList()));
    }
    public EditableAdvertisement() { }
  }
}
