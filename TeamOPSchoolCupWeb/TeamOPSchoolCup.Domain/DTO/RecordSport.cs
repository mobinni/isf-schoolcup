﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class RecordSport
  {
    public SportGender SportGender { get; set; }
    public List<EventInfo> EventInfos { get; set; }
    public class EventInfo
    {
      public int EventId { get; set; }
      public string EventName { get; set; }
    }
  
  }
  public class SportGender
  {
    public int SportId { get; set; }
    public short Gender_Value { get; set; }
  }
 
}
