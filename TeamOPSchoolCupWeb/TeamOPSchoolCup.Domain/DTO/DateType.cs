﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO
{
  public enum DateType : short
  {
    NORESTRICTIONS=0,
    FUTURE,
    STUDENTBIRTHDAY,
    PAST
    
  }
}
