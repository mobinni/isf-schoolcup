﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class StudentMemberships
  {
    public Guid StudentId { get; set; }
    public List<TeamInfo> TeamInfos { get; set; }
    public class TeamInfo
    {
      public int TeamId { get; set; }
      public string Name { get; set; }
    }
  }
}
