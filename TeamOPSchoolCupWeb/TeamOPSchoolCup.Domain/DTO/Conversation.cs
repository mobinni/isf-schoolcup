﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class Conversation
  {
    //Combined class of Topic and Message
    
    public string Title { get; set; }
    public bool Spotlight { get; set; }
    public string Content { get; set; }
    public DateTime DatePosted { get; set; }
    public int TeamId { get; set; }
    public int TopicId { get; set; }
  }
}
