﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class BasicMemberInfo
  {
    [Required]
    public string Name { get; set; }
    [Required]
    public string Email { get; set; }
    [Required]
    public string Password { get; set; }
    [Required]
    public short Gender_Value { get; set; }
    public DateTime? BirthDay { get; set; }

    public Gender Gender
    {
      get { return (Gender)Gender_Value; }
      set { Gender_Value = (short)value; }
    }
  }
}
