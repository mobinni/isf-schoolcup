﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class RegionForJson
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public int? ParentId { get; set; }
    public List<RegionForJson> Children { get; set; }

    public RegionForJson(Region region)
    {
      this.Id = region.RegionId;
      this.Name = region.Name;
      this.ParentId = region.ParentId;
      this.Children = new List<RegionForJson>();
      foreach (Region child in region.Children)
      {
        this.Children.Add(new RegionForJson(child));
      }
    }
  }
}
