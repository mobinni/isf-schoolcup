﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO
{
  public enum ParticipantAge : int { AGE14 = 14, AGE15 = 15, AGE16 = 16, AGE17 = 17 }
}
