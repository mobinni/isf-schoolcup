﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class CreatableRecord
  {
    [Required]
    public double Result { get; set; }
    [Required]
    public DateTime RecordDate { get; set; }
    [Required]
    [StringLength(100)]
    public string Meeting { get; set; }
    [Required]
    [StringLength(50)]
    public string Name { get; set; }
    [Required]
    public int Age { get; set; }
    [Required]
    public short Gender_Value { get; set; }
    public Gender Gender
    {
      get { return (Gender)Gender_Value; }
      set { Gender_Value = (short)value; }
    }
    public int EventId { get; set; }
    public int SportId { get; set; }
   
  }
}
