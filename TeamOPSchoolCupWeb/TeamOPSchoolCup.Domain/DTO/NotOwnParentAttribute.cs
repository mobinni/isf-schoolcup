﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.DTO
{
  class NotOwnParentAttribute : ValidationAttribute
  {
    public NotOwnParentAttribute() { }

    public override bool IsValid(object value)
    {
      if (value == null) return true;
      if (value.GetType() != typeof(Region)) return true;
      Region r = (Region)value;
      if (r.ParentId == null) return true;
      return !(r.RegionId == r.ParentId.Value);
    }
  }
}
