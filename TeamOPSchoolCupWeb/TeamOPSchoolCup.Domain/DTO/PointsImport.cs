﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class PointsImport
  {
    public struct ImportLine
    {
      public int PointValue;
      public double BoysMeasure;
      public double GirlsMeasure;
    }

    public List<ImportLine> Lines;
    public int CompetitionYear;
    public Event Event;

    public PointsImport()
    {
      this.Lines = new List<ImportLine>();
    }
  }
}