﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class EditableMember
  {
    public Guid MemberId { get; set; }

    [StringLength(30)]
    public string Name { get; set; }

    [StringLength(50)]
    public string Street { get; set; }

    [MinLength(4)]
    [MaxLength(10)]
    public string Zipcode { get; set; }

    [StringLength(50)]
    public string City { get; set; }

    [StringLength(50)]
    public string Country { get; set; }

    public string PhoneNumber { get; set; }

    public int Language { get; set; }
  }
}
