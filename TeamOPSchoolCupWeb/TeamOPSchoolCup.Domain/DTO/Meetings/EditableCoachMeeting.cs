﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO.Meetings
{
  public class EditableCoachMeeting
  {
    public int? CoachMeetingId { get; set; }
    public String Name { get; set; }
    public String Location { get; set; }
    public DateTime Date { get; set; }
    public String EventsStr { get; set; }
    public int TeamId { get; set; }
  }
}
