﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO.Meetings
{
  public class ResultsBulkCreate
  {
    public int MeetingId { get; set; }
    public List<ResultNode> ResultNodes { get; set; }
    public Foo FooProp { get; set; }

    public class ResultNode {
      public double ResultScore { get; set; }
      public int Event { get; set; }
      public Guid Student { get; set; }
      public int Team { get; set; }
    }

    public class Foo
    {
      public string Bar { get; set; }
    }
  }
}
