﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class StudentComparison
  {
    public List<RelevantStudentInfo> Students { get; set; }
    public List<ComparisonLine> ComparisonLines { get; set; }
    public StudentComparison() { ComparisonLines = new List<ComparisonLine>();
                                 Students = new List<RelevantStudentInfo>();
    }

    public class ComparisonLine
    {
      public int EventId { get; set; }
      public string EventName { get; set; }
      public List<ResultRankInfo> ResultRank { get; set; }
      public List<ResultPerDate> ResultPerDate { get; set; }

      public ComparisonLine()
      {
        ResultRank = new List<ResultRankInfo>();
        ResultPerDate = new List<ResultPerDate>();
      }
    }
    
    public class ResultRankInfo {
      public ResultInfo Result { get; set; }
      public int? Rank { get; set; }
      public ResultRankInfo()
      {
        Result = new ResultInfo();
      }
      public class ResultInfo
      {
        public double ResultScore { get; set; }
        public DateTime DateAcquired { get; set; }
        //Maybe more data to come if we want custom annotations?
      }
    } 
    public class RelevantStudentInfo
    {
      public Guid StudentId { get; set; }
      public string Name { get; set; }
    }
  }
}
