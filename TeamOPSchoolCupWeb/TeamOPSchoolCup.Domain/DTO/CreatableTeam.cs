﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Services;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.Resources;
using System.Web;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class CreatableTeam
  {
    public int TeamId { get; set; }
    [Required]
    [StringLength(30)]
    public string Name { get; set; }
    public short Gender_Value { get; set; }
    public bool IsParticipating { get; set; }
    public Guid MemberId { get; set; }
    public int SportId { get; set; }
    public int SchoolId { get; set; }
    public HttpPostedFileBase Photo { get; set; }
    public HttpPostedFileBase Logo { get; set; }

    public CreatableTeam() { }
  }
}
