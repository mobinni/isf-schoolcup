﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO
{
  public struct AdCtr
  {
    public decimal Ctr { get; set; }
    public decimal Clicks { get; set; }
    public decimal Views { get; set; }
  }
}
