﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class MeetingDetails
  {
    public int MeetingId { get; set; }
    public String Name { get; set; }
    public String Location { get; set; }
    public DateTime Date { get; set; }
    public List<ResultsGroup> ResultsGroups { get; set; }

    public MeetingDetails() { ResultsGroups = new List<ResultsGroup>(); }

    public class ResultsGroup
    {
      public int EventId { get; set; }
      public String EventName { get; set; }
      public Gender Gender { get; set; } //Should only use Male or Female here, not unisex (because we need separate lists)
      public List<ResultInfo> Results { get; set; }

      public ResultsGroup() { Results = new List<ResultInfo>(); }
    }

    public class ResultInfo
    {
      public Guid MemberId { get; set; }
      public String Name { get; set; }
      public int Age { get; set; }
      public String School { get; set; }
      public double Score { get; set; }
    }
  }
}
