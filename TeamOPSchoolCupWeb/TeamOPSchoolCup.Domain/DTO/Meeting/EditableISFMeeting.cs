﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO.Meeting
{
  public class EditableISFMeeting
  {
    public int? ISFMeetingId { get; set; }
    public String Name { get; set; }
    public String Location { get; set; }
    public DateTime Date { get; set; }
    public List<int> Events { get; set; }
    public List<int> Regions { get; set; }
    public String EventsStr { get; set; }
    public String RegionsStr { get; set; }
  }
}
