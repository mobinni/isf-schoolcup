﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO.Meeting
{
  public class EditableRegionalMeeting
  {
    public int? RegionalMeetingId { get; set; }
    public String Name { get; set; }
    public String Location { get; set; }
    public DateTime Date { get; set; }
    public String EventsStr { get; set; }
  }
}
