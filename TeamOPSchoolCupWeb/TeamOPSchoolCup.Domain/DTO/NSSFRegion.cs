﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class NSSFRegion
  {
    [Required]
    [StringLength(30)]
    public string Name { get; set; }
    [Required]
    [StringLength(50)]
    public string Street { get; set; }
    [Required]
    [MinLength(4)]
    [MaxLength(10)]
    public string Zipcode { get; set; }
    [Required]
    [StringLength(50)]
    public string City { get; set; }
    //[Required]
    [StringLength(50)]
    public string Country { get; set; }
    [Required]
    public string PhoneNumber { get; set; }
    [Required]
    public string FaxNumber { get; set; }
    [Required]
    [StringLength(50)]
    public string Email { get; set; }
    [Required]
    public int RegionId { get; set; }
  }
}
