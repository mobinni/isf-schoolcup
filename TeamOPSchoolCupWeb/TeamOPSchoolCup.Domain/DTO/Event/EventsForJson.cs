﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class EventForJson
  {
    public int Id { get; set; }
    public String Name { get; set; }
    public int SportId { get; set; }
  }
}
