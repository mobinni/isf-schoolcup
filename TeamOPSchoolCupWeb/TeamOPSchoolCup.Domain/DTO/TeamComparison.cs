﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Ranking;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class TeamComparison
  {
    public RelevantTeamInfo Team1 { get; set; }
    public RelevantTeamInfo Team2 { get; set; }
    public List<ComparisonLine> ComparisonLines { get; set; }

    public TeamComparison() { ComparisonLines = new List<ComparisonLine>(); }
    
    public class ComparisonLine
    {
      public int EventId { get; set; }
      public string EventName { get; set; }
      public List<ResultInfo> ResultsT1 { get; set; }
      public List<ResultInfo> ResultsT2 { get; set; }
      public int? RankT1 { get; set; }
      public int? RankT2 { get; set; }

      public ComparisonLine()
      {
        ResultsT1 = new List<ResultInfo>();
        ResultsT2 = new List<ResultInfo>();
      }
    }

    public class ResultInfo
    {
      public double ResultScore { get; set; }
      public DateTime DateAcquired { get; set; }
      //Maybe more data to come if we want custom annotations?
    }

    public class RelevantTeamInfo
    {
      public int TeamId { get; set; }
      public string Name { get; set; }
      public double Points { get; set; }
      public int? Rank { get; set; }
    }

    public class PointInfo
    {
      public double Points { get; set; }
      public DateTime DateAcquired { get; set; }
    }

    public class PointEvent
    {
      public DateTime Date { get; set; }
      public double? PointsT1 { get; set; }
      public double? PointsT2 { get; set; }
    }

    public class TeamResultEvent
    {
      public DateTime Date { get; set; }
      public double? ResultT1 { get; set; }
      public double? ResultT2 { get; set; }
      public String EventName { get; set; }
    }

    public struct StudentResultEvent
    {
      public DateTime Date { get; set; }
      public String EventName { get; set; }
      public List<StudentResult> StudentsTeam1 { get; set; }
      public List<StudentResult> StudentsTeam2 { get; set; }
    }

    public struct StudentResult
    {
      public String StudentName { get; set; }
      public double? Result { get; set; }
    }

    public class StudentsParticipatingEvents
    {
      public Guid Id { get; set; }
      public String Name { get; set; }
      public Dictionary<string, bool> ParticipatingEvents { get; set; }

      public StudentsParticipatingEvents() { ParticipatingEvents = new Dictionary<string, bool>(); }
    }

    public class StudentsInTeamsWithEvents
    {
      public int TeamId { get; set; }
      public String Name { get; set; }
      public List<StudentsParticipatingEvents> Students { get; set; }

      public StudentsInTeamsWithEvents() { Students = new List<StudentsParticipatingEvents>(); }
    }
  }
}
