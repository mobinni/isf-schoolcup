﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.DTO
{
  public class AdViewing
  {
    public Advertisement Advertisement { get; set; }
    public int RegionId { get; set; }
    public Gender? Gender { get; set; }
    public int SportId { get; set; }
  }
}
