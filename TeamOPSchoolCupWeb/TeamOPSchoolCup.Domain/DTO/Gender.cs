﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO
{
  public enum Gender : short
  {
    UNISEX=0,
    MALE=1,
    FEMALE=2
  }
}
