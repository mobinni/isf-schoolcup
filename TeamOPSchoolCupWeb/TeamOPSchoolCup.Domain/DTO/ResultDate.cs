﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.DTO
{
  
  public class ResultPerDate
  {
    public DateTime Date { get; set; }
    public List<double> Results { get; set; }
    
  }
}
