﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Domain.POCO.Meetings
{
  public class RegionalMeeting
  {
    //Attributes of the Table
    public int MeetingId { get; set; }
    
    //Foreign Keys
    public Guid MemberId { get; set; }
    public int RegionId { get; set; }
    
    //Relation to other Classes
    public virtual Meeting Meeting { get; set; }
    public virtual NSSFRep NSSFRep { get; set; }
    public virtual Region Region { get; set; }
  }
}
