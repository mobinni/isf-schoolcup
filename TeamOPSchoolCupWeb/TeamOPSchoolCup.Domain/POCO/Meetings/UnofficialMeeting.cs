﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Domain.POCO.Meetings
{
  public class UnofficialMeeting
  {
    public int MeetingId { get; set; }

    public int CoachId { get; set; }

    public virtual Meeting Meeting { get; set; }
    public virtual Coach Coach { get; set; }
    public List<Team> Teams { get; set; }
  }
}
