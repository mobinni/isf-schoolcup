﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Domain.POCO.Meetings
{
  public class ISFMeeting
  {
    //Attributes of the Table
    public int MeetingId { get; set; }
    
    //Foreign Keys
    public Guid MemberId { get; set; }

    //Relation to other Classes
    public virtual Meeting Meeting { get; set; }
    public virtual ISFAdmin ISFAdmin { get; set; }
    public virtual List<Region> Regions { get; set; }
  }
}
