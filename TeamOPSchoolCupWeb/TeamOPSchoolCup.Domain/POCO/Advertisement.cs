﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Advertisement
  {
    public int AdvertisementId { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorCompReq", ErrorMessageResourceType = typeof(AdvertisementStrings))]
    [LocalizedDisplayName("ModelCompany", NameResourceType = typeof(AdvertisementStrings))]
    public String Company { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorBeDaReq", ErrorMessageResourceType = typeof(AdvertisementStrings))]
    [LocalizedDisplayName("ModelBegin", NameResourceType = typeof(AdvertisementStrings))]
    public DateTime Begin { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorEnDaReq", ErrorMessageResourceType = typeof(AdvertisementStrings))]
    [LocalizedDisplayName("ModelEnd", NameResourceType = typeof(AdvertisementStrings))]
    public DateTime End { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorUrlXReq", ErrorMessageResourceType = typeof(AdvertisementStrings))]
    [LocalizedDisplayName("ModelUrl", NameResourceType = typeof(AdvertisementStrings))]
    public String Url { get; set; }
    public bool ForGenderMale { get; set; }
    public bool ForGenderFemale { get; set; }

    public List<Region> Regions { get; set; }
    public List<Sport> Sports { get; set; }
    public List<AdvertisementStatistic> AdvertisementStatistics { get; set; }
  }
}
