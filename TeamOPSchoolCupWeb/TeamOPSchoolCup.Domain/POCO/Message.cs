﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel.DataAnnotations;

using TeamOPSchoolCup.Domain.POCO.Members;


namespace TeamOPSchoolCup.Domain.POCO
{
  public class Message
  {
    //Attributes of the Table
    public int MessageId { get; set; }
    [Required]
    [MaxLength(255)]
    public string Content { get; set; }
    public DateTime DatePosted { get; set; }
    
    //Foreign Keys
    public int TopicId { get; set; } 
    
    //Relations to other tables
    public virtual Topic Topic { get; set; }
    public Coach Coach { get; set; }
    public Student Student { get; set; }
  }
}
