﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.DTO;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Member
  {
    //We'll need to change this to make the id an FK to ASPSec
    //  But I doubt that EF won't add its own PK.
    [Key]
    public Guid MemberId { get; set; }

    [Required]
    [StringLength(30)]
    public string Name { get; set; }

    public DateTime? BirthDate { get; set; }

    [StringLength(50)]
    public string Street { get; set; }

    [MinLength(4)]
    [MaxLength(10)]
    public string Zipcode { get; set; }

    [StringLength(50)]
    public string City { get; set; }

    [StringLength(50)]
    public string Country { get; set; }

    [Required]
    [StringLength(50)]
    public string Email { get; set; }

    [Required]
    public String Password { get; set; }

    public int Language { get; set; }

    [Required]
    public short Gender_Value { get; set; }

    public string PhoneNumber { get; set; }
    
    public Guid MobileAccessKey { get; set; }

    //Relation to tables
    public virtual Student Student { get; set; }
    public virtual SCC SCC { get; set; }
    public virtual Coach Coach { get; set; }
    public virtual ISFAdmin ISFAdmin { get; set; }
    public virtual NSSFRep NSSFRep { get; set; }
    //Might want to add lazy-loaded references to subclasses

    public Gender Gender
    {
      get { return (Gender)Gender_Value; }
      set { Gender_Value = (short)value; }
    }

    public DateTime? LastLoginDate { get; set; }
    public DateTime? CreateDate { get; set; }
    public List<Role> Roles { get; set; }
    
    /*public int getId() {
      return UserId;
    }
    */

    public Member()
    {
      Roles = new List<Role>();
    }
  }
}
