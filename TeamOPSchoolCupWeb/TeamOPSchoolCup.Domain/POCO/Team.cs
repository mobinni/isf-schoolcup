﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.POCO.Meetings;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Team
  {
    //Attributes
    public int TeamId { get; set; }
    [Required]
    [StringLength(30)]
    public string Name { get; set; }
    public string Photo { get; set; }
    public string Logo { get; set; }
    public short Gender_Value { get; set; }
    public bool IsParticipating { get; set; } //This should be set to false every year

    //Foreign keys
    public int SportId { get; set; }
    public int SchoolId { get; set; }

    //Navigation properties
    public virtual List<TeamCoach> TeamCoaches { get; set; }
    public virtual List<TeamMembership> TeamMemberships { get; set; }
    public virtual Sport Sport { get; set; }
    public virtual School School { get; set; }
    //public virtual List<Meeting> Meetings { get; set; }
    public virtual List<TeamResult> TeamResults { get; set; }
    public virtual List<Topic> Topics { get; set; }
    public Gender Gender
    {
      get { return (Gender)Gender_Value; }
      set { Gender_Value = (short)value; }
    }
    public virtual List<CoachMeeting> CoachMeetings { get; set; }
  }
    
}
