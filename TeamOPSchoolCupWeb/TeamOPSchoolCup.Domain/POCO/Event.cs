﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.DTO;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Event : IEquatable<Event>
  {

    //Attributes of the Table

    public enum EUnit { TIME = 1, DISTANCE = 2 }
    public enum EDirectionality { ASC = 1, DESC = 2 } //Which results are better? Higher or lower? ASC = Higher is better, DESC = Lower is better 
    public int EventId { get; set; }
    [Required]
    [MaxLength(200)]
    public string Name { get; set; }
    public int Unit_Value { get; set; }
    public int Directionality_Value { get; set; }
    public bool IsOfficial { get; set; }
    public double MinimumValue { get; set; }
    public double MaximumValue { get; set; }
    [Required]
    public short Gender_Value { get; set; }

    public Gender Gender
    {
      get { return (Gender)Gender_Value; }
      set { Gender_Value = (short)value; }
    }
    //Foreign Keys

   
    public int? NSSFId { get; set; }
    public int SportId { get; set; }


    //Relations to other tables
    public virtual List<Result> Results { get; set; }
    public virtual Sport Sport { get; set; }
    public virtual NSSF NSSF { get; set; }
    public virtual List<Point> Points { get; set; }
    public virtual List<Record> Records { get; set; }
    public virtual List<Meeting> Meetings { get; set; }

    public EUnit Unit
    {
      get { return (EUnit)Unit_Value; }
      set { Unit_Value = (int)value; }
    }
    public EDirectionality Directionality
    {
      get { return (EDirectionality)Directionality_Value; }
      set { Directionality_Value = (int)value; }
    }

    public override int GetHashCode()
    {
      return this.EventId;
    }

    public bool Equals(Event other)
    {
      return this.GetHashCode() == other.GetHashCode();
    }
  }
}
