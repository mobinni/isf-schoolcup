﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.DataVisualization.Charting;

using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO.Members;


namespace TeamOPSchoolCup.Domain.POCO
{

  public class MyChart : Chart
  {

    public object[][] DataSerie { get; set; }
    public MyChart()
    {
      
    }

    
   
    public void InitialiseDataSerieArray(int count)
    {

      object[][] test = new object[count][];

      DataSerie = test;
    }
    
    
    
  }
}
