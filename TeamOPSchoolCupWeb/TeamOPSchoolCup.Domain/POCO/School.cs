﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Domain.POCO
{
  //Haven't localized lengths yet, as I'm not sure that they need to stay/are good.
  public class School
  {
    //Attributes
    public int SchoolId { get; set; }

    [Required(ErrorMessageResourceName="ModelErrorNameReq",ErrorMessageResourceType=typeof(SchoolStrings))]
    [StringLength(100)]
    [LocalizedDisplayName("ModelName", NameResourceType = typeof(SchoolStrings))]
    public string Name { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorStreetReq", ErrorMessageResourceType = typeof(SchoolStrings))]
    [StringLength(50)]
    [LocalizedDisplayName("ModelStreet", NameResourceType = typeof(SchoolStrings))]
    public string Street { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorCityReq", ErrorMessageResourceType = typeof(SchoolStrings))]
    [StringLength(50)]
    [LocalizedDisplayName("ModelCity", NameResourceType = typeof(SchoolStrings))]
    public string City { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorZipcodeReq", ErrorMessageResourceType = typeof(SchoolStrings))]
    [MinLength(4)]
    [MaxLength(10)]
    [LocalizedDisplayName("ModelZipcode", NameResourceType = typeof(SchoolStrings))]
    public string Zipcode { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorAbbrReq", ErrorMessageResourceType = typeof(SchoolStrings))]
    [LocalizedDisplayName("ModelAbbrName", NameResourceType = typeof(SchoolStrings))]
    public string AbbrName { get; set; }
    [Required(ErrorMessageResourceName = "ModelErrorEmailReq", ErrorMessageResourceType = typeof(SchoolStrings))]
    [StringLength(50)]
    [LocalizedDisplayName("ModelEmail", NameResourceType = typeof(SchoolStrings))]
    public string Email { get; set; }

    //Foreign Keys
    public int NSSFId { get; set; }

    //Navigation properties
    public virtual List<SCC> SCCs { get; set; }
    public virtual NSSF NSSF { get; set; }
    public virtual List<Team> Teams { get; set; }
  }
}
