﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.ComponentModel.DataAnnotations;
namespace TeamOPSchoolCup.Domain.POCO
{
  public class TeamCoach
  {
    //Attributes
    public DateTime DateJoined { get; set; }
    public DateTime DateLeft { get; set; }

    //Foreign Keys (which also are PKs)
    public Guid MemberId { get; set; }
    public int TeamId { get; set; }

    //Relation to tables
    public virtual Coach Coach { get; set; }
    public virtual Team Team { get; set; }
 
  }
}
