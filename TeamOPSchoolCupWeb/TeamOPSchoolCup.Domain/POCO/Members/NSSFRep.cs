﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.POCO.Meetings;

namespace TeamOPSchoolCup.Domain.POCO.Members
{
  public class NSSFRep
  {
    //Attributes of the Table
    
    //Foreign Keys
    public Guid MemberId { get; set; }
    public int NSSFId { get; set; }
    
    //Relation to other Classes
    public virtual Member Member { get; set; }
    public virtual NSSF NSSF { get; set; }
    public virtual List<RegionalMeeting> RegionalMeetings { get; set; }
  }
}
