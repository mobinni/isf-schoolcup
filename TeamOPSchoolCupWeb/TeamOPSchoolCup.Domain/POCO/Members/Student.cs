﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Ranking;

namespace TeamOPSchoolCup.Domain.POCO.Members
{
  public class Student 
  {
    //Attributes of the Table
    public String Photo { get; set; } //TODO Need to work out in detail how we're going to save photos
    public bool IsParticipating { get; set; }
    
    //Foreign Keys
    public Guid MemberId { get; set; }

    //Relation to other Classes
    public virtual Member Member { get; set; }
    
    // UserId in the table is a FK to asp.net user table

    public virtual List<TeamMembership> TeamMemberShips { get; set; }
    public List<BestStudentResult> BestStudentResults { get; set; }
    public virtual List<Topic> Topics { get; set; }
    public virtual List<Message> Messages { get; set; }
  }
}