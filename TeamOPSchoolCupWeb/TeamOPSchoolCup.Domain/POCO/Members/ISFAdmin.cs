﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Meetings;

namespace TeamOPSchoolCup.Domain.POCO.Members
{
  public class ISFAdmin
  {
    //Attributes of the Table
    
    //Foreign Keys
    public Guid MemberId { get; set; }

    //Relation to other Classes
    public virtual Member Member { get; set; }
    public virtual List<ISFMeeting> ISFMeetings { get; set; }
  }
}
