﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.POCO.Meetings;

namespace TeamOPSchoolCup.Domain.POCO.Members
{
  public class Coach
  {
    //Attributes of the Table
    public string Photo { get; set; } //TODO Need to work out in detail how we're going to save photos
    public bool IsParticipating { get; set; }
    
    //Foreign Keys
    public Guid MemberId { get; set; }
    public int SchoolId { get; set; }
    
    //Relation to other Classes
    public virtual Member Member { get; set; }
    public virtual School School { get; set; }
    public virtual List<CoachMeeting> UnofficialMeetings { get; set; }
    public virtual List<Topic> Topics { get; set; }
    public virtual List<Message> Messages { get; set; }
    public virtual List<TeamCoach> TeamCoaches { get; set; }
  }
}
