﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.POCO.Members
{
  public class SCC
  {
    //Attributes of the Table
    
    //Foreign Keys
     public Guid MemberId { get; set; }
     public int SchoolId { get; set; }

    //Relation to other Classes
    public virtual Member Member { get; set; }
    public virtual School School { get; set; }
  }
}
