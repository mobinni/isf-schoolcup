﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Record
  {
    public int RecordId { get; set; }
    [Required]
    public double Result { get; set; }
    [Required]
    public DateTime RecordDate { get; set; }
    [Required]
    [StringLength(100)]
    public string Meeting { get; set; }
    [Required]
    public int Age { get; set; }

    public int RecordHolderId { get; set; }
    public int EventId { get; set; }
    public int SportId { get; set; }
    
    public RecordHolder RecordHolder { get; set; }
    public Event Event { get; set; }
    public Sport Sport { get; set; }
  }
}
