﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class User
  {
    //We'll need to change this to make the id an FK to ASPSec
    //  But I doubt that EF won't add its own PK.
    public int UserId { get; set; }
    public String Name { get; set; }
    public DateTime BirthDate { get; set; }
    public String Address { get; set; }
    public String Email { get; set; }
    public Boolean Sex { get; set; }
    public String Photo { get; set; } //TODO Need to work out in detail how we're going to save photos
    public String PhoneNumber { get; set; }


    //Might want to add lazy-loaded references to subclasses

    /*
    public int getId() {
      return UserId;
    }
    */
  }
}
