﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.POCO.Ranking
{
  public class BestTeamResult
  {
    //Attributes - Foreign Keys
    public int MeetingId { get; set; }
    public int TeamId { get; set; }
    public int CompetitionYear { get; set; }

    //Navigation properties
    public virtual TeamResult TeamResult { get; set; }
  }
}
