﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.POCO.Ranking
{
  public class TeamResult
  {
    //Attributes - Foreign Keys
    public int TeamId { get; set; }
    public int MeetingId { get; set; }
    public double PointValue { get; set; }

    //Navigation Properties
    public virtual Team Team { get; set; }
    public virtual Meeting Meeting { get; set; }
  }
}
