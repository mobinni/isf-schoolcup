﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.DTO;

namespace TeamOPSchoolCup.Domain.POCO.Ranking
{
  public class Point
  {
    //Attributes
    public int PointId { get; set; }
    public short Gender_Value { get; set; }
    public int PointValue { get; set; }
    public double Measure { get; set; }
    public int CompetitionYear { get; set; }

    //Foreign Keys
    public int EventId { get; set; }

    //Navigation Properties
    public Event Event { get; set; }

    public Gender Gender
    {
      get { return (Gender)Gender_Value; }
      set { Gender_Value = (short)value; }
    }
  }
}
