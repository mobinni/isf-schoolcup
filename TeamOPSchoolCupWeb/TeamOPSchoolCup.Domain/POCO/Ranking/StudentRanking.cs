﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Domain.POCO.Ranking
{
  public class StudentRanking
  {
    //Attributes -- Foreign Keys
    public Guid MemberId { get; set; }
    public int EventId { get; set; }
    public int CurrentWorldRanking { get; set; }
    public int BestWorldRankingThisYear { get; set; }

    //Navigation Properties
    public Student Student { get; set; }
    public Event Event { get; set; }
  }
}
