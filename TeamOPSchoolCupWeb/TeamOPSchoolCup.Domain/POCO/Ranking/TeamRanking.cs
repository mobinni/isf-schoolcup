﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.POCO.Ranking
{
  public class TeamRanking
  {
    //Attributes - Foreign Keys
    public int TeamId { get; set; }
    public int EventId { get; set; }
    public int CurrentWorldRanking { get; set; }
    public int BestWorldRankingThisYear { get; set; }

    //Navigation Properties
    public Team Team { get; set; }
  }
}
