﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Domain.POCO.Ranking
{
  public class BestStudentResult
  {
    //Attributes
    public int CompetitionYear { get; set; }
    public bool IsOfficial { get; set; }

    //Foreign keys
    public Guid MemberId { get; set; }
    public int EventId { get; set; }
    public int ResultId { get; set; }

    //Navigation properties
    public virtual Student Student { get; set; }
    public virtual Event Event { get; set; }
    public virtual Result Result { get; set; }

    public BestStudentResult()
    {
      this.CompetitionYear = DateTime.Today.Year;
    }
  }
}
