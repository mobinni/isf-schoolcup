﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.Resources;
using TeamOPSchoolCup.Domain.DTO;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Sport : IComparable<Sport>
  {
    public int SportId { get; set; }
    [LocalizedDisplayName("ModelName", NameResourceType = typeof(SportStrings))]
    public string Name { get; set; }

    [Required(ErrorMessageResourceName = "ModelErrorAbbrReq",ErrorMessageResourceType = typeof(SportStrings))]
    [StringLength(3, ErrorMessageResourceName = "ModelErrorAbbrLength",ErrorMessageResourceType = typeof(SportStrings))]
    [LocalizedDisplayName("ModelAbbr", NameResourceType = typeof(SportStrings))]
    public string Abbreviation { get; set; }

    public List<Advertisement> Advertisements { get; set; }
    public List<Team> Teams { get; set; }
    public List<Event> Events { get; set; }
    public virtual List<Record> Record { get; set; }
    public int CompareTo(Sport other)
    {
      return Name.CompareTo(other.Name);
    }
  }
}
