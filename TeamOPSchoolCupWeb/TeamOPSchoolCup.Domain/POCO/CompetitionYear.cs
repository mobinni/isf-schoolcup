﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class CompetitionYear
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int CompetitionYearValue { get; set; }
  }
}
