﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using System.ComponentModel.DataAnnotations;
namespace TeamOPSchoolCup.Domain.POCO
{
  public class Meeting
  {
    public int MeetingId { get; set; }
    [Required]
    [StringLength(150)]
    public String Name { get; set; }
    [Required]
    [StringLength(150)]
    public String Location { get; set; }
    [Required]
    public bool IsOfficial { get; set; } //Not using Officiality for now, because with ALL as one of the values, it woudn't be very wise for data storage
    [Required]
    public DateTime Date { get; set; }
    

    public ISFMeeting ISFMeeting { get; set; }
    public RegionalMeeting RegionalMeeting { get; set; }
    public CoachMeeting CoachMeeting { get; set; }
    public List<TeamResult> TeamResults { get; set; }
    public List<Result> Results { get; set; }
    public List<Event> Events { get; set; }
    
  }
}
