﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Domain.POCO
{
  [NotOwnParent(ErrorMessageResourceName="ModelErrorNotOwnParent",ErrorMessageResourceType=typeof(RegionStrings))]
  public class Region : IComparable<Region>
  {
    //Attributes of the Table
    public int RegionId { get; set; }
    [LocalizedDisplayName("ModelName",NameResourceType=typeof(RegionStrings))]
    [Required(ErrorMessageResourceName="ModelErrorNameReq",ErrorMessageResourceType=typeof(RegionStrings))]
    [StringLength(50,ErrorMessageResourceName="ModelErrorNameLength",ErrorMessageResourceType=typeof(RegionStrings))]
    public string Name { get; set; }
    public DateTime DateJoined { get; set; }

    //Foreign Keys
    [Column("Parent")]
    [LocalizedDisplayName("ModelParent", NameResourceType = typeof(RegionStrings))]
    public int? ParentId { get; set; }

    //Relation to other Classes
    [ForeignKey("ParentId")]
    [LocalizedDisplayName("ModelParent",NameResourceType=typeof(RegionStrings))]
    public virtual Region Parent { get; set; }
    public virtual List<Region> Children { get; set; }
    public virtual NSSF NSSF { get; set; }
    public List<ISFMeeting> ISFMeetings { get; set; }
    public List<RegionalMeeting> RegionalMeetings { get; set; }
    public List<Advertisement> Advertisements { get; set; }

    public int CompareTo(Region other)
    {
      return Name.CompareTo(other.Name);
    }
  }
}
