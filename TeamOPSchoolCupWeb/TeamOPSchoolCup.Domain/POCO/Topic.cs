﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Topic : IComparable<Topic>
  {
    //Attributes of the Table
    public int TopicId { get; set; }
    [Required]
    [StringLength(50)]
    public string Title { get; set; }
    public bool Spotlight { get; set; }

    //Foreign Keys
    public int TeamId { get; set; }

    //Relations to other tables
    public virtual List<Message> Messages { get; set; }
    public virtual Team Team { get; set; }
    public Coach Coach { get; set; }
    public Student Student { get; set; }
   
    public int CompareTo(Topic other) {
      if (other == null || other.Messages.Count == 0) return 0;
      if (Messages.Count == 0 || Messages == null) return 1;
      return Messages.First<Message>().DatePosted.CompareTo(other.Messages.First<Message>().DatePosted);
    }
  }
}
