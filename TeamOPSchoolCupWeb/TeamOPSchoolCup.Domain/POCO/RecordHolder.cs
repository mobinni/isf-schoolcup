﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TeamOPSchoolCup.Domain.DTO;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class RecordHolder
  {
    public int RecordHolderId { get; set; }
    [Required]
    [StringLength(50)]
    public string Name { get; set; }
    [Required]
    public short Gender_Value { get; set; }
    public Gender Gender
    {
      get { return (Gender)Gender_Value; }
      set { Gender_Value = (short)value; }
    }
    public List<Record> Records { get; set; }
  }
}
