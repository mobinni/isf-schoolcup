﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.DTO;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class AdvertisementStatistic
  {
    public int AdvertisementId { get; set; }
    public int RegionId { get; set; }
    public short Gender_Value { get; set; }
    public int SportId { get; set; }

    public int ViewCount { get; set; }
    public int ClickCount { get; set; }

    public Advertisement Advertisement { get; set; }
    public Region Region { get; set; }

    public Gender? Gender
    {
      get
      {
        if (Gender_Value==0) return null;
        return (Gender)Gender_Value; 
      }
      set
      {
        if (!value.HasValue) Gender_Value = 0;
        Gender_Value = (short)value; }
    }
  }
}
