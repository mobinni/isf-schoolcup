﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Role
  {
    [Key]
    public Guid RoleId { get; set; }

    [Required]
    public String RoleName { get; set; }

    public virtual List<Member> Members { get; set; }
  }
}
