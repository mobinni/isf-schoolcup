﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.ComponentModel.DataAnnotations;
namespace TeamOPSchoolCup.Domain.POCO
{
  public class TeamMembership
  {
    //Attributes
    public DateTime DateJoined { get; set; }
    public DateTime DateLeft { get; set; }

    //Foreign Keys (which also are PKs)
    public Guid MemberId { get; set; }
    public int TeamId { get; set; }

    //Relation to tables
    public virtual Student Student { get; set; }
    public virtual Team Team { get; set; }
    public List<Result> Results { get; set; }
  }
}
