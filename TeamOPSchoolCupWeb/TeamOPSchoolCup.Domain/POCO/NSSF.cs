﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class NSSF
  {
    //Attributes of the Table
    public int NSSFId { get; set; }
    [Required]
    [StringLength(30)]
    public string Name { get; set; }
    [Required]
    [StringLength(50)]
    public string Street { get; set; }
    [Required]
    [MinLength(4)]
    [MaxLength(10)]
    public string Zipcode { get; set; }
    [Required]
    [StringLength(50)]
    public string City { get; set; }
    //[Required]
    [StringLength(50)]
    public string Country { get; set; }
    [Required]
    public string PhoneNumber { get; set; }
    [Required]
    public string FaxNumber { get; set; }
    [Required]
    [StringLength(50)]
    public string Email { get; set; }
    public DateTime DateAdded { get; set; }

    //Foreign Keys
   // [Column("Region")]
    //public int RegionId { get; set; }
    

    //Relations to other tables
    
    public virtual Region Region { get; set; }
    public virtual List<Event> Events { get; set; }
    public virtual List<School> Schools { get; set; }
    public virtual List<NSSFRep> NSSFReps { get; set; }
    public virtual List<RegionalMeeting> RegionalMeetings { get; set; }

    public NSSF()
    {
      NSSFReps = new List<NSSFRep>();
    }

  }
}
