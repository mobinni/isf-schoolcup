﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TeamOPSchoolCup.Domain.POCO
{
  public class Result
  {
    //Attributes of the Table
    public int ResultId { get; set; }
    [Required]
    public double ResultScore { get; set; }
    //public int? PointValue { get; set; }

    //Foreign Keys
    public int EventId { get; set; }
    public Guid MemberId { get; set; }
    public int TeamId { get; set; }
    public int MeetingId { get; set; }
    
    //Relations to other tables
    public virtual Event Event { get; set; }
    public virtual TeamMembership TeamMembership { get; set; }
    public virtual Meeting Meeting { get; set; }
  }
}
