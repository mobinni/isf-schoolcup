﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Domain.Exceptions
{
  public class TeamOPSchoolCupException : Exception
  {

    public TeamOPSchoolCupException(String message) : base(message)
    {
      
    }
  }
}
