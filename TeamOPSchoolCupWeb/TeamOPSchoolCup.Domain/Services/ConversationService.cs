﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.DTO;
using System.Data;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Domain.Services
{
  public class ConversationService
  {
    #region ADD
    public static bool AddTopic(Coach coach=null, Student student = null,Topic topic = null, Conversation conversation = null)
    {
      bool n;
      using (EFDbContext db = new EFDbContext()) 
      {
        n = AddTopic(db,coach,student,topic, conversation);
      }
      return n;
    }
    public static bool AddTopic(EFDbContext db, Coach coach = null, Student student = null, Topic topic = null, Conversation conversation = null)
    {

      Topic top = topic;
      if (top == null)
      {
        topic = new Topic { Title = conversation.Title, Spotlight = conversation.Spotlight, Team = TeamService.GetTeam(db, conversation.TeamId),Coach = (coach!=null?MemberService.GetCoach(db,coach.MemberId):null),Student=(student!=null?MemberService.GetStudent(db,student.MemberId):null) };

        db.Topics.Add(topic);

        db.SaveChanges();

        return (AddMessage(db, new Message { Content = conversation.Content, DatePosted = conversation.DatePosted }, coachId: (coach != null ? (Guid?)coach.MemberId : null), studentId: (student != null ? (Guid?)student.MemberId : null), topicId: topic.TopicId));

      }
      
        db.Topics.Add(topic);

        return (db.SaveChanges() >0);
      
      
      
     }
    public static bool AddMessage(Message message, Guid? coachId = null, Guid? studentId = null, int? topicId = null)
    {
      bool n;
      using (EFDbContext db = new EFDbContext())
      {
        n = AddMessage(db, message, coachId, studentId, topicId);
      }
      return n;
    }
    public static bool AddMessage(EFDbContext db, Message message, Guid? coachId = null, Guid? studentId = null, int? topicId = null) 
    {
      if (topicId != null)
      {
        message.Topic = GetTopic(db,topicId.Value);
      }
      if (coachId != null) { message.Coach = MemberService.GetCoach(db,coachId.Value); }
      if (studentId != null) { message.Student = MemberService.GetStudent(db, studentId.Value); }      
      db.Messages.Add(message);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    public static Topic GetTopic(int id) 
    {
      Topic topic;
      using (EFDbContext db = new EFDbContext()) 
      {
        topic = GetTopic(db, id);
      }
      return topic;
    }
    public static Topic GetTopic(EFDbContext db, int id)
    {
      //Topic topic = db.Topics.Include("Team").Include("Coach.Member").Include("Student.Member").Include("Messages").Single(t => t.TopicId == id);
      return db.Topics.Include("Team").Include("Coach.Member").Include("Student.Member").Include("Messages").Single(t => t.TopicId == id);
    }

    public static List<Topic> GetTopicsOfTeam(Team team, bool sort) {
      List<Topic> topics;
      using (EFDbContext db = new EFDbContext()) {
        topics = GetTopicsOfTeam(db, team, sort);
      }
      return topics;
    }
    public static List<Topic> GetTopicsOfTeam(EFDbContext db, Team team, bool sort) {
      List<Topic> topics = db.Topics.Include("Team").Include("Coach.Member").Include("Student.Member").Include("Messages").Where(t => t.TeamId == team.TeamId).ToList<Topic>();
      if (sort) topics.Sort();

      return topics;
    }
    public static Message GetFirstMessage(Topic topic)
    {
      if (topic == null) return null;
      Message message;
      using (EFDbContext db = new EFDbContext())
      {
        message = GetFirstMessage(db, topic);
      }
      return message;
    }

    public static Topic GetLatestSpotlightTopic(Team team) {
      Topic topic = null;
      List<Topic> topics;
      using (EFDbContext db = new EFDbContext()){
        topics = GetTopicsOfTeam(db, team, true);  
      }
      foreach (Topic t in topics) {
        if (t.Spotlight == true) topic = t;
      }

      return topic;
    }

    public static Message GetFirstMessage(EFDbContext db, Topic topic)
    {
      if (topic.Messages.Count == 0 ) { return null; }
      return db.Topics.Single(t => t.TopicId == topic.TopicId).Messages.First();
      
    }

    public static List<Message> GetMessagesOfTopic(int id) {
      List<Message> messages = null;

      using (EFDbContext db = new EFDbContext()) {
        messages = GetMessagesOfTopic(db, id);
      }

      return messages;
    }

    public static List<Message> GetMessagesOfTopic(EFDbContext db,int id) {
      List<Message> messages = null;

      messages = db.Messages.Include("Coach.Member").Include("Student.Member").Where(m => m.TopicId == id).ToList();

      return messages;
    }
    #endregion
    #region UPDATE
    public static bool UpdateFirstMessageOfTopic(Topic topic, string content) 
    {
      bool n;
      using (EFDbContext db = new EFDbContext())
      {
        n = UpdateFirstMessageOfTopic(db, topic, content);
      }
      return n;
    }
    public static bool UpdateFirstMessageOfTopic(EFDbContext db, Topic topic, string content) 
    {
      Message message = ConversationService.GetFirstMessage(topic);

      message.Content = string.Format("{0}\n{1} [{2}]:\n{3}", message.Content, TeamOPSchoolCup.Domain.Resources.TeamStrings.mchange, DateTime.Now.ToString("MMMM dd, yyyy h:mm"), content);
      db.Entry(message).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region DELETE
    public static bool DeleteTopic(Topic topic) 
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) 
      {
        b = DeleteTopic(db, topic);
      }
      return b;
    }
    public static bool DeleteTopic(EFDbContext db,Topic topic)
    {
      if (topic.Messages.Count == 1) 
      { 
        db.Messages.Remove(topic.Messages.First());
        db.SaveChanges();
        db.Topics.Remove(topic);
      }
      return (db.SaveChanges() > 0);
    }
    #endregion
  }
}
