﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace TeamOPSchoolCup.Domain.Services
{
  public class MailService
  {
    /*
     * subject: The subject of the mail
     * recipients: The recipients of the mail. Each recipient receives a separate mail.
     * body: The body of the email. Can contain Html
     * isHtml: Defines if the body contains Html.
     */
    public static void SendMail(String subject, string recipient, String body, bool isHtml)
    {
      SendMail(subject, new List<string> { recipient }, body, isHtml);
    }

    public static void SendMail(String subject, List<string> recipients, String body, bool isHtml)
    {
      SmtpClient client = new SmtpClient();
      MailMessage message = new MailMessage();

      foreach (String recipient in recipients)
      {
        try
        {
          message.Subject = subject;
          message.From = new MailAddress("isfmail@wimtibackx.be"); //Moet echt geldig zijn!! Dit kan eigenlijk gewoon het adres zijn waarmee wordt ingelogd op smtp server (zie authInfo)
          message.To.Add(recipient);
          message.IsBodyHtml = isHtml; //Aanzetten om html te kunnen gebruiken
          message.Body = body;

          NetworkCredential authInfo = new NetworkCredential("isfmail@wimtibackx.be", "Q7edK7Sh"); //Account waarmee wordt ingelogd

          client.Host = "mail.wimtibackx.be";
          client.Port = 587;
          client.EnableSsl = false; //Gmail: true, Versio: false
          client.UseDefaultCredentials = false;
          client.Credentials = authInfo;
          client.DeliveryMethod = SmtpDeliveryMethod.Network;

          client.Send(message);
        }
        catch (Exception)
        {
        }
      }


    }
  }
}
