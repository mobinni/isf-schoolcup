﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using System.Collections;
using System.Data;
using TeamOPSchoolCup.Domain.DTO;

namespace TeamOPSchoolCup.Domain.Services
{
  public class AdvertisementService
  {
    #region ADD
    public static bool AddAdvertisement(Advertisement advertisement)
    {
      bool b;
      using(EFDbContext db = new EFDbContext())
      {
        b = AddAdvertisement(db,advertisement);
      }
      return b;
    }
    public static bool AddAdvertisement(EFDbContext db,Advertisement advertisement) 
    {
      db.Advertisements.Add(advertisement);
      return (db.SaveChanges() > 0);
      
    }
    public static bool AddAdvertisementFromEditableAd(EditableAdvertisement advertisement)
    {
      bool b;
      using (EFDbContext db = new EFDbContext())
      {
        b = AddAdvertisementFromEditableAd(db, advertisement);
      }
      return b;
    }
    public static bool AddAdvertisementFromEditableAd(EFDbContext db, EditableAdvertisement edAd)
    {
      Advertisement ad = new Advertisement
      {
        Company = edAd.Company,
        Begin = edAd.Begin,
        End = edAd.End,
        Url = edAd.Url,
        ForGenderMale = edAd.ForGenderMale,
        ForGenderFemale = edAd.ForGenderFemale,
        Regions = new List<Region>(),
        Sports = new List<Sport>()
      };
      string[] regIds = edAd.RegionsStr.Split(',');
      foreach (string regIdString in regIds)
      {
        //If we can prepopulate the checkboxes and hidden field on view side, this'll work.
        ad.Regions.Add(RegionService.GetRegion(db,Int32.Parse(regIdString)));
      }
      string[] sportIds = edAd.SportsStr.Split(',');
      foreach (string sportIdString in sportIds)
      {
        //If we can prepopulate the checkboxes and hidden field on view side, this'll work.
        ad.Sports.Add(SportService.GetSport(db, Int32.Parse(sportIdString)));
      }
      db.Advertisements.Add(ad);
      bool bResult = (db.SaveChanges() > 0);
      edAd.AdvertisementId = ad.AdvertisementId;
      return bResult;

    }
    #endregion
    /*
     * Get an applicable advertisement for a user
     */
    public static AdViewing GetApplicableAdvertisement(Member member=null,Team team=null)
    {
      AdViewing ad = null;
      using (EFDbContext db = new EFDbContext()) { ad = GetApplicableAdvertisement(db, member,team); }
      return ad;
    }

    public static AdViewing GetApplicableAdvertisement(EFDbContext db,Member member=null,Team team=null)
    {
      AdViewing adViewing = null;
      if (member != null)
      {
        //We hebben een member, daarvoor een ad zoeken
        Gender gender = member.Gender;
        List<int> sportIds = SportService.GetSports(db, member).Select(x => x.SportId).ToList();
        List<Region> regions = RegionService.GetRegionsUp(db,RegionService.GetRegionOfMember(db, member.MemberId));
        foreach (Region region in regions)
        {
          List<Advertisement> ads = db.Advertisements.Include("Regions").Include("Sports").Include("AdvertisementStatistics")
            .Where(x => x.Begin <= DateTime.Today && x.End >= DateTime.Today).Where(x => x.Regions.Any(y => y.RegionId == region.RegionId))
            .Where(x => (gender == Gender.MALE ? x.ForGenderMale == true : x.ForGenderFemale == true)).Where(x => x.Sports.Any(y => sportIds.Contains(y.SportId))).ToList();/*GetAdvertisements(db, region);*/
          if (ads == null || ads.Count == 0) continue;
          ads.Where(x => x.AdvertisementStatistics.Where(y => y.RegionId == region.RegionId).Where(y => y.Gender == gender).Where(y => y.SportId == (sportIds.Count == 1 ? sportIds.First() : 0)).ToList().Count == 0).ToList().ForEach(x =>
          {
            CreateAdvertisementStatistic(db, x, region, gender, (sportIds.Count == 1 ? sportIds.First() : 0));
            db.Entry(x).Reload();
          });
          ads = ads.OrderBy(x => x.AdvertisementStatistics.Single(y => y.RegionId == region.RegionId && y.Gender == gender && y.SportId == (sportIds.Count == 1 ? sportIds.First() : 0)).ViewCount).ToList();
          Advertisement ad = ads.First();
          AdvertisementStatistic adstat = ad.AdvertisementStatistics.Single(y => y.RegionId == region.RegionId && y.Gender == gender && y.SportId == (sportIds.Count == 1 ? sportIds.First() : 0));
          adstat.ViewCount++;
          db.Entry(adstat).State = EntityState.Modified;
          db.SaveChanges();
          adViewing = new AdViewing { Advertisement = ad, RegionId = region.RegionId, Gender = gender, SportId = (sportIds.Count == 1 ? sportIds.First() : 0) };
          //return ad;
          break;
        }
        //return null;
      }
      else if (team != null)
      {
        //We zitten op een teampagina, daarvoor een ad zoeken
        Gender gender = team.Gender;
        int sportId = team.Sport.SportId;
        List<Region> regions = RegionService.GetRegionsUp(db,team.School.NSSF.Region);
        foreach (Region region in regions)
        {
          List<Advertisement> ads = db.Advertisements.Include("Regions").Include("Sports").Include("AdvertisementStatistics")
            .Where(x => x.Begin <= DateTime.Today && x.End >= DateTime.Today).Where(x => x.Regions.Any(y => y.RegionId == region.RegionId))
            .Where(x => (gender == Gender.MALE ? x.ForGenderMale == true : x.ForGenderFemale == true)).Where(x => x.Sports.Any(y => y.SportId == sportId)).ToList();/*GetAdvertisements(db, region);*/
          if (ads == null || ads.Count == 0) continue;
          ads.Where(x => x.AdvertisementStatistics.Where(y => y.RegionId == region.RegionId).Where(y => y.Gender == gender).Where(y => y.SportId == sportId).ToList().Count == 0).ToList().ForEach(x =>
          {
            CreateAdvertisementStatistic(db, x, region, gender, sportId);
            db.Entry(x).Reload();
          });
          ads = ads.OrderBy(x => x.AdvertisementStatistics.Single(y => y.RegionId == region.RegionId && y.Gender == gender && y.SportId == sportId).ViewCount).ToList();
          Advertisement ad = ads.First();
          AdvertisementStatistic adstat = ad.AdvertisementStatistics.Single(y => y.RegionId == region.RegionId && y.Gender == gender && y.SportId == sportId);
          adstat.ViewCount++;
          db.Entry(adstat).State = EntityState.Modified;
          db.SaveChanges();
          adViewing = new AdViewing { Advertisement = ad, RegionId = region.RegionId, Gender = gender, SportId = sportId };
          break;
          //return ad;
        }
        //return null;
      }
      else
      {
        //Een algemene ad (both genders, all sports, world region)
        List<int> sportIds = SportService.GetSports(db, false).Select(x => x.SportId).ToList();
        Region region = RegionService.GetWorld(db);
        List<Advertisement> ads = db.Advertisements.Include("Regions").Include("Sports").Include("AdvertisementStatistics")
          .Where(x => x.Begin <= DateTime.Today && x.End >= DateTime.Today).Where(x => x.Regions.Any(y => y.RegionId == region.RegionId))
          .Where(x => x.ForGenderMale == true && x.ForGenderFemale == true).Where(x => x.Sports.Any(y => sportIds.Contains(y.SportId))).ToList();/*GetAdvertisements(db, region);*/
        if (ads == null || ads.Count == 0) return null;
        ads.Where(x => x.AdvertisementStatistics.Where(y => y.RegionId == region.RegionId).Where(y => y.Gender_Value==0).Where(y => y.SportId == 0).ToList().Count == 0).ToList().ForEach(x =>
        {
          CreateAdvertisementStatistic(db, x, region, null, 0);
          db.Entry(x).Reload();
        });
        ads = ads.OrderBy(x => x.AdvertisementStatistics.Single(y => y.RegionId == region.RegionId && y.Gender == null && y.SportId == 0).ViewCount).ToList();
        Advertisement ad = ads.First();
        AdvertisementStatistic adstat = ad.AdvertisementStatistics.Single(y => y.RegionId == region.RegionId && y.Gender == null && y.SportId == 0);
        adstat.ViewCount++;
        db.Entry(adstat).State = EntityState.Modified;
        db.SaveChanges();
        adViewing = new AdViewing { Advertisement = ad, RegionId = region.RegionId, Gender = null, SportId = 0 };
        //return ad;
      }
      return adViewing;
    }

    /*
     * Create an adstat for an ad-region combo
     */
    private static bool CreateAdvertisementStatistic(EFDbContext db, Advertisement ad, Region region,Gender? gender, int sportId)
    {
      AdvertisementStatistic adstat = new AdvertisementStatistic { Advertisement = ad, Region = region, ViewCount = 0, ClickCount = 0, SportId=sportId};
      if (gender.HasValue) adstat.Gender = gender;
      db.AdvertisementStatistics.Add(adstat);
      return (db.SaveChanges() > 0);
    }

    /*
     * Get advertisements by a multitude of conditions
     */
    public static List<Advertisement> GetAdvertisements(Region region = null,Sport sport = null)
    {
      List<Advertisement> ads = null;
      using (EFDbContext db = new EFDbContext()) { ads = GetAdvertisements(db, region,sport); }
      return ads;
    }
    public static List<Advertisement> GetAdvertisements(EFDbContext db, Region region = null, Sport sport = null)
    {
      //TODO Should account for historicality later
      List<Advertisement> advertisements = db.Advertisements.Include("Regions").Include("Sports").Include("AdvertisementStatistics").Where(x => x.Begin <= DateTime.Today && x.End >= DateTime.Today).ToList();
      if (region != null) advertisements = advertisements.Where(x => x.Regions.Any(y => y.RegionId==region.RegionId)).ToList();
      if (sport != null) advertisements = advertisements.Where(x => x.Sports.Any(s => s.SportId == sport.SportId)).ToList();
      return advertisements;
    }

    /*
     * Get an ad by id
     */
    public static Advertisement GetAd(int id)
    {
      Advertisement ad = null;
      using (EFDbContext db = new EFDbContext()) { ad=GetAd(db, id); }
      return ad;
    }
    public static Advertisement GetAd(EFDbContext db,int id)
    {
      return db.Advertisements.Include("AdvertisementStatistics").Include("Sports").Include("Regions").Single(x => x.AdvertisementId == id);
    }

    /*
     * Register a click
     */
    public static bool RegisterClick(Advertisement ad, int regionId, Gender? gender,int sportId)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = RegisterClick(db, ad, regionId, gender, sportId); }
      return b;
    }

    public static bool RegisterClick(EFDbContext db,Advertisement ad, int regionId,Gender? gender,int sportId)
    {
      AdvertisementStatistic adstat=ad.AdvertisementStatistics.Single(y => y.RegionId == regionId && y.SportId==sportId && y.Gender==gender);
      adstat.ClickCount++;
      db.Entry(adstat).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }


    public static bool EditAdvertisementFromEditableAd(EditableAdvertisement advertisement)
    {
      bool b;
      using (EFDbContext db = new EFDbContext())
      {
        b = EditAdvertisementFromEditableAd(db, advertisement);
      }
      return b;
    }
    public static bool EditAdvertisementFromEditableAd(EFDbContext db, EditableAdvertisement edAd)
    {
      Advertisement ad = AdvertisementService.GetAd(db, edAd.AdvertisementId);
      ad.Company = edAd.Company;
      ad.Begin = edAd.Begin;
      ad.End = edAd.End;
      ad.Url = edAd.Url;
      ad.ForGenderMale = edAd.ForGenderMale;
      ad.ForGenderFemale = edAd.ForGenderFemale;
      ad.Regions = new List<Region>();
      ad.Sports = new List<Sport>();
      string[] regIds = edAd.RegionsStr.Split(',');
      foreach (string regIdString in regIds)
      {
        //If we can prepopulate the checkboxes and hidden field on view side, this'll work.
        ad.Regions.Add(RegionService.GetRegion(db, Int32.Parse(regIdString)));
      }
      string[] sportIds = edAd.SportsStr.Split(',');
      foreach (string sportIdString in sportIds)
      {
        //If we can prepopulate the checkboxes and hidden field on view side, this'll work.
        ad.Sports.Add(SportService.GetSport(db, Int32.Parse(sportIdString)));
      }
      db.Entry(ad).State = EntityState.Modified;
      bool bResult = (db.SaveChanges() > 0);
      //edAd.AdvertisementId = ad.AdvertisementId;
      return bResult;

    }
  }
}
