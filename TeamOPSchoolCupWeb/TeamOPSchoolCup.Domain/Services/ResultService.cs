﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using TeamOPSchoolCup.Domain.DTO;

namespace TeamOPSchoolCup.Domain.Services
{
  public class ResultService
  {
    #region ADD
    /*
     * Add a single result, this redirects to AddResults(db, List<Result>, Meeting)
     */
    public static bool AddResult(Result result)
    {
      bool r;
      using (EFDbContext db = new EFDbContext())
      {
        r = AddResult(db, result);
      }
      return r;
    }

    public static bool AddResult(EFDbContext db, Result result)
    {
      return ResultService.AddResults(db, new List<Result> { result }, MeetingService.GetMeeting(db,result.MeetingId));
    }
    /*
     * Add a list of results within a meeting
     */
    public static bool AddResults(List<Result> results, Meeting meeting)
    {
      bool b = false;
      using (EFDbContext db = new EFDbContext())
      {
        b = ResultService.AddResults(db, results, meeting);
      }
      return b;
    }

    public static bool AddResults(EFDbContext db, List<Result> results, Meeting meeting)
    {
      meeting = MeetingService.GetMeeting(db,meeting.MeetingId);
      results.ForEach(x => db.Results.Add(x));
      bool b = (db.SaveChanges() > 0);
      //To calculate meetings, we need to include some stuff we set via id:
      results.ForEach(x =>
      {
        //db.Entry(x).Reference("TeamMembership").Load();
        //db.Entry(x.TeamMembership).Reference("Student").Load();
        db.Entry(x).Reference("Event").Load();
        //db.Entry(x).Reference("Meeting").Load();
      });
      RunRankingCalculations(db, results, meeting);
      return b;
    }
    #endregion
    #region GET
    /*
     * Get a list of results based on a bunch of optional parameters. Usage advice: use named parameters
     * List of possible parameters: TeamMembership, Team, Student, Event, CompetitionYear, isOfficial, Meeting
     */
    public static List<Result> GetResults(TeamMembership teamMembership = null, Team team = null, Student student = null, Event pEvent = null, int? competitionYear = null, bool? isOfficial = null, Meeting meeting = null)
    {
      List<Result> r;
      using (EFDbContext db = new EFDbContext())
      {
        r = GetResults(db, teamMembership, team, student, pEvent, competitionYear, isOfficial, meeting);
      }
      return r;
    }

    public static List<Result> GetResults(EFDbContext db, TeamMembership teamMembership = null, Team team = null, Student student = null, Event pEvent = null, int? competitionYear = null, bool? isOfficial = null, Meeting meeting = null)
    {
      List<Result> results = db.Results.Include("Event.Sport").Include("TeamMembership.Team.School").Include("TeamMembership.Student.Member").Include("Meeting").ToList();
      if (teamMembership != null) { results = results.Where(x => x.TeamMembership.Equals(teamMembership)).ToList(); }
      if (team != null) { results = results.Where(x => x.TeamId == team.TeamId).ToList(); }
      if (student != null) { results = results.Where(x => x.MemberId == student.MemberId).ToList(); }
      if (pEvent != null) { results = results.Where(x => x.EventId.Equals(pEvent.EventId)).ToList(); }
      if (competitionYear != null) { results = results.Where(x => x.Meeting.Date.Year == competitionYear.Value).ToList(); }
      if (isOfficial.HasValue)
      {
        if (!isOfficial.Value) results = results.Where(x => x.Meeting.IsOfficial == false || x.Event.IsOfficial == false).ToList();
        else results = results.Where(x => x.Meeting.IsOfficial == true && x.Event.IsOfficial == true).ToList();
      }
      if (meeting != null) { results = results.Where(x => x.MeetingId == meeting.MeetingId).ToList(); }
      return results;
    }

    /*
     * Experiment: we always use the Id's in the main method for GetResults, so allow to pass the id's on directly, so that we have to load less results
     */
    public static List<Result> GetResults(EFDbContext db, int? teamId=null, Guid? studentId=null, int? eventId=null, int? competitionYear = null, bool? isOfficial = null, int? meetingId = null)
    {
      List<Result> results = db.Results.Include("Event.Sport").Include("TeamMembership.Team.School").Include("TeamMembership.Student.Member").Include("Meeting").ToList();
      if (teamId.HasValue) { results = results.Where(x => x.TeamId == teamId).ToList(); }
      if (studentId.HasValue) { results = results.Where(x => x.MemberId == studentId).ToList(); }
      if (eventId.HasValue) { results = results.Where(x => x.EventId == eventId).ToList(); }
      if (competitionYear != null) { results = results.Where(x => x.Meeting.Date.Year == competitionYear.Value).ToList(); }
      if (isOfficial.HasValue)
      {
        if (!isOfficial.Value) results = results.Where(x => x.Meeting.IsOfficial == false || x.Event.IsOfficial == false).ToList();
        else results = results.Where(x => x.Meeting.IsOfficial == true && x.Event.IsOfficial == true).ToList();
      }
      if (meetingId.HasValue) { results = results.Where(x => x.MeetingId == meetingId).ToList(); }
      return results;
    }
    /*
     * Get a single result by its id
     */
    public static Result GetResult(int resultId)
    {
      Result r;
      using (EFDbContext db = new EFDbContext())
      {
        r = GetResult(db, resultId);
      }

      return r;
    }

    public static Result GetResult(EFDbContext db, int resultId)
    {
      return db.Results.Include("Event.Points").Single(x => x.ResultId == resultId);
    }
    /*
     * Get the result with the best score from a list of results with the same event
     */
    public static Result GetResultWithBestScoreWithinEvent(List<Result> results)
    {
      if (results.Count == 0) return null;
      results = OrderListResultsByBestScoreWithinEvent(results);
      return results.First();
    }
    public static List<Result> OrderListResultsByBestScoreWithinEvent(List<Result> results)
    {
      if (results.First().Event.Directionality == Event.EDirectionality.ASC)
      {
        results = results.OrderByDescending(x => x.ResultScore).ToList();
      }
      else
      {
        results = results.OrderBy(x => x.ResultScore).ToList();
      }
      return results;
    }
    public static List<Result> GetMyResults(Guid studentId, bool? official, int? sportId, int? teamId, int? meetingId, int? pEventId)
    {
      List<Result> results = null;
      using (EFDbContext db = new EFDbContext())
      {
        results = GetMyResults(db, studentId, official, sportId, teamId, meetingId, pEventId);
      }
      return results;
    }
    public static List<Result> GetMyResults(EFDbContext db, Guid studentId, bool? official, int? sportId, int? teamId, int? meetingId, int? pEventId)
    {
      List<Result> results = db.Results.Include("Meeting").Include("Event.Sport").Where(r=> r.MemberId == studentId).ToList();
      if (official != null){ results = results.Where(r => r.Meeting.IsOfficial == official).ToList();}
      if (sportId != null) { results = results.Where(r => r.Event.SportId == sportId.Value).ToList(); }
      if (teamId != null) { results = results.Where(r => r.TeamId == teamId.Value).ToList(); }
      if (meetingId != null) { results = results.Where(r => r.MeetingId == meetingId.Value).ToList(); }
      if (pEventId != null) { results = results.Where(r => r.EventId == pEventId.Value).ToList(); }
      
      return results;
    }
    public static List<Result> GetTeamResults(int teamId, bool? official, int? sportId, int? meetingId, int? pEventId)
    {
      List<Result> results = null;
      using (EFDbContext db = new EFDbContext())
      {
        results = GetTeamResults(db, teamId, official, sportId, meetingId, pEventId);
      }
      return results;
    }
    public static List<Result> GetTeamResults(EFDbContext db, int teamId, bool? official, int? sportId, int? meetingId, int? pEventId)
    {
      List<Result> results = db.Results.Include("Meeting").Include("Event.Sport").Where(r => r.TeamId == teamId).ToList();
      if (official != null) { results = results.Where(r => r.Meeting.IsOfficial == official).ToList(); }
      if (sportId != null) { results = results.Where(r => r.Event.SportId == sportId.Value).ToList(); }
      if (meetingId != null) { results = results.Where(r => r.MeetingId == meetingId.Value).ToList(); }
      if (pEventId != null) { results = results.Where(r => r.EventId == pEventId.Value).ToList(); }

      return results;
    }
    #endregion
    #region UPDATE
    /*
     * Update a result
     */
    public static bool UpdateResult(Result result)
    {
      bool r;
      using (EFDbContext db = new EFDbContext())
      {
        r = UpdateResult(db, result);
      }

      return r;
    }

    public static bool UpdateResult(EFDbContext db, Result result)
    {
      db.Entry(result).State = EntityState.Modified;

      bool b = (db.SaveChanges() > 0);
      RunRankingCalculations(db, new List<Result> { result }, result.Meeting);
      return b;
    }
    #endregion

    /*
     * Run the calculations for rankings, after add or update
     */
    private static void RunRankingCalculations(EFDbContext db, List<Result> results, Meeting meeting)
    {
      results.ForEach(x => RankingService.CalculateBestStudentResult(db, x,meeting));
      if (meeting.IsOfficial)
      {
        List<int> teamIds = results.Select(x => x.TeamId).Distinct().ToList();
        teamIds.ForEach(x => RankingService.AddTeamResult(db, meeting, TeamService.GetTeam(db, x)));
        /*List<Team> teams = new List<Team>();
        foreach (Result r in results)
        {

          if (!teams.Any(x => x.TeamId == r.TeamId))
          {
            teams.Add(r.TeamMembership.Team);
          }
        }

        foreach (Team t in teams)
        {
          RankingService.AddTeamResult(db, meeting, t);
        }*/
      }
      //CalculateBestStudentResult(student, event, year, officiality)
      //  Gets the event with the best resultscore within these parameters
      //If official (which would mean that all the results are added in one go)
      //  addTeamResult(meeting, team): get the best result per event, calculate its points, and add those together
      //    setBestTeamResult(team): get the teamResult with the highest points
    }
    public static List<ResultPerDate> GetResultsPerDate(List<List<Result>>lr)
    {
      
      List<ResultPerDate> rpd = new List<ResultPerDate>();
      HashSet<DateTime> dates = new HashSet<DateTime>();

      foreach (List<Result> results in lr)
      {
        foreach (Result r in results)
        {
          dates.Add(r.Meeting.Date);
        }
      } 
      foreach (DateTime date in dates)
      {
        List<double> resultScores = new List<double>();
        foreach (List<Result> results in lr)
        {
          try{
            double result = results.Single(r => r.Meeting.Date == date).ResultScore;
            resultScores.Add(result);
          }
          catch( InvalidOperationException e){
             resultScores.Add(0);
             continue;
          }
          
        }


        rpd.Add(new ResultPerDate
        {
          Date = date,
          Results = resultScores
        });
      }
      
      return rpd;
    }
  }
}