﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.DAL;

namespace TeamOPSchoolCup.Domain.Services
{
  public class ComparisonService
  {
    public static void GenerateJson(StudentComparison sc, StringBuilder builder, JavaScriptSerializer serializer)
    {
      builder.Append("[");

      foreach (StudentComparison.ComparisonLine cl in sc.ComparisonLines)
      {
        builder.Append(serializer.Serialize(new
        {
          name1 = sc.Students[0].Name,
          result1 = cl.ResultRank[0].Result.ResultScore,
          rank1 = cl.ResultRank[0].Rank,
          eventName = cl.EventName,
          rank2 = cl.ResultRank[1].Rank,
          result2 = cl.ResultRank[1].Result.ResultScore,
          name2 = sc.Students[1].Name
        }));
        builder.Append(",");
      }
      if (builder.Length > 0) builder.Length--;
      if (builder.Length > 0) builder.Append("]");
    }

    public static void GenerateJson(TeamComparison comparison, StringBuilder builder, JavaScriptSerializer serializer)
    {
      builder.Append("[");
      foreach (TeamComparison.ComparisonLine line in comparison.ComparisonLines)
      {
        builder.Append(serializer.Serialize(new
        {
          name1 = comparison.Team1.Name,
          result1 = line.ResultsT1[0].ResultScore,
          rank1 = line.RankT1,
          eventName = line.EventName,
          rank2 = line.RankT2,
          result2 = line.ResultsT2[0].ResultScore,
          name2 = comparison.Team2.Name
        }));
        builder.Append(",");
      }
      if (comparison.ComparisonLines.Count > 0)
      {
        builder.Append(serializer.Serialize(new
        {
          name1 = comparison.Team1.Name,
          result1 = comparison.Team1.Points,
          rank1 = comparison.Team1.Rank,
          eventName = "Overall points",
          rank2 = comparison.Team2.Rank,
          result2 = comparison.Team2.Points,
          name2 = comparison.Team2.Name
        }));
      }
      if (builder.Length > 0 && comparison.ComparisonLines.Count == 0) builder.Length--;
      if (builder.Length > 0) builder.Append("]");
    }

    public static StudentComparison GetStudentComparison(List<Student> students, int? year = null)
    {
      StudentComparison sc = new StudentComparison();

      List<Event> commonEvents = new List<Event>();
      List<Student> studs = students.OrderBy(s => s.MemberId).ToList();
      for (int i = 0; i < studs.Count; i++)
      {
        if (i == 0)
        {
          commonEvents = EventService.GetEvents(studentId: students[i].MemberId,year: year);
        }
        else
        {
          commonEvents = EventService.GetCommonEvents(studentId1:students[i].MemberId,events: commonEvents, year:year );
        }
        sc.Students.Add(new StudentComparison.RelevantStudentInfo { StudentId = students[i].MemberId, Name = students[i].Member.Name });
        
      }                

      foreach (Event pEvent in commonEvents)
      {
        StudentComparison.ComparisonLine cl = new StudentComparison.ComparisonLine();
        cl.EventId = pEvent.EventId;
        cl.EventName = pEvent.Name;
        List<List<Result>> results = new List<List<Result>>();
        foreach (Student stud in studs)
        {
          BestStudentResult bsr = RankingService.GetBestStudentResult(stud, pEvent, pEvent.IsOfficial, year);
          StudentComparison.ResultRankInfo.ResultInfo result;
          result = new StudentComparison.ResultRankInfo.ResultInfo()
          {
            ResultScore = bsr.Result.ResultScore,
            DateAcquired = bsr.Result.Meeting.Date
          };
         
          StudentRanking rank = RankingService.GetRank(stud, pEvent);
          int? acRank = null;
          if (rank != null) acRank = rank.CurrentWorldRanking;
          cl.ResultRank.Add(new StudentComparison.ResultRankInfo{ Rank = acRank,Result = result});
          results.Add(ResultService.GetResults(student: stud, pEvent: pEvent, competitionYear: year, isOfficial: pEvent.IsOfficial));
        
        }
         foreach (ResultPerDate rpd in ResultService.GetResultsPerDate(results))
          {
            cl.ResultPerDate.Add(rpd);
          }
        cl.ResultPerDate = cl.ResultPerDate.OrderBy(r => r.Date).ToList();
        sc.ComparisonLines.Add(cl);
      }

      return sc;
    }

    public static TeamComparison GetTeamComparison(Team team1, Team team2)
    {
      TeamComparison teamComparison = null;
      using (EFDbContext db = new EFDbContext()) { teamComparison = GetTeamComparison(db, team1, team2); }
      return teamComparison;
    }

    public static TeamComparison GetTeamComparison(EFDbContext db,Team team1, Team team2)
    {
      TeamComparison teamComparison = new TeamComparison();
      int year=DateTime.Today.Year; //Always the current year

      TeamRanking trT1 = RankingService.GetRank(db,team1);
      TeamRanking trT2 = RankingService.GetRank(db,team2);
      int? acTrT1 = (trT1 == null ? null : (int?)trT1.CurrentWorldRanking);
      int? acTrT2 = (trT2 == null ? null : (int?)trT2.CurrentWorldRanking);
      double pointsT1 = RankingService.GetBestTeamResult(db,team1, year).TeamResult.PointValue;
      double pointsT2 = RankingService.GetBestTeamResult(db,team2, year).TeamResult.PointValue;
      teamComparison.Team1 = new TeamComparison.RelevantTeamInfo { TeamId = team1.TeamId, Name = team1.Name, Points = pointsT1, Rank = acTrT1 };
      teamComparison.Team2 = new TeamComparison.RelevantTeamInfo { TeamId = team2.TeamId, Name = team2.Name, Points = pointsT2, Rank = acTrT2 };

      List<Event> commonEvents = EventService.GetCommonEventsOfTeam(db,team1, team2, year);

      foreach (Event cEvent in commonEvents)
      {
        TeamComparison.ComparisonLine cl = new TeamComparison.ComparisonLine();
        cl.EventId = cEvent.EventId;
        cl.EventName = cEvent.Name;
        List<Result> resultsT1 = ResultService.OrderListResultsByBestScoreWithinEvent(ResultService.GetResults(team: team1, pEvent: cEvent, competitionYear: year));
        List<Result> resultsT2 = ResultService.OrderListResultsByBestScoreWithinEvent(ResultService.GetResults(team: team2, pEvent: cEvent, competitionYear: year));
        resultsT1.ForEach(x => cl.ResultsT1.Add(new TeamComparison.ResultInfo { ResultScore = x.ResultScore, DateAcquired = x.Meeting.Date }));
        resultsT2.ForEach(x => cl.ResultsT2.Add(new TeamComparison.ResultInfo { ResultScore = x.ResultScore, DateAcquired = x.Meeting.Date }));
        TeamRanking rankT1 = RankingService.GetRank(db,team1, cEvent);
        TeamRanking rankT2 = RankingService.GetRank(db,team2, cEvent);
        cl.RankT1 = (rankT1 == null ? null : (int?)rankT1.CurrentWorldRanking);
        cl.RankT2 = (rankT2 == null ? null : (int?)rankT2.CurrentWorldRanking);
        teamComparison.ComparisonLines.Add(cl);
      }

      return teamComparison;
    }
  }
}
