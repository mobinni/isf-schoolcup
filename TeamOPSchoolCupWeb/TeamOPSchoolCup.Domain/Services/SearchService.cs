﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Data.Objects;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.Services
{
  public class SearchService
  {
  /*  public static List<object> Search(string searchquery)
    {
      List<object> objects = new List<object>() ;
      using (EFDbContext db = new EFDbContext()) 
      {
        objects = Search(db, searchquery.ToLower());
      }
      return objects;
    }
    public static List<object> Search(EFDbContext db,string searchquery)
    {
      List<object> objects = new List<object>();

      objects.AddRange(SearchStudents(db, searchquery));
      objects.AddRange(SearchTeams(db, searchquery));
      objects.AddRange(SearchMeetings(db, searchquery));
      objects.AddRange(SearchCoaches(db, searchquery));
      return objects;
    } */
    public static List<Student> SearchStudents(EFDbContext db, string searchquery, short? gender_value = null)
    {
      List<Student> students = MemberService.GetStudents(db).Where(s => s.Member.Name.ToLower().Contains(searchquery.ToLower())).ToList();
      if (gender_value != null) { students = students.Where(s=> s.Member.Gender_Value == gender_value.Value).ToList();}
      return students;
    }

    public static List<Student> SearchStudents(string searchquery, short? gender_value = null)
    {
        using (EFDbContext db = new EFDbContext())
        {
          return SearchStudents(db, searchquery, gender_value);
        }
    }
    public static List<Team> SearchTeams(EFDbContext db, string searchquery)
    {
      return TeamService.GetTeams(db).Where(t => t.Name.ToLower().Contains(searchquery.ToLower())).ToList();
    }
    public static List<Team> SearchTeams(string searchquery)
    {
        using (EFDbContext db = new EFDbContext())
        {
            return SearchTeams(db, searchquery);
        }
    }

    public static List<Meeting> SearchMeetings(EFDbContext db, string searchquery)
    {
      return MeetingService.GetMeetings(db).Where(m => m.Name.ToLower().Contains(searchquery.ToLower())).ToList();
    }
    public static List<Meeting> SearchMeetings(string searchquery)
    {
        using (EFDbContext db = new EFDbContext())
        {
            return SearchMeetings(db, searchquery);
        }
    }
    public static List<Coach> SearchCoaches(EFDbContext db, string searchquery)
    {
      return MemberService.GetCoaches(db).Where(c => c.Member.Name.ToLower().Contains(searchquery.ToLower())).ToList();
    }

    public static List<Coach> SearchCoaches(string searchquery)
    {
        using (EFDbContext db = new EFDbContext())
        {
            return SearchCoaches(db, searchquery);
        }
    }
    public static Type GetType(object obj)
    {
      return ObjectContext.GetObjectType(obj.GetType());
    }
  }
}
