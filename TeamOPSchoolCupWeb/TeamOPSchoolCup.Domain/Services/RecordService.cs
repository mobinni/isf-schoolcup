﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using System.Data;
using TeamOPSchoolCup.Domain.DTO;

namespace TeamOPSchoolCup.Domain.Services
{
  public class RecordService
  {
    #region ADD
    public static bool AddRecord(Record record)
    {
      bool b;
      using (EFDbContext db = new EFDbContext())
      {
        b = AddRecord(db, record);
      }
      return b;
    }
    public static bool AddRecord(EFDbContext db, Record record)
    {
      db.Records.Add(record);
      return (db.SaveChanges() > 0);
    }
    public static bool AddRecord(CreatableRecord record)
    {
      bool b;
      using (EFDbContext db = new EFDbContext())
      {
        b = AddRecord(db, record);
      }
      return b;
    }
    public static bool AddRecord(EFDbContext db, CreatableRecord record)
    {
      RecordHolder recHolder = new RecordHolder
      {
       
        Gender_Value = record.Gender_Value,
        Name = record.Name
      };
     AddRecordHolder(db, recHolder);
      Record rec = new Record
      {
        Age = record.Age,
        Meeting = record.Meeting,
        Event = EventService.GetEvent(db,record.EventId),
        Sport = SportService.GetSport(db,record.SportId),
        RecordHolder = GetRecordHolder(db,recHolder.Name),
        Result = record.Result,
        RecordDate = record.RecordDate
      };
      db.Records.Add(rec);
     
      
      return (db.SaveChanges()> 0);
    }
    public static bool AddRecordHolder(RecordHolder recordHolder)
    {
      bool b;
      using (EFDbContext db = new EFDbContext())
      {
        b = AddRecordHolder(db, recordHolder);
      }
      return b;
    }
    public static bool AddRecordHolder(EFDbContext db, RecordHolder recordHolder)
    {
      if (db.RecordHolders.SingleOrDefault(r => r.Name.ToLower().Equals(recordHolder.Name.ToLower())) != null)
      {
        return true;
      }
      db.RecordHolders.Add(recordHolder);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    public static Record GetRecord(int id)
    {
      Record record;
      using (EFDbContext db = new EFDbContext())
      {
        record = GetRecord(db, id);
      }
      return record;
    }
    public static Record GetRecord(EFDbContext db, int id)
    {

      return db.Records.Include("Sport").Include("Event").Include("RecordHolder").Single(r => r.RecordId == id);
    }
    public static List<Record> GetRecords(int? id = null, int? pEventId = null, DateTime? recordDate = null, int? sportId = null, int? recordHolderId = null, short? gender_value = null)
    {

      List<Record> records;
      using (EFDbContext db = new EFDbContext())
      {
        records = GetRecords(db, id, pEventId, recordDate, sportId, recordHolderId,gender_value);
      }
      return records;
    }
    public static List<Record> GetRecords(EFDbContext db, int? id = null, int? pEventId = null, DateTime? recordDate = null, int? sportId = null, int? recordHolderId = null, short? gender_value = null)
    {
      List<Record> records = db.Records.Include("RecordHolder").Include("Sport").Include("Event").ToList<Record>();
      if (id != null) records = records.Where(r => r.RecordId == id).ToList<Record>();
      if (pEventId.HasValue) records = records.Where(r => r.Event.EventId == pEventId.Value).ToList<Record>();
      if (recordDate != null) records = records.Where(r => r.RecordDate == recordDate).ToList<Record>();
      if (sportId.HasValue) records = records.Where(r => r.Sport.SportId == sportId.Value).ToList<Record>();
      if (recordHolderId != null) records = records.Where(r => r.RecordHolder.RecordHolderId == recordHolderId).ToList();
      if (gender_value != null)
        if (gender_value == 0) { records = records.Where(e => e.RecordHolder.Gender_Value == gender_value).ToList(); }
        else { records = records.Where(e => e.RecordHolder.Gender_Value == gender_value || e.RecordHolder.Gender_Value == 0).ToList(); }
      return records;
    }
    public static RecordHolder GetRecordHolder(string name)
    {
      RecordHolder recordHolder;
      using (EFDbContext db = new EFDbContext())
      {
        recordHolder = GetRecordHolder(db, name);
      }
      return recordHolder;
    }
    public static RecordHolder GetRecordHolder(EFDbContext db, string name)
    {

      return db.RecordHolders.Include("Records").Single(r => r.Name.ToLower().Equals(name.ToLower()));
    }
    public static List<RecordHolder> GetRecordHolders()
    {
      List<RecordHolder> recordHolders = null;
      using (EFDbContext db = new EFDbContext())
      {
        recordHolders = GetRecordHolders(db);
      }
      return recordHolders;
    }
    public static List<RecordHolder> GetRecordHolders(EFDbContext db)
    {
      HashSet<RecordHolder> recordHolders = new HashSet<RecordHolder>();

      foreach(Record record in GetRecords(db)){
        recordHolders.Add(record.RecordHolder);
      }
      return recordHolders.ToList() ;
    }
    public static List<RecordHolder> GetRecordHoldersOfEvent(int? eventId = null, short? gender_value = null)
    {
      List<RecordHolder> recordHolders = null;
      using (EFDbContext db = new EFDbContext())
      {
        recordHolders = GetRecordHoldersOfEvent(db, eventId, gender_value);
      }
      return recordHolders;
    }
    public static List<RecordHolder> GetRecordHoldersOfEvent(EFDbContext db, int? eventId = null, short? gender_value = null)
    {
      HashSet<RecordHolder> recordHolders = new HashSet<RecordHolder>();
      Event pEvent = null;
      if (eventId != null)
      {
        pEvent = EventService.GetEvent(eventId.Value);
        foreach (Record record in GetRecords(db, pEventId: pEvent.EventId))
        {
          if (gender_value != null)
          {
            if (record.RecordHolder.Gender_Value == gender_value) { recordHolders.Add(record.RecordHolder); }
          }


        }
        return recordHolders.ToList() ;
      }
      else
      {
        if (gender_value != null)
        {
          return db.RecordHolders.Include("Records").Where(r=>r.Gender_Value == gender_value).ToList();
        }
        return db.RecordHolders.Include("Records").ToList();
      }
      
    }
    public static List<RecordHolder> GetRecordHoldersOfSport(int? sportId = null, short? gender_value = null)
    {
      List<RecordHolder> recordHolders = null;
      using (EFDbContext db = new EFDbContext())
      {
        recordHolders = GetRecordHoldersOfSport(db, sportId, gender_value);
      }
      return recordHolders;
    }
    public static List<RecordHolder> GetRecordHoldersOfSport(EFDbContext db, int? sportId = null, short? gender_value = null)
    {
      List<RecordHolder> recordHolders = db.RecordHolders.Include("Records").ToList();
      
      if (sportId != null)
      {
        recordHolders = recordHolders.Where(r => r.Records.Select(x => x.SportId).First() == sportId.Value).ToList();
      }
      if (gender_value != null)
      {
       recordHolders = recordHolders.Where(r => r.Gender_Value == gender_value).ToList();
      }

      return recordHolders;

    }
    #endregion
    #region UPDATE
    public static bool UpdateRecord(Record record)
    {
      bool r;
      using (EFDbContext db = new EFDbContext())
      {
        r = UpdateRecord(db, record);
      }

      return r;
    }

    public static bool UpdateRecord(EFDbContext db, Record record)
    {
      db.Entry(record).State = EntityState.Modified;

      return (db.SaveChanges() > 0);
    }
    #endregion
  }
}
