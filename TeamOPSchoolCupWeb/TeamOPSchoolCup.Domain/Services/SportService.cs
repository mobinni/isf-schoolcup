﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Domain.Services
{
  public class SportService
  {
    /*
     * Converts Sport objects into JSON strings
     */
    public static void GenerateJson(List<Sport> sports, StringBuilder builder, JavaScriptSerializer serializer)
    {
      builder.Append("[");
      foreach (Sport sport in sports)
      {
        builder.Append(serializer.Serialize(new { 
          id = sport.SportId, 
          name = sport.Name, 
          abbreviation = sport.Abbreviation 
        }));
        builder.Append(",");
      }
      if (builder.Length > 0) builder.Length--;
      if (builder.Length > 0) builder.Append("]");
    }

    #region ADD
    /*
     * Add a sport
     */
    public static bool AddSport(Sport sport)
    {
      bool s;
      using(EFDbContext db = new EFDbContext()){
        s = AddSport(db, sport);
      }

      return s;
    }
    public static bool AddSport(EFDbContext db, Sport sport)
    {
      db.Sports.Add(sport);

      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    /*
     * Get a list of sports
     * @param sort: should the list be sorted alphabetically by name
     */
    public static List<Sport> GetSports(bool sort)
    {
      List<Sport> sports;
      using (EFDbContext db = new EFDbContext())
      {
        sports = GetSports(db, sort);
      }

      return sports;
    }
    public static List<Sport> GetSports(EFDbContext db, bool sort)
    {
      List<Sport> sports = db.Sports.ToList<Sport>();
      if(sort) sports.Sort();

      return sports;
    }
    /*
     * Get sports played by a member. Separate method as it's quite big on dependencies....
     */
    public static List<Sport> GetSports(EFDbContext db, Member member)
    {
      member = db.Members.Include("ISFAdmin").Include("NSSFRep").Include("SCC").Include("Coach.TeamCoaches.Team.Sport").Include("Student.TeamMemberships.Team.Sport").Single(x => x.MemberId == member.MemberId);
      List<Sport> sports = GetSports(db, false);
      if (member == null || member.ISFAdmin != null || member.NSSFRep != null || member.SCC != null) { return sports; }
      if (member.Coach != null) { return sports.Where(x => member.Coach.TeamCoaches.Any(y => y.Team.SportId == x.SportId)).ToList(); }
      if (member.Student != null) { return sports.Where(x => member.Student.TeamMemberShips.Any(y => y.Team.SportId == x.SportId)).ToList(); }
      return null;
    }

    /*
     * Get a single sport by id or abbr
     */
    public static Sport GetSport(int? sportId=null, string abbr=null)
    {
      Sport sport;
      using (EFDbContext db = new EFDbContext()) { sport = GetSport(db, sportId,abbr); }
      return sport;
    }

    public static Sport GetSport(EFDbContext db, int? sportId=null,string abbr=null)
    {
      List<Sport> sports = db.Sports.Include("Events").ToList();
      if (sportId.HasValue) return sports.SingleOrDefault(x => x.SportId == sportId.Value);
      if (abbr != null) return sports.SingleOrDefault(x => x.Abbreviation.Equals(abbr));
      return null;
    }
    #endregion
    #region UPDATE
    public static bool UpdateSport(Sport sport)
    {
      bool s;
      using (EFDbContext db = new EFDbContext())
      {
        s = UpdateSport(db, sport);
      }

      return s;
    }
    public static bool UpdateSport(EFDbContext db, Sport sport)
    {
      db.Entry(sport).State = EntityState.Modified;

      return (db.SaveChanges() > 0);
    }
    #endregion
  }
}