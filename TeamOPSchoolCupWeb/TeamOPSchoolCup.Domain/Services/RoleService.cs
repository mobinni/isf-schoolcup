﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Domain.Services
{
  public class RoleService
  {
    public static bool AddRole(String roleName)
    {
      using(EFDbContext db = new EFDbContext()){
        return AddRole(db, roleName);
      }
    }

    public static bool AddRole(EFDbContext db, String roleName)
    {
      if (RoleExists(db, roleName)) throw new TeamOPSchoolCupException(String.Format(ErrorStrings.RoleAlreadyExists, roleName));

      Role role = new Role() { RoleId = Guid.NewGuid(), RoleName = roleName};
      db.Roles.Add(role);

      return (db.SaveChanges() > 0);
    }

    public static bool AddUserToRole(String username, string roleName)
    {
      using(EFDbContext db = new EFDbContext()){
        return AddUserToRole(db, username, roleName);
      }
    }

    public static bool AddUserToRole(EFDbContext db, String username, String roleName)
    {
      Member member = MemberService.GetMember(db, email: username);
      Role role = GetRole(db, roleName);
      if (role == null) AddRole(db, roleName);
      if(member == null) throw new TeamOPSchoolCupException(ErrorStrings.MemberDoesntExistException);
      if (IsUserInRole(db, username, roleName)) throw new TeamOPSchoolCupException(String.Format(ErrorStrings.MemberAlreadyInRole, roleName));

      member.Roles.Add(role);

      return (db.SaveChanges() > 0);
    }

    public static bool RemoveUserFromRole(String username, String roleName)
    {
      using(EFDbContext db = new EFDbContext()){
        return RemoveUserFromRole(db, username, roleName);
      }
    }

    public static bool RemoveUserFromRole(EFDbContext db, String username, String roleName)
    {
      Member member = MemberService.GetMember(db, email: username);
      Role role = GetRole(db, roleName);
      if (member == null) throw new TeamOPSchoolCupException(ErrorStrings.MemberDoesntExistException);
      if (role == null) throw new TeamOPSchoolCupException(String.Format(ErrorStrings.RoleNotExists, roleName));

      if(!member.Roles.Remove(role)) return false;

      return (db.SaveChanges() > 0);
    }

    public static Role GetRole(String roleName)
    {
      using(EFDbContext db = new EFDbContext()){
        return GetRole(db, roleName);
      }
    }

    public static Role GetRole(EFDbContext db, String roleName)
    {
      return db.Roles.FirstOrDefault<Role>(r => r.RoleName == roleName);
    }

    public static List<Role> GetRoles()
    {
      using (EFDbContext db = new EFDbContext())
      {
        return GetRoles(db);
      }
    }

    public static List<Role> GetRoles(EFDbContext db)
    {
      return db.Roles.ToList<Role>();
    }

    public static List<Role> GetUserRoles(String username)
    {
      using(EFDbContext db = new EFDbContext()){
        return GetUserRoles(db, username);
      }
    }

    public static List<Role> GetUserRoles(EFDbContext db, String username)
    {
      Member member = MemberService.GetMember(db, email: username);
      if (member == null) throw new TeamOPSchoolCupException(ErrorStrings.MemberDoesntExistException);

      return member.Roles.ToList<Role>();
    }

    public static bool RoleExists(String roleName)
    {
      using(EFDbContext db = new EFDbContext()){
        return RoleExists(db, roleName);  
      }
    }

    public static bool RoleExists(EFDbContext db, String roleName)
    {
      return (GetRole(db, roleName) != null);
    }

    public static bool IsUserInRole(String username, String roleName)
    {
      using(EFDbContext db = new EFDbContext()){
        return IsUserInRole(db, username, roleName);
      }
    }

    public static bool IsUserInRole(EFDbContext db, String username, String roleName)
    {
      Member member = MemberService.GetMember(db, email: username);
      Role role = GetRole(db, roleName);
      if (member == null) throw new TeamOPSchoolCupException(ErrorStrings.MemberDoesntExistException);
      if(role == null) return false;

      return member.Roles.Contains(role);
    }
  }
}
