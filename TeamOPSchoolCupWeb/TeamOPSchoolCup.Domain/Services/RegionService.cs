﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Collections;
using System.Data;
using System.Web.Script.Serialization;

namespace TeamOPSchoolCup.Domain.Services
{
  public class RegionService
  {
    private static int level = -1;
    /*
     * Recursively converts Region objects into JSON strings
     */
    public static void GenerateJson(Region region, StringBuilder builder, JavaScriptSerializer serializer)
    {
      level++;
      builder.Append(serializer.Serialize(new { 
        id = region.RegionId, 
        level = level, 
        name = region.Name 
      }));

      foreach (Region r in RegionService.GetChildren(region, true))
      {
        builder.Append(",");
        GenerateJson(r, builder, serializer);
      }
      level--;
    }
    #region ADD
    /*
     * This will add a Region to the database by giving a Region as parameter
     */
    public static bool AddRegion(Region region)
    {
      bool r;
      using (EFDbContext db = new EFDbContext())
      {
        r = AddRegion(db, region);
      }

      return r;
    }

    public static bool AddRegion(EFDbContext db, Region region)
    {
      db.Regions.Add(region);

      return (db.SaveChanges() > 0);
    }
    /*
     * This will add a Region to the database by giving it's name and the name of the parent as parameter
     */
    public static bool AddRegion(string regionName, string parentName)
    {
      bool r;
      using (EFDbContext db = new EFDbContext())
      {
        r = AddRegion(db, regionName, parentName);
      }

      return r;
    }

    public static bool AddRegion(EFDbContext db, string regionName, string parentName)
    {
      db.Regions.Add(new Region { Name = regionName, Parent = db.Regions.Where(r => r.Name == parentName).First() });
      return db.SaveChanges() > 0 ? true : false;
    }

    #endregion
    #region GET
    
    /*
     * This will return the Region that doesn't have a parent
     */
    public static Region GetWorld()
    {
      Region root;
      using(EFDbContext db = new EFDbContext()){
        root = GetWorld(db);
      }

      return root;
    }
    
    public static Region GetWorld(EFDbContext db) 
    {
      return db.Regions.Include("Children").Single(r => r.ParentId == null);
    }

    /*
     * This will give the regions and not the full region table
     */
    public static List<Region> GetRegions()
    {
      List<Region> regions;
      using (EFDbContext db = new EFDbContext())
      {
        regions = GetRegions(db);
      }

      return regions;
    }

    
    public static List<Region> GetRegions(EFDbContext db)
    {
      return db.Regions.Where(r => r.Parent.Parent.Parent.ParentId == null).ToList();
    }

   
    /*
     * This will return a list of all the Regions that are in the database
     */
    public static List<Region> GetAllRegions()
    {
      List<Region> regions;
      using (EFDbContext db = new EFDbContext())
      {
        regions = GetAllRegions(db);
      }

      return SortRegions(regions);
    }
   
    public static List<Region> GetAllRegions(EFDbContext db)
    {
      return db.Regions.ToList();
    }

    public static List<Region> GetStructuredRegions() {
        List<Region> regions = new List<Region>();
        Region world = RegionService.GetWorld();
        regions.Add(world);
        foreach (Region continent in RegionService.GetChildren(world, true))
        {
            regions.Add(continent);
            foreach (Region country in RegionService.GetChildren(continent, true))
            {
                regions.Add(country);
                foreach (Region region in RegionService.GetChildren(country, true))
                {
                    regions.Add(region);
                }
            }
        }
        return regions;
    }

    /*
     * This will return a list of the Children of a specific Region
     * true = Alphabetically sorted list
     */
    public static List<Region> GetChildren(Region region, bool sort,bool recursive=false)
    {
      List<Region> children;
      using (EFDbContext db = new EFDbContext())
      {
        children = GetChildren(db, region, sort,recursive);
      }

      return children;
    }

    
    public static List<Region> GetChildren(EFDbContext db, Region region, bool sort, bool recursive=false)
    {
      List<Region> children = db.Regions.Where(r => r.ParentId == region.RegionId).ToList();
      if (recursive)
      {
        List<Region> additions = new List<Region>();
        foreach (Region r in children) {
          additions.AddRange(GetChildren(db,r,sort,true));
        }
        children.AddRange(additions);
      }
      if (sort) children.Sort();

      return children;
    }
    /*
     * This will return the parent of a specific Region
     */
    public static Region GetParent(Region region)
    {
      Region parent;
      using (EFDbContext db = new EFDbContext())
      {
        parent = GetParent(db, region);
      }

      return parent;
    }

    public static Region GetParent(EFDbContext db, Region region)
    {
      return db.Regions.Single(r => r.RegionId == region.ParentId);
    }

    /*
     * Gets parents all the way to root, starting at a specific region
     */
    public static List<Region> GetRegionsUp(EFDbContext db,Region region)
    {
      List<Region> regions = new List<Region>();
      region = GetRegion(db,region.RegionId);
      regions.Add(region);
      if (region.Parent != null) regions.AddRange(GetRegionsUp(db,region.Parent));
      return regions;
    }

    /*
    * This will return a specific Region by giving his name as parameter
    */
    public static Region GetRegion(string regionName)
    {
      Region r;
      using (EFDbContext db = new EFDbContext())
      {
        r = GetRegion(db, regionName);
      }
      return r;
    }

    public static Region GetRegion(EFDbContext db, string regionName)
    {
      return db.Regions.Where(x => x.Name.Equals(regionName)).FirstOrDefault();
    }
    /*
   * This will return a specific Region by giving his id as parameter
   */
    public static Region GetRegion(int regionId)
    {
      Region r;
      using (EFDbContext db = new EFDbContext())
      {
        r = GetRegion(db, regionId);
      }

      return r;
    }

    public static Region GetRegion(EFDbContext db, int regionId)
    {
      return db.Regions.Include("Parent").Single(x => x.RegionId==regionId);
    }
    #endregion
    #region UPDATE
    /*
     * This will commit the changes that has been made to a specific Region to the database
     */
    public static bool UpdateRegion(Region region)
    {
      bool r;
      using(EFDbContext db = new EFDbContext()){
        r = UpdateRegion(db, region);
      }

      return r;
    }

    public static bool UpdateRegion(EFDbContext db, Region region)
    {
      db.Entry(region).State = EntityState.Modified;

      return (db.SaveChanges() > 0);
    }
    #endregion
    /*
    * This will ASC sort the table 
    */
    public static List<Region> SortRegions(List<Region> regions)
    {
      regions.Sort();
      return regions;
    }

    /*
     * This will return the Region of a specific Member
     */
    public static Region GetRegionOfMember(Guid memberId)
    {
      Region r;
      using (EFDbContext db = new EFDbContext())
      {
        r = GetRegionOfMember(db, memberId);
      }

      return r;
    }

    public static Region GetRegionOfMember(EFDbContext db, Guid memberId)
    {
      Region startingRegion = null;
      Member member = db.Members.Include("ISFAdmin").Include("NSSFRep.NSSF.Region").Include("SCC.School.NSSF.Region").Include("Coach.TeamCoaches.Team.School.NSSF.Region").Include("Student.TeamMemberships.Team.School.NSSF.Region").Single(x => x.MemberId == memberId);
      if (member == null || member.ISFAdmin != null) { startingRegion = RegionService.GetWorld(db); }
      else if (member.NSSFRep != null) { startingRegion = member.NSSFRep.NSSF.Region; }
      else if (member.SCC != null) { startingRegion = member.SCC.School.NSSF.Region; }
      else if (member.Coach != null) { startingRegion = member.Coach.School.NSSF.Region; }
      else if (member.Student != null) { startingRegion = member.Student.TeamMemberShips.First().Team.School.NSSF.Region; }
      return startingRegion;
    }

    public static void GetChildrenAndSort(EFDbContext db, ref Region region)
    {
      foreach (Region child in region.Children)
      {
        db.Entry(child).Collection("Children").Load();
        Region x = child;
        if (child.Children.Count > 0) GetChildrenAndSort(db,ref x);
      }
      region.Children.Sort((a, b) => a.Name.CompareTo(b.Name));
    }

    public static Region GetFullTree()
    {
      Region world = null;
      using (EFDbContext db = new EFDbContext())
      {
        world = RegionService.GetWorld(db);
        GetChildrenAndSort(db,ref world);
      }
      return world;
    }

  }
}
