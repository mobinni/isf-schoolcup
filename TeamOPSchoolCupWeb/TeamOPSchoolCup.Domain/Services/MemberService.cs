﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.POCO;
using System.Web.Security;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Objects;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.Exceptions;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.Resources;
using TeamOPSchoolCup.Domain.DAL;
namespace TeamOPSchoolCup.Domain.Services
{
  public class MemberService
  {
    #region STUDENT
    public static void GenerateJson(Student student, Team team, StringBuilder builder, JavaScriptSerializer serializer)
    {
      builder.Append(serializer.Serialize(new
      {
        id = student.MemberId,
        name = student.Member.Name,
        teamId = team.TeamId
      }));
      builder.Append(",");
    }
    #region ADD
    /*
     * This will add a Student to the database
     */
    public static bool AddStudent(Student student)
    {
      bool s;
      using (EFDbContext db = new EFDbContext())
      {
        s = AddStudent(db, student);
      }

      return s;
    }

    public static bool AddStudent(EFDbContext db, Student student)
    {
      db.Members.Add(student.Member);
      db.SaveChanges();
      student.MemberId = student.Member.MemberId;
      db.Students.Add(student);
      return (db.SaveChanges() > 0);
    }

    /*
     * Adds a student/member with data from a BasicStudentInfo
     * Uses AddStudent(db,student) internally
     */
    public static bool AddStudent(BasicMemberInfo bsi)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b=AddStudent(db,bsi); }
      return b;
    }

    public static bool AddStudent(EFDbContext db, BasicMemberInfo bsi)
    {
      Guid guid = Guid.NewGuid();

      Student s = new Student
      {
        Member = new Member
        {
          MemberId = guid,
          BirthDate = bsi.BirthDay.Value,
          Name = bsi.Name,
          Email = bsi.Email,
          Gender = bsi.Gender,
          Password =bsi.Password
        },
        MemberId = guid
      };
      
      return AddStudent(db, s); 
    }
    public static bool AddStudentToTeam(Guid studentId, int teamId)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()){
        b = AddStudentToTeam(db,studentId,teamId);
      }
      return b;
    }
    public static bool AddStudentToTeam(EFDbContext db, Guid studentId, int teamId)
    {
      Team t = TeamService.GetTeam(db, teamId);
      Student student = MemberService.GetStudent(studentId);
      int age = MemberService.GetAgeAtBeginningOfYear(student.Member.BirthDate.Value);
      if (!(student.Member.Gender.Equals(t.Gender) && age <= 17 && age >= 14)) { throw new TeamOPSchoolCupException(ErrorStrings.StudentDoesntFitInTeamException); }
      
      return TeamService.AddTeamMembership(db, teamId, student.MemberId);
      
      
    }
    public static bool AddCoachToTeam(Guid coachId, int teamId)
    {
      bool b;
      using (EFDbContext db = new EFDbContext())
      {
        b = AddCoachToTeam(db, coachId, teamId);
      }
      return b;
    }
    public static bool AddCoachToTeam(EFDbContext db, Guid coachId, int teamId)
    {
      Team t = TeamService.GetTeam(db, teamId);
      bool hasActiveCoach = false;
      foreach (TeamCoach tc in t.TeamCoaches)
      {
        if(!Roles.IsUserInRole(tc.Coach.Member.Email,"Inactive"))
        {
          hasActiveCoach = true;
        }
      }
      if (!hasActiveCoach)
      {
        return TeamService.AddTeamCoach(db, teamId, coachId);
      }
      return false;
     }
    #endregion
    #region GET
    /*
     * This will return a list of the Students that are in the database
     */
    public static List<Student> GetStudents()
    {
      List<Student> students;
      using (EFDbContext db = new EFDbContext())
      {
        students = GetStudents(db);
      }

      return students;
    }

    public static List<Student> GetStudents(EFDbContext db)
    {
      return db.Students.Include("Member").Include("Member.ISFAdmin").Include("Member.NSSFRep").Include("Member.SCC").Include("Member.Coach").Include("Member.Student").ToList();
    }

    /*
     * This will return a Student by giving his id as parameter
     */
    public static Student GetStudent(Guid id)
    {
      Student s;
      using (EFDbContext db = new EFDbContext())
      {
        s = GetStudent(db, id);
      }
      return s;
    }

    public static Student GetStudent(EFDbContext db, Guid id)
    {
      return db.Students.Include("Member").Include("TeamMemberships.Team").Single(x => x.Member.MemberId == id);
    }
    #endregion

    #endregion
    #region COACH
    #region ADD
    /*
     * This will add a Coach to the database
     */
    public static bool AddCoach(Coach coach)
    {
      bool c;
      using (EFDbContext db = new EFDbContext())
      {
        c = AddCoach(db, coach);
      }

      return c;
    }

    public static bool AddCoach(EFDbContext db, Coach coach)
    {
      db.Members.Add(coach.Member);
      db.SaveChanges();
      coach.MemberId = coach.Member.MemberId;
      db.Coaches.Add(coach);
      return (db.SaveChanges() > 0);
    }
    /*
     * Adds a coach/member with data from a BasicMemberInfo
     * Uses AddStudent(db,coach) internally
     */
    public static bool AddCoach(BasicMemberInfo bci, int schoolId)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = AddCoach(db, bci, schoolId); }
      return b;
    }
    public static bool AddCoach(EFDbContext db, BasicMemberInfo bci, int schoolId)
    {
      Guid guid = Guid.NewGuid();
      Coach c = new Coach
      {
        Member = new Member
        {
          MemberId = guid,
          Name = bci.Name,
          Email = bci.Email,
          Gender = bci.Gender,
          Password = bci.Password
        },
        SchoolId = schoolId
      };
      bool b = AddCoach(db, c);
      return b;
    }
    #endregion
    #region GET
    /*
     * This will return a list of the Coaches that are in the database
     */
    public static List<Coach> GetCoaches()
    {
      List<Coach> coaches;
      using (EFDbContext db = new EFDbContext())
      {
        coaches = GetCoaches(db);
      }

      return coaches;
    }

    public static List<Coach> GetCoaches(EFDbContext db)
    {
      return db.Coaches.Include("Member").Include("Member.ISFAdmin").Include("Member.NSSFRep").Include("Member.SCC").Include("Member.Coach").Include("Member.Student").Include("TeamCoaches").ToList();
    }

    /*
     * This will return a Coach by giving his id as parameter
     */
    public static Coach GetCoach(Guid id)
    {
      Coach c;
      using (EFDbContext db = new EFDbContext())
      {
        c = GetCoach(db, id);
      }
      return c;
    }

    public static Coach GetCoach(EFDbContext db, Guid id)
    {
      return db.Coaches.Include("Member").Single(x => x.Member.MemberId == id);
    }
    #endregion
    #endregion
    #region NSSFREP
    #region ADD
    /*
     * This will add a NSSFRep to the database
     */
    public static bool AddNSSFRep(NSSFRep nssfrep, int nssfId)
    {
      bool n;
      using (EFDbContext db = new EFDbContext())
      {
        n = AddNSSFRep(db, nssfrep, nssfId);
      }

      return n;
    }

    public static bool AddNSSFRep(EFDbContext db, NSSFRep nssfrep, int nssfId)
    {
      NSSF nssf = db.NSSFs.Single(n => n.NSSFId == nssfId);
      Member member = nssfrep.Member;
      db.Members.Add(member);
      db.SaveChanges();
      nssfrep.MemberId = nssfrep.Member.MemberId;
      db.NSSFReps.Add(nssfrep);
      bool b = false;
      try
      {
        b = NSSFService.AddNSSFRep(db, nssfrep, nssf);
        return b;
      }
      catch (TeamOPSchoolCupException exc)
      {

        db.NSSFReps.Remove(db.NSSFReps.Single(n => n.NSSFId == nssfrep.NSSFId));
        db.Members.Remove(db.Members.Single(m => m.MemberId == nssfrep.MemberId));
        db.SaveChanges();
        return b;
      }



    }
    #endregion
    #region GET
    /*
     * This will return a list of the NSSFReps that are in the database
     */
    public static List<NSSFRep> GetNSSFReps()
    {
      List<NSSFRep> nssfreps;
      using (EFDbContext db = new EFDbContext())
      {
        nssfreps = GetNSSFReps(db);
      }

      return nssfreps;
    }

    public static List<NSSFRep> GetNSSFReps(EFDbContext db)
    {
      return db.NSSFReps.Include("Member").Include("Member.ISFAdmin").Include("Member.NSSFRep").Include("Member.SCC").Include("Member.Coach").Include("Member.Student").ToList();
    }

    /*
     * This will return a NSSFRep by giving his id as parameter
     */
    public static NSSFRep GetNSSFRep(Guid id)
    {
      NSSFRep n;
      using (EFDbContext db = new EFDbContext())
      {
        n = GetNSSFRep(db, id);
      }
      return n;
    }
    public static NSSFRep GetNSSFRep(EFDbContext db, Guid id)
    {
      return db.NSSFReps.Include("Member").Include("Member.ISFAdmin").Include("Member.NSSFRep").Include("Member.SCC").Include("Member.Coach").Include("Member.Student").Include("NSSF.Region").Single(x => x.Member.MemberId == id);
    }


    #endregion
    #endregion
    #region SCC
    #region ADD
    /*
     * This will add a SCC to the database
     */
    public static bool AddSCC(SCC scc, int schoolId)
    {
      bool s;
      using (EFDbContext db = new EFDbContext())
      {
        s = AddSCC(db, scc, schoolId);
      }

      return s;
    }

    public static bool AddSCC(EFDbContext db, SCC scc, int schoolId)
    {

      School school = SchoolService.GetSchool(db, schoolId);
      Member member = scc.Member;
      db.Members.Add(member);
      db.SaveChanges();
      scc.MemberId = member.MemberId;
      db.SCCs.Add(scc);

      if (SchoolService.AddSCC(db, scc, school))
      {
        return (true);
      }
      else
      {
        db.SCCs.Remove(db.SCCs.Single(n => n.School.SchoolId == scc.School.SchoolId));
        db.Members.Remove(db.Members.Single(m => m.MemberId == scc.MemberId));
        db.SaveChanges();
        return false;
      }

    }
    #endregion
    #region GET
    /*
     * This will return a list of the SCCs that are in the database
     */
    public static List<SCC> GetSCCs()
    {
      List<SCC> sccs;
      using (EFDbContext db = new EFDbContext())
      {
        sccs = GetSCCs(db);
      }

      return sccs;
    }

    public static List<SCC> GetSCCs(EFDbContext db)
    {
      return db.SCCs.Include("Member").Include("Member.ISFAdmin").Include("Member.NSSFRep").Include("Member.SCC").Include("Member.Coach").Include("Member.Student").ToList();
    }

    /*
     * This will return a SCC by giving his id as parameter
     */
    public static SCC GetSCC(Guid id)
    {
      SCC c;
      using (EFDbContext db = new EFDbContext())
      {
        c = GetSCC(db, id);
      }
      return c;
    }

    public static SCC GetSCC(EFDbContext db, Guid id)
    {
      return db.SCCs.Include("Member").Single(x => x.Member.MemberId == id);
    }
    #endregion
    #endregion
    #region ISFADMIN
    #region ADD
    /*
     * This will add a ISFAdmin to the database
     */
    public static bool AddISFAdmin(ISFAdmin isfadmin)
    {
      bool i;
      using (EFDbContext db = new EFDbContext())
      {
        i = AddISFAdmin(db, isfadmin);
      }

      return i;
    }

    public static bool AddISFAdmin(EFDbContext db, ISFAdmin isfadmin)
    {
      db.Members.Add(isfadmin.Member);
      db.SaveChanges();
      isfadmin.MemberId = isfadmin.Member.MemberId;
      db.ISFAdmins.Add(isfadmin);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    /*
     * This will return a list of the ISFAdmins that are in the database
     */
    public static List<ISFAdmin> GetISFAdmins()
    {
      List<ISFAdmin> isfadmins;
      using (EFDbContext db = new EFDbContext())
      {
        isfadmins = GetISFAdmins(db);
      }

      return isfadmins;
    }

    public static List<ISFAdmin> GetISFAdmins(EFDbContext db)
    {
      return db.ISFAdmins.Include("Member").Include("Member.ISFAdmin").Include("Member.NSSFRep").Include("Member.SCC").Include("Member.Coach").Include("Member.Student").ToList();
    }

    /*
     * This will return a ISFAdmin by giving his id as parameter
     */
    public static ISFAdmin GetISFAdmin(Guid id)
    {
      ISFAdmin i;
      using (EFDbContext db = new EFDbContext())
      {
        i = GetISFAdmin(db, id);
      }
      return i;
    }

    public static ISFAdmin GetISFAdmin(EFDbContext db, Guid id)
    {
      return db.ISFAdmins.Include("Member").Single(x => x.Member.MemberId == id);
    }
    #endregion
    #endregion
    #region GET
    /*
     * This will return a Member by giving it's id as a parameter
     */
    public static Member GetMember(Guid? memberId = null, Guid? mobileAccessKey = null, string email = null)
    {
      Member m;
      using (EFDbContext db = new EFDbContext())
      {
        m = GetMember(db, memberId, mobileAccessKey, email);
      }

      return m;
    }
    public static Member GetMember(EFDbContext db, Guid? memberId = null, Guid? mobileAccessKey = null, string email = null)
    {
      List<Member> members = db.Members.Include("Roles").Include("ISFAdmin").Include("NSSFRep.NSSF.Region").Include("SCC").Include("Coach.TeamCoaches.Team").Include("Student").Include("Student.TeamMemberships.Team").Include("Student.TeamMemberships.Results.Meeting").Include("Student.TeamMemberships.Results.Event").ToList();

      if (memberId.HasValue) return members.FirstOrDefault(x => x.MemberId==memberId);
      if (mobileAccessKey.HasValue) return members.FirstOrDefault(x => x.MobileAccessKey == mobileAccessKey);
      if (email!=null) return members.FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));

      return null;
    }
    public static List<Member> GetMembers()
    {
      List<Member> m;
      using (EFDbContext db = new EFDbContext())
      {
        m = GetMembers(db);
      }

      return m;
    }
    public static List<Member> GetMembers(EFDbContext db)
    {
      return db.Members.Include("ISFAdmin").Include("NSSFRep").Include("SCC").Include("Coach").Include("Student").ToList();

    }

    #endregion
    #region UPDATE
    /*
     * This will commit the changes that has been made to a specific Member to the database
     */
    public static bool UpdateMember(Member member = null, EditableMember m = null)
    {
      bool r;
      using (EFDbContext db = new EFDbContext())
      {
        r = UpdateMember(db, member, m);
      }

      return r;
    }

    public static bool UpdateMember(EFDbContext db, Member member = null, EditableMember m = null)
    {
      if (m != null)
      {
        Member mem = MemberService.GetMember(m.MemberId);
        mem.Name = m.Name;
        mem.City = m.City;
        mem.Country = m.Country;
        mem.MemberId = m.MemberId;
        mem.PhoneNumber = m.PhoneNumber;
        mem.Street = m.Street;
        mem.Zipcode = m.Zipcode;
        mem.Language = m.Language;
        db.Entry(mem).State = EntityState.Modified;
        return (db.SaveChanges() > 0);
      }
      if (member != null)
      {
        db.Entry(member).State = EntityState.Modified;
      }
      return (db.SaveChanges() > 0);

    }
    #endregion

    public static bool ChangeEmail(Member member, string newEmail)
    {
      using (EFDbContext db = new EFDbContext())
      {
        return ChangeEmail(db, member, newEmail);
      }
    }

    public static bool ChangeEmail(EFDbContext db, Member member, string newEmail)
    {
      member.Email = newEmail;

      return (db.SaveChanges() > 0);
    }
    public static Type GetType(Member member)
    {
      return ObjectContext.GetObjectType(member.GetType());
    }

    public static bool ChangePassword(Member member, String newPassword)
    {
      using (EFDbContext db = new EFDbContext())
      {
        return ChangePassword(db, member, newPassword);
      }
    }

    public static bool ChangePassword(EFDbContext db, Member member, String newPassword)
    {
      member.Password = newPassword;
      db.Entry(member).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }

    public static bool UpdateLastLoginDate(Member member)
    {
      using (EFDbContext db = new EFDbContext())
      {
        return UpdateLastLoginDate(db, member);
      }
    }

    public static bool UpdateLastLoginDate(EFDbContext db, Member member)
    {
      member.LastLoginDate = DateTime.Now;

      return (db.SaveChanges() > 0);
    }

    public static int GetAgeAtBeginningOfYear(DateTime dt)
    {
      TimeSpan span = new DateTime(DateTime.Today.Year, 1, 1) - dt;
      DateTime age = DateTime.MinValue + span;
      return age.Year - 1; //Because minvalue is 1/1/1
    }

    public static void ConfirmMember(Member member)
    {
      using(EFDbContext db = new EFDbContext()){
        ConfirmMember(db, member);
      }
    }

    public static void ConfirmMember(EFDbContext db, Member member)
    {
      string password = RandomString(6, 12);

      member.Password = BCrypt.HashPassword(password, BCrypt.GenerateSalt(12));
      string body = String.Format("{0}{1}{2}{3}{4}", String.Format(MailStrings.NamedGreeting, member.Name), MailStrings.DefaultAutomatedWarning,
          String.Format(MailStrings.SendCredentials, member.Email, password),
          MailStrings.DefaultWrongAction, MailStrings.DefaultSignoff);
      MailService.SendMail(MailStrings.SubjectSendCredentials, new List<string>() { member.Email }, body, true);

      MemberService.UpdateMember(member: member);
      if (member.ISFAdmin != null) RoleService.AddUserToRole(db, member.Email, "ISFAdmin");
      if (member.NSSFRep != null) RoleService.AddUserToRole(db, member.Email, "NSSFRep");
      if (member.SCC != null) RoleService.AddUserToRole(db, member.Email, "SCC");
      if (member.Coach != null) RoleService.AddUserToRole(db, member.Email, "Coach");
      if (member.Student != null) RoleService.AddUserToRole(db, member.Email, "Student");
    }

    private static string RandomString(int minSize, int maxSize)
    {
      Random random = new Random((int)DateTime.Now.Ticks);
      StringBuilder builder = new StringBuilder();

      for (int i = 0; i < random.Next(minSize, maxSize + 1); i++)
      {
        char c;

        if (random.NextDouble() < 0.7)
        {
          c = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + (random.NextDouble() >= 0.5 ? 65 : 97))));
        }
        else //Add numeric char
        {
          c = Convert.ToChar(Convert.ToInt32(Math.Floor(10 * random.NextDouble() + 48)));
        }
        builder.Append(c);
      }

      return builder.ToString();
    }
  }
}
