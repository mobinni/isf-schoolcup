﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using System.Data;
using System.Web.Script.Serialization;

namespace TeamOPSchoolCup.Domain.Services
{
  public class RankingService
  {
    /*
     * Converts the first 100 BestStudentResult objects into JSON strings
     */
    public static void GenerateJson(List<BestStudentResult> bsrs, StringBuilder builder, JavaScriptSerializer serializer)
    {
      builder.Append("[");
      for (int i = 0; i < bsrs.Count && i < 100; i++)
      {
        BestStudentResult bsr = bsrs[i];
        builder.Append(serializer.Serialize(new
        {
          rank = i + 1,
          entityId = bsr.Student.Member.MemberId,
          name = bsr.Student.Member.Name,
          result = bsr.Result.ResultScore,
          region = bsr.Student.Member.Country,
          extra1 = bsr.Result.Meeting.Date.ToShortDateString(),
          extra2 = bsr.Result.Meeting.Name
        }));
        builder.Append(",");
      }
      if (builder.Length > 0) builder.Length--;
      if (builder.Length > 0) builder.Append("]");
    }

    public static void GenerateJson(List<BestTeamResult> btrs, StringBuilder builder, JavaScriptSerializer serializer)
    {
      builder.Append("[");
      for (int i = 0; i < btrs.Count && i < 100; i++)
      {
        BestTeamResult btr = btrs[i];
        builder.Append(serializer.Serialize(new
        {
          rank = i + 1,
          entityId = btr.TeamResult.Team.TeamId,
          name = btr.TeamResult.Team.Name,
          result = btr.TeamResult.PointValue,
          region = btr.TeamResult.Meeting.Location,
          extra1 = btr.TeamResult.Team.School.Name,
          extra2 = btr.TeamResult.Meeting.Name
        }));
        builder.Append(",");
      }
      if (builder.Length > 0) builder.Length--;
      if (builder.Length > 0) builder.Append("]");
    }

    #region POINTS
    #region IMPORT
    /*
     * Import points with a PointsImport DTO
     */
    public static bool ImportPoints(PointsImport pi)
    {
      bool success;
      using (EFDbContext db = new EFDbContext())
      {
        success = ImportPoints(db, pi);
      }
      return success;
    }

    public static bool ImportPoints(EFDbContext db, PointsImport pi)
    {
      foreach (PointsImport.ImportLine line in pi.Lines)
      {
        if (line.BoysMeasure != 0) db.Points.Add(new Point() { CompetitionYear = pi.CompetitionYear, EventId = pi.Event.EventId, Gender = Gender.MALE, Measure = line.BoysMeasure, PointValue = line.PointValue });
        if (line.GirlsMeasure != 0) db.Points.Add(new Point() { CompetitionYear = pi.CompetitionYear, EventId = pi.Event.EventId, Gender = Gender.FEMALE, Measure = line.GirlsMeasure, PointValue = line.PointValue });
      }
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region CALCULATE
    /*
     * Calculate the points for a given result, is available
     */
    public static int? CalculatePoints(Result result)
    {
      int? i = null;
      using (EFDbContext db = new EFDbContext()) { i = CalculatePoints(db, result); }
      return i;
    }

    public static int? CalculatePoints(EFDbContext db, Result result)
    {
      List<Point> points = db.Points.Where(x => x.EventId == result.EventId && x.CompetitionYear == result.Meeting.Date.Year && x.Gender_Value == ((short)result.TeamMembership.Student.Member.Gender)).OrderByDescending(x => x.PointValue).ToList();
      if (points == null) return null;
      Point p;
      if (result.Event.Directionality == Event.EDirectionality.ASC)
      {
        p = points.Where(x => x.Measure <= result.ResultScore).FirstOrDefault();
      }
      else
      {
        p = points.Where(x => x.Measure >= result.ResultScore).FirstOrDefault();
      }
      if (p == null) return null;
      return p.PointValue;
    }
    #endregion
    #endregion
    #region BESTSTUDENTRESULT
    #region CALCULATE
    /*
     * Calculate the relevant best student result, given a single result
     */
    public static bool CalculateBestStudentResult(Result result,Meeting meeting)
    {
      bool b = false;
      using (EFDbContext db = new EFDbContext())
      {
        b = CalculateBestStudentResult(db, result,meeting);
      }
      return b;
    }

    public static bool CalculateBestStudentResult(EFDbContext db, Result result,Meeting meeting)
    {
      List<Result> results = ResultService.GetResults(db, teamId: result.TeamId, studentId: result.MemberId, eventId: result.EventId, competitionYear: meeting.Date.Year, isOfficial: (meeting.IsOfficial && result.Event.IsOfficial));
      Result bestResult = ResultService.GetResultWithBestScoreWithinEvent(results);
      BestStudentResult bsr = null;
      if ((bsr = RankingService.GetBestStudentResult(db, result.MemberId, result.EventId, (meeting.IsOfficial && result.Event.IsOfficial),meeting.Date.Year )) != null)
      {
        if (bsr.ResultId == bestResult.ResultId) return true;
        bsr.Result = bestResult;
        db.Entry(bsr).State = EntityState.Modified;
        return (db.SaveChanges() > 0);
      }
      bsr = new BestStudentResult { MemberId=result.MemberId, EventId = result.EventId, IsOfficial = (meeting.IsOfficial && result.Event.IsOfficial), CompetitionYear = meeting.Date.Year, Result = result };
      db.BestStudentResults.Add(bsr);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    /*
     * Get the best student result from a student, event, year, isOfficial
     */
    public static BestStudentResult GetBestStudentResult(Student student, Event pEvent, bool isOfficial, int? year = null)
    {
      BestStudentResult bsr = null;
      using (EFDbContext db = new EFDbContext()) { bsr = GetBestStudentResult(db, student, pEvent, isOfficial, year); }
      return bsr;
    }

    public static BestStudentResult GetBestStudentResult(EFDbContext db, Student student, Event pEvent, bool isOfficial, int? year = null)
    {
      if (year != null) return db.BestStudentResults.Include("Result.Meeting").Where(x => x.MemberId == student.MemberId && x.Event.EventId == pEvent.EventId && x.CompetitionYear == year && x.IsOfficial == isOfficial).FirstOrDefault(); 
      return db.BestStudentResults.Include("Result.Meeting").Where(x => x.MemberId == student.MemberId && x.Event.EventId == pEvent.EventId && x.CompetitionYear == DateTime.Today.Year && x.IsOfficial == isOfficial).FirstOrDefault();
    }
    /*
     * Temp, id's instead of objects so we have to include less
     */
    public static BestStudentResult GetBestStudentResult(EFDbContext db, Guid studentId, int eventId, bool isOfficial, int? year = null)
    {
      if (year != null) return db.BestStudentResults.Include("Result.Meeting").Where(x => x.MemberId == studentId && x.Event.EventId == eventId && x.CompetitionYear == year && x.IsOfficial == isOfficial).FirstOrDefault();
      return db.BestStudentResults.Include("Result.Meeting").Where(x => x.MemberId == studentId && x.Event.EventId == eventId && x.CompetitionYear == DateTime.Today.Year && x.IsOfficial == isOfficial).FirstOrDefault();
    }

    /*
     * Get the student ranking with a number of possible parameters
     */
    public static List<BestStudentResult> GetStudentRanking(Event pEvent, int? year = null, bool? isOfficial = null, Gender? gender = null, List<Region> regions = null, ParticipantAge? participantAge = null, Team team = null)
    {
      List<BestStudentResult> bsrs = null;
      using (EFDbContext db = new EFDbContext())
      {
        bsrs = GetStudentRanking(db, pEvent, year, isOfficial, gender, regions, participantAge, team);
      }
      return bsrs;
    }

    public static List<BestStudentResult> GetStudentRanking(EFDbContext db, Event pEvent, int? year = null, bool? isOfficial = null, Gender? gender = null, List<Region> regions = null, ParticipantAge? participantAge = null, Team team = null)
    {
      List<BestStudentResult> bsrs = db.BestStudentResults.Include("Result.Meeting.RegionalMeeting").Include("Student.Member").Include("Result.Meeting.ISFMeeting").Include("Result.Meeting.CoachMeeting").Include("Event").Where(x => x.EventId == pEvent.EventId).ToList();
      if (year.HasValue) bsrs = bsrs.Where(x => x.CompetitionYear == year).ToList();
      if (isOfficial.HasValue) bsrs = bsrs.Where(x => x.IsOfficial == isOfficial).ToList();
      if (gender.HasValue) bsrs = bsrs.Where(x => x.Student.Member.Gender == gender).ToList();
      if (participantAge.HasValue) bsrs = bsrs.Where(x =>
      {
        TimeSpan span = DateTime.Today - x.Student.Member.BirthDate.Value;
        DateTime age = DateTime.MinValue + span;
        // note: MinValue is 1/1/1 so we have to subtract...
        return ((age.Year - 1) == (int)participantAge);
        /*int Years = Age.Year - 1;
        int Months = Age.Month - 1;
        int Days = Age.Day - 1;
        return (new DateTime((new DateTime(x.Result.Meeting.Date.Year, 01, 01) - x.Student.Member.BirthDate).Milliseconds).Year == (int)participantAge);*/
      }).ToList();
      if (regions != null && regions.Count != 0)
      {
        List<Region> allregions = new List<Region>();
        allregions.AddRange(regions);
        foreach (Region r in regions)
        {
          allregions.AddRange(RegionService.GetChildren(db, r, false, true));
        }
        bsrs = bsrs.Where(x =>
        {
          if (x.Result.Meeting.ISFMeeting != null) return allregions.Intersect(x.Result.Meeting.ISFMeeting.Regions).Count() > 0;
          if (x.Result.Meeting.RegionalMeeting != null) return allregions.Contains(x.Result.Meeting.RegionalMeeting.Region);
          if (x.Result.Meeting.CoachMeeting != null) return allregions.Contains(x.Result.Meeting.CoachMeeting.Team.School.NSSF.Region);
          return false;
        }).ToList();
      }
      if (team != null) { bsrs = bsrs.Where(x => x.Result.TeamMembership.TeamId == team.TeamId).ToList(); }
      if (pEvent.Directionality == Event.EDirectionality.ASC) bsrs = bsrs.OrderByDescending(x => x.Result.ResultScore).ToList();
      else bsrs=bsrs.OrderBy(x => x.Result.ResultScore).ToList();
      return bsrs;
    }
    #endregion
    #endregion
    #region TEAMRESULT
    #region CALCULATE
    /*
     * Add a teamresult
     * Calculates the points in the meeting
     */
    public static bool AddTeamResult(Meeting meeting, Team team)
    {
      bool b = false;
      using (EFDbContext db = new EFDbContext()) { b = AddTeamResult(db, meeting, team); }
      return b;
    }

    public static bool AddTeamResult(EFDbContext db, Meeting meeting, Team team)
    {
      List<Result> results = ResultService.GetResults(db, meeting: meeting, team: team);
      int totalPoints = 0;
      IEnumerable<IGrouping<int, Result>> groupinglist = results.GroupBy(x => x.EventId);
      foreach (IGrouping<int, Result> group in groupinglist)
      {
        Result bestResult = ResultService.GetResultWithBestScoreWithinEvent(group.ToList());
        int? specificPoints = RankingService.CalculatePoints(db, bestResult);
        if (specificPoints.HasValue) totalPoints += specificPoints.Value;
      }
      TeamResult tr=db.TeamResults.FirstOrDefault(x => x.TeamId == team.TeamId && x.MeetingId == meeting.MeetingId);
      if (tr != null)
      {
        tr.PointValue = totalPoints;
        db.Entry(tr).State = EntityState.Modified;
      }
      else
      {
        tr = new TeamResult { Meeting = meeting, Team = team, PointValue = totalPoints };
        db.TeamResults.Add(tr);
      }
      bool b = (db.SaveChanges() > 0);
      RankingService.SetBestTeamResult(db, tr);
      return b;
    }
    #endregion
    #region GET
    /*
     * Get the Results that make up a teamresult
     */
    public static List<Result> GetResultsByTeamResult(TeamResult tr)
    {
      List<Result> wantedResults;
      using (EFDbContext db = new EFDbContext()) { wantedResults = GetResultsByTeamResult(db, tr); }
      return wantedResults;
    }

    public static List<Result> GetResultsByTeamResult(EFDbContext db, TeamResult tr)
    {
      List<Result> wantedResults = new List<Result>();
      List<Result> possibleResults = ResultService.GetResults(db, meeting: tr.Meeting, team: tr.Team);
      IEnumerable<IGrouping<int, Result>> groupinglist = possibleResults.GroupBy(x => x.EventId);
      foreach (IGrouping<int, Result> group in groupinglist)
      {
        wantedResults.Add(ResultService.GetResultWithBestScoreWithinEvent(group.ToList()));
      }
      return wantedResults;
    }
    #endregion
    #endregion
    #region BESTTEAMRESULT
    #region CALCULATE
    /*
     * Sets the best team result
     */
    public static bool SetBestTeamResult(TeamResult tr)
    {
      bool b = false;
      using (EFDbContext db = new EFDbContext()) { b = SetBestTeamResult(db, tr); }
      return b;
    }

    public static bool SetBestTeamResult(EFDbContext db, TeamResult tr)
    {
      List<TeamResult> trs = db.TeamResults.Where(x => x.TeamId == tr.TeamId && x.Meeting.Date.Year == tr.Meeting.Date.Year).ToList();
      TeamResult bestTr = trs.OrderByDescending(x => x.PointValue).First();
      BestTeamResult btr = db.BestTeamResults.Where(x => x.TeamId == tr.TeamId && x.CompetitionYear == tr.Meeting.Date.Year).FirstOrDefault();
      if (btr != null)
      {
        if (btr.TeamId == bestTr.TeamId && btr.MeetingId == bestTr.MeetingId) return true;
        db.BestTeamResults.Remove(btr);
        db.SaveChanges();
      }
      btr = new BestTeamResult { TeamId = bestTr.TeamId, MeetingId = bestTr.MeetingId, CompetitionYear = bestTr.Meeting.Date.Year };
      db.BestTeamResults.Add(btr);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    /*
     * Get the team ranking
     */
    public static List<BestTeamResult> GetTeamRanking(Sport sport, Gender? gender = null, int? year = null, List<Region> regions = null)
    {
      List<BestTeamResult> btrs = null;
      using (EFDbContext db = new EFDbContext())
      {
        btrs = GetTeamRanking(db, sport, gender, year, regions);
      }
      return btrs;
    }

    public static List<BestTeamResult> GetTeamRanking(EFDbContext db, Sport sport, Gender? gender = null, int? year = null, List<Region> regions = null)
    {
      List<BestTeamResult> btrs = db.BestTeamResults.Include("TeamResult.Team").Include("TeamResult.Team.School").Include("TeamResult.Meeting").Where(x => x.TeamResult.Team.SportId == sport.SportId).ToList();
      if (year.HasValue) btrs = btrs.Where(x => x.CompetitionYear == year).ToList();
      if (regions != null && regions.Count != 0)
      {
        List<Region> allregions = new List<Region>();
        allregions.AddRange(regions);
        foreach (Region r in regions)
        {
          allregions.AddRange(RegionService.GetChildren(db, r, false, true));
        }
        btrs = btrs.Where(x => allregions.Contains(x.TeamResult.Team.School.NSSF.Region)).ToList();

        /*btrs = btrs.Where(x =>
        {
          if (x.TeamResult.Meeting.ISFMeeting != null) return allregions.Intersect(x.TeamResult.Meeting.ISFMeeting.Regions).Count() > 0;
          if (x.TeamResult.Meeting.RegionalMeeting != null) return allregions.Contains(x.TeamResult.Meeting.RegionalMeeting.Region);
          if (x.TeamResult.Meeting.CoachMeeting != null) return allregions.Contains(x.TeamResult.Meeting.CoachMeeting.Teams.First().School.NSSF.Region);
          return false;
        }).ToList();*/
      }
      if (gender.HasValue) btrs = btrs.Where(x => x.TeamResult.Team.Gender == gender).ToList();
      btrs = btrs.OrderByDescending(x => x.TeamResult.PointValue).ToList();
      return btrs;
    }

    public static BestTeamResult GetBestTeamResult(Team team, int year)
    {
      BestTeamResult btr = null;
      using (EFDbContext db = new EFDbContext()) { btr = GetBestTeamResult(db, team, year); }
      return btr;
    }

    public static BestTeamResult GetBestTeamResult(EFDbContext db, Team team, int year)
    {
      BestTeamResult btr = db.BestTeamResults.Include("TeamResult.Team").Include("TeamResult.Meeting").Include("TeamResult.Meeting.Results").Include("TeamResult.Meeting.Results.Event").Where(x => x.TeamResult.TeamId == team.TeamId && x.CompetitionYear == year).FirstOrDefault();
      return btr;
    }


    #endregion
    #endregion
    #region RANKING
    /*
     * Get a teamranking, either overall(!) or on a specific event
     */
    public static TeamRanking GetRank(Team team, Event pEvent = null)
    {
      TeamRanking tr = null;
      using (EFDbContext db = new EFDbContext()) { tr = GetRank(db, team, pEvent); }
      return tr;
    }

    public static List<TeamRanking> GetRankings(Team team)
    {
      List<TeamRanking> teamRankings = null;

      using (EFDbContext db = new EFDbContext())
      {
        teamRankings = GetRankings(db, team);
      }
      return teamRankings;
    }

    public static List<TeamRanking> GetRankings(EFDbContext db, Team team)
    {
      List<TeamRanking> tr = db.TeamRankings.Where(x => x.TeamId == team.TeamId).ToList();

      return tr;
    }

    public static List<StudentRanking> GetRankings(Student student)
    {
      List<StudentRanking> studentRankings = null;

      using (EFDbContext db = new EFDbContext())
      {
        studentRankings = GetRankings(db, student);
      }
      return studentRankings;
    }

    public static List<StudentRanking> GetRankings(EFDbContext db, Student student)
    {
      List<StudentRanking> sr = db.StudentRankings.Include("Event").Where(x => x.MemberId == student.MemberId).ToList();

      return sr;
    }

    public static TeamRanking GetRank(EFDbContext db, Team team, Event pEvent = null)
    {
      List<TeamRanking> tr = db.TeamRankings.Where(x => x.TeamId == team.TeamId).ToList();
      if (pEvent == null) return tr.Where(x => x.EventId==0).FirstOrDefault();
      else return tr.Where(x => x.EventId == pEvent.EventId).FirstOrDefault();
    }

    /*
     * Get a studentranking, either max or on a specific event
     */
    public static StudentRanking GetRank(Student student, Event pEvent = null)
    {
      StudentRanking tr = null;
      using (EFDbContext db = new EFDbContext()) { tr = GetRank(db, student, pEvent); }
      return tr;
    }
    public static StudentRanking GetRank(EFDbContext db, Student student, Event pEvent = null)
    {
      List<StudentRanking> tr = db.StudentRankings.Where(x => x.MemberId == student.MemberId).ToList();
      if (pEvent == null) return tr.OrderBy(x => x.CurrentWorldRanking).FirstOrDefault();
      else return tr.Where(x => x.EventId == pEvent.EventId).FirstOrDefault();
    }
    #endregion


  }
}