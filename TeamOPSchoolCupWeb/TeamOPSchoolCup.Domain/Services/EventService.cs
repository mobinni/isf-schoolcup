﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using System.Web.Script.Serialization;
using System.Data;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Domain.Services
{
  public  class EventService
  {
    /*
     * Converts Sport objects into JSON strings
     */
    public static void GenerateJson(List<Event> events, StringBuilder builder, JavaScriptSerializer serializer)
    {
      builder.Append("[");
      foreach (Event e in events)
      {
        builder.Append(serializer.Serialize(new { 
          id = e.EventId, 
          name = e.Name, 
          sport = e.SportId 
        }));
        builder.Append(",");
      }
      if (builder.Length > 0) builder.Length--;
      if (builder.Length > 0) builder.Append("]");
    }

    public static void GenerateJson(List<Event> events, Meeting meeting, StringBuilder builder, JavaScriptSerializer serializer)
    {
      foreach (Event e in events)
      {
        builder.Append(serializer.Serialize(new { 
          id = e.EventId, 
          name = e.Name, 
          sport = e.SportId,
          meetingId = meeting.MeetingId
        }));
        builder.Append(",");
      }
    }

    public static bool AddEvent(Event pEvent)
    {
      bool e;
      using (EFDbContext db = new EFDbContext())
      {
        e = AddEvent(db, pEvent);
      }
      return e;
    }

    public static bool AddEvent(EFDbContext db, Event pEvent)
    {
      db.Events.Add(pEvent);
      return (db.SaveChanges() > 0);
    }
    public static Event GetEvent(string name)
    {
      Event r;
      using (EFDbContext db = new EFDbContext())
      {
        r = GetEvent(db, name);
      }
      return r;
    }

    public static Event GetEvent(EFDbContext db, string name)
    {
      return db.Events.Where(x => x.Name.Equals(name)).FirstOrDefault();
    }



    public static Event GetEvent(int id)
    {
        Event event1;
        using (EFDbContext db = new EFDbContext()) {
            event1 = GetEvent(db, id);
        }
        return event1;
    }

    public static Event GetEvent(EFDbContext db, int id)
    {
        return db.Events.Include("Sport").Single(x => x.EventId == id);
    }

    public static List<Event> GetEvents(Guid? studentId = null, int? year = null, int? teamId = null,bool? official=null,int? regionId=null)
    {
        List<Event> events;
        using (EFDbContext db = new EFDbContext())
        {
          events = GetEvents(db, studentId,year,teamId,official,regionId);
        }

        return events;
    }


    public static List<Event> GetEvents(EFDbContext db, Guid? studentId = null, int? year=null, int? teamId = null,bool? official=null,int? regionId=null)
    {
      List<Event> events = db.Events.Include("Sport").Include("NSSF").ToList<Event>();
      if (studentId != null)
      {
        List<Event> pEvents = new List<Event>();
        foreach (Event pEvent in events)
        {
          BestStudentResult bsr1 = RankingService.GetBestStudentResult(db,MemberService.GetStudent(db, studentId.Value), pEvent, pEvent.IsOfficial, year);
          if (bsr1 != null)
          {
            pEvents.Add(pEvent);
          }
        }
        events = pEvents;
      }
      if (year != null)
      {
        List<Event> pEvents = new List<Event>();
        foreach (Event pEvent in events)
        {
           if (pEvent.Points.Where(p=> p.CompetitionYear == year).ToList() != null)
          {
            pEvents.Add(pEvent);
          }
        }
        events = pEvents;
      }
      if (teamId != null)
      {
        HashSet<Event> pEvents = new HashSet<Event>();
        Team team = TeamService.GetTeam(db,teamId.Value);

        foreach (Event pEvent in events)
        {
          foreach (TeamMembership tm in team.TeamMemberships)
          {
            BestStudentResult bsr1 = RankingService.GetBestStudentResult(MemberService.GetStudent(db, tm.MemberId), pEvent, pEvent.IsOfficial, year);
            if (bsr1 != null)
            {
              pEvents.Add(pEvent);
            }
          }
        }
        events = pEvents.ToList();
      }
      if (official != null)
      {
        events = events.Where(x => {
          if (!official.Value && regionId.HasValue) {
            return (x.NSSFId == null || x.NSSF.Region.RegionId==regionId.Value);
          }
          return x.IsOfficial == official;
        }).ToList();
      }
      if (regionId != null)
      {
        events = events.Where(x => x.NSSFId == null || x.NSSF.Region.RegionId == regionId).ToList();
      }
        return events;
      
    }

    public static List<Event> GetOfficialEvents() 
    {
      List<Event> events;
      using (EFDbContext db = new EFDbContext())
      {
        events = GetOfficialEvents(db);
      }


      return events;
    }

    public static List<Event> GetOfficialEvents(EFDbContext db)
    {
        return db.Events.Include("Sport").Include("NSSF").Where(e => e.IsOfficial == true).ToList<Event>();
    }



    public static List<Event> GetEventsOfSport(int? sportId, short? gender_value = null) 
    {
      List<Event> events;
      using (EFDbContext db = new EFDbContext())
      {
        events = GetEventsOfSport(db, sportId, gender_value);
      }

      return events;
    }

    public static List<Event> GetEventsOfSport(EFDbContext db, int? sportId, short? gender_value = null)
    {
      List<Event> events = db.Events.ToList();
      if (sportId != null) { events = events.Where(e => e.Sport.SportId == sportId.Value).ToList<Event>(); }
      if (gender_value != null)
        if (gender_value == 0) { events = events.Where(e => e.Gender_Value == gender_value).ToList(); }
        else { events = events.Where(e => e.Gender_Value == gender_value || e.Gender_Value == 0).ToList(); }
      return events ;
    }
    public static List<Event> GetEventsOfMeeting(Meeting meeting)
    {
      List<Event> events;
      using (EFDbContext db = new EFDbContext())
      {
        events = GetEventsOfMeeting(db, meeting);
      }

      return events;
    }

    public static List<Event> GetEventsOfMeeting(EFDbContext db, Meeting meeting)
    {

      return db.Meetings.Include("Events").Single(m => m.MeetingId == meeting.MeetingId).Events.ToList();
    }
    public static List<Event> GetCommonEvents(Guid studentId1, Guid? studentId2 = null, List<Event> events = null, int? year = null)
    {
      List<Event> pEvents;
      using (EFDbContext db = new EFDbContext())
      {
        pEvents = GetCommonEvents(db, studentId1, studentId2, events, year);
      }

      return pEvents;
    }

    public static List<Event> GetCommonEvents(EFDbContext db, Guid studentId1, Guid? studentId2 = null, List<Event> events = null, int? year = null)
    {
      List<Event> pEvents = new List<Event>();
      if (studentId2 != null)
      {
        
        foreach (Event pEvent in db.Events.ToList<Event>())
        {
          BestStudentResult bsr1 = RankingService.GetBestStudentResult(MemberService.GetStudent(db, studentId1), pEvent, pEvent.IsOfficial, year: year);
          BestStudentResult bsr2 = RankingService.GetBestStudentResult(MemberService.GetStudent(db, studentId2.Value), pEvent, pEvent.IsOfficial, year: year);
          if (bsr1 != null && bsr2 != null)
          {
            pEvents.Add(pEvent);
          }
        }
      }
      if (events != null)
      {
        foreach (Event pEvent in events)
        {
          BestStudentResult bsr1 = RankingService.GetBestStudentResult(MemberService.GetStudent(db, studentId1), pEvent, pEvent.IsOfficial, year: year);
          if (bsr1 != null)
          {
            pEvents.Add(pEvent);
          }
        }
      }
      return pEvents;
    }

    public static List<Event> GetCommonEventsOfTeam(Team t1, Team t2, int year)
    {
      List<Event> events = null;
      using (EFDbContext db = new EFDbContext()) { events = GetCommonEventsOfTeam(db, t1, t2, year); }
      return events;
    }

    public static List<Event> GetCommonEventsOfTeam(EFDbContext db,Team t1, Team t2, int year)
    {
      List<Event> events = new List<Event>();
      HashSet<int> eventsT1 = new HashSet<int>();
      HashSet<int> eventsT2 = new HashSet<int>();
      t1.TeamMemberships.ForEach(x => x.Results.Where(y => y.Meeting.Date.Year == year).ToList().ForEach(y => eventsT1.Add(y.EventId)));
      t2.TeamMemberships.ForEach(x => x.Results.Where(y => y.Meeting.Date.Year == year).ToList().ForEach(y => eventsT2.Add(y.EventId)));
      eventsT1.Intersect(eventsT2).ToList().ForEach(x => events.Add(EventService.GetEvent(x)));
      return events;
    }

    public static bool UpdateEvent(Event event1)
    {
        bool r;
        using (EFDbContext db = new EFDbContext())
        {
            r = UpdateEvent(db, event1);
        }

        return r;
    }

    public static bool UpdateEvent(EFDbContext db, Event event1)
    {
        db.Entry(event1).State = EntityState.Modified;

        return (db.SaveChanges() > 0);
    }
  }
}
