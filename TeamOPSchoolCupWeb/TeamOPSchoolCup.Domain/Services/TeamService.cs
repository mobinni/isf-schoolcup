﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Data;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.Exceptions;

namespace TeamOPSchoolCup.Domain.Services
{
  public class TeamService
  {
    public enum Historicality { ALL, HISTORIC, CURRENT }

    public static void GenerateJson(List<Team> teams, StringBuilder builder, JavaScriptSerializer serializer)
    {
      builder.Append("[");
      foreach(Team team in teams){
        builder.Append(serializer.Serialize(new {
          id = team.TeamId,
          name = team.Name
        }));
        builder.Append(",");
      }
      if (builder.Length > 0) builder.Length--;
      if (builder.Length > 0) builder.Append("]");
    }

    #region TEAM
    #region ADD
    /*
     * Add a team
     */
    public static bool AddTeam(Team team)
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = AddTeam(db, team);
      }

      return t;
    }

    public static bool AddTeam(EFDbContext db, Team team)
    {
      //Might have to use custom model

      db.Teams.Add(team);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    /*
     * Get a list of the teams, inc Coach, and Members
     */


    public static List<Team> GetTeams(Coach coach = null, Student student = null, Historicality historicality = Historicality.ALL, School school = null, Region region = null, Sport sport = null)
    {
      List<Team> teams;
      using (EFDbContext db = new EFDbContext()) { teams = GetTeams(db, coach, student, historicality, school, region , sport); }
      return teams;
    }

    public static List<Team> GetTeams(EFDbContext db, Coach coach = null, Student student = null, Historicality historicality = Historicality.ALL, School school = null, Region region = null, Sport sport = null)
    {
      List<Team> teams = db.Teams.Include("TeamCoaches.Coach.Member").Include("Sport.Events").Include("School.NSSF.Region").Include("TeamMemberships.Results").Include("TeamResults.Meeting").Include("TeamMemberships.Student.Member").ToList();
      if (coach != null) teams = teams.Where(x => x.TeamCoaches.Single(y=> y.DateLeft>= DateTime.Today).MemberId == coach.MemberId).ToList();
      if (student != null)
      {
        switch (historicality)
        {
          case Historicality.HISTORIC:
            teams = teams.Where(x => x.TeamMemberships.Any(y => y.MemberId == student.MemberId && DateTime.Today <= y.DateLeft)).ToList();
            break;
          case Historicality.CURRENT:
            teams = teams.Where(x => x.TeamMemberships.Any(y => y.MemberId == student.MemberId && (y.DateLeft == null || y.DateLeft >= DateTime.Today))).ToList();
            break;
          case Historicality.ALL:
            teams = teams.Where(x => x.TeamMemberships.Any(y => y.MemberId == student.MemberId)).ToList();
            break;
        }
      }
      if (school != null) { teams = teams.Where(x => x.SchoolId == school.SchoolId).ToList(); }
      if (region != null) { teams = teams.Where(x => x.School.NSSF.Region.RegionId == region.RegionId).ToList(); }
      if (sport != null) { teams = teams.Where(t => t.SportId == sport.SportId).ToList(); }
      return teams;
    }
    #endregion
    #region UPDATE
    /*
     * Update a team
     */
    public static bool UpdateTeam(Team team)
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = UpdateTeam(db, team);
      }

      return t;
    }

    public static bool UpdateTeam(EFDbContext db, Team team)
    {
      db.Entry(team).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region ACTIVATION
    /*
     * Deactivate all teams, to be used at the end of the competitionyear
     */
    public static bool DeactivateAllTeams()
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = DeactivateAllTeams(db);
      }

      return t;
    }

    public static bool DeactivateAllTeams(EFDbContext db)
    {
      db.Teams.ToList().ForEach(t =>
      {
        t.IsParticipating = false;
        db.Entry(t).State = EntityState.Modified;
      });
      return (db.SaveChanges() > 0);
    }

    /*
     * Deactivate a specific team by id
     */
    public static bool DeactivateTeam(int teamId)
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = DeactivateTeam(db, teamId);
      }

      return t;
    }

    public static bool DeactivateTeam(EFDbContext db, int teamId)
    {
      Team t = db.Teams.Find(teamId);
      t.IsParticipating = false;
      db.Entry(t).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }

    /*
     * Activate a specific team by id
     */
    public static bool ActivateTeam(int teamId)
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = ActivateTeam(db, teamId);
      }

      return t;
    }

    public static bool ActivateTeam(EFDbContext db, int teamId)
    {
      Team t = db.Teams.Find(teamId);
      t.IsParticipating = true;
      db.Entry(t).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }
    #endregion
    #endregion
    #region TEAMMEMBERSHIP
    #region ADD
    /*
     * Add a teammembership
     */
    public static bool AddTeamMembership(int teamId, Guid studentId)
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = AddTeamMembership(db, teamId, studentId);
      }

      return t;
    }

    public static bool AddTeamMembership(EFDbContext db, int teamId, Guid studentId)
    {
      Student s = db.Students.Include("TeamMemberships.Team.Sport").Include("Member").Single(x => x.MemberId == studentId);
      Team t = db.Teams.Include("Sport").Single(x => x.TeamId == teamId);
      List<TeamMembership> tms = GetTeamMemberships(db, student: s, historicality: Historicality.CURRENT);
      if (tms != null && tms.Count > 0)
      {
        if (tms.First().Team.SchoolId != t.SchoolId)
        {
          throw new TeamOPSchoolCupException("Can't add student to team");
        }
      }
      
      if (tms.Any(x => x.Team.Sport.Equals(t.Sport))) throw new TeamOPSchoolCupException("Can't add student to team");;
      if (s.Member.Gender != t.Gender) return false;
      TeamMembership tm = new TeamMembership { Team = t, Student = s, DateJoined = DateTime.Now, DateLeft = new DateTime(DateTime.Now.Year, 12, 31) };
      db.TeamMemberships.Add(tm);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    /*
     * Get teammemberships with team, student, or historicality
     */
    public static List<TeamMembership> GetTeamMemberships(Team team = null, Student student = null, Historicality historicality = Historicality.ALL, School school = null)
    {
      List<TeamMembership> teamMemberships;
      using (EFDbContext db = new EFDbContext()) { teamMemberships = GetTeamMemberships(db, team, student, historicality, school); }
      return teamMemberships;
    }

    public static List<TeamMembership> GetTeamMemberships(EFDbContext db, Team team = null, Student student = null, Historicality historicality = Historicality.ALL, School school = null)
    {
      List<TeamMembership> teamMemberships = db.TeamMemberships.Include("Team").Include("Student.Member").ToList();
      if (team != null) teamMemberships = teamMemberships.Where(x => x.TeamId == team.TeamId).ToList();
      if (student != null) teamMemberships = teamMemberships.Where(x => x.MemberId == student.MemberId).ToList();
      if (school != null)
      {
        List<int> applicableTeams = GetTeams(school: school).Select(x => x.TeamId).ToList();
        teamMemberships = teamMemberships.Where(x => applicableTeams.Contains(x.TeamId)).ToList();
      }
      if (historicality == Historicality.CURRENT) teamMemberships = teamMemberships.Where(x => x.DateLeft == null || x.DateLeft > DateTime.Today).ToList();
      if (historicality == Historicality.HISTORIC) teamMemberships = teamMemberships.Where(x => x.DateLeft > DateTime.Today).ToList();
      return teamMemberships;
    }

    /*
     * As a separate method and no db-less method as it is highly experimental and only used in dbinit
     */
    public static List<TeamMembership> GetTeamMembershipsOfRegion(EFDbContext db, Region region)
    {
      List<TeamMembership> teamMemberships = db.TeamMemberships.Include("Team.School.NSSF.Region").Include("Student.Member").Where(x => x.Team.School.NSSF.Region.RegionId == region.RegionId).ToList();
      return teamMemberships;
    }
    /*
     * Get potential teammembers (here, not in teammembership because it has more to do with teams than with students)
     */
    public static List<Student> GetPotentialTeamMembers(Team team)
    {
      List<Student> students = null;
      using (EFDbContext db = new EFDbContext()) { students = GetPotentialTeamMembers(db, team); }
      return students;
    }
    public static List<Student> GetPotentialTeamMembers(EFDbContext db, Team team)
    {
      //Well, this method could be cleaner, but it should work.
      List<Student> students = TeamService.GetTeamMemberships(db, historicality: TeamService.Historicality.CURRENT, school: team.School).Select(x => x.Student).ToList();
      students = students.Where(x => x.Member.Gender == team.Gender).Where(x => !x.TeamMemberShips.Select(tm => tm.Team.SportId).Contains(team.SportId)).ToList();
      return students;
    }
    #endregion
    #region UPDATE
    /*
     * Prolong an existing teammembership, if the student is active for another year
     * Als een teamlid meerdere jaren meedoet in eenzelfde team dan moet een teammembership kunnen verlengd worden
     */
    public static bool ProlongTeamMembership(int teamId, Guid studentId)
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = ProlongTeamMembership(db, teamId, studentId);
      }

      return t;
    }

    public static bool ProlongTeamMembership(EFDbContext db, int teamId, Guid studentId)
    {
      Member student=MemberService.GetMember(studentId);
      if (RoleService.IsUserInRole(student.Email,"Inactive")) {
        RoleService.RemoveUserFromRole(student.Email,"Inactive");
      }
      if (!student.Student.IsParticipating) {
        student.Student.IsParticipating=true;
        db.Entry(student.Student);
        db.SaveChanges();
      }
      TeamMembership tm = db.TeamMemberships.Single(x => x.MemberId == studentId && x.TeamId == teamId);
      tm.DateLeft = new DateTime(tm.DateLeft.Year + 1, 12, 31);
      db.Entry(tm).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }
    #endregion
    #endregion
    #region TEAMCOACH
    #region ADD
    /*
     * Add a teammembership
     */
    public static bool AddTeamCoach(int teamId, Guid coachId)
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = AddTeamCoach(db, teamId, coachId);
      }

      return t;
    }

    public static bool AddTeamCoach(EFDbContext db, int teamId, Guid coachId)
    {
      Coach c = db.Coaches.Include("TeamCoaches.Team.Sport").Include("Member").Single(x => x.MemberId == coachId);
      Team t = db.Teams.Include("Sport").Single(x => x.TeamId == teamId);
      List<TeamCoach> tcs = GetTeamCoaches(db, coach: c, historicality: Historicality.CURRENT);
      if (c.SchoolId != t.SchoolId)
      {
        throw new TeamOPSchoolCupException("A coach can only be a coach in his own school");
      }
      TeamCoach tc = new TeamCoach { Team = t, Coach = c, DateJoined = DateTime.Now, DateLeft = new DateTime(DateTime.Now.Year, 12, 31) };
      db.TeamCoaches.Add(tc);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    /*
     * Get teammemberships with team, student, or historicality
     */
    public static List<TeamCoach> GetTeamCoaches(Team team = null, Coach coach = null, Historicality historicality = Historicality.ALL, School school = null)
    {
      List<TeamCoach> teamCoaches;
      using (EFDbContext db = new EFDbContext()) { teamCoaches = GetTeamCoaches(db, team, coach, historicality, school); }
      return teamCoaches;
    }

    public static List<TeamCoach> GetTeamCoaches(EFDbContext db, Team team = null, Coach coach = null, Historicality historicality = Historicality.ALL, School school = null)
    {
      List<TeamCoach> teamCoaches = db.TeamCoaches.Include("Team").Include("Coach.Member").ToList();
      if (team != null) teamCoaches = teamCoaches.Where(x => x.TeamId == team.TeamId).ToList();
      if (coach != null) teamCoaches = teamCoaches.Where(x => x.MemberId == coach.MemberId).ToList();
      if (school != null)
      {
        List<int> applicableTeams = GetTeams(school: school).Select(x => x.TeamId).ToList();
        teamCoaches = teamCoaches.Where(x => applicableTeams.Contains(x.TeamId)).ToList();
      }
      if (historicality == Historicality.CURRENT) teamCoaches = teamCoaches.Where(x => x.DateLeft == null || x.DateLeft > DateTime.Today).ToList();
      if (historicality == Historicality.HISTORIC) teamCoaches = teamCoaches.Where(x => x.DateLeft > DateTime.Today).ToList();
      return teamCoaches;
    }
    
    #endregion
    #region UPDATE
    /*
     * Prolong an existing teammembership, if the student is active for another year
     * Als een teamlid meerdere jaren meedoet in eenzelfde team dan moet een teammembership kunnen verlengd worden
     */
    public static bool ProlongTeamCoach(int teamId, Guid coachId)
    {
      bool t;
      using (EFDbContext db = new EFDbContext())
      {
        t = ProlongTeamCoach(db, teamId, coachId);
      }

      return t;
    }

    public static bool ProlongTeamCoach(EFDbContext db, int teamId, Guid coachId)
    {
      Member member = MemberService.GetMember(coachId);
      if (RoleService.IsUserInRole(member.Email, "Inactive"))
      {
        RoleService.RemoveUserFromRole(member.Email, "Inactive");
      }
      if (!member.Coach.IsParticipating)
      {
        member.Coach.IsParticipating = true;
        db.Entry(member.Student);
        db.SaveChanges();
      }
      TeamCoach tc = db.TeamCoaches.Single(x => x.MemberId == coachId && x.TeamId == teamId);
      tc.DateLeft = new DateTime(tc.DateLeft.Year + 1, 12, 31);
      db.Entry(tc).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }
    #endregion
    #endregion


    /*
     * Get a Team by ID
     */
    public static Team GetTeam(int id)
    {
      Team team;
      using (EFDbContext db = new EFDbContext())
      {
        team = GetTeam(db, id);
      }

      return team;
    }
    /*
     * Get a team by its name
     */
    public static Team GetTeam(string name)
    {
      Team team;
      using (EFDbContext db = new EFDbContext())
      {
        team = GetTeam(db, name);
      }
      return team;
    }
    public static Team GetTeam(EFDbContext db, int id)
    {
      return GetTeams(db).Single(x => x.TeamId == id);
    }
    public static Team GetTeam(EFDbContext db, string name)
    {
      return GetTeams(db).Single(x => x.Name == name);
    }

    public static Coach GetCurrentCoachOfTeam(Team team) {
      Coach coach;
      using (EFDbContext db = new EFDbContext()) {
        coach = GetCurrentCoachOfTeam(db, team);
      }
      return coach;
    }

    public static Coach GetCurrentCoachOfTeam(EFDbContext db, Team team) {
      return team.TeamCoaches.Single(x => x.DateLeft >= DateTime.Today).Coach;
    }

    /*
     * Get students by team
     */
    public static List<Student> GetStudents(int teamId)
    {
      List<Student> students;
      using (EFDbContext db = new EFDbContext())
      {
        students = GetStudents(db, teamId);
      }

      return students;
    }

    public static List<Student> GetStudents(EFDbContext db, int teamId)
    {
      return db.Students.Include("TeamMemberships").Where(x => x.TeamMemberShips.Any(y => y.TeamId == teamId && DateTime.Today < y.DateLeft)).ToList();
    }

    /*
     * Get Coach by team
     */
    public static Coach GetCoach(int teamId)
    {
      Coach c;
      using (EFDbContext db = new EFDbContext())
      {
        c = GetCoach(db, teamId);
      }

      return c;
    }

    public static Coach GetCoach(EFDbContext db, int teamId)
    {
      return db.Coaches.Include("Teams").Where(x => x.MemberId == db.Teams.Find(teamId).TeamCoaches.Single(t => t.DateLeft >= DateTime.Today).MemberId).First();
    }

    public static bool EditTeamFromEditableTeam(EditableTeam team)
    {
      bool b;
      using (EFDbContext db = new EFDbContext())
      {
        b = EditTeamFromEditableTeam(db, team);
      }
      return b;
    }
    public static bool EditTeamFromEditableTeam(EFDbContext db, EditableTeam edTeam)
    {
      Team team = GetTeam(edTeam.TeamId);
      team.Name = edTeam.Name;
      team.Gender_Value = edTeam.Gender_Value;
      team.IsParticipating = edTeam.IsParticipating;

      db.Entry(team).State = EntityState.Modified;
      bool bResult = (db.SaveChanges() > 0);
      //edAd.AdvertisementId = ad.AdvertisementId;
      return bResult;

    }
    /*
    public static bool CreateTeamFromCreatableTeam(CreatableTeam team) {
      bool b;
      using (EFDbContext db = new EFDbContext()) {
        b = CreateTeamFromCreatableTeam(db, team);
      }
      return b;
    }

    public static bool CreateTeamFromCreatableTeam(EFDbContext db,CreatableTeam cteam) {
      Team team = new Team();
      team.Name = cteam.Name;
      team.SportId = cteam.SportId;
      team.SchoolId = cteam.SchoolId;
      team.MemberId = cteam.MemberId;
      team.IsParticipating = cteam.IsParticipating;
      team.Gender_Value = cteam.Gender_Value;
      return AddTeam(db, team);
    }*/
  }
}
