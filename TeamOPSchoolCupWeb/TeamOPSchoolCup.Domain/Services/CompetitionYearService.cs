﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using System.Web.Script.Serialization;

namespace TeamOPSchoolCup.Domain.Services
{
  public class CompetitionYearService
  {
    /*
     * Converts CompetitionYear objects into JSON strings
     */
    public static void GenerateJson(List<CompetitionYear> competitionYears, StringBuilder builder, JavaScriptSerializer serializer)
    {
      int id = 1;
      builder.Append("[");
      foreach (CompetitionYear cy in competitionYears)
      {
        builder.Append(serializer.Serialize(new { 
          id = id, 
          name = cy.CompetitionYearValue 
        }));
        builder.Append(",");
        id++;
      }
      if (builder.Length > 0) builder.Length--;
      if (builder.Length > 0) builder.Append("]");
    }

    public static List<CompetitionYear> GetCompetitionYears()
    {
      List<CompetitionYear> cys = null;
      using (EFDbContext db = new EFDbContext()) { cys = GetCompetitionYears(db); }
      return cys;
    }
    public static List<CompetitionYear> GetCompetitionYears(EFDbContext db)
    {
      return db.CompetitionYears.ToList();
    }

    public static bool AddCompetitionYear(CompetitionYear cy)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = AddCompetitionYear(db,cy); }
      return b;
    }
    public static bool AddCompetitionYear(EFDbContext db,CompetitionYear cy)
    {
      db.CompetitionYears.Add(cy);
      return (db.SaveChanges() > 0);
    }
  }
}
