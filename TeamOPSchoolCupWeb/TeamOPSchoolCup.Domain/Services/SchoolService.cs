﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using System.Data;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Domain.Services
{
  public class SchoolService
  {
    #region ADD
    /*
     * Add a school
     */
    public static bool AddSchool(School school,string url)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = AddSchool(db, school,url); }
      return b;
    }
    public static bool AddSchool(EFDbContext db, School school,string url)
    {
      db.Schools.Add(school);
      bool b=db.SaveChanges() > 0;
      if (b)
      {
        string body = string.Format("{0}{1}{2}{3}{4}", MailStrings.DefaultAnonGreeting, MailStrings.DefaultAutomatedWarning, string.Format(MailStrings.SchoolAdded, url), MailStrings.DefaultWrongAction, MailStrings.DefaultSignoff);
        MailService.SendMail(MailStrings.SubjectSchoolAdded, new List<string>() { school.Email }, body, true);
      }
      return b;
    }
    public static bool AddSCC(SCC scc, School school)
    {
      bool n;
      using (EFDbContext db = new EFDbContext())
      {
        n = AddSCC(db, scc, school);
      }
      return n;
    }
    public static bool AddSCC(EFDbContext db, SCC scc, School school)
    {
      bool b = true;
      List<SCC> sccs = db.Schools.Single(n => n.SchoolId == school.SchoolId).SCCs.ToList<SCC>();



      foreach (SCC nr in sccs)
      {
        Member mem = db.Members.Where(m => m.MemberId == nr.MemberId).First();
        MembershipUser user = Membership.GetUser(mem.Email);
        if (user != null)
        {
          if (!Roles.IsUserInRole(mem.Email, "Inactive"))
          {
            b = false;
          }
        }
      }
      if (b)
      {
        db.Schools.Single(n => n.SchoolId == school.SchoolId).SCCs.Add(scc);
      }
      else
      {
        throw new TeamOPSchoolCupException(String.Format(ErrorStrings.SchoolAlreadyHasSCCException, school.Name));
      }


      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET
    /*
     * Get a single school by id, name, or abbr
     */
    public static School GetSchool(int? id = null, string name = null, string abbr = null, SCC scc = null, Team team = null,string email=null)
    {
      School s = null;
      using (EFDbContext db = new EFDbContext()) { s = GetSchool(db, id, name, abbr, scc, team,email); }
      return s;
    }
    public static School GetSchool(EFDbContext db, int? id = null, string name = null, string abbr = null, SCC scc = null, Team team = null,string email=null)
    {
      List<School> schools = db.Schools.Include("NSSF").Include("Teams").Include("SCCs").ToList();
      if (id.HasValue) return schools.Single(x => x.SchoolId == id);
      if (name != null) return schools.Single(x => x.Name == name);
      if (abbr != null) return schools.Single(x => x.AbbrName == abbr);
      if (scc != null) return schools.Where(x => x.SCCs.Where(s => s.MemberId == scc.MemberId).First().MemberId == scc.MemberId).First();
      if (team != null) return schools.Single(x => x.Teams.Any(y => y.TeamId == team.TeamId));
      if (email != null) return schools.Single(s => s.Email.ToLower().Equals(email.ToLower()));
      return null;
    }

    /*
     * Get multiple schools
     */
    public static List<School> GetSchools(NSSF nssf = null, Region region = null)
    {
      List<School> schools;
      using (EFDbContext db = new EFDbContext()) { schools = GetSchools(db, nssf, region); }
      return schools;
    }
    public static List<School> GetSchools(EFDbContext db, NSSF nssf = null, Region region = null)
    {
      List<School> schools = db.Schools.Include("NSSF").Include("Teams").Include("SCCs").ToList();
      if (nssf != null) schools = schools.Where(x => x.NSSFId == nssf.NSSFId).ToList();
      if (region != null) schools = schools.Where(s => s.NSSF.Region.RegionId == region.RegionId).ToList();
      return schools;
    }
    #endregion
    #region UPDATE
    /*
     * Update a school
     */
    public static bool UpdateSchool(School school)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = UpdateSchool(db, school); }
      return b;
    }
    public static bool UpdateSchool(EFDbContext db, School school)
    {
      db.Entry(school).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }
    #endregion
  }
}
