﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Web.Security;
using System.Data;
using TeamOPSchoolCup.Domain.Resources;
using TeamOPSchoolCup.Domain.Exceptions;
namespace TeamOPSchoolCup.Domain.Services
{
  public class NSSFService
  {
    #region ADD
    /*
     * This will add a NSSFRep to a specific NSSF if that NSSF doesn't have a active NSSFRep
     */
    public static bool AddNSSFRep(NSSFRep nssfrep, NSSF nssf)
    {
      bool n;
      using (EFDbContext db = new EFDbContext())
      {
        n = AddNSSFRep(db, nssfrep, nssf);
      }
      return n;
    }
    public static bool AddNSSFRep(EFDbContext db, NSSFRep nssfrep, NSSF nssf)
    {
      bool b = true;
      List<NSSFRep> nssfreps = db.NSSFs.Single(n => n.NSSFId == nssf.NSSFId).NSSFReps.ToList<NSSFRep>();


      foreach (NSSFRep nr in nssfreps)
      {
        Member mem = db.Members.Where(m => m.MemberId == nr.MemberId).First();
        //MembershipUser user = Membership.GetUser(mem.UserId);
        MembershipUser user = Membership.GetUser(mem.Email);
        if (user != null)
        {
          if (!Roles.IsUserInRole(mem.Email, "Inactive"))
          {
            b = false;
          }
        }
      }
      if (b)
      {
        db.NSSFs.Single(n => n.NSSFId == nssf.NSSFId).NSSFReps.Add(nssfrep);
      }
      else
      {
        throw new TeamOPSchoolCupException(String.Format(ErrorStrings.NSSFAlreadyHasRepException, nssf.Name));
      }


      return (db.SaveChanges() > 0);
    }
    /*
     * This will add a NSSF to the database
     */
    public static bool AddNSSF(NSSF nssf)
    {
      bool n;
      using (EFDbContext db = new EFDbContext())
      {
        n = AddNSSF(db, nssf);
      }
      return n;
    }

    public static bool AddNSSF(EFDbContext db, NSSF nssf)
    {
      if (NSSFService.GetNSSFs(db, nssf.Region).Count > 0)
      {
        throw new TeamOPSchoolCupException(ErrorStrings.NSSF_RegionAlreadyHasNSSF);
      }
      db.NSSFs.Add(nssf);
      return (db.SaveChanges() > 0);
    }
    #endregion
    #region GET

    public static NSSF GetNSSF(string name = null, int? nssfid = null, string email = null, Guid? repId = null)

    {
      NSSF n;
      using (EFDbContext db = new EFDbContext())
      {
        n = GetNSSF(db, name, nssfid, email,repId);

      }
      return n;
    }

    public static NSSF GetNSSF(int? nssfid = null) {
      NSSF n;
      using (EFDbContext db = new EFDbContext()) {
        n = GetNSSF(db, nssfid);

      }
      return n;
    }

    public static NSSF GetNSSF(EFDbContext db, int? nssfid = null) {
      NSSF n;
      n = db.NSSFs.Include("Region").Single(ns => ns.NSSFId == nssfid);
      return n;
    }

    public static List<NSSF> GetNSSFs(Region region = null)
    {
      List<NSSF> nssfs;
      using (EFDbContext db = new EFDbContext())
      {
        nssfs = GetNSSFs(db, region);
      }
      return nssfs;
    }

    public static List<NSSF> GetNSSFs(EFDbContext db, Region region = null)
    {
      List<NSSF> nssfs = db.NSSFs.Include("NSSFReps").Include("Region").ToList();
      if (region != null) nssfs = nssfs.Where(n => n.Region.RegionId == region.RegionId).ToList();
      return nssfs;
    }


    public static NSSF GetNSSF(EFDbContext db, string name = null, int? nssfid = null, string email = null, Guid? repId = null)
    {

     NSSF nssf =null;
     if(name != null) nssf = db.NSSFs.Include("Region").Where(x => x.Name.Equals(name)).FirstOrDefault();
     if (nssfid != null) nssf = db.NSSFs.Include("Region").Where(x => x.NSSFId == nssfid).FirstOrDefault();
     if (email != null) nssf = db.NSSFs.Include("Region").Where(n => n.Email.ToLower().Equals(email.ToLower())).FirstOrDefault();
     if (repId.HasValue) nssf = MemberService.GetNSSFRep(db, repId.Value).NSSF;
     return nssf;

    }

    public static NSSFRep GetNSSFRepOfNSSF(NSSF nssf)
    {
      NSSFRep n;
      using (EFDbContext db = new EFDbContext())
      {
        n = GetNSSFRepOfNSSF(db, nssf);
      }

      return n;
    }

    public static NSSFRep GetNSSFRepOfNSSF(EFDbContext db, NSSF nssf)
    {
      NSSFRep n = null; ;
      foreach (NSSFRep nrp in db.NSSFs.Single(ns => ns.Name == nssf.Name).NSSFReps.ToList<NSSFRep>())
      {
        Member mem = MemberService.GetMember(db, memberId: nrp.MemberId);
        if (!Roles.IsUserInRole(mem.Email, "Inactive"))
        {
          n = MemberService.GetNSSFRep(db, nrp.MemberId);
        }
      }
      return n;
    }

    private List<NSSFRep> getNSSFReps(EFDbContext db, int nssfid) {
      List<NSSFRep> nssfreps = null;
      NSSF nssf = GetNSSF(db, nssfid);

      nssfreps = nssf.NSSFReps;
      return nssfreps;
    }

    public static Region GetRegion(NSSF nssf)
    {
      return RegionService.GetRegion(nssf.Region.RegionId);
    }

    public static Region GetRegion(EFDbContext db, NSSF nssf)
    {
      return RegionService.GetRegion(db, nssf.Region.RegionId);
    }
    public static bool SetNSSFRep(NSSFRep nssfrep, NSSF nssf)
    {
      bool n;
      using (EFDbContext db = new EFDbContext())
      {
        n = SetNSSFRep(db, nssfrep, nssf);
      }
      return n;
    }
    #endregion
    public static bool SetNSSFRep(EFDbContext db, NSSFRep nssfrep, NSSF nssf)
    {

      foreach (NSSFRep nr in db.NSSFs.Single(n => n.Name == nssf.Name).NSSFReps.ToList<NSSFRep>())
      {
        Member mem = db.Members.Where(m => m.MemberId == nr.MemberId).First();
        if (!Roles.IsUserInRole(mem.Email, "Inactive"))
        {
          Roles.AddUserToRole(mem.Email, "Inactive");
        }
      }

      db.NSSFs.Single(n => n.NSSFId == nssf.NSSFId).NSSFReps.Add(nssfrep);
      return (db.SaveChanges() > 0);
    }




    #region UPDATE
    /*
     * Update a nssf
     */
    public static bool UpdateNSSF(NSSF nssf)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = UpdateNSSF(db, nssf); }
      return b;
    }
    public static bool UpdateNSSF(EFDbContext db, NSSF nssf)
    {
      db.Entry(nssf).State = EntityState.Modified;

      return (db.SaveChanges() > 0);
    }
    #endregion
  }
}
