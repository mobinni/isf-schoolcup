﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Data;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.DTO.Meetings;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Domain.Services
{
  public class MeetingService
  {
    public static void GenerateJson(Team team, List<CoachMeeting> meetings, StringBuilder builder, JavaScriptSerializer serializer)
    {
      foreach (CoachMeeting meeting in meetings)
      {
        builder.Append(serializer.Serialize(new
        {
          id = meeting.MeetingId,
          name = meeting.Meeting.Name,
          date = meeting.Meeting.Date.ToShortDateString(),
          teamId = team.TeamId
        }));
        builder.Append(",");
      }
    }

    #region ADD
    /*
     * Add unofficial meeting
     */
    public static bool AddCoachMeeting(CoachMeeting coachMeeting)
    {
      bool e;
      using (EFDbContext db = new EFDbContext())
      {
        e = AddCoachMeeting(db, coachMeeting);
      }
      return e;
    }
    public static bool AddCoachMeeting(EFDbContext db, CoachMeeting coachMeeting)
    {
      if (coachMeeting.Meeting.Events.Select(x => x.SportId).Distinct().ToList().Count() != 1) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_EventsMustBeInSameSport);
      if (coachMeeting.Team == null && coachMeeting.TeamId == 0) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_CoachMeetingNeedsTeam);
      if (coachMeeting.Meeting.Events == null || coachMeeting.Meeting.Events.Count == 0) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_MeetingNeedsEventException);

      db.Meetings.Add(coachMeeting.Meeting);
      db.SaveChanges();
      coachMeeting.MeetingId = coachMeeting.Meeting.MeetingId;
      db.CoachMeetings.Add(coachMeeting);

      return (db.SaveChanges() > 0);
    }
    public static bool AddCoachMeeting(EditableCoachMeeting erm, Guid memberId)
    {
      bool b = false;
      using (EFDbContext db = new EFDbContext()) { b = AddCoachMeeting(db, erm, memberId); }
      return b;
    }
    public static bool AddCoachMeeting(EFDbContext db, EditableCoachMeeting erm, Guid memberId)
    {
      CoachMeeting im = new CoachMeeting
      {
        MemberId = memberId,
        TeamId = erm.TeamId,
        Meeting = new Meeting
        {
          Name = erm.Name,
          Date = erm.Date,
          Location = erm.Location,
          IsOfficial = false
        }
      };
      im.Meeting.Events = new List<Event>();
      erm.EventsStr.Split(',').ToList().ForEach(x => im.Meeting.Events.Add(EventService.GetEvent(db, Int32.Parse(x))));
      return MeetingService.AddCoachMeeting(db, im);
    }

    /*
     * Add a regional meeting
     */
    public static bool AddRegionalMeeting(RegionalMeeting regionalmeeting)
    {
      bool e;
      using (EFDbContext db = new EFDbContext())
      {
        e = AddRegionalMeeting(db, regionalmeeting);
      }
      return e;
    }
    public static bool AddRegionalMeeting(EFDbContext db, RegionalMeeting regionalmeeting)
    {
      if (regionalmeeting.Meeting.Events.Select(x => x.SportId).Distinct().ToList().Count() != 1) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_EventsMustBeInSameSport);
      if (regionalmeeting.Meeting.Events == null || regionalmeeting.Meeting.Events.Count == 0) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_MeetingNeedsEventException);

      db.Meetings.Add(regionalmeeting.Meeting);
      db.SaveChanges();
      regionalmeeting.MeetingId = regionalmeeting.Meeting.MeetingId;
      db.RegionalMeetings.Add(regionalmeeting);

      return (db.SaveChanges() > 0);
    }
    public static bool AddRegionalMeeting(EditableRegionalMeeting erm, Guid memberId)
    {
      bool b = false;
      using (EFDbContext db = new EFDbContext()) { b = AddRegionalMeeting(db, erm, memberId); }
      return b;
    }
    public static bool AddRegionalMeeting(EFDbContext db, EditableRegionalMeeting erm, Guid memberId)
    {
      RegionalMeeting im = new RegionalMeeting
      {
        MemberId = memberId,
        RegionId = NSSFService.GetNSSF(db, repId: memberId).Region.RegionId,
        Meeting = new Meeting
        {
          Name = erm.Name,
          Date = erm.Date,
          Location = erm.Location,
          IsOfficial = true
        }
      };
      im.Meeting.Events = new List<Event>();
      erm.EventsStr.Split(',').ToList().ForEach(x => im.Meeting.Events.Add(EventService.GetEvent(db, Int32.Parse(x))));
      return MeetingService.AddRegionalMeeting(db, im);
    }

    /*
     * Add an ISF Meeting
     */
    public static bool AddISFMeeting(ISFMeeting isfmeeting)
    {
      bool e;
      using (EFDbContext db = new EFDbContext())
      {
        e = AddISFMeeting(db, isfmeeting);
      }
      return e;
    }
    public static bool AddISFMeeting(EFDbContext db, ISFMeeting isfmeeting)
    {
      if (isfmeeting.Meeting.Events.Select(x => x.SportId).Distinct().ToList().Count() != 1) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_EventsMustBeInSameSport);
      if (isfmeeting.Meeting.Events == null || isfmeeting.Meeting.Events.Count == 0) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_MeetingNeedsEventException);
      if (isfmeeting.Regions.Count == 0) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_ISFMeetingNeedsRegion);

      db.Meetings.Add(isfmeeting.Meeting);
      db.SaveChanges();
      isfmeeting.MeetingId = isfmeeting.Meeting.MeetingId;
      db.ISFMeetings.Add(isfmeeting);

      return (db.SaveChanges() > 0);
    }
    public static bool AddISFMeeting(EditableISFMeeting eim, Guid memberId)
    {
      bool b = false;
      using (EFDbContext db = new EFDbContext()) { b = AddISFMeeting(db, eim, memberId); }
      return b;
    }
    public static bool AddISFMeeting(EFDbContext db, EditableISFMeeting eim, Guid memberId)
    {
      ISFMeeting im = new ISFMeeting
      {
        MemberId = memberId,
        Meeting = new Meeting
        {
          Name = eim.Name,
          Date = eim.Date,
          Location = eim.Location,
          IsOfficial = true
        }
      };
      im.Meeting.Events = new List<Event>();
      eim.EventsStr.Split(',').ToList().ForEach(x => im.Meeting.Events.Add(EventService.GetEvent(db, Int32.Parse(x))));
      im.Regions = new List<Region>();
      eim.RegionsStr.Split(',').ToList().ForEach(x => im.Regions.Add(RegionService.GetRegion(db, Int32.Parse(x))));
      return MeetingService.AddISFMeeting(db, im);
    }
    #endregion
    #region GET
    /*
     * Get ISF Meetings
     */
    public static List<ISFMeeting> GetISFMeetings()
    {
      List<ISFMeeting> isfmeetings = null;
      using (EFDbContext db = new EFDbContext()) { isfmeetings = GetISFMeetings(db); }
      return isfmeetings;
    }
    public static List<ISFMeeting> GetISFMeetings(EFDbContext db)
    {
      return db.ISFMeetings.Include("Meeting").ToList();
    }

    /*
     * Get Regional Meetings
     */
    public static List<RegionalMeeting> GetRegionalMeetings(NSSF nssf)
    {
      List<RegionalMeeting> regionalMeetings = null;
      using (EFDbContext db = new EFDbContext()) { regionalMeetings = GetRegionalMeetings(db, nssf); }
      return regionalMeetings;
    }
    public static List<RegionalMeeting> GetRegionalMeetings(EFDbContext db, NSSF nssf)
    {
      return db.RegionalMeetings.Include("Meeting").Where(x => x.NSSFRep.NSSFId == nssf.NSSFId).ToList();
    }

    /*
     * Get Unofficial Meetings
     */
    public static List<CoachMeeting> GetCoachMeetings(Team team)
    {
      List<CoachMeeting> unofficialMeetings = null;
      using (EFDbContext db = new EFDbContext()) { unofficialMeetings = GetCoachMeetings(db, team); }
      return unofficialMeetings;
    }

    public static List<CoachMeeting> GetCoachMeetings(EFDbContext db, Team team)
    {
      //return db.CoachMeetings.Include("Meeting").Where(x => x.MemberId == team.TeamCoaches.Single(t => t.DateLeft >= DateTime.Today).MemberId).ToList();
      Guid teamMemberId = team.TeamCoaches.Where(t => t.DateLeft >= DateTime.Today).Select(t => t.MemberId).FirstOrDefault();

      return db.CoachMeetings.Include("Meeting").Include("Meeting.Events").Where(x => x.MemberId == teamMemberId).ToList();
    }

    /*
     * Get Upcoming meetings
     */
    public static List<Meeting> GetUpcomingMeetings(Team team = null, Student student = null, bool? isOfficial = null)
    {
      List<Meeting> meetings;
      using (EFDbContext db = new EFDbContext()) { meetings = GetUpcomingMeetings(db, team, student, isOfficial); }
      return meetings;
    }

    public static List<Meeting> GetUpcomingMeetings(EFDbContext db, Team team = null, Student student = null, bool? isOfficial = null)
    {
      List<Meeting> meetings = db.Meetings.Include("RegionalMeeting").Include("CoachMeeting").Include("ISFMeeting").Where(x => x.Date >= DateTime.Today).ToList();
      int nssfId = 0;
      List<Guid> coachIds = new List<Guid>();
      if (team != null)
      {
        nssfId = team.School.NSSFId;
        coachIds.Add(team.TeamCoaches.Single(x => x.DateLeft >= DateTime.Today).MemberId);
      }
      else if (student != null)
      {
        nssfId = student.TeamMemberShips.First().Team.School.NSSFId;
        student.TeamMemberShips.ForEach(x => coachIds.Add(x.Team.TeamCoaches.Single(t => t.DateLeft >= DateTime.Today).MemberId));
      }

      if (nssfId != 0 && coachIds.Count > 0)
      {
        meetings = meetings.Where(x => x.RegionalMeeting == null || x.RegionalMeeting.NSSFRep.NSSFId == nssfId).ToList();
        meetings = meetings.Where(x => x.CoachMeeting == null || coachIds.Contains(x.CoachMeeting.MemberId)).ToList();
      }

      if (isOfficial.HasValue)
      {
        meetings = meetings.Where(x => (x.CoachMeeting == null) == isOfficial.Value).ToList();
      }
      return meetings;
    }

    /*
     * Get meetings by year and/or region
     */
    public static List<Meeting> GetMeetings(Region region = null, int? year = null, bool? official = null, bool? upcoming = null, Team team = null)
    {
      List<Meeting> meetings;
      using (EFDbContext db = new EFDbContext()) { meetings = GetMeetings(db, region, year, official, upcoming, team); }
      return meetings;
    }
    public static List<Meeting> GetMeetings(EFDbContext db, Region region = null, int? year = null, bool? official = true, bool? upcoming = true, Team team = null)
    {
      List<Meeting> meetings = db.Meetings.Include("RegionalMeeting").Include("ISFMeeting").Include("CoachMeeting").Include("Results").ToList();
      if (team != null)
      {
        meetings=meetings.Where(x =>
        {
          if (x.Results.Count == 0) return true;
          if (x.Results.Any(y => y.TeamId == team.TeamId)) return true;
          return false;
        }).ToList();
        meetings = meetings.Where(x =>
        {
          if (x.RegionalMeeting != null)
          {
            return (x.RegionalMeeting.RegionId == team.School.NSSF.Region.RegionId);
          }
          if (x.ISFMeeting != null)
          {
            return (x.ISFMeeting.Regions.Select(y => y.RegionId).Contains(team.School.NSSF.Region.RegionId));
          }
          if (x.CoachMeeting != null)
          {
            return (x.CoachMeeting.TeamId == team.TeamId);
          }
          return false;
        }).ToList();
      }
      if (year.HasValue) meetings = meetings.Where(x => x.Date.Year == year).ToList();

      if (region != null)
      {
        List<Region> wantedRegions = new List<Region>();
        wantedRegions.Add(region);
        wantedRegions.AddRange(RegionService.GetChildren(db, region, false, true));
        meetings = meetings.Where(x =>
        {
          if (x.ISFMeeting != null) return wantedRegions.Select(y => y.RegionId).Intersect(x.ISFMeeting.Regions.Select(y => y.RegionId)).Count() > 0;
          if (x.RegionalMeeting != null) return wantedRegions.Select(y => y.RegionId).Contains(x.RegionalMeeting.RegionId);
          if (x.CoachMeeting != null) return false; //Coachmeetings shouldn't show up on a region-wide search, as they are private to the teams invited.
          return false;
        }).ToList();
      }
      if (official.HasValue) meetings = meetings.Where(x => x.IsOfficial == official).ToList();
      if (upcoming.HasValue) meetings = meetings.Where(x => x.Date >= DateTime.Today).ToList();
      return meetings;
    }

    /*
     * Get the meetings that are relevant for a user, ordered by date
     */
    public static List<Meeting> GetRelevantMeetings(Member member)
    {
      List<Meeting> meetings;
      using (EFDbContext db = new EFDbContext()) { meetings = GetRelevantMeetings(db, member); }
      return meetings;
    }
    public static List<Meeting> GetRelevantMeetings(EFDbContext db, Member member)
    {
      member = MemberService.GetMember(db, member.MemberId);
      List<Meeting> meetings = new List<Meeting>();
      if (member.ISFAdmin != null)
      {
        meetings.AddRange(MeetingService.GetISFMeetings(db).Select(x => x.Meeting).ToList());
      }
      else if (member.NSSFRep != null)
      {
        meetings.AddRange(MeetingService.GetRegionalMeetings(db, member.NSSFRep.NSSF).Select(x => x.Meeting).ToList());
      }
      else if (member.Coach != null || member.Student != null)
      {
        if (member.Coach != null)
        {
          foreach (Team team in member.Coach.TeamCoaches.Select(x => x.Team))
          {
            meetings.AddRange(MeetingService.GetMeetings(db, team: team, upcoming: null, official: null));
          }
        }
        if (member.Student != null)
        {
          foreach (Team team in member.Student.TeamMemberShips.Select(x => x.Team))
          {
            meetings.AddRange(MeetingService.GetMeetings(db, team: team, upcoming: null, official: null));
          }
        }
        List<int> meetingIds = meetings.Select(x => x.MeetingId).Distinct().ToList();
        meetings.Clear();
        foreach (int meetingId in meetingIds)
        {
          meetings.Add(MeetingService.GetMeeting(db, meetingId));
        }
      }
      return meetings;
    }

    public static bool CanUserEditMeeting(Meeting meeting, Member member)
    {
      if (meeting.ISFMeeting != null) return meeting.ISFMeeting.MemberId == member.MemberId;
      else if (meeting.RegionalMeeting != null) return meeting.RegionalMeeting.MemberId == member.MemberId;
      else if (meeting.CoachMeeting != null) return meeting.CoachMeeting.MemberId == member.MemberId;
      return false;
    }
    public static bool CanUserAddResultsToMeeting(Meeting meeting, Member member)
    {
      if (meeting.ISFMeeting != null) return member.ISFAdmin != null;
      if (meeting.RegionalMeeting != null)
      {
        if (member.NSSFRep == null) return false;
        return member.NSSFRep.NSSF.Region.RegionId == meeting.RegionalMeeting.RegionId;
      }
      if (meeting.CoachMeeting != null)
      {
        if (member.Coach != null)
        {
          return member.Coach.TeamCoaches.Select(x => x.TeamId).Contains(meeting.CoachMeeting.TeamId);
        }
        if (member.Student != null)
        {
          return member.Student.TeamMemberShips.Select(x => x.TeamId).Contains(meeting.CoachMeeting.TeamId);
        }
      }
      return false;
    }

    public static List<Meeting> GetMeetingsToWhichUserCanAddResult(Member member,Team team=null)
    {
      List<Meeting> meetings;
      using (EFDbContext db = new EFDbContext()) { meetings = GetMeetingsToWhichUserCanAddResult(db, member,team); }
      return meetings;
    }
    public static List<Meeting> GetMeetingsToWhichUserCanAddResult(EFDbContext db,Member member,Team team=null)
    {
      if (member.ISFAdmin != null)
      {
        return GetISFMeetings(db).Select(x => x.Meeting).ToList();
      }
      else if (member.NSSFRep != null)
      {
        return GetRegionalMeetings(db, member.NSSFRep.NSSF).Select(x => x.Meeting).ToList();
      }
      else if (member.Coach != null || member.Student != null)
      {
        List<Meeting> meetings=new List<Meeting>();
        if (member.Coach != null) {
          if (team == null)
          {
            member.Coach.TeamCoaches.ForEach(x => meetings.AddRange(GetCoachMeetings(db, x.Team).Select(y => y.Meeting).ToList()));
          }
          else
          {
            meetings.AddRange(GetCoachMeetings(db, team).Select(y => y.Meeting).ToList());
          }
        } else if (member.Student != null) {
          if (team == null)
          {
            member.Student.TeamMemberShips.ForEach(x => meetings.AddRange(GetCoachMeetings(db, x.Team).Select(y => y.Meeting).ToList()));
          }
          else
          {
            meetings.AddRange(GetCoachMeetings(db, team).Select(y => y.Meeting).ToList());
          }
        }
        return meetings;
      }
      return new List<Meeting>();
    }

    /*
    * Get a meeting by id
    */
    public static Meeting GetMeeting(int id)
    {
      Meeting meeting = null;
      using (EFDbContext db = new EFDbContext()) { meeting = GetMeeting(db, id); }
      return meeting;
    }

    public static Meeting GetMeeting(EFDbContext db, int id)
    {

      return db.Meetings.Include("CoachMeeting.Team").Include("RegionalMeeting").Include("ISFMeeting.Regions").Include("Results.TeamMembership.Student.Member").Include("Results.TeamMembership.Team.School").Include("Results.Event").Include("TeamResults").Include("Events").Single(x => x.MeetingId == id);
    }
    #endregion
    #region UPDATE
    /*
     * Update a meeting
     */
    public static bool UpdateMeeting(Meeting meeting)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = UpdateMeeting(db, meeting); }
      return b;
    }
    public static bool UpdateMeeting(EFDbContext db, Meeting meeting)
    {
      if (meeting.Events == null || meeting.Events.Count == 0) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_MeetingNeedsEventException);
      db.Entry(meeting).State = EntityState.Modified;

      return (db.SaveChanges() > 0);
    }

    public static bool UpdateISFMeeting(EditableISFMeeting meeting)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = UpdateISFMeeting(db, meeting); }
      return b;
    }
    public static bool UpdateISFMeeting(EFDbContext db, EditableISFMeeting meeting)
    {
      if (String.IsNullOrEmpty(meeting.EventsStr)) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_MeetingNeedsEventException);
      Meeting originalMeeting = GetMeeting(db, meeting.ISFMeetingId.Value);
      originalMeeting.Name = meeting.Name;
      originalMeeting.Location = meeting.Location;
      originalMeeting.Date = meeting.Date;
      List<Region> newRegions = new List<Region>();
      meeting.RegionsStr.Split(',').ToList().ForEach(x => newRegions.Add(RegionService.GetRegion(db, Int32.Parse(x))));
      List<Event> newEvents = new List<Event>();
      meeting.EventsStr.Split(',').ToList().ForEach(x => newEvents.Add(EventService.GetEvent(db, Int32.Parse(x))));
      originalMeeting.ISFMeeting.Regions = newRegions;
      originalMeeting.Events = newEvents;
      db.Entry(originalMeeting).State = EntityState.Modified;
      db.Entry(originalMeeting.ISFMeeting).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }

    public static bool UpdateRegionalMeeting(EditableRegionalMeeting meeting)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = UpdateRegionalMeeting(db, meeting); }
      return b;
    }
    public static bool UpdateRegionalMeeting(EFDbContext db, EditableRegionalMeeting meeting)
    {
      if (String.IsNullOrEmpty(meeting.EventsStr)) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_MeetingNeedsEventException);
      Meeting originalMeeting = GetMeeting(db, meeting.RegionalMeetingId.Value);
      originalMeeting.Name = meeting.Name;
      originalMeeting.Location = meeting.Location;
      originalMeeting.Date = meeting.Date;
      List<Event> newEvents = new List<Event>();
      meeting.EventsStr.Split(',').ToList().ForEach(x => newEvents.Add(EventService.GetEvent(db, Int32.Parse(x))));
      originalMeeting.Events = newEvents;
      db.Entry(originalMeeting).State = EntityState.Modified;
      db.Entry(originalMeeting.RegionalMeeting).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }

    public static bool UpdateCoachMeeting(EditableCoachMeeting meeting)
    {
      bool b;
      using (EFDbContext db = new EFDbContext()) { b = UpdateCoachMeeting(db, meeting); }
      return b;
    }
    public static bool UpdateCoachMeeting(EFDbContext db, EditableCoachMeeting meeting)
    {
      if (String.IsNullOrEmpty(meeting.EventsStr)) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_MeetingNeedsEventException);
      Meeting originalMeeting = GetMeeting(db, meeting.CoachMeetingId.Value);
      originalMeeting.Name = meeting.Name;
      originalMeeting.Location = meeting.Location;
      originalMeeting.Date = meeting.Date;
      List<Event> newEvents = new List<Event>();
      meeting.EventsStr.Split(',').ToList().ForEach(x => newEvents.Add(EventService.GetEvent(db, Int32.Parse(x))));
      originalMeeting.Events = newEvents;
      db.Entry(originalMeeting).State = EntityState.Modified;
      db.Entry(originalMeeting.CoachMeeting).State = EntityState.Modified;
      return (db.SaveChanges() > 0);
    }
    #endregion

    /*
     * CancelMeeting
     * 
     */
    //TEST METHODS VOOR CHARTCONTROLLER
    public static List<RegionalMeeting> GetRegionalMeetings()
    {
      List<RegionalMeeting> regionalMeetings = null;
      using (EFDbContext db = new EFDbContext()) { regionalMeetings = GetRegionalMeetings(db); }
      return regionalMeetings;
    }
    public static List<RegionalMeeting> GetRegionalMeetings(EFDbContext db)
    {
      return db.RegionalMeetings.Include("Meeting.TeamResults").ToList();
    }

    public static EditableISFMeeting GetEditableISFMeeting(int isfMeetingId)
    {
      EditableISFMeeting eim = null;
      using (EFDbContext db = new EFDbContext()) { eim = GetEditableISFMeeting(db, isfMeetingId); }
      return eim;
    }
    public static EditableISFMeeting GetEditableISFMeeting(EFDbContext db, int isfMeetingId)
    {
      Meeting im = GetMeeting(db, isfMeetingId);
      if (im == null || im.ISFMeeting == null) { return null; }
      EditableISFMeeting eim = new EditableISFMeeting
      {
        Name = im.Name,
        Location = im.Location,
        Date = im.Date,
        ISFMeetingId = im.MeetingId,
        EventsStr = String.Join(",", im.Events.Select(x => x.EventId).ToList()),
        RegionsStr = String.Join(",", im.ISFMeeting.Regions.Select(x => x.RegionId).ToList())
      };
      return eim;
    }

    public static EditableRegionalMeeting GetEditableRegionalMeeting(int regionalMeetingId)
    {
      EditableRegionalMeeting erm = null;
      using (EFDbContext db = new EFDbContext()) { erm = GetEditableRegionalMeeting(db, regionalMeetingId); }
      return erm;
    }
    public static EditableRegionalMeeting GetEditableRegionalMeeting(EFDbContext db, int regionalMeetingId)
    {
      Meeting im = GetMeeting(db, regionalMeetingId);
      if (im == null || im.RegionalMeeting == null) { return null; }
      EditableRegionalMeeting erm = new EditableRegionalMeeting
      {
        Name = im.Name,
        Location = im.Location,
        Date = im.Date,
        RegionalMeetingId = im.MeetingId,
        EventsStr = String.Join(",", im.Events.Select(x => x.EventId).ToList())
      };
      return erm;
    }

    public static EditableCoachMeeting GetEditableCoachMeeting(int coachMeetingId)
    {
      EditableCoachMeeting ecm = null;
      using (EFDbContext db = new EFDbContext()) { ecm = GetEditableCoachMeeting(db, coachMeetingId); }
      return ecm;
    }
    public static EditableCoachMeeting GetEditableCoachMeeting(EFDbContext db, int coachMeetingId)
    {
      Meeting im = GetMeeting(db, coachMeetingId);
      if (im == null || im.CoachMeeting == null) { return null; }
      EditableCoachMeeting erm = new EditableCoachMeeting
      {
        Name = im.Name,
        Location = im.Location,
        Date = im.Date,
        CoachMeetingId = im.MeetingId,
        EventsStr = String.Join(",", im.Events.Select(x => x.EventId).ToList())
      };
      return erm;
    }

    public static bool MeetingHasResults(int meetingId)
    {
      bool b = false;
      using (EFDbContext db = new EFDbContext()) { b = MeetingHasResults(db, meetingId); }
      return b;
    }
    public static bool MeetingHasResults(EFDbContext db, int meetingId)
    {
      Meeting m = GetMeeting(db, meetingId);
      return (m.Results.Count > 0);
    }
  }
}
