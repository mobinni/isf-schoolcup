﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.Services;

namespace TeamOPSchoolCup.Web.Controllers
{ 
    public class TeamController : Controller
    {
        private EFDbContext db = new EFDbContext();
        private TeamService ts = new TeamService();

        public ViewResult Profile(int id)
        {
          return View(ts.GetTeam(id));
        }

        public ViewResult Dashboard(int id)
        {
          //TODO
          //  Some meetings stuff
          //  Some conversations stuff
          //  Some bestResults stuff
          //  Some ranking stuff
          return View();
        }

        //
        // GET: /Team/Create

        public ActionResult Create()
        {
            ViewBag.CoachId = new SelectList(db.Coaches.Include("Member"), "CoachId", "Member.Name");
            return View();
        } 

        //
        // POST: /Team/Create

        [HttpPost]
        public ActionResult Create(Team team)
        {
            if (ModelState.IsValid)
            {
                db.Teams.Add(team);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.CoachId = new SelectList(db.Coaches, "CoachId", "Photo", team.CoachId);
            return View(team);
        }
        
        //
        // GET: /Team/Edit/5
 
        public ActionResult Edit(int id)
        {
            Team team = db.Teams.Find(id);
            ViewBag.CoachId = new SelectList(db.Coaches, "CoachId", "Photo", team.CoachId);
            return View(team);
        }

        //
        // POST: /Team/Edit/5

        [HttpPost]
        public ActionResult Edit(Team team)
        {
            if (ModelState.IsValid)
            {
                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CoachId = new SelectList(db.Coaches, "CoachId", "Photo", team.CoachId);
            return View(team);
        }

        //
        // GET: /Team/Delete/5
 
        public ActionResult Delete(int id)
        {
            Team team = db.Teams.Find(id);
            return View(team);
        }

        //
        // POST: /Team/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Team team = db.Teams.Find(id);
            db.Teams.Remove(team);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult AddTeamMembership(int teamId)
        {
          //TODO Get all students within the school
          List<Student> students = db.Students.ToList();
          ViewBag.Students = students;
          return View();
        }

        [HttpPost]
        public ActionResult AddTeamMembership(int teamId, int studentId)
        {
          if (ts.AddTeamMembership(teamId, studentId))
          {
            return RedirectToAction("Details", new { id = teamId });
          }
          else
          {
            //TODO Errorpage
            return null;
          }
        }
    }
}