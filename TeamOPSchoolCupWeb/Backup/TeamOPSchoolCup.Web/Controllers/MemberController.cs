﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Web.Security;
using TeamOPSchoolCup.Web.Models;
namespace TeamOPSchoolCup.Web.Controllers
{
  //This is rather temporary, TODO to be integrated in AccountController
  public class MemberController : Controller
  {
    EFDbContext db = new EFDbContext();
    MemberService ms = new MemberService();
    //
    // GET: /User/
    //Profile page
    public ActionResult Profile(int memberId)
    {
      return View();
    }
    //GET:
    //public ActionResult Create() {
    //  return View();
    //}
    /*POST:
    [HttpPost]
    public ActionResult Create() {

      return View();
    }*/
    //GET:
    public ActionResult Settings(int memId) {
      Member member = db.Members.Where(me => me.MemberId == memId).First();
     
      ViewBag.Member = member;
      return View(member);
    }
    [HttpPost]
    public ActionResult Settings(Member member)
    {
      if (ModelState.IsValid)
      {
        
        if (ms.ChangeLanguage(member, member.Language))
        {
          return RedirectToAction("Index");
        }
        else {
          return RedirectToAction("Settings");
          
        }
      }
      
      return View(member);
    }
   
    //GET:
    public ActionResult addStudent() {

      return View();
    }
    //POST
    [HttpPost]
    public ActionResult addStudent(Student student)
    {
      if (ModelState.IsValid)
      {
        
        
        if(ms.AddStudent(student)){
        return RedirectToAction("Index");
        }else{
          return RedirectToAction("addStudent");
        }
      }

     
      return View();
    }
    /* All have GET and POST
     * Update
     * Delete
     * ChangeName
     * ChangeEmail
     * ChangePassword
     * ChangeLanguage
     * Settings
     * Dashboard(memberId -> studentId of coachId)
     */
    // GET: /Account/ChangePassword

    //[Authorize]
    public ActionResult ChangePassword(int memId)
    {
      Member member = db.Members.Where(me => me.MemberId == memId).First();
      ChangePasswordModel chpsm = new ChangePasswordModel();
      chpsm.Member = member;
      return View(chpsm);
    }

    //
    // POST: /Account/ChangePassword

    //[Authorize]
    [HttpPost]
    public ActionResult ChangePassword(ChangePasswordModel model)
    {
      if (ModelState.IsValid)
      {

        // ChangePassword will throw an exception rather
        // than return false in certain failure scenarios.
        bool changePasswordSucceeded;
        try
        {
          
          
          changePasswordSucceeded = ms.ChangePassword(model.Member, model.OldPassword, model.NewPassword);
        }
        catch (Exception)
        {
          changePasswordSucceeded = false;
        }

        if (changePasswordSucceeded)
        {
          return RedirectToAction("ChangePasswordSuccess");
        }
        else
        {
          ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
        }
      }

      // If we got this far, something failed, redisplay form
      return View(model);
    }

    //
    // GET: /Account/ChangePasswordSuccess

    public ActionResult ChangePasswordSuccess()
    {
      return View();
    }
  }
}
