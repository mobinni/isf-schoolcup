﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Services;

namespace TeamOPSchoolCup.Web.ISFMembership
{
  public class ISFRoleProvider : RoleProvider
  {
    public override void AddUsersToRoles(string[] usernames, string[] roleNames)
    {
      foreach (String username in usernames) 
      {
        Member member = MemberService.GetMember(email: username);
        if (member == null) continue;

        foreach (String roleName in roleNames)
        {
          Role role = RoleService.GetRole(roleName); 
          if (role == null) continue;

          RoleService.AddUserToRole(username, roleName);
        }
      }
    }

    public override string ApplicationName
    {
      get
      {
        return this.GetType().Assembly.GetName().Name.ToString();
      }
      set
      {
        this.ApplicationName = this.GetType().Assembly.GetName().Name.ToString();
      }
    }

    public override void CreateRole(string roleName)
    {
      RoleService.AddRole(roleName);
    }

    public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
    {
      throw new NotImplementedException();
    }

    public override string[] FindUsersInRole(string roleName, string usernameToMatch)
    {
      throw new NotImplementedException();
    }

    public override string[] GetAllRoles()
    {
      return RoleService.GetRoles().Select(r => r.RoleName).ToArray<String>();
    }

    public override string[] GetRolesForUser(string username)
    {
      return RoleService.GetUserRoles(username).Select(r => r.RoleName).ToArray<String>();
    }

    public override string[] GetUsersInRole(string roleName)
    {
      throw new NotImplementedException();
    }

    public override bool IsUserInRole(string username, string roleName)
    {
      return RoleService.IsUserInRole(username, roleName);
    }

    public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
    {
      foreach (String username in usernames)
      {
        Member member = MemberService.GetMember(email: username);
        if(member == null) continue;

        foreach(String roleName in roleNames){
          Role role = RoleService.GetRole(roleName);
          if (role == null) continue;

          RoleService.RemoveUserFromRole(username, roleName);
        }
      }
    }

    public override bool RoleExists(string roleName)
    {
      return RoleService.RoleExists(roleName);
    }
  }
}