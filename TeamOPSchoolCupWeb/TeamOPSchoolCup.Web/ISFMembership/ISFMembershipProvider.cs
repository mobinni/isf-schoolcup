﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Resources;
using TeamOPSchoolCup.Domain.Services;

namespace TeamOPSchoolCup.Web.ISFMembership
{
  public class ISFMembershipProvider : MembershipProvider
  {
    public override string ApplicationName
    {
      get
      {
        return this.GetType().Assembly.GetName().Name.ToString();
      }
      set
      {
        this.ApplicationName = this.GetType().Assembly.GetName().Name.ToString();
      }
    }

    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    {
      if (string.IsNullOrEmpty(username)) return false;
      if (string.IsNullOrEmpty(oldPassword)) return false;
      if (string.IsNullOrEmpty(newPassword)) return false;

      Member member = MemberService.GetMember(email: username);
      if (member == null) return false;

      String hashedPass = member.Password;
      Boolean verificationSucceeded = (hashedPass != null && BCrypt.CheckPassword(oldPassword, member.Password));
      if (!verificationSucceeded) return false;

      String salt = BCrypt.GenerateSalt(12);
      String newHashedPass = BCrypt.HashPassword(newPassword, salt);
      if (newHashedPass.Length > 128) return false;

      return MemberService.ChangePassword(member, newHashedPass);
    }

    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    {
      throw new NotSupportedException();
    }

    public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    {
      if (string.IsNullOrEmpty(username))
      {
        status = MembershipCreateStatus.InvalidUserName;
        return null;
      }
      if (string.IsNullOrEmpty(password))
      {
        status = MembershipCreateStatus.InvalidPassword;
        return null;
      }
      if (string.IsNullOrEmpty(email))
      {
        status = MembershipCreateStatus.InvalidEmail;
        return null;
      }

      if (MemberService.GetMember(email: username) != null)
      {
        status = MembershipCreateStatus.DuplicateUserName;
        return null;
      }

      status = MembershipCreateStatus.Success;
      return new MembershipUser(Membership.Provider.Name, email, Guid.NewGuid(), email, null, null, isApproved, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);
    }

    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
      throw new NotSupportedException();
    }

    public override bool EnablePasswordReset
    {
      get { throw new NotSupportedException(); }
    }

    public override bool EnablePasswordRetrieval
    {
      get { throw new NotSupportedException(); }
    }

    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
      throw new NotSupportedException();
    }

    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
      throw new NotSupportedException();
    }

    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
      throw new NotSupportedException();
    }

    public override int GetNumberOfUsersOnline()
    {
      throw new NotSupportedException();
    }

    public override string GetPassword(string username, string answer)
    {
      throw new NotSupportedException();
    }

    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
      if (string.IsNullOrEmpty(username)) return null;
      Member member = MemberService.GetMember(email: username);
      if (member == null) throw new TeamOPSchoolCupException(ErrorStrings.MemberDoesntExistException);
      
      return new MembershipUser(Membership.Provider.Name, member.Email, member.MemberId, member.Email, null, null, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);
    }

    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
      if (!(providerUserKey is Guid)) return null;

      Member member = MemberService.GetMember(memberId: (Guid)providerUserKey);
      if (member == null) return null;

      return new MembershipUser(Membership.Provider.Name, member.Email, member.MemberId, member.Email, null, null, true, false, member.CreateDate.Value, member.LastLoginDate.Value, DateTime.Now, DateTime.Now, DateTime.Now);

    }

    public override string GetUserNameByEmail(string email)
    {
      throw new NotSupportedException();
    }

    public override int MaxInvalidPasswordAttempts
    {
      get { return 5; }
    }

    public override int MinRequiredNonAlphanumericCharacters
    {
      get { return 0; }
    }

    public override int MinRequiredPasswordLength
    {
      get { return 6; }
    }

    public override int PasswordAttemptWindow
    {
      get { return 0; }
    }

    public override MembershipPasswordFormat PasswordFormat
    {
      get { return MembershipPasswordFormat.Hashed; }
    }

    public override string PasswordStrengthRegularExpression
    {
      get { return String.Empty; }
    }

    public override bool RequiresQuestionAndAnswer
    {
      get { return false; }
    }

    public override bool RequiresUniqueEmail
    {
      get { return true; }
    }

    public override string ResetPassword(string username, string answer)
    {
      throw new NotSupportedException();
    }

    public override bool UnlockUser(string userName)
    {
      throw new NotSupportedException();
    }

    public override void UpdateUser(MembershipUser user)
    {
      throw new NotSupportedException();
    }

    public override bool ValidateUser(string username, string password)
    {
      if (string.IsNullOrEmpty(username))
      {
        return false;
      }
      if (string.IsNullOrEmpty(password))
      {
        return false;
      }
      Member member = MemberService.GetMember(email: username);
      if (member == null) return false;

      String hashedPassword = member.Password;
      Boolean verificationSucceeded = (hashedPassword != null && BCrypt.CheckPassword(password, hashedPassword));
      if (!verificationSucceeded) return false;
      if(Roles.IsUserInRole(username, "NotConfirmed")) throw new TeamOPSchoolCupException(ErrorStrings.NotConfirmed);

      MemberService.UpdateLastLoginDate(member);
      return true;
    }
  }
}