﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TeamOPSchoolCup.Web.Helpers
{
  public static class HtmlHelperExtensions
  {
    // Extension method
    public static MvcHtmlString ActionImage(this HtmlHelper html, string action, string controllerName, object routeValues, string imagePath, string alt,string linktext=null)
    {
      var url = new UrlHelper(html.ViewContext.RequestContext);

      // build the <img> tag
      var imgBuilder = new TagBuilder("img");
      imgBuilder.MergeAttribute("src", url.Content(imagePath));
      imgBuilder.MergeAttribute("alt", alt);
      string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

      // build the <a> tag
      var anchorBuilder = new TagBuilder("a");

      anchorBuilder.MergeAttribute("href", url.Action(action, controllerName, routeValues));
      anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
      if (linktext != null) { anchorBuilder.InnerHtml += "<span>" + linktext + "</span>"; }
      string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

      return MvcHtmlString.Create(anchorHtml);
    }
  }
}