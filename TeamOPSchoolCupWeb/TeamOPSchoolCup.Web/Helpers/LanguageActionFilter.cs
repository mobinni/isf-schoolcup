﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Services;
using System.Web.Routing;

namespace TeamOPSchoolCup.Web.Helpers
{
  public class LanguageActionFilter : FilterAttribute, IAuthorizationFilter
  {
    public void OnAuthorization(AuthorizationContext filterContext)
    {
      if (filterContext.IsChildAction) { return; }
      if (!filterContext.HttpContext.User.Identity.IsAuthenticated) { return; }
      Member mem = MemberService.GetMember(email: filterContext.HttpContext.User.Identity.Name);
      string lang=System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
      int langId=(int)Enum.Parse(typeof(Culture),lang,true);
      if (mem.Language != 0 && mem.Language != langId)
      {
        string newLang=Enum.GetName(typeof(Culture), mem.Language);
        if (String.IsNullOrEmpty(newLang)) { return; }
        var routeValueDictionary = new RouteValueDictionary(filterContext.RouteData.Values);
        if (routeValueDictionary.ContainsKey("culture")) {
          routeValueDictionary.Remove("culture");
        } 
        routeValueDictionary.Add("culture",newLang);

        filterContext.Result = new RedirectToRouteResult(routeValueDictionary);
      }
    }
  }
}