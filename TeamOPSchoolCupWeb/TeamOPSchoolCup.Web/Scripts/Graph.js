﻿ function drawMyResultsGraph(data,eventName) {
      if(data == null){
      return false;
      }
      var scData = data;
      var data2=new google.visualization.DataTable();
      data2.addColumn('date','Date');
      for(var i = 0; i < scData.Students.length;i++)
      {
      data2.addColumn('number',scData.Students[i].Name);
      }
      
     
      for (var i=0; i< scData.ComparisonLines.length; i++) {
        if( scData.ComparisonLines[i].EventName == eventName){
         var line = scData.ComparisonLines[i];
        }
      }
      if (line.ResultPerDate.length==1) {
        var specificResults=line.ResultPerDate[0];
        var barData=new google.visualization.DataTable();
        barData.addColumn('string','Result');
        for (var i=0; i<scData.Students.length; i++) { barData.addColumn('number',scData.Students[i].Name); }
        var date=new Date(parseInt(specificResults.Date.replace("/Date(", "").replace(")/",""), 10));
        var dataList=[date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()];
        for (var i=0; i<specificResults.Results.length; i++) { dataList.push(specificResults.Results[i]); }
        barData.addRow(dataList);
        var colChart=new google.visualization.ColumnChart(document.getElementById('chart'));
        colChart.draw(barData, { 'width': 500, 'height': 250,'legend':{'position':'right','alignment':'start'}});
        return true;
      } else {
      for (var i = 0; i< line.ResultPerDate.length;i++)
      {

       var result = line.ResultPerDate[i];
          var data3 = [new Date(parseInt(result.Date.replace("/Date(", "").replace(")/",""), 10))];
          
          for(var j = 0; j < result.Results.length;j++)
          {
          data3.push(result.Results[j])
          } 
          data2.addRow(data3);
           
      }
      
      console.log(data2);
      var linechart = new google.visualization.LineChart(document.getElementById('chart'));
      linechart.draw(data2, { 'width': 500, 'height': 250,'legend':{'position':'right','alignment':'start'}/*,'hAxis':{viewWindow: { min: new Date(new Date().getFullYear(), 0, 1), max: new Date(new Date().getFullYear(), 11, 31) }}*/});
     
     return true;
     }
}
   
//TeamComparison: points graph
//pointsEvents should be List<TeamComparison.PointEvent>
//target should be object like target={divId:'linechart_div',width:800,height:400}
function drawPointsGraph(pointEvents,target) {
    var data=new google.visualization.DataTable();
    data.addColumn('date','Date');
    data.addColumn('number',tcData.Team1.Name);
    data.addColumn('number',tcData.Team2.Name);

    var pointsT1=[];
    var pointsT2=[];
    for (var i=0; i<pointEvents.length; i++) {
        var pointEvent=pointEvents[i];
        date = new Date(parseInt(pointEvent.Date.replace("/Date(", "").replace(")/",""), 10));
        data.addRow([date,pointEvent.PointsT1,pointEvent.PointsT2]);
        if (pointEvent.PointsT1!= null) pointsT1.push(pointEvent.PointsT1);
        if (pointEvent.PointsT2!= null) pointsT2.push(pointEvent.PointsT2);
    }
    if (pointsT1.length<=1 || pointsT2.length<=1) {
        //Do a bar with the highest instead
        var barData=new google.visualization.DataTable();
        barData.addColumn('string','Points');
        barData.addColumn('number',tcData.Team1.Name);
        barData.addColumn('number',tcData.Team2.Name);
        barData.addRow(['Points',Math.max.apply(Math, pointsT1),Math.max.apply(Math, pointsT2)]);
        var colChart=new google.visualization.ColumnChart(document.getElementById(target.divId));
        colChart.draw(barData, {
            'width': target.width, 
            'height': target.height,
        });
    } else {
        var linechart = new google.visualization.LineChart(document.getElementById(target.divId));
        linechart.draw(data, {
            'width': target.width, 
            'height': target.height,
        });
    }
    return true;
}

//TeamComparison: teamresults graph
//teamResultEvents should be List<TeamComparison.TeamResultEvent>
//target should be object like target={divId:'trh_graph',width:800,height:400}
function drawTeamResultsGraph(wantedEvent,teamResultEvents,target) {
    var data=new google.visualization.DataTable();
    data.addColumn('date','Date');
    data.addColumn('number',tcData.Team1.Name);
    data.addColumn('number',tcData.Team2.Name);

    var resultsT1=[];
    var resultsT2=[];
    if (teamResultEvents.length==1) { return false; }
    for (var i=0; i<teamResultEvents.length; i++) {
        var teamResultEvent=teamResultEvents[i];
        date = new Date(parseInt(teamResultEvent.Date.replace("/Date(", "").replace(")/",""), 10));
        if (teamResultEvent.EventName==wantedEvent) {
            data.addRow([date,teamResultEvent.ResultT1,teamResultEvent.ResultT2]);
            if (teamResultEvent.ResultT1!=null) { resultsT1.push(teamResultEvent.ResultT1); }
            if (teamResultEvent.ResultT2!=null) { resultsT2.push(teamResultEvent.ResultT2); }
        }
    }
    if (resultsT1.length<=1 || resultsT2.length<=1) {
        //Do a bar with the highest instead
        var barData=new google.visualization.DataTable();
        barData.addColumn('string',wantedEvent);
        barData.addColumn('number',tcData.Team1.Name);
        barData.addColumn('number',tcData.Team2.Name);
        barData.addRow([wantedEvent,Math.max.apply(Math, resultsT1),Math.max.apply(Math, resultsT2)]);
        var colChart=new google.visualization.ColumnChart(document.getElementById(target.divId));
        colChart.draw(barData, {
            'width': target.width, 
            'height': target.height,
        });
    } else {
        var linechart = new google.visualization.LineChart(document.getElementById(target.divId));
        linechart.draw(data, {
            'width': target.width, 
            'height': target.height
        });
    }
    return true;
}