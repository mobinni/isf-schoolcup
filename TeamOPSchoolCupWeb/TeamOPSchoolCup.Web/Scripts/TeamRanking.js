﻿$(document).ready(function () {
    var nextPage = 0;
    var selectedItems = [];
    // set current year selected
    var d = new Date();
    $('#team_years').val(d.getFullYear());

    $('#team_rankings').hide();

    $('#team').click(function () {
        nextPage = 0; // reset pages
        if ($('#student_rankings').is(':visible')) {
            $('#student_rankings').hide();
        }
        if ($('#team_rankings').is(':visible')) {
            $('#team_rankings').hide();
            $('#team_rankingtable').empty();
            $('#student').show('blind', {}, 1000);
        } else {
            $('#team_rankings').show(1000);
            refreshTable();
            $('#student').hide('blind', {}, 1000);
        }
    });

    var language = $('body').attr('data-language');
    // Sports dropdown change
    $('#team_sports').change(function () {
        refreshTable();
    });

    // Events dropdown change
    $('#team_events').change(function () {
        refreshTable();
    });

    // Region dropdown change
    $('#team_regions').change(function () {
        refreshTable();
    });

    // Genders dropdown change
    $('#team_genders').change(function () {
        refreshTable();
    });

    // Year dropdown change
    $('#team_years').change(function () {
        refreshTable();
    });

    // next click
    $('#team_rankingtable').on('click', '#next', function () {
        nextPage++;
        refreshTable();
    });

    // previous click
    $('#team_rankingtable').on('click', '#previous', function () {
        nextPage--;
        refreshTable();
    });


    $('#team_rankingtable').on('click', '#team_compare', function () {
        var language = $('body').attr('data-language');

        setSelectedItems();

    });

    function setSelectedItems() {
        var selectedItem = []
        $('#team_rankingtable').find('input[type=checkbox]').each(function () {
            if ($(this).is(':checked')) {
                var isIn = false;

                if ($(this).attr('value') == selectedItem[0]) {
                    isIn = true;
                }

                if (!isIn) {
                    selectedItem.push($(this).attr('value'));
                }
            }
        });

        selectedItems = selectedItem;


    }
    $('#compareTeam').on('click', function () {
        setSelectedItems();
        compare();
    });
    function compare() {

        var currentSport = $('#team_sports').val();
        var currentYear = $('#team_years').val();
        var language = $('body').attr('data-language');
        if (selectedItems.length == 2) {
            window.location.href = '/' + language + '/Comparison/CompareTeam?id1=' + selectedItems[0] + '&id2=' + selectedItems[1];
        }

    }



    function refreshTable() {
        var currentSport = $('#team_sports').val();
        var currentRegion = $('#team_regions').val();
        var currentYear = $('#team_years').val();
        var currentGender = $('#team_genders').val();
        $.ajax({
            type: 'GET',
            url: 'Ranking/LoadTeamRankings?sportId=' + currentSport
            + '&gender=' + currentGender + '&year=' + currentYear + '&regionId=' + currentRegion + '&page=' + nextPage,
            success: function (data) {
                $('#team_rankingtable').html(data); //the HTML I returned from the controller
                setSelectedItems();
            },
            error: function (errorData) { onError(errorData); }
        });
    }

});