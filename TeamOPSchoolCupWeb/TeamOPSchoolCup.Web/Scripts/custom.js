﻿$(document).ready(function () {
    $search_box = $("#search_box");

    // If the user clicks the input box and the text is "search here!",
    //    set it to blank
    $search_box.focus(function () {
        if ($search_box.attr("value") == "Search..." || $search_box.attr("value") == "Zoek...") {

            // Set it to an empty string
            $search_box.attr("value", "");
        }
    });



    $search_box.blur(function () {
        if ($search_box.attr("value") == "") {
            $search_box.attr("value", "Search...");
        }
    });

    /* Log on dialog */

    // Dialog
    $("#login-form").dialog({
        autoOpen: false,
        height: 270,
        width: 350,
        modal: true,
        draggable: true,
        resizable: false,
        buttons: {
            Login: function () {
                $("#loginform").submit();
                // Check here if login was successful, if yes close, if not give feedback
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            },
        },
    });

    // Log on button
    $("#login-button").click(function () {
        $("#login-form").dialog("open");
    });

    /*
     * Languagepicker
     */
    (function($){
        var languagesVisible=false;
        
        var closeLanguageList=function() {
            languagesVisible=false;
            $("#languagelist").hide();
            $("#popupoverlay").hide().off("click","**");
        };

        var showLanguageList=function() {
            languagesVisible=true;
            $("#popupoverlay").height($(document).height()).show().on("click",closeLanguageList);
            var languagelist=$("#languagelist").show();
            languagelist.css("top","-"+(languagelist.outerHeight()+$("#currentlanguage").outerHeight())+"px");
        }

        $("#languagepicker").click(showLanguageList);
    }($));
});