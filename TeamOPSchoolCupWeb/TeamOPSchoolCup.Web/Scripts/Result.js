﻿$(document).ready(function () {
    if ($("#pagebody").hasClass("actionIndex")) {
        var nextPage = 0;
        // next click
        $('#result_table').on('click', '#next', function () {
            nextPage++;
            refreshTable();

        });

        // previous click
        $('#result_table').on('click', '#previous', function () {
            nextPage--;
            refreshTable();

        });

        function refreshTable() {
            $.ajax({
                type: 'GET',
                url: document.location.href + '/LoadResultTable?page=' + nextPage,
                success: function (data) {
                    $('#result_table').html(data); //the HTML I returned from the controller
                },
                error: function (errorData) { onError(errorData); }
            });
        }
    } else if ($("#pagebody").hasClass("actionCreate")) {
        var language = $("body").data("language");

        function resetTeams(row) {
            $(".teamsSelect[data-row='" + row + "']").html("").attr("disabled", "disabled");
        }
        function resetSchools(row) {
            $(".schoolsSelect[data-row='" + row + "']").html("").attr("disabled", "disabled");
        }
        function resetStudents(row) {
            $(".studentsSelect[data-row='" + row + "']").html("").attr("disabled", "disabled");
        }
        function watchChangeRegions() {
            $(".regionsSelect").on("change", function () {
                var $correctRegionSelect = $(this);
                if ($correctRegionSelect.val() == "") {
                    resetSchools($correctRegionSelect.data("row"));
                    return;
                }
                $.ajax({
                    url: "/" + language + "/Result/GetSchools?regionId=" + $correctRegionSelect.val(),
                    method: "GET",
                    success: function (data) {
                        if (data.length == 0) {
                            resetSchools($correctRegionSelect.data("row"));
                            return;
                        }
                        var optionsStr = "<option></option>";
                        for (var i = 0; i < data.length; i++) {
                            optionsStr += "<option value='" + data[i].Id + "'>" + data[i].Name + "</option>";
                        }
                        $(".schoolsSelect[data-row='" + $correctRegionSelect.data("row") + "']").html(optionsStr).removeAttr("disabled");
                    }
                });
            });
        }
        function watchChangeSchools() {
            $(".schoolsSelect").on("change", function () {
                var $correctSchoolSelect = $(this);
                if ($correctSchoolSelect.val() == "") {
                    resetTeams($correctSchoolSelect.data("row"));
                    return;
                }
                $.ajax({
                    url: "/" + language + "/Result/GetTeams?schoolId=" + $correctSchoolSelect.val() + "&sportId=" + sport,
                    method: "GET",
                    success: function (data) {
                        if (data.length == 0) {
                            resetTeams($correctSchoolSelect.data("row"));
                            return;
                        }
                        var optionsStr = "<option></option>";
                        for (var i = 0; i < data.length; i++) {
                            optionsStr += "<option value='" + data[i].Id + "'>" + data[i].Name + "</option>";
                        }
                        $(".teamsSelect[data-row='" + $correctSchoolSelect.data("row") + "']").html(optionsStr).removeAttr("disabled");
                    }
                });
            });
        }
        function watchChangeTeams() {
            $(".teamsSelect").on("change", function () {
                var $correctTeamSelect = $(this);
                if ($correctTeamSelect.val() == "") {
                    resetStudents($correctTeamSelect.data("row"));
                    return;
                }
                $.ajax({
                    url: "/" + language + "/Result/GetStudents?teamId=" + $correctTeamSelect.val(),
                    method: "GET",
                    success: function (data) {
                        if (data.length == 0) {
                            resetStudents($correctTeamSelect.data("row"));
                            return;
                        }
                        var optionsStr = "<option></option>";
                        for (var i = 0; i < data.length; i++) {
                            optionsStr += "<option value='" + data[i].Id + "'>" + data[i].Name + " (" + data[i].Email + ")</option>";
                        }
                        $(".studentsSelect[data-row='" + $correctTeamSelect.data("row") + "']").html(optionsStr).removeAttr("disabled");
                    }
                });
            });
        }

        function setEvents() {
            $(".regionsSelect").off();
            $(".schoolsSelect").off();
            $(".teamsSelect").off();
            if (createType == 1) {
                watchChangeRegions();
                watchChangeSchools();
                watchChangeTeams();
            } else if (createType == 2) {
                watchChangeSchools();
                watchChangeTeams();
            }
        }

        $(".actionLinksTop #add1 a").click(function (e) {
            e.stopPropagation();
            var nextRowStr = "<tr>" + $("#nextRowTempl").html() + "</tr>";
            nextRowStr = nextRowStr.replace(/\$\$ID\$\$/g, nextRow);
            $("tr.tablebottomactions").before(nextRowStr);
            nextRow += 1;
            setEvents();
            return false;
        });

        setEvents();
    } else if ($("#pagebody").hasClass("actionCreateSingle")) {
        var language = $("body").data("language");
        $("#TeamId").change(function () {
            //Now load meetings
            if ($("#TeamId").val() == "") { return true; }
            $.ajax({
                url: "/" + language + "/Result/GetMeetingsForTeam?teamId=" + $("#TeamId").val(),
                success: function (data) {
                    var htmlStr = "<option></option>";
                    for (var i = 0; i < data.length; i++) {
                        htmlStr += "<option value='" + data[i].Id + "'>" + data[i].Name + " (" + data[i].Date + ")</option>";
                    }
                    $("#MeetingId").html(htmlStr);
                }
            });
            if (memberType == "Coach") {
                $.ajax({
                    url: "/" + language + "/Result/GetStudentsForTeam?teamId=" + $("#TeamId").val(),
                    success: function (data) {
                        var htmlStr = "<option></option>";
                        for (var i = 0; i < data.length; i++) {
                            htmlStr += "<option value='" + data[i].Id + "'>" + data[i].Name + " (" + data[i].Email + ")</option>";
                        }
                        $("#MemberId").html(htmlStr);
                    }
                });
            }
        });
        $("#MeetingId").change(function () {
            if ($("#MeetingId").val() == "") { return true; }
            $.ajax({
                url: "/" + language + "/Result/GetEventsForMeeting?meetingId=" + $("#MeetingId").val(),
                success: function (data) {
                    var htmlStr = "<option></option>";
                    for (var i = 0; i < data.length; i++) {
                        htmlStr += "<option value='" + data[i].Id + "'>" + data[i].Name + "</option>";
                    }
                    $("#EventId").html(htmlStr);
                }
            });
        });
    }
});