﻿/*$(document).ready(function () {
    $("#year").change( function () {
        $("#filterform").submit();
    });
    $("#regionId").change(function () {
        $("#filterform").submit();
    });
});*/

$(document).ready(function () {
    var language = $('body').data('language');

    if ($("#pagebody").hasClass("actionIndex")) {
        var nextPage = 0;

        $("#year").change(function () {
            nextPage = 0;
            refreshTablePublicMeetings();
        });
        $("#regionId").change(function () {
            nextPage = 0;
            refreshTablePublicMeetings();
        });

        function setNextPreviousPublicMeetings() {
            $("#publicmeetings").off();
            $('#publicmeetings').on('click', '#next', function (e) {
                e.stopPropagation();
                nextPage++;
                refreshTablePublicMeetings();
                return false;
            });

            // previous click
            $('#publicmeetings').on('click', '#previous', function (e) {
                e.stopPropagation();
                nextPage--;
                refreshTablePublicMeetings();
                return false;
            });
        }
        setNextPreviousPublicMeetings();
        function refreshTablePublicMeetings() {
            $.ajax({
                type: 'GET',
                url: '/' + language + '/Meeting/GetMeetings?page=' + nextPage + '&year=' + $("#year").val() + '&regionId=' + $("#regionId").val(),
                success: function (data) {
                    $("#publicmeetings").html(data);
                    setNextPreviousPublicMeetings();
                }
            });
        }
    }

    if ($("#pagebody").hasClass("actionIndex") || ($("#pagebody").hasClass("controllerTeam") && $("#pagebody").hasClass("actionDashboard"))) {
        var nextPage = startingPage; //Only relevant the first time, so it's not a problem that it gets set only once

        function setNextPrevious() {
            $("#ownmeetings").off();
            $('#ownmeetings').on('click', '#next', function (e) {
                e.stopPropagation();
                nextPage++;
                refreshTable();
                return false;
            });

            // previous click
            $('#ownmeetings').on('click', '#previous', function (e) {
                e.stopPropagation();
                nextPage--;
                refreshTable();
                return false;
            });
        }
        setNextPrevious();
        function refreshTable() {
            var url = '/' + language + '/Meeting/OwnMeetings?page=' + nextPage;
            if ($("#pagebody").hasClass("controllerTeam") && $("#pagebody").hasClass("actionDashboard")) {
                url = '/' + language + '/Meeting/MeetingsForTeam?teamId=' + teamId + '&page=' + nextPage
            }
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#ownmeetings").html(data);
                    setNextPrevious();
                }
            });
        }
    }
    if ($("#pagebody .content-form").hasClass("hasDynatree")) {
        //These modify the initial data before we dynatree it
        var findIdInArr = function (id, arr) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].Id == id) {
                    return arr[i];
                }
                if (arr[i].Children != undefined) {
                    var childSearchResult = findIdInArr(id, arr[i].Children);
                    if (childSearchResult != null) {
                        return childSearchResult;
                    }
                }
            }
            return null;
        }

        var expandItem = function (reg, arr) {
            reg.expand = true;
            if (reg.ParentId != undefined && reg.ParentId != 1) { expandItem(findIdInArr(reg.ParentId, arr), arr); }
        }

        var selectItem = function (id, arr) {
            var result = findIdInArr(id, arr);
            result.select = true;
            expandItem(result, arr);
        }

        if ($("#pagebody .content-form").hasClass("hasDynatreeForEvents")) {
            function selectInitialEvents() {
                if (selectedEvents != "") {
                    var selSprArr = selectedEvents.split(",");
                    for (var i = 0; i < selSprArr.length; i++) {
                        selectItem(selSprArr[i], events);
                    }
                }
            }
            function makeEvents(partialEvents) {
                $("#eventsSelectAll").off();
                $("#eventsSelectNone").off();
                $("#eventsselectcontainer").empty().append("<ul id='eventsselect'></ul>");
                $("#eventsselect").dynatree({
                    children: partialEvents,
                    onSelect: function (select, node) {

                        // Get a list of all selected TOP nodes
                        var selRootNodes = node.tree.getSelectedNodes(true);
                        // ... and convert to a key array:
                        var selRootKeys = $.map(selRootNodes, function (node) {
                            return node.data.Id;
                        });
                        $("#hiddenevents").val(selRootKeys.join(","));
                    }
                });

                $("#eventsSelectAll").click(function (e) { $("#eventsselect").dynatree("getRoot").select(true); e.stopPropagation(); return false; });
                $("#eventsSelectNone").click(function (e) { $("#eventsselect").dynatree("getRoot").select(false); e.stopPropagation(); return false; });
            }
            $("#hiddenevents").val(selectedEvents);
            selectInitialEvents();
            if ($("#pagebody .content-form").hasClass("hasDynatreeForEventsNoSports")) {
                makeEvents(events);
            } else {
                $("#Sports").change(function (e) {
                    var partialEvents = [];
                    var wantedEvent = parseInt($("#Sports").val());
                    for (var i = 0; i < events.length; i++) {
                        if (events[i].SportId == wantedEvent) {
                            partialEvents.push(events[i]);
                        }
                    }
                    makeEvents(partialEvents);
                });
                $("#Sports").change();
            }
        }

        if ($("#pagebody .content-form").hasClass("hasDynatreeForRegions")) {
            function selectInitialRegions() {
                if (selectedRegions != "") {
                    if (selectedRegions == "1") {
                        for (var i = 0; i < regions.length; i++) {
                            regions[i].select = true;
                        }
                    } else {
                        var selRegArr = selectedRegions.split(",");
                        for (var i = 0; i < selRegArr.length; i++) {
                            selectItem(selRegArr[i], regions);
                        }
                    }

                }
            }
            function makeRegionsDynatree() {
                $("#regionselect").dynatree({
                    children: regions,
                    onSelect: function (select, node) {
                        // Get a list of all selected TOP nodes and convert to a key array:
                        var selRootKeys = $.map(node.tree.getSelectedNodes(true), function (node) {
                            return node.data.Id;
                        });
                        $("#hiddenregions").val(selRootKeys.join(","));
                        var foundCount = 0;
                        for (var i = 0; i < regions.length; i++) {
                            if ($.inArray(regions[i].Id, selRootKeys) !== -1) { foundCount++; }
                        }
                        if (foundCount == regions.length) { $("#hiddenregions").val("1"); } //World override
                    }
                });
            }
            $("#hiddenregions").val(selectedRegions);
            selectInitialRegions();
            makeRegionsDynatree();
            $("#regionsSelectAll").click(function (e) { $("#regionselect").dynatree("getRoot").select(true); e.stopPropagation(); return false; });
            $("#regionsSelectNone").click(function (e) { $("#regionselect").dynatree("getRoot").select(false); e.stopPropagation(); return false; });

            if (selectedRegions == "1") { $("#regionsSelectAll").click(); } //When we select all earlier, that doesn't always work... No clue why -- Wim

            $("#regionselect .dynatree-container").css("height", "150px").css("overflow-y", "scroll");
        }
    } else if ($("#pagebody").hasClass("actionDetail")) {
        var tab = '#tab_info';
        var nextPage = 0;
        var tabNum = 0;

        $('#tabs').tabs({
            activate: function (event, ui) {
                var active = $("#tabs").tabs("option", "active");
                determineTab(active);
            }
        });

        function determineTab(active) {
            nextPage = 0;
            tabNum = active;
            setEventHandlers();
        }

        function setEventHandlers() {
            // next click
            var selector = "#tabcontents div.resultTab:nth-child(" + (tabNum + 1) + ")";
            $(selector).off();
            $(selector).on('click', '#next', function (e) {
                e.stopPropagation();
                nextPage++;
                refreshTableDetailsPage();
                return false;
            });

            // previous click
            $(selector).on('click', '#previous', function (e) {
                e.stopPropagation();
                nextPage--;
                refreshTableDetailsPage();
                return false;
            });
        }

        function refreshTableDetailsPage() {
            var selector = "#tabcontents div.resultTab:nth-child(" + (tabNum + 1) + ")";
            var $tab = $(selector);
            $.ajax({
                url: "/" + language + "/Meeting/GetMeetingResults?meetingId=" + meetingId + "&eventId=" + $tab.attr("data-eventId") + "&gender=" + $tab.attr("data-gender") + "&page=" + nextPage,
                success: function (data) {
                    $tab.html(data);
                    setEventHandlers();
                }
            });
        }
    }
});