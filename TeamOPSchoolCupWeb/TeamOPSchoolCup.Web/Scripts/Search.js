﻿$(document).ready(function () {
    var nextPage = 0;
    var tab = '#tab_student';
    var controllerMethod = 'Student';
    setEventHandlers();


    $('#tabs').tabs({
        activate: function (event, ui) {
            var active = $("#tabs").tabs("option", "active");
            determineTab(active);
        }
    });

    function determineTab(active) {
        if (active == 0) {
            tab = '#tab_student';
            controllerMethod = 'Student';
            setEventHandlers();
        } else if (active == 1) {
            tab = '#tab_team';
            controllerMethod = 'Team';
            setEventHandlers();
        } else if (active == 2) {
            tab = '#tab_coach';
            controllerMethod = 'Coach';
            setEventHandlers();
        } else if (active == 3) {
            tab = '#tab_meeting';
            controllerMethod = 'Meeting';
            setEventHandlers();
        }
    }

    function setEventHandlers() {
        nextPage = 0;
        // next click
        $(tab).off();
        $(tab).on('click', '#next', function (e) {
            e.stopPropagation();
            nextPage++;
            refreshTable();
            return false;
        });

        // previous click
        $(tab).on('click', '#previous', function (e) {
            e.stopPropagation();
            nextPage--;
            refreshTable();
            return false;
        });
        refreshTable();
    }



    function refreshTable() {
        var search_query = $('#search_query').html();
        var language = $('body').attr('data-language');
        $.ajax({
            type: 'GET',
            url: '/' + language + '/Search/Load' + controllerMethod + 'Results?query=' + search_query + '&page=' + nextPage,
            success: function (data) {
                $(tab).html(data);
            },
            error: function (errorData) { onError(errorData); }
        });
    }

});