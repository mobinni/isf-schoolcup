﻿$(document).ready(function () {
    var nextPage = 0;
    var selectedItems = [];
    // set current year selected
    var d = new Date();
    $('#student_years').val(d.getFullYear());

    // show or hide rankings
    $('#student_rankings').hide();

    $('#student').click(function () {
        nextPage = 0; // reset page
        if ($('#team_rankings').is(':visible')) {
            $('#team_rankings').hide();
        }
        if ($('#student_rankings').is(':visible')) {
            $('#student_rankings').hide();
            $('#student_rankingtable').empty();
            $('#team').show('blind', {}, 1000);
        } else {
            $('#student_rankings').show(1000);
            refreshTable();
            $('#team').hide();
        }
    });



    var language = $('body').attr('data-language');
    // Sports dropdown change
    $('#student_sports').change(function () {
        var sportID = $('#student_sports').val();
        $.ajax({
            type: 'GET',
            url: '/' + language + '/Ranking/LoadEventsFromSport?id=' + sportID,
            success: function (data) {
                //alert(data.toString());
                $('#student_events').empty();
                $('#student_events').html(data); //the HTML I returned from the controller
                refreshTable();
            },
            error: function (errorData) { onError(errorData); }
        });
    });

    // Events dropdown change
    $('#student_events').change(function () {
        refreshTable();
    });

    // Region dropdown change
    $('#student_regions').change(function () {
        refreshTable();
    });

    // Genders dropdown change
    $('#student_genders').change(function () {
        refreshTable();
    });

    // Meeting Type dropdown change
    $('#student_meetings').change(function () {
        refreshTable();
    });

    // Year dropdown change
    $('#student_years').change(function () {
        refreshTable();
    });

    // Ages dropdown change
    $('#student_ages').change(function () {
        refreshTable();
    });

    // next click
    $('#student_rankingtable').on('click', '#next', function () {
        nextPage++;
        refreshTable();

    });

    // previous click
    $('#student_rankingtable').on('click', '#previous', function () {
        nextPage--;
        refreshTable();

    });

    $('#student_rankingtable').on('click', '#student_compare', function () {
        var currentYear = $('#student_years').val();
        var language = $('body').attr('data-language');
        setSelectedItems();

    });

    $('#compare_me').on('click', function () {

        setSelectedItems();

    });

    function setSelectedItems() {
        var selectedItem = []
        if ($('#compare_me').is(':checked')) {
            selectedItem.push($('#compare_me').attr('value'));
        }
        $('#student_rankingtable').find('input[type=checkbox]').each(function () {
            if ($(this).is(':checked')) {
                var isIn = false;

                if ($(this).attr('value') == selectedItem[0]) {
                    isIn = true;
                }

                if (!isIn) {
                    selectedItem.push($(this).attr('value'));
                }
            }
        });

        selectedItems = selectedItem;


    }
    $('#compare').on('click', function () {
        setSelectedItems();
        compare();
    });
    function compare() {

        var currentSport = $('#student_sports').val();
        var currentYear = $('#student_years').val();
        var language = $('body').attr('data-language');
        if (selectedItems.length == 2) {
            window.location.href = '/' + language + '/Comparison/Compare?memberIds=' + selectedItems.join().toString() + '&sportId='
                + currentSport + '&year=' + currentYear;
        }

    }
    function refreshTable() {

        var language = $('body').attr('data-language');
        var currentSport = $('#student_sports').val();
        var currentEvent = $('#student_events').val();
        var currentRegion = $('#student_regions').val();
        var currentYear = $('#student_years').val();
        var currentGender = $('#student_genders').val();
        var currentAge = $('#student_ages').val();
        $.ajax({
            type: 'GET',
            url: 'Ranking/LoadStudentRankings?pEventId=' + currentEvent + '&year=' + currentYear +
            '&sportId=' + currentSport + '&regionId=' + currentRegion + '&gender=' + currentGender + '&age=' + currentAge
                       + '&page=' + nextPage,
            success: function (data) {
                $('#student_rankingtable').html(data); //the HTML I returned from the controller


            },
            error: function (errorData) { onError(errorData); }
        });
    }


});