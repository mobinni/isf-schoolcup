﻿var gender_value= 1;
var sportID = 0;
$(document).ready(function () {
    var nextPage = 0;
    // set current year selected


    var language = $('body').attr('data-language');
    // Sports dropdown change
    $('#SportId').change(function () {

        sportID = $('#SportId').val();
        check();
    });
    $('#Gender').change(function () {

       
        if ("MALE" == $('#Gender').val()) {
            gender_value = 1;
        }
        if ("FEMALE" == $('#Gender').val()) {
            gender_value = 2;
        }
        check();
    });

    function check() {
        if (sportID != 0) {

            SetEventSelect();
        }
    }


    function SetEventSelect() {

        $.ajax({
            type: 'GET',
            url: 'LoadEventsFromSportAndGender?sportId=' + sportID + '&gender_value=' + gender_value,
            success: function (data) {

               
                $("#eventSelect").empty();
                var select = document.getElementById("eventSelect");

                for (var i = 0; i < data.EventInfos.length; i++) {

                    var opt = data.EventInfos[i];
                    
                    var el = document.createElement("option");
                    el.text = opt.EventName;
                    el.id = opt.EventId;
                    el.value = opt.EventId;
                    select.appendChild(el);
                    
                    
                }
                
            },
            error: function (errorData) { onError(errorData); }
        });

    }


});