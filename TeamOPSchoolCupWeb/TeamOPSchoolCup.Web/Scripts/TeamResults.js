﻿var teamID = 0;
var memberID = 0;
function onLoad(teamId,memberId) {
    teamID = teamId;
    memberID = memberId;
}
$(document).ready(function () {
    var nextPage = 0;
    var isOfficial = null;
    // set current year selected


   
    // Team dropdown change
    $('#Teams').change(function () {
        var teamID = $('#Teams').val();
        $.ajax({
            type: 'GET',
            url: '/' + language + '/SchoolCup/LoadMeetingsFromTeam?id=' + teamID,
            success: function (data) {
                //alert(data.toString());
                $('#Meetings').empty();
                $('#Meetings').html(data); //the HTML I returned from the controller
                refreshTable();
            },
            error: function (errorData) { onError(errorData); }
        });
    });
    // Meeting dropdown change
    $('#Meetings').change(function () {
        var meetingID = $('#Meetings').val();
        $.ajax({
            type: 'GET',
            url: '/' + language + '/SchoolCup/LoadEventsFromMeeting?id=' + meetingID,
            success: function (data) {
                //alert(data.toString());
                $('#Events').empty();
                $('#Events').html(data); //the HTML I returned from the controller
                refreshTable();
            },
            error: function (errorData) { onError(errorData); }
        });
    });
    // Event Type dropdown change
    $('#Events').change(function () {
        refreshTable();
    });



    // next click
    $('#myResultDiv').on('click', '#next', function () {
        nextPage++;
        refreshTable();

    });

    // previous click
    $('#myResultDiv').on('click', '#previous', function () {
        nextPage--;
        refreshTable();

    });
    // Official checkbox change

    $('#Official').change(function () {
        if ($('#Official').val() == "true") {
            isOfficial = true;
        }
        if ($('#Official').val() == "false") {
            isOfficial = false;
        }
        if ($('#Official').val() == "null") {
            isOfficial = null
        }
        refreshTable();
    });



    function refreshTable() {

        var language = $('body').attr('data-language');
        var currentSport = $('#Sports').val();
        var currentEvent = $('#Events').val();
        var currentMeeting = $('#Meetings').val();
        var currentOfficial = $('#Official').val();

        $.ajax({
            type: 'GET',
            url: 'LoadTeamResult?teamId=' + teamID + '&official=' + isOfficial + '&sportId=' + currentSport +
            '&meetingId=' + currentMeeting + '&pEventId=' + currentEvent + '&page=' + nextPage,
            success: function (data) {

                $('#myResultDiv').html(data); //the HTML I returned from the controller


            },
            error: function (errorData) { onError(errorData); }
        });
    }


});