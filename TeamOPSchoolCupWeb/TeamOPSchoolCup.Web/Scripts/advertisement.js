﻿$(document).ready(function () {

    var findIdInArr = function (id, arr) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].Id == id) {
                return arr[i];
            }
            if (arr[i].Children != undefined) {
                var childSearchResult = findIdInArr(id, arr[i].Children);
                if (childSearchResult != null) {
                    return childSearchResult;
                }
            }
        }
        return null;
    }

    var expandItem = function (reg, arr) {
        reg.expand = true;
        if (reg.ParentId != undefined && reg.ParentId != 1) { expandItem(findIdInArr(reg.ParentId, arr), arr); }
    }

    var selectItem = function (id, arr) {
        var result = findIdInArr(id, arr);
        result.select = true;
        expandItem(result, arr);
    }

    $("#hiddenregions").val(selectedRegions);
    $("#hiddensports").val(selectedSports);

    if (selectedRegions != "") {
        if (selectedRegions == "1") {
            for (var i = 0; i < regions.length; i++) {
                regions[i].select = true;
            }
        } else {
            var selRegArr = selectedRegions.split(",");
            for (var i = 0; i < selRegArr.length; i++) {
                selectItem(selRegArr[i], regions);
            }
        }

    }

    if (selectedSports != "") {
        var selSprArr = selectedSports.split(",");
        for (var i = 0; i < selSprArr.length; i++) {
            selectItem(selSprArr[i], sports);
        }
    }

    $("#regionselect").dynatree({
        children: regions,
        onSelect: function (select, node) {
            // Get a list of all selected TOP nodes
            var selRootNodes = node.tree.getSelectedNodes(true);
            // ... and convert to a key array:
            var selRootKeys = $.map(selRootNodes, function (node) {
                return node.data.Id;
            });
            $("#hiddenregions").val(selRootKeys.join(","));
            var foundCount = 0;
            for (var i = 0; i < regions.length; i++) {
                if ($.inArray(regions[i].Id, selRootKeys) !== -1) { foundCount++; }
            }
            if (foundCount == regions.length) { $("#hiddenregions").val("1"); } //World override
        }
    });

    $("#sportselect").dynatree({
        children: sports,
        onSelect: function (select, node) {

            // Get a list of all selected TOP nodes
            var selRootNodes = node.tree.getSelectedNodes(true);
            // ... and convert to a key array:
            var selRootKeys = $.map(selRootNodes, function (node) {
                return node.data.Id;
            });
            $("#hiddensports").val(selRootKeys.join(","));
        }
    });

    $("#regionsSelectAll").click(function (e) { $("#regionselect").dynatree("getRoot").select(true); e.stopPropagation(); return false; });
    $("#regionsSelectNone").click(function (e) { $("#regionselect").dynatree("getRoot").select(false); e.stopPropagation(); return false; });
    $("#sportsSelectAll").click(function (e) { $("#sportselect").dynatree("getRoot").select(true); e.stopPropagation(); return false; });
    $("#sportsSelectNone").click(function (e) { $("#sportselect").dynatree("getRoot").select(false); e.stopPropagation(); return false; });

    if (selectedRegions == "1") { $("#regionsSelectAll").click(); } //When we select all earlier, that doesn't always work... No clue why -- Wim

    $("#regionselect .dynatree-container").css("height", "150px").css("overflow-y", "scroll");
});