﻿/*************************************************************************
jquery.dynatree.js
Dynamic tree view control, with support for lazy loading of branches.

Copyright (c) 2006-2013, Martin Wendt (http://wwWendt.de)
Dual licensed under the MIT or GPL Version 2 licenses.
http://code.google.com/p/dynatree/wiki/LicenseInfo

A current version and some documentation is available at
http://dynatree.googlecode.com/

$Version: 1.2.4$
$Revision: 644, 2013-02-12 21:39:36$

@depends: jquery.js
@depends: jquery.ui.core.js
*/

(function ($) {

    /*************************************************************************
    *	Common tool functions.
    */

    var Class = {
        create: function () {
            return function () {
                this.initialize.apply(this, arguments);
            };
        }
    };

    /** Compare two dotted version strings (like '10.2.3').
    * @returns {Integer} 0: v1 == v2, -1: v1 < v2, 1: v1 > v2
    */
    function versionCompare(v1, v2) {
        var v1parts = ("" + v1).split("."),
		v2parts = ("" + v2).split("."),
		minLength = Math.min(v1parts.length, v2parts.length),
		p1, p2, i;
        // Compare tuple pair-by-pair.
        for (i = 0; i < minLength; i++) {
            // Convert to integer if possible, because "8" > "10".
            p1 = parseInt(v1parts[i], 10);
            p2 = parseInt(v2parts[i], 10);
            if (isNaN(p1)) { p1 = v1parts[i]; }
            if (isNaN(p2)) { p2 = v2parts[i]; }
            if (p1 == p2) {
                continue;
            } else if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            }
            // one operand is NaN
            return NaN;
        }
        // The longer tuple is always considered 'greater'
        if (v1parts.length === v2parts.length) {
            return 0;
        }
        return (v1parts.length < v2parts.length) ? -1 : 1;
    }


    /*************************************************************************
    *	Class DynaTreeNode
    */
    var DynaTreeNode = Class.create();

    DynaTreeNode.prototype = {
        initialize: function (parent, tree, data) {
            this.parent = parent;
            this.tree = tree;
            this.data = data;
            this.li = null; // not yet created
            this.span = null; // not yet created
            this.ul = null; // not yet created
            this.childList = null; // no subnodes yet
            this.hasSubSel = false;
            this.bExpanded = false;
            this.bSelected = false;
        },

        render: function (useEffects, includeInvisible) {
            var parent = this.parent, cn = this.tree.options.classNames;

            if (!parent && !this.ul) {
                // Root node has only a <ul>
                this.li = this.span = null;
                this.ul = document.createElement("ul");
                this.ul.className = "dynatree-container";
            } else if (parent) {
                // Create <li><span /> </li>
                if (!this.li) {
                    this.li = document.createElement("li");
                    this.li.dtnode = this;
                    this.span = document.createElement("span");
                    this.span.className = cn.title;
                    this.li.appendChild(this.span);

                    if (!parent.ul) {
                        // This is the parent's first child: create UL tag
                        // (Hidden, because it will be
                        parent.ul = document.createElement("ul");
                        parent.ul.style.display = "none";
                        parent.li.appendChild(parent.ul);
                    }

                    parent.ul.appendChild(this.li);
                }
                this.span.innerHTML = "<span class='" + (this.hasChildren() === true ? cn.expander : "dynatree-connector") + "'></span>" +
				"<span class='" + cn.checkbox + "'></span>" + '<span href="#" class="' + cn.title + '">' + this.data.Name + '</span>';
                // Set classes for current status
                var cnList = [];
                cnList.push(cn.node);
                if (this.bExpanded) { cnList.push("dynatree-expanded"); }
                if (this.hasChildren() !== false) { cnList.push("dynatree-has-children"); }
                if (this.bSelected) { cnList.push(cn.selected); }
                if (this.hasSubSel) { cnList.push(cn.partsel); }
                this.span.className = cnList.join(" ");
            }
            // Visit child nodes
            if ((this.bExpanded || includeInvisible === true) && this.childList) {
                for (var i = 0, l = this.childList.length; i < l; i++) {
                    this.childList[i].render(false, includeInvisible);
                }
            }
            // Hide children, if node is collapsed
            if (this.ul) { this.ul.style.display = (this.bExpanded || !parent) ? "" : "none"; }
        },

        /** Check if node has children (returns undefined, if not sure). */
        hasChildren: function () { return !!this.childList; },

        _parentList: function (includeRoot, includeSelf) {
            var l = [];
            var dtn = includeSelf ? this : this.parent;
            while (dtn) {
                if (includeRoot || dtn.parent) {
                    l.unshift(dtn);
                }
                dtn = dtn.parent;
            }
            return l;
        },
        getLevel: function () {
            /**
            * Return node depth. 0: System root node, 1: visible top-level node.
            */
            var level = 0;
            var dtn = this.parent;
            while (dtn) {
                level++;
                dtn = dtn.parent;
            }
            return level;
        },

        _getTypeForOuterNodeEvent: function (event) {
            /** Return the inner node span (title, checkbox or expander) if
            *  event.target points to the outer span.
            *  This function should fix issue #93:
            *  FF2 ignores empty spans, when generating events (returning the parent instead).
            */
            var cns = this.tree.options.classNames;
            var target = event.target;
            // Only process clicks on an outer node span (probably due to a FF2 event handling bug)
            if (target.className.indexOf(cns.node) < 0) { return null; }
            // Event coordinates, relative to outer node span:
            var eventX = event.pageX - target.offsetLeft;
            var eventY = event.pageY - target.offsetTop;

            for (var i = 0, l = target.childNodes.length; i < l; i++) {
                var cn = target.childNodes[i];
                var x = cn.offsetLeft - target.offsetLeft;
                var y = cn.offsetTop - target.offsetTop;
                var nx = cn.clientWidth, ny = cn.clientHeight;
                if (eventX >= x && eventX <= (x + nx) && eventY >= y && eventY <= (y + ny)) {
                    if (cn.className == cns.expander) { return "expander"; }
                    else if (cn.className == cns.checkbox) { return "checkbox"; }
                }
            }
            return null;
        },

        getEventTargetType: function (event) {
            // Return the part of a node, that a click event occured on.
            // Note: there is no check, if the event was fired on THIS node.
            var tcn = event && event.target ? event.target.className : "",
			cns = this.tree.options.classNames;

            if (tcn === cns.expander) { return "expander"; }
            else if (tcn === cns.checkbox) { return "checkbox"; }
            else if (tcn.indexOf(cns.node) >= 0) { return this._getTypeForOuterNodeEvent(event); }
            return null;
        },

        isVisible: function () {
            // Return true, if all parents are expanded.
            var parents = this._parentList(true, false);
            for (var i = 0, l = parents.length; i < l; i++) {
                if (!parents[i].bExpanded) { return false; }
            }
            return true;
        },

        _setSubSel: function (hasSubSel) {
            if (hasSubSel) {
                this.hasSubSel = true;
                $(this.span).addClass(this.tree.options.classNames.partsel);
            } else {
                this.hasSubSel = false;
                $(this.span).removeClass(this.tree.options.classNames.partsel);
            }
        },
        /**
        * Fix selection and partsel status, of parent nodes, according to current status of
        * end nodes.
        */
        _updatePartSelectionState: function () {
            var sel;
            // Return `true` or `false` for end nodes and remove part-sel flag
            if (!this.hasChildren()) {
                sel = (this.bSelected);
                this._setSubSel(false);
                return sel;
            }
            // Return `true`, `false`, or `undefined` for parent nodes
            var i, l,
			cl = this.childList,
			allSelected = true,
			allDeselected = true;
            for (i = 0, l = cl.length; i < l; i++) {
                var n = cl[i],
				s = n._updatePartSelectionState();
                if (s !== false) {
                    allDeselected = false;
                }
                if (s !== true) {
                    allSelected = false;
                }
            }
            if (allSelected) { sel = true; }
            else if (allDeselected) { sel = false; }
            else { sel = undefined; }

            this._setSubSel(sel === undefined);
            this.bSelected = (sel === true);
            return sel;
        },

        /**
        * Fix selection status, after this node was (de)selected in multi-hier mode.
        * This includes (de)selecting all children.
        */
        _fixSelectionState: function () {
            var p, i, l;
            if (this.bSelected) {
                // Select all children
                this.visit(function (node) {
                    node.parent._setSubSel(true);
                    node._select(true, false, false);
                });
                // Select parents, if all children are selected
                p = this.parent;
                while (p) {
                    p._setSubSel(true);
                    var allChildsSelected = true;
                    for (i = 0, l = p.childList.length; i < l; i++) {
                        var n = p.childList[i];
                        if (!n.bSelected) {
                            allChildsSelected = false;
                            break;
                        }
                    }
                    if (allChildsSelected) { p._select(true, false, false); }
                    p = p.parent;
                }
            } else {
                // Deselect all children
                this._setSubSel(false);
                this.visit(function (node) {
                    node._setSubSel(false);
                    node._select(false, false, false);
                });
                // Deselect parents, and recalc hasSubSel
                p = this.parent;
                while (p) {
                    p._select(false, false, false);
                    var isPartSel = false;
                    for (i = 0, l = p.childList.length; i < l; i++) {
                        if (p.childList[i].bSelected || p.childList[i].hasSubSel) {
                            isPartSel = true;
                            break;
                        }
                    }
                    p._setSubSel(isPartSel);
                    p = p.parent;
                }
            }
        },

        _select: function (sel, fireEvents, deep) {
            // Select - but not focus - this node.
            var opts = this.tree.options;
            if (this.bSelected === sel) { return; }

            this.bSelected = sel;
            $(this.span).toggleClass(opts.classNames.selected);
            if (deep) { this._fixSelectionState(); }
            if (fireEvents && opts.onSelect) { opts.onSelect.call(this.tree, !!sel, this); }
        },

        select: function (sel) { return this._select(sel !== false, true, true); },
        toggleSelect: function () { return this.select(!this.bSelected); },
        isSelected: function () { return this.bSelected; },

        _expand: function (bExpand, forceSync) {
            if (this.bExpanded === bExpand) { return; }
            if (!bExpand && this.getLevel() < 1) { return; }
            this.bExpanded = bExpand;

            this.render(true);
        },

        isExpanded: function () { return this.bExpanded; },

        expand: function (flag) {
            flag = (flag !== false);
            if (!this.childList && !false && flag) {
                return; // Prevent expanding empty nodes
            } else if (this.parent === null && !flag) {
                return; // Prevent collapsing the root
            }
            this._expand(flag);
        },

        toggleExpand: function () { this.expand(!this.bExpanded); },

        _onClick: function (event) {
            var targetType = this.getEventTargetType(event);
            if (targetType === "expander") { this.toggleExpand(); }
            else if (targetType === "checkbox") { this.toggleSelect(); }
            event.preventDefault();
        },

        visit: function (fn, includeSelf) {
            // Call fn(node) for all child nodes. Stop iteration, if fn() returns false.
            var res = true;
            if (includeSelf === true) {
                res = fn(this);
                if (res === false || res == "skip") {
                    return res;
                }
            }
            if (this.childList) {
                for (var i = 0, l = this.childList.length; i < l; i++) {
                    res = this.childList[i].visit(fn, true);
                    if (res === false) {
                        break;
                    }
                }
            }
            return res;
        },

        _addChildNode: function (dtnode, beforeNode) {
            var tree = this.tree;

            // --- Update and fix dtnode attributes if necessary
            dtnode.parent = this;

            // --- Add dtnode as a child
            if (this.childList === null) { this.childList = []; }
            if (beforeNode) {
                var iBefore = $.inArray(beforeNode, this.childList);
                if (iBefore < 0) { throw "<beforeNode> must be a child of <this>"; }
                this.childList.splice(iBefore, 0, dtnode);
            } else { this.childList.push(dtnode); }

            dtnode.bExpanded = (dtnode.data.expand === true);
            dtnode.bSelected = (dtnode.data.select === true);

            if (1 >= dtnode.getLevel()) { this.bExpanded = true; }


            // In multi-hier mode, update the parents selection state
            if (dtnode.bSelected) {
                var p = this;
                while (p) {
                    if (!p.hasSubSel) { p._setSubSel(true); }
                    p = p.parent;
                }
            }
            // render this node and the new child
            if (tree.bEnableUpdate) { this.render(); }
            return dtnode;
        },

        addChild: function (obj, beforeNode) {
            if (typeof (obj) == "string") { throw "Invalid data type for " + obj; }
            else if (!obj || obj.length === 0) { return; }
            else if (obj instanceof DynaTreeNode) { return this._addChildNode(obj, beforeNode); }

            if (!obj.length) { /*Passed a single data object*/obj = [obj]; }
            var prevFlag = this.tree.enableUpdate(false);

            var tnFirst = null;
            for (var i = 0, l = obj.length; i < l; i++) {
                var data = obj[i];
                var dtnode = this._addChildNode(new DynaTreeNode(this, this.tree, data), beforeNode);
                if (!tnFirst) { tnFirst = dtnode; }
                // Add child nodes recursively
                if (data.Children) { dtnode.addChild(data.Children, null); }
            }
            this.tree.enableUpdate(prevFlag);
            return tnFirst;
        }
    };

    /*************************************************************************
    * class DynaTree
    */
    var DynaTree = Class.create();
    DynaTree.version = "$Version: 1.2.4$";
    DynaTree.prototype = {
        // Constructor
        //	initialize: function(divContainer, options) {
        initialize: function ($widget) {
            // instance members
            this.$widget = $widget;
            this.options = $widget.options;
            this.$tree = $widget.element;
            this.timer = null;
            // find container element
            this.divTree = this.$tree.get(0);
        },

        // member functions

        _load: function (callback) {
            var opts = this.options;
            this.bEnableUpdate = true;
            this._nodeCount = 1;

            $(this.divTree).empty();

            // Create the root element
            this.tnRoot = new DynaTreeNode(null, this, {});
            this.tnRoot.bExpanded = true;
            this.tnRoot.render();
            this.divTree.appendChild(this.tnRoot.ul);

            var root = this.tnRoot, prevFlag = this.enableUpdate(false);

            root.addChild(opts.children); // Read structure from node array
            root._updatePartSelectionState(); // Fix part-sel flags
            this.enableUpdate(prevFlag); // Render html markup
            this.$widget.bind(); // bind event handlers

            // Set focus, if possible (this will also fire an event and write a cookie)
            if (callback) { callback.call(this, "ok"); }
        },

        redraw: function () { this.tnRoot.render(false, false); },
        enable: function () { this.$widget.enable(); },
        disable: function () { this.$widget.disable(); },

        getRoot: function () {
            return this.tnRoot;
        },

        getSelectedNodes: function (stopOnParents) {
            var nodeList = [];
            this.tnRoot.visit(function (node) {
                if (node.bSelected) {
                    nodeList.push(node);
                    if (stopOnParents === true) {
                        return "skip"; // stop processing this branch
                    }
                }
            });
            return nodeList;
        },

        enableUpdate: function (bEnable) {
            if (this.bEnableUpdate == bEnable) { return bEnable; }
            this.bEnableUpdate = bEnable;
            if (bEnable) { this.redraw(); }
            return !bEnable; // return previous value
        },

        visit: function (fn, includeRoot) { return this.tnRoot.visit(fn, includeRoot); }
    };

    /*************************************************************************
    * Widget $(..).dynatree
    */

    $.widget("ui.dynatree", {
        _init: function () {
            if (versionCompare($.ui.version, "1.8") < 0) { return this._create(); }
        },

        _create: function () {
            // The widget framework supplies this.element and this.options.
            this.options.event += ".dynatree"; // namespace event
            // Create the DynaTree object
            this.tree = new DynaTree(this);
            this.tree._load();
        },

        bind: function () {
            // Prevent duplicate binding
            this.unbind();

            var eventNames = "click.dynatree";
            this.element.bind(eventNames, function (event) {
                var dtnode = $.ui.dynatree.getNode(event.target);
                if (!dtnode) {
                    return true;  // Allow bubbling of other events
                }
                try {
                    if (event.type == "click") { return dtnode._onClick(event); }
                } catch (e) {
                    var _ = null; // issue 117
                }
            });
        },

        unbind: function () { this.element.unbind(".dynatree"); },

        enable: function () {
            this.bind();
            // Call default disable(): remove -disabled from css:
            $.Widget.prototype.enable.apply(this, arguments);
        },

        disable: function () {
            this.unbind();
            // Call default disable(): add -disabled to css:
            $.Widget.prototype.disable.apply(this, arguments);
        },

        // --- getter methods (i.e. NOT returning a reference to $)
        getRoot: function () { return this.tree.getRoot(); },
        getSelectedNodes: function () { return this.tree.getSelectedNodes(); }
    });


    // The following methods return a value (thus breaking the jQuery call chain):
    if (versionCompare($.ui.version, "1.8") < 0) { $.ui.dynatree.getter = "getSelectedNodes getRoot"; }

    /*******************************************************************************
    * Tools in ui.dynatree namespace
    */
    $.ui.dynatree.version = "$Version: 1.2.4$";

    /**
    * Return a DynaTreeNode object for a given DOM element
    */
    $.ui.dynatree.getNode = function (el) {
        if (el instanceof DynaTreeNode) { return el; }
        if (el.selector !== undefined) { el = el[0]; } // el was a jQuery object: use the DOM element
        // TODO: for some reason $el.parents("[dtnode]") does not work (jQuery 1.6.1)
        // maybe, because dtnode is a property, not an attribute
        while (el) {
            if (el.dtnode) { return el.dtnode; }
            el = el.parentNode;
        }
        return null;
    };

    /*******************************************************************************
    * Plugin default options:
    */
    $.ui.dynatree.prototype.options = {
        children: null, // Init tree structure from this object array.
        onSelect: null,

        classNames: {
            node: "dynatree-node",
            expander: "dynatree-expander",
            connector: "dynatree-connector",
            checkbox: "dynatree-checkbox",
            title: "dynatree-title",
            selected: "dynatree-selected",
            partsel: "dynatree-partsel"
        }
    };
    //
    if (versionCompare($.ui.version, "1.8") < 0) { $.ui.dynatree.defaults = $.ui.dynatree.prototype.options; }
} (jQuery));