﻿var gender_value = 1;
var tab = '#male';

$(document).ready(function () {
    var nextPage = 0;
    // set current year selected

    $('#tabs').tabs({
        activate: function (event, ui) {
            var active = $("#tabs").tabs("option", "active");
            determineTab(active);

        }
    });

    function determineTab(active) {
        nextPage = 0;
        if (active == 0) {
            tab = '#male';
            gender_value = 1;
        } else if (active == 1) {
            tab = '#female';
            gender_value = 2;
        }
    }
    var language = $('body').attr('data-language');
    // Sports dropdown change
    $('#male').find('#Sports').change(function () {
        gender_value = 1;
        var sportID = $('#male').find('#Sports').val();
        if (sportID == "") {
            sportID = null;
        }
        $.ajax({
            type: 'GET',
            url: '/' + language + '/Record/LoadEventsFromSport?id=' + sportID + '&gender_value=' + gender_value,
            success: function (data) {
                //alert(data.toString());
                $('#male').find('#Events').empty();
                $('#male').find('#Events').html(data); //the HTML I returned from the controller
                refreshTable();
            },
            error: function (errorData) { onError(errorData); }
        });
        $.ajax({
            type: 'GET',
            url: '/' + language + '/Record/LoadRecordHoldersFromSport?id=' + sportID + '&gender_value=' + gender_value,
            success: function (data) {
                //alert(data.toString());
                $('#male').find('#RecordHolders').empty();
                $('#male').find('#RecordHolders').html(data); //the HTML I returned from the controller
                refreshTable();
            },
            error: function (errorData) { onError(errorData); }
        });
    });
    $('#male').find('#Events').change(function () {
        gender_value = 1;
        var eventID = $('#male').find('#Events').val();
        if (eventID == "") {
            eventID = null;
            var sportID = $('#male').find('#Sports').val();
            if (sportID == "") {
                sportID = null;
            }
            $.ajax({
                type: 'GET',
                url: '/' + language + '/Record/LoadRecordHoldersFromSport?id=' + sportID + '&gender_value=' + gender_value,
                success: function (data) {
                    //alert(data.toString());
                    $('#male').find('#RecordHolders').empty();
                    $('#male').find('#RecordHolders').html(data); //the HTML I returned from the controller
                    refreshTable();
                },
                error: function (errorData) { onError(errorData); }
            });
        }
        else {
            $.ajax({
                type: 'GET',
                url: '/' + language + '/Record/LoadRecordHoldersFromEvent?id=' + eventID + '&gender_value=' + gender_value,
                success: function (data) {
                    //alert(data.toString());
                    $('#male').find('#RecordHolders').empty();
                    $('#male').find('#RecordHolders').html(data); //the HTML I returned from the controller
                    refreshTable();
                },
                error: function (errorData) { onError(errorData); }
            });
        }
    });
    // Event Type dropdown change
    $('#male').find('#RecordHolders').change(function () {
        gender_value = 1;
        refreshTable();
    });



    // next click
    $('#male').find('#myResultDiv').on('click', '#next', function () {
        gender_value = 1;
        nextPage++;
        refreshTable();

    });

    // previous click
    $('#male').find('#myResultDiv').on('click', '#previous', function () {
        gender_value = 1;
        nextPage--;
        refreshTable();

    });

    $('#female').find('#Sports').change(function () {
        gender_value = 2;
        var sportID = $('#female').find('#Sports').val();
        $.ajax({
            type: 'GET',
            url: '/' + language + '/Record/LoadEventsFromSport?id=' + sportID + '&gender_value=' + gender_value,
            success: function (data) {
                //alert(data.toString());
                $('#female').find('#Events').empty();
                $('#female').find('#Events').html(data); //the HTML I returned from the controller
                refreshTable();
            },
            error: function (errorData) { onError(errorData); }
        });
        $.ajax({
            type: 'GET',
            url: '/' + language + '/Record/LoadRecordHoldersFromSport?id=' + sportID + '&gender_value=' + gender_value,
            success: function (data) {
                
                $('#female').find('#RecordHolders').empty();
                $('#female').find('#RecordHolders').html(data); //the HTML I returned from the controller
                refreshTable();
            },
            error: function (errorData) { onError(errorData); }
        });
    });
    $('#female').find('#Events').change(function () {
        gender_value = 2;
        var eventID = $('#female').find('#Events').val();
        if (eventID == "") {
            eventID = null;
            var sportID = $('#female').find('#Sports').val();
            if (sportID == "") {
                sportID = null;
            }
            $.ajax({
                type: 'GET',
                url: '/' + language + '/Record/LoadRecordHoldersFromSport?id=' + sportID + '&gender_value=' + gender_value,
                success: function (data) {
                    //alert(data.toString());
                    $('#female').find('#RecordHolders').empty();
                    $('#female').find('#RecordHolders').html(data); //the HTML I returned from the controller
                    refreshTable();
                },
                error: function (errorData) { onError(errorData); }
            });
        }
        else {
            $.ajax({
                type: 'GET',
                url: '/' + language + '/Record/LoadRecordHoldersFromEvent?id=' + eventID + '&gender_value=' + gender_value,
                success: function (data) {
                    //alert(data.toString());
                    $('#female').find('#RecordHolders').empty();
                    $('#female').find('#RecordHolders').html(data); //the HTML I returned from the controller
                    refreshTable();
                },
                error: function (errorData) { onError(errorData); }
            });
        }
    });
    // Event Type dropdown change
    $('#female').find('#RecordHolders').change(function () {
        gender_value = 2;
        refreshTable();
    });



    // next click
    $('#female').find('#myResultDiv').on('click', '#next', function () {
        gender_value = 2;
        nextPage++;
        refreshTable();

    });

    // previous click
    $('#female').find('#myResultDiv').on('click', '#previous', function () {
        gender_value = 2;
        nextPage--;
        refreshTable();

    });


    function refreshTable() {



        if (gender_value == 1) {
            var language = $('body').attr('data-language');
            var currentSport = $('#male').find('#Sports').val();
            var currentEvent = $('#male').find('#Events').val();
            
            var currentRecordHolder = $('#male').find('#RecordHolders').val();
            
            $.ajax({

                type: 'GET',
                url: '/' + language + '/Record/LoadRecordResult?sportId=' + currentSport + '&eventId=' + currentEvent + '&recordHolderId=' + currentRecordHolder + '&gender_value=' + gender_value +
            '&page=' + nextPage,
                success: function (data) {

                    $('#male').find('#myResultDiv').html(data); //the HTML I returned from the controller


                },
                error: function (errorData) { onError(errorData); }
            });
        }
        if (gender_value == 2) {
            var language = $('body').attr('data-language');
            var currentSport = $('#female').find('#Sports').val();
            var currentEvent = $('#female').find('#Events').val();
           
            var currentRecordHolder = $('#female').find('#RecordHolders').val();
            $.ajax({
                type: 'GET',
                url: '/' + language + '/Record/LoadRecordResult?sportId=' + currentSport + '&eventId=' + currentEvent + '&recordHolderId=' + currentRecordHolder + '&gender_value=' + gender_value +
            '&page=' + nextPage,
                success: function (data) {

                    $('#female').find('#myResultDiv').html(data); //the HTML I returned from the controller


                },
                error: function (errorData) { onError(errorData); }
            });
        }
    }


});