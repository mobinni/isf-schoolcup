﻿$(document).ready(function () {
    var student1 = -1;
    var student2 = $('#student_id').html();
    var clicks = 1;
    $('#year_select').hide();
    $('#sport_select').hide();


    $('#compareToMe').on('click', function () {
        if (clicks % 2 != 0) {
            var currentSport = $('#year_select').show();
            var currentYear = $('#sport_select').show();
            $('#compareToMe span').html('Click here to compare');
            $('#compareToMe').css('margin-left', '-30px');
            $('#compareToMe').css('margin-right', '-30px');
        } else {
            var currentSport = $('#sports').val();
            var currentYear = $('#years').val();
            window.location.href = '/en/comparison/compare?id1=' + student1 + '&id2=' + student2 + '&sportId='
               + currentSport + '&year=' + currentYear;
        }
        clicks++;
    });


});