﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TeamOPSchoolCup.Domain.DAL;
using System.Data.Entity;
using TeamOPSchoolCup.Domain.Services;
using System.Globalization;
using System.Threading;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Web.Helpers;

namespace TeamOPSchoolCup.Web
{
  // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
  // visit http://go.microsoft.com/?LinkId=9394801

  public class MvcApplication : System.Web.HttpApplication
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
      filters.Add(new HandleErrorAttribute
      {
        ExceptionType = typeof(TeamOPSchoolCupException),
        View = "Error",
        Order = 2
      });
      filters.Add(new LanguageActionFilter());
      filters.Add(new HandleErrorAttribute());
    }

    public static void RegisterRoutes(RouteCollection routes) {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

      routes.MapRoute(
          "teamNormal",
          "Team/{action}/{id}",
          new { controller = "Team", action = "Index", id = UrlParameter.Optional }
      );

      routes.MapRoute(
          "teamProfileQuick",
          "Team/{id}",
          new { controller = "Team", action = "Profile", id = UrlParameter.Optional }
      );

      routes.MapRoute( 
          "Default", // Route name
          "{controller}/{action}/{id}", // URL with parameters
          new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
      );


      foreach (Route r in routes)
      {
        if (!(r.RouteHandler is SingleCultureMvcRouteHandler))
        {
          r.RouteHandler = new MultiCultureMvcRouteHandler();
          r.Url = "{culture}/" + r.Url;
          //Adding default culture 
          if (r.Defaults == null)
          {
            r.Defaults = new RouteValueDictionary();
          }
          r.Defaults.Add("culture", Culture.en.ToString());

          //Adding constraint for culture param
          if (r.Constraints == null)
          {
            r.Constraints = new RouteValueDictionary();
          }
          r.Constraints.Add("culture", new CultureConstraint(Enum.GetValues(typeof(Culture))));
        }
      }
    }

    protected void Application_Start() {
      AreaRegistration.RegisterAllAreas();

      RegisterGlobalFilters(GlobalFilters.Filters);
      RegisterRoutes(RouteTable.Routes);

     Database.SetInitializer<EFDbContext>(new EFDbContextInitializer());
      //new RankingService().UpdateAllTheThings();
    }
  }

  public class MultiCultureMvcRouteHandler : MvcRouteHandler
  {
    protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
      var culture = requestContext.RouteData.Values["culture"].ToString();
      var ci = new CultureInfo(culture);
      Thread.CurrentThread.CurrentUICulture = ci;
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
      return base.GetHttpHandler(requestContext);
    }
  }
  public class SingleCultureMvcRouteHandler : MvcRouteHandler { }

  public class CultureConstraint : IRouteConstraint
  {
    private string[] _values;
    public CultureConstraint(params string[] values)
    {
      this._values = values;
    }

    public CultureConstraint(Array array)
    {
      List<String> niceValues=new List<String>();
      foreach (var x in array)
      {
        niceValues.Add(x.ToString());
      }
      this._values = niceValues.ToArray();
    }
    

    public bool Match(HttpContextBase httpContext, Route route, string parameterName,
                        RouteValueDictionary values, RouteDirection routeDirection)
    {

      // Get the value called "parameterName" from the 
      // RouteValueDictionary called "value"
      string value = values[parameterName].ToString();
      // Return true is the list of allowed values contains 
      // this value.
      return _values.Contains(value);

    }

  }

  public enum Culture
  {
    en = 1,
    nl = 2
  }
}