﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.Exceptions;
using System.Web.Security;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class TestController : Controller
  {
    //
    // GET: /Test/



    public String Index()
    {
      String body = "Hello xxxxx,<br /><br />"
        + "This is an automated message from TeamOP SchoolCup<br />"
        + "You can confirm your account by clicking <a href=\"http://google.be\">here</a>";
      MailService.SendMail("Dit is een onderwerp", new List<String>() { "diumelia@gmail.com" }, body, false);
      return "email gezonden";
    }

    public String Test(int id)
    {
      Team team = TeamService.GetTeam(id);
      BestTeamResult btr = RankingService.GetBestTeamResult(team, 2013);
      StringBuilder builder = new StringBuilder();
      foreach (Result r in btr.TeamResult.Meeting.Results)
      {
        builder.Append(r.Event.Name).Append("<br />");
      }
      return builder.ToString();
    }

    public String Send()
    {
      MailService.SendMail("test", "diumelia@gmail.com", "Dit is een  mail van deployment", false);
      return null;
    }

    public ActionResult Exception()
    {
      throw new TeamOPSchoolCupException("Empty exception message");
    }

    [HttpPost]
    public ActionResult Test2(String name)
    {
      Member member = MemberService.GetMember(memberId: new Guid("B6F5E520-032F-486D-B566-473274CAB327"));
      member.Name = name;

      if (MemberService.UpdateMember(member: member)) return Json(new { result = "success" });
      return Json(new { result = "fail" });
    }

  }
}