﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Web.Helpers;
using TeamOPSchoolCup.Domain.POCO;

namespace TeamOPSchoolCup.Web.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Results(String query)
        {
          if (query.Equals("")) {
            return RedirectToAction("Index", "Home");  
          }
            ViewBag.query = query;
            return View();
        }

        public ActionResult LoadStudentResults(string query, int? page, int? pageSize)
        {
            int pageSizeValue = pageSize.HasValue ? pageSize.Value : 5;
            List<Student> student_results = SearchService.SearchStudents(query);
            var paginatedResults = new PaginatedList<Student>(student_results.ToList(), page ?? 0, pageSizeValue);
            
            return PartialView("Partials/_StudentResults", paginatedResults);
        }

        public ActionResult LoadTeamResults(string query, int? page, int? pageSize)
        {
            int pageSizeValue = pageSize.HasValue ? pageSize.Value : 15;
            List<Team> team_results = SearchService.SearchTeams(query);
            var paginatedResults = new PaginatedList<Team>(team_results.ToList(), page ?? 0, pageSizeValue);
            return PartialView("Partials/_TeamResults", paginatedResults);
        }

        public ActionResult LoadCoachResults(string query, int? page, int? pageSize)
        {
            int pageSizeValue = pageSize.HasValue ? pageSize.Value : 15;
            List<Coach> coach_results = SearchService.SearchCoaches(query);
            var paginatedResults = new PaginatedList<Coach>(coach_results.ToList(), page ?? 0, pageSizeValue);
            return PartialView("Partials/_CoachResults", paginatedResults);
        }

        public ActionResult LoadMeetingResults(string query, int? page, int? pageSize)
        {
            int pageSizeValue = pageSize.HasValue ? pageSize.Value : 15;
            List<Meeting> meeting_results = SearchService.SearchMeetings(query);
            var paginatedResults = new PaginatedList<Meeting>(meeting_results.ToList(), page ?? 0, pageSizeValue);
            return PartialView("Partials/_MeetingResults", paginatedResults);
        }
    }
}
