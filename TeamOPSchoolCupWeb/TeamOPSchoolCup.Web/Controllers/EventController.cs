﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using System.Text;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.Services;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class EventController : Controller
  {

    //
    // GET: /Event/

    public ViewResult Index()
    {
        List<Event> events = EventService.GetOfficialEvents();
      return View(events);
    }

    //
    // GET: /Event/Create

    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Create()
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      ViewBag.NSSFId = new SelectList(NSSFService.GetNSSFs(), "NSSFId", "Name");
      ViewBag.SportId = new SelectList(SportService.GetSports(true), "SportId", "Name");
      return View();
    }

    //
    // POST: /Event/Create

    [Authorize(Roles = "ISFAdmin")]
    [HttpPost]
    public ActionResult Create(Event event1)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { return RedirectToAction("Index", "Home"); }
      if (ModelState.IsValid)
      {
          EventService.AddEvent(event1);
          return RedirectToAction("Index");
      }

      ViewBag.NSSFId = new SelectList(NSSFService.GetNSSFs(), "NSSFId", "Name", event1.NSSFId);
      return View(event1);
    }

    //
    // GET: /Event/Edit/5

    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Edit(int id)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
        Event event1 = EventService.GetEvent(id);
      ViewBag.NSSFId = new SelectList(NSSFService.GetNSSFs(), "NSSFId", "Name", event1.NSSFId);
      ViewBag.SportId = new SelectList(SportService.GetSports(true), "SportId", "Name", event1.SportId);
      return View(event1);
    }

    //
    // POST: /Event/Edit/5

    [Authorize(Roles = "ISFAdmin")]
    [HttpPost]
    public ActionResult Edit(Event event1)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid)
      {
        EventService.UpdateEvent(event1);
        return RedirectToAction("Index");
      }
      ViewBag.NSSFId = new SelectList(NSSFService.GetNSSFs(), "NSSFId", "Name", event1.NSSFId);
      ViewBag.SportId = new SelectList(SportService.GetSports(true), "SportId", "Name", event1.SportId);
      return View(event1);
    }
  }
}