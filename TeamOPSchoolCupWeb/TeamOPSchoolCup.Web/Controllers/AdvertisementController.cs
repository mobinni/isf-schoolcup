﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DTO;
using System.Web.Script.Serialization;
using System.IO;
using TeamOPSchoolCup.Web.Helpers;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Resources;
using TeamOPSchoolCup.Domain.Exceptions;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class AdvertisementController : Controller
  {
    //
    // GET: /Advertisement/
    [Authorize(Roles="ISFAdmin")]
    public ActionResult Index()
    {
      List<Advertisement> ads = AdvertisementService.GetAdvertisements();
      return View(ads);
    }

    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Details(int id)
    {
      Advertisement ad = AdvertisementService.GetAd(id);
      decimal clicks=ad.AdvertisementStatistics.Sum(x => x.ClickCount);
      decimal views = ad.AdvertisementStatistics.Sum(x => x.ViewCount);
      decimal ctr = (views == 0 ? 0 : ((clicks / views) * 100));
      ViewBag.Statistics=new AdCtr  { Ctr=ctr, Clicks=clicks, Views=views };
     /* ViewBag.Statistics=new { ctr=ctr, clicks=clicks, views=views };
      ViewBag.StatCtr = ctr;
      ViewBag.StatClicks = clicks;
      ViewBag.*/
      return View(AdvertisementService.GetAd(id));
    }

    public ActionResult FooterAd()
    {
      Member member = MemberService.GetMember(email:User.Identity.Name);
      Team team = null;
      var rd = ControllerContext.ParentActionViewContext.RouteData;
      var currentAction = rd.GetRequiredString("action");
      var currentController = rd.GetRequiredString("controller");
      if (currentController.ToLowerInvariant().Equals("team") && currentAction.ToLowerInvariant().Equals("profile")) {
        team = TeamService.GetTeam(Int32.Parse(rd.Values["id"].ToString()));
      }
      AdViewing ad = AdvertisementService.GetApplicableAdvertisement(member:member, team: team);
      if (ad == null) return null;
      return PartialView("FooterAd",ad);
    }

    public ActionResult AdRedirect(int adId, int regionId, int genderValue, int sportId)
    {
      Advertisement ad = AdvertisementService.GetAd(adId);
      Gender? gender = null;
      if (genderValue!=0) gender = (Gender)genderValue;
      AdvertisementService.RegisterClick(ad, regionId, gender, sportId);
      return new RedirectResult(ad.Url);
    }

    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Create()
    {
      //ViewBag.Parent = (parentId.HasValue ? RegionService.GetRegion(parentId.Value) : null);
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Region world = RegionService.GetFullTree();
      ViewBag.RegionsJson=new JavaScriptSerializer().Serialize(new RegionForJson(world));
      List<Sport> sports = SportService.GetSports(false).ToList();
      List<object> sportObj = new List<object>();
      foreach (Sport sport in sports)
      {
        sportObj.Add(new { Id = sport.SportId, Name = sport.Name });
      }
      ViewBag.SportsJson = new JavaScriptSerializer().Serialize(sportObj);
      return View();
    }

    //
    // POST: /Region/Create

    [HttpPost]
    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Create(EditableAdvertisement edAdvertisement)
    {
      //Advertisement realAd = edAdvertisement.GetFilledBase();
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid && edAdvertisement.File != null && edAdvertisement.File.ContentLength > 0 /*&& edAdvertisement.File.ContentType != "application/vnd.ms-excel"*/)
      {
        if (AdvertisementService.AddAdvertisementFromEditableAd(edAdvertisement))
        {
          new ImageHelper().Save(new System.Drawing.Bitmap(edAdvertisement.File.InputStream), 728, 90, 100, Path.Combine(Server.MapPath("~/Upload/Advertisement"), "" + edAdvertisement.AdvertisementId + ".jpeg"));
          //string path = Path.Combine(Server.MapPath("~/Upload/Advertisement"), edAdvertisement.File.FileName);
          //edAdvertisement.File.SaveAs(path);
        }
        return RedirectToAction("Details", new { id=edAdvertisement.AdvertisementId });
      }
      if (edAdvertisement.File == null || edAdvertisement.File.ContentLength == 0) { ModelState.AddModelError("", AdvertisementStrings.ModelErrorFileInvalid); }
      //ViewBag.ParentId = new SelectList(RegionService.GetAllRegions(), "RegionId", "Name", region.ParentId);
      Region world = RegionService.GetFullTree();
      ViewBag.RegionsJson = new JavaScriptSerializer().Serialize(new RegionForJson(world));
      List<Sport> sports = SportService.GetSports(false).ToList();
      List<object> sportObj = new List<object>();
      foreach (Sport sport in sports)
      {
        sportObj.Add(new { Id = sport.SportId, Name = sport.Name });
      }
      ViewBag.SportsJson = new JavaScriptSerializer().Serialize(sportObj);
      return View(edAdvertisement);
    }

    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Edit(int id)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Advertisement advertisement = AdvertisementService.GetAd(id);
      EditableAdvertisement edAd = new EditableAdvertisement(advertisement);
      Region world = RegionService.GetFullTree();
      ViewBag.RegionsJson = new JavaScriptSerializer().Serialize(new RegionForJson(world));
      List<Sport> sports = SportService.GetSports(false).ToList();
      List<object> sportObj = new List<object>();
      foreach (Sport sport in sports)
      {
        sportObj.Add(new { Id = sport.SportId, Name = sport.Name });
      }
      ViewBag.SportsJson = new JavaScriptSerializer().Serialize(sportObj);
      //Could use the strings in edAd here...
      //ViewBag.SelectedRegions = new JavaScriptSerializer().Serialize(advertisement.Regions.Select(x => x.RegionId).ToList());
     // ViewBag.SelectedSports = new JavaScriptSerializer().Serialize(advertisement.Sports.Select(x => x.SportId).ToList());
      return View(edAd);
    }

    //
    // POST: /Test/Edit/5

    [HttpPost]
    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Edit(EditableAdvertisement edAd)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      //Advertisement realAd = edAdvertisement.GetFilledBase();
      if (ModelState.IsValid)
      {
        if (AdvertisementService.EditAdvertisementFromEditableAd(edAd) && edAd.File != null && edAd.File.ContentLength > 0)
        {
          new ImageHelper().Save(new System.Drawing.Bitmap(edAd.File.InputStream), 728, 90, 100, Path.Combine(Server.MapPath("~/Upload/Advertisement"), "" + edAd.AdvertisementId + ".jpeg"));
        }
        return RedirectToAction("Details", new { id = edAd.AdvertisementId });
      }
      //ViewBag.ParentId = new SelectList(RegionService.GetAllRegions(), "RegionId", "Name", region.ParentId);
      Region world = RegionService.GetFullTree();
      ViewBag.RegionsJson = new JavaScriptSerializer().Serialize(new RegionForJson(world));
      List<Sport> sports = SportService.GetSports(false).ToList();
      List<object> sportObj = new List<object>();
      foreach (Sport sport in sports)
      {
        sportObj.Add(new { Id = sport.SportId, Name = sport.Name });
      }
      ViewBag.SportsJson = new JavaScriptSerializer().Serialize(sportObj);
      //Could use the strings in edAd here...
     // ViewBag.SelectedRegions = new JavaScriptSerializer().Serialize(advertisement.Regions.Select(x => x.RegionId).ToList());
      //ViewBag.SelectedSports = new JavaScriptSerializer().Serialize(advertisement.Sports.Select(x => x.SportId).ToList());
      return View(edAd);
    }
  }
}
