﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Web.Models;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class ApiController : Controller
  {
    private static String ANDROID_UA = "ISFphoneDroid";
    private StringBuilder jsonBuilder = new StringBuilder();
    JavaScriptSerializer serializer = new JavaScriptSerializer();

    private bool IsCorrectUA()
    {
      return Request.UserAgent.Equals(ANDROID_UA);
    }

    public String Sports()
    {
      if (!IsCorrectUA()) return null;

      List<Sport> sports = SportService.GetSports(true);

      SportService.GenerateJson(sports, jsonBuilder, serializer);
      return jsonBuilder.ToString();
    }

    public String Events()
    {
      if (!IsCorrectUA()) return null;

      List<Event> events = EventService.GetEvents();

      EventService.GenerateJson(events, jsonBuilder, serializer);
      return jsonBuilder.ToString();
    }

    public String Regions()
    {
      if (!IsCorrectUA()) return null;

      Region worldRegion = RegionService.GetWorld();

      jsonBuilder.Append("[");
      RegionService.GenerateJson(worldRegion, jsonBuilder, serializer);
      if (jsonBuilder.Length > 0) jsonBuilder.Append("]");

      return jsonBuilder.ToString();
    }

    public String CompetitionYears()
    {
      if (!IsCorrectUA()) return null;

      List<CompetitionYear> competitionYears = CompetitionYearService.GetCompetitionYears();

      CompetitionYearService.GenerateJson(competitionYears, jsonBuilder, serializer);
      return jsonBuilder.ToString();
    }

    public String StudentRanking(int eventId, int? year = null, bool? isOfficial = null, int? gender = null, int? regionId = null, int? age = null)
    {
      if (!IsCorrectUA()) return null;

      Event e = EventService.GetEvent(eventId);
      Gender? newGender = null;
      List<Region> regions = new List<Region>();
      ParticipantAge? newAge = null;

      if (gender.HasValue) newGender = (Gender)Enum.ToObject(typeof(Gender), gender);
      if (age.HasValue) newAge = (ParticipantAge)Enum.ToObject(typeof(ParticipantAge), age);
      if (regionId.HasValue)
      {
        regions.Add(RegionService.GetRegion(regionId.Value));
      }
      else
      {
        regions = null;
      }
      List<BestStudentResult> bsrs = RankingService.GetStudentRanking(pEvent: e, year: year, isOfficial: isOfficial, gender: newGender, regions: regions, participantAge: newAge);

      RankingService.GenerateJson(bsrs, jsonBuilder, serializer);
      return jsonBuilder.ToString();
    }

    public String TeamRanking(int sportId, int? gender = null, int? year = null, int? regionId = null)
    {
      if (!IsCorrectUA()) return null;

      Sport sport = SportService.GetSport(sportId: sportId);
      List<Region> regions = new List<Region>();
      Gender? newGender = null;
      if (gender.HasValue) newGender = (Gender)Enum.ToObject(typeof(Gender), gender.Value);
      if (regionId.HasValue)
      {
        regions.Add(RegionService.GetRegion(regionId.Value));
      }
      else
      {
        regions = null;
      }
      List<BestTeamResult> btrs = RankingService.GetTeamRanking(sport: sport, gender: newGender, year: year, regions: regions);

      RankingService.GenerateJson(btrs, jsonBuilder, serializer);
      return jsonBuilder.ToString();
    }

    public String StudentComparison(Guid studentId1, Guid studentId2, int? year)
    {
      if (!IsCorrectUA()) return null;

      int currentYear = (year.HasValue ? year.Value : DateTime.Today.Year);
      Student student1 = MemberService.GetStudent(studentId1);
      Student student2 = MemberService.GetStudent(studentId2);
      List<Student> students = new List<Student>() { student1, student2 };
      StudentComparison studentComparison = ComparisonService.GetStudentComparison(students, currentYear);

      ComparisonService.GenerateJson(studentComparison, jsonBuilder, serializer);
      return jsonBuilder.ToString();
    }

    public String TeamComparison(int teamId1, int teamId2)
    {
      if (!IsCorrectUA()) return null;

      Team team1 = TeamService.GetTeam(teamId1);
      Team team2 = TeamService.GetTeam(teamId2);
      TeamComparison comparison = ComparisonService.GetTeamComparison(team1, team2);

      ComparisonService.GenerateJson(comparison, jsonBuilder, serializer);
      return jsonBuilder.ToString();
    }

    public String Teams(Guid? mobileAccessKey)
    {
      if (!IsCorrectUA()) return null;

      List<Team> teams = new List<Team>();
      Member member = MemberService.GetMember(mobileAccessKey: mobileAccessKey);
      if (member.Student != null)
      {
        teams.AddRange(TeamService.GetTeams(student: member.Student));
      }
      else if (member.Coach != null)
      {
        teams.AddRange(TeamService.GetTeams(coach: member.Coach));
      }

      TeamService.GenerateJson(teams, jsonBuilder, serializer);
      return jsonBuilder.ToString();
    }

    public String Meetings(Guid? mobileAccessKey)
    {
      if (!IsCorrectUA()) return null;

      List<Team> teams = new List<Team>();
      Member member = MemberService.GetMember(mobileAccessKey: mobileAccessKey);

      if (member.Student != null)
      {
        teams.AddRange(TeamService.GetTeams(student: member.Student));
      }
      else if (member.Coach != null)
      {
        teams.AddRange(TeamService.GetTeams(coach: member.Coach));
      }

      jsonBuilder.Append("[");
      foreach (Team team in teams)
      {
        List<CoachMeeting> meetings = MeetingService.GetCoachMeetings(team);
        MeetingService.GenerateJson(team, meetings, jsonBuilder, serializer);
      }
      if (jsonBuilder.Length > 0) jsonBuilder.Length--;
      if (jsonBuilder.Length > 0) jsonBuilder.Append("]");

      return jsonBuilder.ToString();
    }

    public String MeetingEvents(Guid? mobileAccessKey)
    {
      if (!IsCorrectUA()) return null;

      List<Team> teams = new List<Team>();
      Member member = MemberService.GetMember(mobileAccessKey: mobileAccessKey);

      if (member.Student != null)
      {
        teams.AddRange(TeamService.GetTeams(student: member.Student));
      }
      else if (member.Coach != null)
      {
        teams.AddRange(TeamService.GetTeams(coach: member.Coach));
      }

      jsonBuilder.Append("[");
      foreach (Team team in teams)
      {
        List<CoachMeeting> coachMeetings = MeetingService.GetCoachMeetings(team);

        foreach (CoachMeeting coachMeeting in coachMeetings)
        {
          EventService.GenerateJson(coachMeeting.Meeting.Events, coachMeeting.Meeting, jsonBuilder, serializer);
        }
      }
      if (jsonBuilder.Length > 0) jsonBuilder.Length--;
      if (jsonBuilder.Length > 0) jsonBuilder.Append("]");

      return jsonBuilder.ToString();
    }

    public String TeamStudents(Guid? mobileAccessKey)
    {
      if (!IsCorrectUA()) return null;

      List<Team> teams = new List<Team>();
      Member member = MemberService.GetMember(mobileAccessKey: mobileAccessKey);

      if (member.Student != null)
      {
        teams.AddRange(TeamService.GetTeams(student: member.Student));
      }
      else if (member.Coach != null)
      {
        teams.AddRange(TeamService.GetTeams(coach: member.Coach));
      }

      jsonBuilder.Append("[");
      foreach (Team team in teams)
      {
        List<TeamMembership> teamMemberships = TeamService.GetTeamMemberships(team: team, historicality: TeamService.Historicality.CURRENT);

        foreach (TeamMembership tm in teamMemberships)
        {
          MemberService.GenerateJson(tm.Student, team, jsonBuilder, serializer);
        }
      }
      if (jsonBuilder.Length > 0) jsonBuilder.Length--;
      if (jsonBuilder.Length > 0) jsonBuilder.Append("]");

      return jsonBuilder.ToString();
    }

    [HttpPost]
    public ActionResult PostResult(Guid mobileAccessKey, double resultScore, int teamId, int meetingId, int eventId, Guid? studentId = null)
    {
      if (!IsCorrectUA()) return Json(new { result = "fail"});

      Member member = MemberService.GetMember(mobileAccessKey: mobileAccessKey);
      if (member == null) return Json(new { result = 1, error = "Not authorized" });
      if (member.Coach != null && !studentId.HasValue) return Json(new { result = 1, error = "Student is required" });

      Student student;

      if (member.Student != null && !studentId.HasValue)
      {
        student = member.Student;
      }
      else
      {
        student = MemberService.GetStudent(studentId.Value);
      }

      Result result = new Result() { MeetingId = meetingId, EventId=eventId, MemberId = student.MemberId, ResultScore = resultScore, TeamId = teamId};

      if (ResultService.AddResult(result)) return Json(new { result = 0});

      return Json(new { result = 1, error = "Could not save result" });
    }

    public ActionResult Index()
    {
      if (!IsCorrectUA()) return null;

      return View();
    }

    [HttpPost]
    public ActionResult Index(LogOnModel model)
    {
      if (!IsCorrectUA()) return null;

      model.RememberMe = false;
      if (ModelState.IsValid)
      {
        if (Membership.ValidateUser(model.Email, model.Password))
        {
          FormsAuthentication.SetAuthCookie(model.Email, false);

          return RedirectToAction("SetPermission");
        }
      }
      return View();
    }

    public ActionResult SetPermission()
    {
      if (!IsCorrectUA()) return null;
      if (!User.Identity.IsAuthenticated) return RedirectToAction("Index");
      if (!User.IsInRole("Student") && !User.IsInRole("Coach")) return Content("Only students and coaches can post results.", "text/html");

      return View(false);
    }

    [HttpPost]
    public ActionResult SetPermission(Boolean? grant)
    {
      if (!IsCorrectUA()) return null;
      if (!User.Identity.IsAuthenticated) return RedirectToAction("Index");
      if (!User.IsInRole("Student") && !User.IsInRole("Coach"))
      {
        FormsAuthentication.SignOut();
        return Content("Only students and coaches can post results.", "text/html");
      }

      Member member = MemberService.GetMember(memberId: null, email: User.Identity.Name);

      if (member.MobileAccessKey == new Guid("00000000-0000-0000-0000-000000000000"))
      {
        member.MobileAccessKey = Guid.NewGuid();
        MemberService.UpdateMember(member: member);
      }
      ViewBag.MAK = member.MobileAccessKey;
      ViewBag.IsStudent = (member.Student != null);

      return View(true);
    }
  }
}