﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class SportController : Controller
  {
    //
    // GET: /Sport/

    public ViewResult Index()
    {
      return View(SportService.GetSports(true));
    }

    //
    // GET: /Sport/Details/5

    public ViewResult Details(int id)
    {
      Sport sport = SportService.GetSport(id);
      return View(sport);
    }

    //
    // GET: /Sport/Create

    public ActionResult Create()
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      return View();
    }

    //
    // POST: /Sport/Create

    [HttpPost]
    public ActionResult Create(Sport sport)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid)
      {
        SportService.AddSport(sport);
        return RedirectToAction("Index");
      }

      return View(sport);
    }

    //
    // GET: /Sport/Edit/5

    public ActionResult Edit(int id)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Sport sport = SportService.GetSport(id);
      return View(sport);
    }

    //
    // POST: /Sport/Edit/5

    [HttpPost]
    public ActionResult Edit(Sport sport)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid)
      {
        SportService.UpdateSport(sport);
        return RedirectToAction("Index");
      }
      return View(sport);
    }
  }
}