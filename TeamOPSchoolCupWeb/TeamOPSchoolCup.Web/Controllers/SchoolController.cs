﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.Resources;
using System.IO;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Exceptions;


namespace TeamOPSchoolCup.Web.Controllers
{
  public class SchoolController : Controller
  {

    //
    // GET: /School/

    public ViewResult Index()
    {
      return View(SchoolService.GetSchools());
    }

    //
    // GET: /School/Details/5

    public ViewResult Details(int id)
    {
      return View(SchoolService.GetSchool(id:id));
    }

    //
    // GET: /School/Create
    [Authorize(Roles = "NSSFRep")]
    public ActionResult Create()
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      return View();
    }

    //
    // POST: /School/Create

    [HttpPost]
    [Authorize(Roles = "NSSFRep")]
    public ActionResult Create(School school)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Member mem = MemberService.GetMember(email: User.Identity.Name);
      school.NSSFId = mem.NSSFRep.NSSFId;
      if (ModelState.IsValid)
      {
        SchoolService.AddSchool(school, Url.Action("AddSCC", "Member", new { schoolemail = school.Email }, Request.Url.Scheme));
        return RedirectToAction("Index"); 
      }
      return View(school);
    }

    //
    // GET: /School/Edit/5
    [Authorize(Roles = "SCC")]
    public ActionResult Edit(int id)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      School school = SchoolService.GetSchool(id:id);
      return View(school);
    }

    //
    // POST: /School/Edit/5

    [HttpPost]
    [Authorize(Roles = "SCC")]
    public ActionResult Edit(School school)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid)
      {
        SchoolService.UpdateSchool(school);
        return RedirectToAction("Index");
      }
      return View(school);
    }

    [Authorize(Roles="NSSFRep")]
    public ActionResult Import()
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      return View();
    }

    [HttpPost]
    [Authorize(Roles="NSSFRep")]
    public ActionResult Import(HttpPostedFileBase file)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      NSSF nssf = NSSFService.GetNSSF(repId:MemberService.GetMember(email: User.Identity.Name).MemberId);
      if (file == null || file.ContentLength <= 0 || file.ContentType != "application/vnd.ms-excel") throw new TeamOPSchoolCupException(ErrorStrings.ImportSchool_ReadError);

      string line;
      bool firstline = true;
      using (StreamReader r = new StreamReader(file.InputStream))
      {
        while ((line = r.ReadLine()) != null)
        {
          if (firstline) { firstline = false; continue; }
          if (!ModelState.IsValid) continue;
          string[] values = line.Split(';');
          if (String.IsNullOrWhiteSpace(values[0])) continue;
          School s = new School
          {
            Name=values[0],
            Street=values[1],
            City=values[2],
            Zipcode=values[3],
            AbbrName=values[4],
            Email=values[5],
            NSSFId=nssf.NSSFId
          };
          SchoolService.AddSchool(s, Url.Action("AddSCC", "Member", new { schoolemail = s.Email }, Request.Url.Scheme));
        }
      }
      return RedirectToAction("Index");
    }
  }
}