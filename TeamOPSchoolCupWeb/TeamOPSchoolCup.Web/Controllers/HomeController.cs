﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using System.IO;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Web.Models;
using TeamOPSchoolCup.Domain.DTO;
using System.Globalization;
using TeamOPSchoolCup.Web.Helpers;
using TeamOPSchoolCup.Domain.Exceptions;
using System.Web.Security;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class HomeController : Controller
  {
    //
    // GET: /Home/

    public ActionResult Index(bool wantToLogin=false, string ReturnUrl="")
    {
    //  TeamService.GetTeams();
      ViewBag.UpcomingMeetings=MeetingService.GetUpcomingMeetings(isOfficial: true);
      var s=SportService.GetSports(true);
      ViewBag.WantToLogin = wantToLogin;
      ViewBag.LoginReturnUrl = ReturnUrl;
      //throw new TeamOPSchoolCupException();
      //throw new SchoolAlreadyHasSCCException(SchoolService.GetSchool(1));
      return View();
    }

    public ActionResult ImportPointsTemp()
    {
      return View();
    }

    //WARNING: Dirty code ahead
        //  Nesting-hell
    //  No use of services
    //  Doesn't account for first line being text
    [HttpPost]
    public ActionResult ImportPointsTemp(HttpPostedFileBase file)
    {
      //Might want to do errormessages =)
      if (file == null || file.ContentLength <= 0 || file.ContentType != "application/vnd.ms-excel") return RedirectToAction("Index");
      //Some hardcoded stuff, for now; User should be able to configure this stuff
      PointsImport pi = new PointsImport() { Event = EventService.GetEvent("long-jumping"), CompetitionYear = 2013 };
      int pointsColumn = 0;
      int boysColumn = 1;
      int girlsColumn = 2;

      string path = Path.Combine(Server.MapPath("~/Upload"), file.FileName);
      file.SaveAs(path);

      string line;
      using (StreamReader r = new StreamReader(path))
      {
        while ((line = r.ReadLine()) != null)
        {
          if (!ModelState.IsValid) continue;
          string[] values = line.Split(';');
          if (String.IsNullOrWhiteSpace(values[pointsColumn])) continue;
          int pointValue = Int32.Parse(values[pointsColumn]);
          pi.Lines.Add(new PointsImport.ImportLine
          {
            PointValue = pointValue,
            BoysMeasure = (String.IsNullOrWhiteSpace(values[boysColumn]) ? 0 : Double.Parse(values[boysColumn])),
            GirlsMeasure = (String.IsNullOrWhiteSpace(values[girlsColumn]) ? 0 : Double.Parse(values[girlsColumn]))
          });
        }
      }
      RankingService.ImportPoints(pi);

      return RedirectToAction("Index");
    }

    //TEMP
    public ActionResult RankingUpdateAllTheThings()
    {
      //RankingService.UpdateAllTheThings();

      return RedirectToAction("Index");
    }

    public ActionResult CultureChanger()
    {
      List<KeyValuePair<int, string>> languageData = new List<KeyValuePair<int, string>>();
      foreach (string str in Enum.GetNames(typeof(Culture)))
      {
        CultureInfo ci=new CultureInfo(str);
        int cultureid=(int)Enum.Parse(typeof(Culture),str,true);
        KeyValuePair<int, string> kvp = new KeyValuePair<int, string>(cultureid, String.Format("{0} - {1}", ci.EnglishName, ci.NativeName));
        if (str == "en")
        {
          kvp = new KeyValuePair<int, string>(cultureid, String.Format("{0}", ci.NativeName));
        }
        languageData.Add(kvp);
      }
      return PartialView("CultureChangerPartial",languageData);
    }

    public ActionResult LoadHomeRankings(int? sportId, int? year, int? regionId, int? page, Gender gender, int? pageSize)
    {
        // number of elements per page
        int pageSizeValue = pageSize.HasValue ? pageSize.Value : 2; // CHANGE THIS LINE IF YOU LOVE LOGIC
        // Parameters
        int sportValue = sportId.HasValue ? sportId.Value : 1;
        int regionValue = regionId.HasValue ? regionId.Value : 1;
        if (gender == Gender.UNISEX) gender = Gender.MALE;

        Region region = RegionService.GetRegion(regionValue);
        // Get BTR
        List<BestTeamResult> btr = RankingService.GetTeamRanking(sport: SportService.GetSport(sportValue), gender: gender, year: 2013, regions: new List<Region> { region });

        // Get Paginated list
        var paginatedBTR = new PaginatedList<BestTeamResult>(btr.ToList(), page ?? 0, pageSizeValue);

        return PartialView("Partials/_HomeRankingTable", paginatedBTR);
    }

    public ActionResult ChangeCulture(Culture lang, string returnUrl)
    {
      if (returnUrl.Length >= 3)
      {
        returnUrl = returnUrl.Substring(3);
      }
      return Redirect("/" + lang.ToString() + returnUrl);
    }

    public ActionResult About() {
      return View();
    }

    public ActionResult Contact() {
      return View();
    }
    protected override void OnException(ExceptionContext filterContext)
    {
      //this.ActionInvoker.InvokeAction(t
    }
  }
}
