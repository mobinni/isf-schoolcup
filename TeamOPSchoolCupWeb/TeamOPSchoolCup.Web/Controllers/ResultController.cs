﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Web.Helpers;
using System.Web.Security;
using TeamOPSchoolCup.Domain.DTO.Meetings;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;


namespace TeamOPSchoolCup.Web.Controllers
{
  public class ResultController : Controller
  { 
    //
    // GET: /Result/

    public ViewResult Index()
    {
      return View();
    }

    public ActionResult LoadResultTable(int? page)
    {
        int pageSizeValue = 5; // CHANGE THIS LINE IF YOU LOVE LOGIC
        var results = ResultService.GetResults().ToList();
        var paginatedResults = new PaginatedList<Result>(results, page ?? 0, pageSizeValue);

        return PartialView("Partials/_ResultTable", paginatedResults);
    }

    //
    // GET: /Result/Details/5

    public ViewResult Details(int id)
    {
      Result result = ResultService.GetResult(id);
      return View(result);
    }

    [Authorize(Roles = "Coach,Student")]
    public ActionResult CreateSingle(int? meetingId)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting); }
      Member member = MemberService.GetMember(email: User.Identity.Name);
      List<Team> teams = new List<Team>();
      if (member.Coach != null)
      {
        teams=member.Coach.TeamCoaches.Where(x => x.DateLeft >=DateTime.Today).Select(x => x.Team).ToList();
      }
      else if (member.Student != null)
      {
        teams=member.Student.TeamMemberShips.Where(x => x.DateLeft >= DateTime.Today).Select(x => x.Team).ToList();
      }
      ViewBag.Teams=new SelectList(teams.Select(x => new { Id = x.TeamId, Name = x.Name }),"Id","Name");
      ViewBag.MemberId = member.MemberId;
      if (meetingId.HasValue)
      {
        Meeting meeting = MeetingService.GetMeeting(meetingId.Value);
        if (meeting.CoachMeeting == null) return RedirectToAction("Index", "Home");
        if (!MeetingService.CanUserAddResultsToMeeting(meeting, member)) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        ViewBag.Team = meeting.CoachMeeting.Team;
        ViewBag.Meeting = meeting;
      }
      ViewBag.MemberType = "";
      if (member.Student != null) { ViewBag.MemberType = "Student"; }
      else if (member.Coach != null) { ViewBag.MemberType = "Coach"; }
      return View();
    }
    public ActionResult GetMeetingsForTeam(int teamId)
    {
      
      Member member=MemberService.GetMember(email:User.Identity.Name);
      List<Meeting> meetings = MeetingService.GetMeetingsToWhichUserCanAddResult(member, TeamService.GetTeam(teamId));
      return Json(meetings.Select(x => new { Id = x.MeetingId, Name = x.Name, Date = String.Format("{0:dd MMM yyyy}",x.Date) }), JsonRequestBehavior.AllowGet);
    }
    public ActionResult GetStudentsForTeam(int teamId)
    {
      List<TeamMembership> teamMemberships=TeamService.GetTeamMemberships(TeamService.GetTeam(teamId));
      return Json(teamMemberships.Select(x => new { Id = x.MemberId, Name = x.Student.Member.Name, Email = x.Student.Member.Email }), JsonRequestBehavior.AllowGet);
    }
    public ActionResult GetEventsForMeeting(int meetingId)
    {
      Member member = MemberService.GetMember(email: User.Identity.Name);
      Meeting meeting = MeetingService.GetMeeting(meetingId);
      return Json(meeting.Events.Where(x => x.Gender == member.Gender || x.Gender == Gender.UNISEX).Select(x => new { Id=x.EventId, Name=x.Name }), JsonRequestBehavior.AllowGet);
    }

    [Authorize(Roles = "Coach,Student")]
    [HttpPost]
    public ActionResult CreateSingle(Result result)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting); }
      Meeting meeting = MeetingService.GetMeeting(result.MeetingId);
      Member member = MemberService.GetMember(email: User.Identity.Name);
      if (meeting.CoachMeeting == null) return RedirectToAction("Index", "Home");
      if (member.Student != null && !member.Student.TeamMemberShips.Select(x => x.TeamId).Contains(meeting.CoachMeeting.TeamId)) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
      if (member.Coach != null && !member.Coach.TeamCoaches.Select(x => x.TeamId).Contains(meeting.CoachMeeting.TeamId)) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
      if (member.Student != null && member.MemberId != result.MemberId) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
      if (ModelState.IsValid)
      {
        ResultService.AddResult(result);
        return RedirectToAction("Dashboard","Team",new { id=result.TeamId });
      }
      return View();
    }

    //
    // GET: /Result/Create
    [Authorize(Roles="ISFAdmin,NSSFRep,Coach")]
    public ActionResult Create(int meetingId)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting); }
      Meeting meeting = MeetingService.GetMeeting(meetingId);
      Member member = MemberService.GetMember(email: User.Identity.Name);
      ViewBag.MeetingId = meeting.MeetingId;
      ViewBag.Events = meeting.Events;
      ViewBag.Type = 0;
      ViewBag.Sport = meeting.Events.First().SportId;
      ViewBag.InitialAmountOfRows = 5;
      if (member.ISFAdmin!=null)
      {
        if (meeting.ISFMeeting == null) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        if (meeting.ISFMeeting.MemberId != member.MemberId) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        ViewBag.Type = 1;
        Region world = RegionService.GetFullTree();
        Dictionary<int, string> regions = new Dictionary<int, string>();
        regions.Add(world.RegionId, world.Name);
        this.helpGetIndentedRegions(world.Children, 1, ref regions);
        ViewBag.Regions2 = regions;
        ViewBag.Regions = RegionService.GetAllRegions();
      }
      else if (member.NSSFRep != null)
      {
        if (meeting.RegionalMeeting == null) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        if (meeting.RegionalMeeting.MemberId != member.MemberId) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        ViewBag.Type = 2;
        Region r = NSSFService.GetNSSF(repId: member.MemberId).Region;
        ViewBag.Schools = SchoolService.GetSchools(region: r);
      }
      else if (member.Coach != null)
      {
        if (meeting.CoachMeeting == null) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        if (meeting.CoachMeeting.MemberId != member.MemberId) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        ViewBag.Type = 3;
        ViewBag.TeamId = meeting.CoachMeeting.TeamId;
        ViewBag.Students = TeamService.GetTeam(meeting.CoachMeeting.TeamId).TeamMemberships.Select(x => x.Student).ToList();
      }
      ViewBag.EventId = new SelectList(EventService.GetEvents(), "EventId", "Name");
      return View();
    }
    public ActionResult GetSchools(int regionId)
    {
      List<School> schools=SchoolService.GetSchools(region:RegionService.GetRegion(regionId));
      return Json(schools.Select(x => new { Id = x.SchoolId, Name = x.Name }).ToList(), JsonRequestBehavior.AllowGet);
    }
    public ActionResult GetTeams(int schoolId,int sportId)
    {
      List<Team> teams = TeamService.GetTeams(school: SchoolService.GetSchool(schoolId),sport:SportService.GetSport(sportId));
      return Json(teams.Select(x => new { Id = x.TeamId, Name = x.Name }).ToList(), JsonRequestBehavior.AllowGet);
    }
    public ActionResult GetStudents(int teamId)
    {
      List<Student> students = TeamService.GetTeam(teamId).TeamMemberships.Select(x => x.Student).ToList();
      return Json(students.Select(x => new { Id = x.MemberId, Name = x.Member.Name, Email = x.Member.Email }).ToList(), JsonRequestBehavior.AllowGet);
    }
    private void helpGetIndentedRegions(List<Region> r,int level,ref Dictionary<int,string> addToDict)
    {
      string spaceLevel = "&nbsp;&nbsp;";
      string spaces = "";
      for (int i = 0; i < level; i++) { spaces += spaceLevel; }
      foreach (Region reg in r)
      {
        addToDict.Add(reg.RegionId, spaces + reg.Name);
        this.helpGetIndentedRegions(reg.Children, level + 1, ref addToDict);
      }
    }

    //
    // POST: /Result/Create

    [HttpPost]
    public ActionResult Create(ResultsBulkCreate bulkResults)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);  }
      List<ResultsBulkCreate.ResultNode> failedItems = new List<ResultsBulkCreate.ResultNode>();
      if (ModelState.IsValid)
      {
        List<Result> results=new List<Result>();
        foreach (ResultsBulkCreate.ResultNode rn in bulkResults.ResultNodes)
        {
          if (TeamService.GetTeamMemberships(TeamService.GetTeam(rn.Team), MemberService.GetStudent(rn.Student), TeamService.Historicality.CURRENT).Count < 1)
          {
            failedItems.Add(rn);
            continue;
          }
          Event e=EventService.GetEvent(rn.Event);
          if (rn.ResultScore < e.MinimumValue || rn.ResultScore > e.MaximumValue)
          {
            failedItems.Add(rn);
            continue;
          }
          Result r = new Result
          {
            ResultScore = rn.ResultScore,
            EventId = rn.Event,
            MeetingId = bulkResults.MeetingId,
            MemberId = rn.Student,
            TeamId = rn.Team
          };
          results.Add(r);
          //Eerst even valideren:
          //  -zit die user in dat team
          //  -is dat een geldige score voor dat event
          //Uiteindelijk een result opbouwen en invoeren
        }
        if (results.Count != 0)
        {
          ResultService.AddResults(results, MeetingService.GetMeeting(bulkResults.MeetingId));
          //ResultService.AddResult(result);
          return RedirectToAction("Detail", "Meeting", new { id = bulkResults.MeetingId });
        }
      }

      //FOR ERRORS:
      Meeting meeting = MeetingService.GetMeeting(bulkResults.MeetingId);
      Member member = MemberService.GetMember(email: User.Identity.Name);
      ViewBag.MeetingId = meeting.MeetingId;
      ViewBag.Events = meeting.Events;
      ViewBag.Type = 0;
      ViewBag.Sport = meeting.Events.First().SportId;
      ViewBag.InitialAmountOfRows = 5;
      if (member.ISFAdmin != null)
      {
        if (meeting.ISFMeeting == null) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        if (meeting.ISFMeeting.MemberId != member.MemberId) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        ViewBag.Type = 1;
        Region world = RegionService.GetFullTree();
        Dictionary<int, string> regions = new Dictionary<int, string>();
        regions.Add(world.RegionId, world.Name);
        this.helpGetIndentedRegions(world.Children, 1, ref regions);
        ViewBag.Regions2 = regions;
        ViewBag.Regions = RegionService.GetAllRegions();
      }
      else if (member.NSSFRep != null)
      {
        if (meeting.RegionalMeeting == null) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        if (meeting.RegionalMeeting.MemberId != member.MemberId) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        ViewBag.Type = 2;
        Region r = NSSFService.GetNSSF(repId: member.MemberId).Region;
        ViewBag.Schools = SchoolService.GetSchools(region: r);
      }
      else if (member.Coach != null)
      {
        if (meeting.CoachMeeting == null) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        if (meeting.CoachMeeting.MemberId != member.MemberId) throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting);
        ViewBag.Type = 3;
        ViewBag.TeamId = meeting.CoachMeeting.TeamId;
        ViewBag.Students = TeamService.GetTeam(meeting.CoachMeeting.TeamId).TeamMemberships.Select(x => x.Student).ToList();
      }
      //ViewBag.EventId = new SelectList(EventService.GetEvents(), "EventId", "Name", result.EventId);
      return View(new ResultsBulkCreate { MeetingId = bulkResults.MeetingId, ResultNodes = failedItems });
    }

    //
    // GET: /Result/Edit/5

    public ActionResult Edit(int id)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Result result = ResultService.GetResult(id);
      ViewBag.EventId = new SelectList(EventService.GetEvents(), "EventId", "Name", result.EventId);
      return View(result);
    }

    //
    // POST: /Result/Edit/5

    [HttpPost]
    public ActionResult Edit(Result result)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid)
      {
        ResultService.UpdateResult(result);
        return RedirectToAction("Index");
      }
      ViewBag.EventId = new SelectList(EventService.GetEvents(), "EventId", "Name", result.EventId);
      return View(result);
    }
  }
}