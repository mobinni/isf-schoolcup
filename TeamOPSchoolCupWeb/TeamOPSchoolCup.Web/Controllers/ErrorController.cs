﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class ErrorController : Controller
  {
    //
    // GET: /Error/

    public ActionResult Index(string aspxerrorpath)
    {
      throw new TeamOPSchoolCupException("");
    }

    public ActionResult Error404()
    {
      this.HttpContext.Response.TrySkipIisCustomErrors = false;
      throw new TeamOPSchoolCupException(ErrorStrings.Http404Exception);
    }

    public ActionResult ExceptionReplacement(string errorString)
    {
      return View("Error", new HandleErrorInfo(new TeamOPSchoolCupException(errorString), "Error", "ExceptionReplacement"));
    }

  }
}
