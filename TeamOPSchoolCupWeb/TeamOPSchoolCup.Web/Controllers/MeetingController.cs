﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Meetings;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.DTO.Meetings;
using TeamOPSchoolCup.Web.Helpers;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class MeetingController : Controller
  {
    public ActionResult Index(int? year, int? regionId)
    {
      //Do we need to have user-specific overviews of meetings? Maybe smaller table with meetings specific to the user?
      Region region = null;
      if (regionId.HasValue) { region = RegionService.GetRegion(regionId.Value); }
      List<Meeting> meetings=MeetingService.GetMeetings(region:region, year:year,official:true,upcoming:true);
      ViewBag.Regions = RegionService.GetAllRegions();
      ViewBag.year = new SelectList(CompetitionYearService.GetCompetitionYears(), "CompetitionYearValue", "CompetitionYearValue");
      ViewBag.SelectedRegionId = regionId;
      Member mem = MemberService.GetMember(email: User.Identity.Name);
      ViewBag.MemberType = "";
      if (mem != null)
      {
        if (mem.ISFAdmin != null) ViewBag.MemberType = "ISFAdmin";
        else if (mem.NSSFRep != null) ViewBag.MemberType = "NSSFRep";
        else if (mem.Coach != null) ViewBag.MemberType = "Coach";
        else if (mem.Student != null) ViewBag.MemberType = "Student";
      }
      return View(meetings);
    }
    public ActionResult GetMeetings(int? year, int? regionId, int? page, int? pageSize)
    {
      Region region = null;
      if (regionId.HasValue) { region = RegionService.GetRegion(regionId.Value); }
      List<Meeting> meetings = MeetingService.GetMeetings(region: region, year: year, official: true, upcoming: true);
      page = page.HasValue ? page.Value : 0;
      pageSize = pageSize.HasValue ? pageSize.Value : 2;
      return PartialView("PublicMeetingList", new PaginatedList<Meeting>(meetings, page.Value, pageSize.Value));
    }

    public ActionResult OwnMeetings(int? page, int? pageSize)
    {
      int pageSizeValue = pageSize.HasValue ? pageSize.Value : 2;
      Member mem = MemberService.GetMember(email: User.Identity.Name);
      if (mem.SCC != null) return null;
      List<Meeting> meetings = MeetingService.GetRelevantMeetings(mem);
      ViewBag.MemberType = "";
      if (mem.ISFAdmin != null) ViewBag.MemberType = "ISFAdmin";
      else if (mem.NSSFRep != null) ViewBag.MemberType = "NSSFRep";
      else if (mem.Coach != null) ViewBag.MemberType = "Coach";
      else if (mem.Student != null) ViewBag.MemberType = "Student";
      ViewBag.Member = mem;
      meetings=meetings.OrderBy(x => x.Date).ToList();
      Meeting firstMeeting=meetings.Where(x => x.Date >= DateTime.Today).FirstOrDefault();
      if (firstMeeting != null)
      {
        int meetingPos = meetings.Select(x => x.MeetingId).ToList().IndexOf(firstMeeting.MeetingId);
        page = page ?? (meetingPos / pageSizeValue);
      }
      else
      {
        page = page ?? 0;
      }
      var paginatedMeetings = new PaginatedList<Meeting>(meetings, page.Value, pageSizeValue);
      return PartialView("OwnMeetings",paginatedMeetings);
    }

    public ActionResult MeetingsForTeam(int teamId, int? page, int? pageSize)
    {
      List<Meeting> meetings = MeetingService.GetMeetings(official: null, upcoming: null, team: TeamService.GetTeam(teamId));
      int pageSizeValue = pageSize.HasValue ? pageSize.Value : 2;
      Member mem = MemberService.GetMember(email: User.Identity.Name);
      ViewBag.MemberType = "";
      if (mem.Coach != null) ViewBag.MemberType = "Coach";
      else if (mem.Student != null) ViewBag.MemberType = "Student";
      ViewBag.Member = mem;
      ViewBag.TeamId = teamId;
      meetings = meetings.OrderBy(x => x.Date).ToList();
      Meeting firstMeeting = meetings.Where(x => x.Date >= DateTime.Today).FirstOrDefault();
      if (firstMeeting != null)
      {
        int meetingPos = meetings.Select(x => x.MeetingId).ToList().IndexOf(firstMeeting.MeetingId);
        page = page ?? (meetingPos / pageSizeValue);
      }
      else
      {
        page = page ?? 0;
      }
      var paginatedMeetings = new PaginatedList<Meeting>(meetings, page.Value, pageSizeValue);
      return PartialView("MeetingsForTeam", paginatedMeetings);
    }

    public ActionResult Detail(int id)
    {
      int pageSize = 3; //Also update in GetMeetingResults (underneath this)
      ViewBag.listsForEvent = null;

      Meeting meeting = MeetingService.GetMeeting(id);
      MeetingDetails details = new MeetingDetails
      {
        MeetingId=meeting.MeetingId,
        Name=meeting.Name,
        Location=meeting.Location,
        Date=meeting.Date,
        ResultsGroups=new List<MeetingDetails.ResultsGroup>()
      };

      if (meeting.Results != null)
      {
        List<int> events = meeting.Results.Select(x => x.EventId).Distinct().ToList();
        foreach (int eventId in events)
        {
          List<Result> applicableResults =ResultService.OrderListResultsByBestScoreWithinEvent(meeting.Results.Where(x => x.EventId == eventId).ToList());
          Event eventObj = EventService.GetEvent(eventId);
          MeetingDetails.ResultsGroup groupMale = new MeetingDetails.ResultsGroup
          {
            EventId=eventId,
            EventName=eventObj.Name,
            Gender=Gender.MALE,
            Results=new List<MeetingDetails.ResultInfo>()
          };
          MeetingDetails.ResultsGroup groupFemale = new MeetingDetails.ResultsGroup
          {
            EventId=eventId,
            EventName=eventObj.Name,
            Gender=Gender.FEMALE,
            Results=new List<MeetingDetails.ResultInfo>()
          };
          foreach (Result result in applicableResults)
          {
            Member resultMember = MemberService.GetMember(result.MemberId);
            MeetingDetails.ResultInfo res = new MeetingDetails.ResultInfo
            {
              MemberId = result.MemberId,
              Name = resultMember.Name,
              Age = DateTime.Today.Year - resultMember.BirthDate.Value.Year,
              School = result.TeamMembership.Team.School.Name,
              Score = result.ResultScore
            };
            if (resultMember.BirthDate > DateTime.Today.AddYears(-res.Age)) res.Age--;
            if (resultMember.Gender == Gender.MALE)
            {
              groupMale.Results.Add(res);
            }
            else
            {
              groupFemale.Results.Add(res);
            }
          }
          if (groupMale.Results.Count > 0) { details.ResultsGroups.Add(groupMale); }
          if (groupFemale.Results.Count > 0) { details.ResultsGroups.Add(groupFemale); }
        }
      }
      if (User.Identity.IsAuthenticated)
      {
        ViewBag.Member = MemberService.GetMember(email: User.Identity.Name);
        ViewBag.MemberType = "";
        if (ViewBag.Member.ISFAdmin != null) ViewBag.MemberType = "ISFAdmin";
        else if (ViewBag.Member.NSSFRep != null) ViewBag.MemberType = "NSSFRep";
        else if (ViewBag.Member.Coach != null) ViewBag.MemberType = "Coach";
        else if (ViewBag.Member.Student != null) ViewBag.MemberType = "Student";
      }
      ViewBag.meeting = meeting;
      return View(details);
    }

    public ActionResult GetMeetingResults(int meetingId, int eventId, Gender gender,int page=0)
    {
      int pageSize=3;
      List<Result> results = ResultService.OrderListResultsByBestScoreWithinEvent(ResultService.GetResults(pEvent: EventService.GetEvent(eventId), meeting: MeetingService.GetMeeting(meetingId)).Where(x => x.TeamMembership.Team.Gender == gender).ToList());
      List<MeetingDetails.ResultInfo> resInfos = new List<MeetingDetails.ResultInfo>();
      foreach (Result result in results)
      {
        Member resultMember = MemberService.GetMember(result.MemberId);
        MeetingDetails.ResultInfo res = new MeetingDetails.ResultInfo
        {
          MemberId = result.MemberId,
          Name = resultMember.Name,
          Age = DateTime.Today.Year - resultMember.BirthDate.Value.Year,
          School = result.TeamMembership.Team.School.Name,
          Score = result.ResultScore
        };
        if (resultMember.BirthDate > DateTime.Today.AddYears(-res.Age)) res.Age--;
        resInfos.Add(res);
      }
      return PartialView("DetailResultTable", new PaginatedList<MeetingDetails.ResultInfo>(resInfos, page, pageSize));
    }

    [Authorize(Roles = "ISFAdmin")]
    public ActionResult CreateISFMeeting()
    {
      Region world = RegionService.GetFullTree();
      ViewBag.RegionsJson = new JavaScriptSerializer().Serialize(new RegionForJson(world));
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents(official:true).ForEach(x => events.Add(new EventForJson {
        Id=x.EventId, Name=x.Name, SportId=x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name");
      return View();
    }

    [Authorize(Roles = "ISFAdmin")]
    [HttpPost]
    public ActionResult CreateISFMeeting(EditableISFMeeting meeting)
    {
      if (ModelState.IsValid)
      {
        MeetingService.AddISFMeeting(meeting,MemberService.GetMember(email:User.Identity.Name).MemberId);
        return RedirectToAction("Index", "Meeting");
      }
      Region world = RegionService.GetFullTree();
      ViewBag.RegionsJson = new JavaScriptSerializer().Serialize(new RegionForJson(world));
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents(official:true).ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name");
      return View();
    }

    [Authorize(Roles = "NSSFRep")]
    public ActionResult CreateRegionalMeeting()
    {
      NSSF nssf = MemberService.GetMember(email: User.Identity.Name).NSSFRep.NSSF;
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents(official:true,regionId:nssf.Region.RegionId).ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name");
      return View();
    }

    [Authorize(Roles = "NSSFRep")]
    [HttpPost]
    public ActionResult CreateRegionalMeeting(EditableRegionalMeeting meeting)
    {
      if (ModelState.IsValid)
      {
        MeetingService.AddRegionalMeeting(meeting, MemberService.GetMember(email: User.Identity.Name).MemberId);
        return RedirectToAction("Index", "Meeting");
      }
      NSSF nssf = MemberService.GetMember(email: User.Identity.Name).NSSFRep.NSSF;
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents(official: true, regionId: nssf.Region.RegionId).ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name");
      return View();
    }

    [Authorize(Roles = "Coach")]
    public ActionResult CreateCoachMeeting(int teamId)
    {
      Team t = TeamService.GetTeam(teamId);
      if (t.TeamCoaches.Single(x=>x.DateLeft>=DateTime.Today).MemberId != MemberService.GetMember(email: User.Identity.Name).MemberId) return RedirectToAction("Schoolcup", "Schoolcup");
      List<EventForJson> events = new List<EventForJson>();
      t.Sport.Events.ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.TeamId = teamId;
      return View();
    }

    [Authorize(Roles = "Coach")]
    [HttpPost]
    public ActionResult CreateCoachMeeting(EditableCoachMeeting meeting)
    {
      if (ModelState.IsValid)
      {
        MeetingService.AddCoachMeeting(meeting, MemberService.GetMember(email: User.Identity.Name).MemberId);
        return RedirectToAction("Dashboard", "Team", new { id = meeting.TeamId });
      }
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents().ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name");

      return View();
    }

    [Authorize(Roles = "ISFAdmin")]
    public ActionResult EditISFMeeting(int isfMeetingId)
    {
      if (MeetingService.MeetingHasResults(isfMeetingId)) return RedirectToAction("Index", "Administration");
      EditableISFMeeting eim = MeetingService.GetEditableISFMeeting(isfMeetingId);
      if (eim == null) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_CouldNotGetEditableMeeting);
      Region world = RegionService.GetFullTree();
      ViewBag.RegionsJson = new JavaScriptSerializer().Serialize(new RegionForJson(world));
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents().ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name",EventService.GetEvent(Int32.Parse(eim.EventsStr.Split(',').First())).SportId);
      return View(eim);
    }

    [Authorize(Roles = "ISFAdmin")]
    [HttpPost]
    public ActionResult EditISFMeeting(EditableISFMeeting meeting)
    {
      if (MeetingService.MeetingHasResults(meeting.ISFMeetingId.Value)) return RedirectToAction("Index", "Administration");
      if (ModelState.IsValid)
      {
        MeetingService.UpdateISFMeeting(meeting);
        return RedirectToAction("Index", "Administration");
      }
      Region world = RegionService.GetFullTree();
      ViewBag.RegionsJson = new JavaScriptSerializer().Serialize(new RegionForJson(world));
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents().ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name");
      return View();
    }

    [Authorize(Roles = "NSSFRep")]
    public ActionResult EditRegionalMeeting(int regionalMeetingId)
    {
      if (MeetingService.MeetingHasResults(regionalMeetingId)) return RedirectToAction("Index", "Administration");
      EditableRegionalMeeting erm = MeetingService.GetEditableRegionalMeeting(regionalMeetingId);
      if (erm == null) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_CouldNotGetEditableMeeting);
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents().ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name",EventService.GetEvent(Int32.Parse(erm.EventsStr.Split(',').First())).SportId);
      return View(erm);
    }

    [Authorize(Roles = "NSSFRep")]
    [HttpPost]
    public ActionResult EditRegionalMeeting(EditableRegionalMeeting meeting)
    {
      if (MeetingService.MeetingHasResults(meeting.RegionalMeetingId.Value)) return RedirectToAction("Index", "Administration");
      if (ModelState.IsValid)
      {
        MeetingService.UpdateRegionalMeeting(meeting);
        return RedirectToAction("Index", "Administration");
      }
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents().ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name");
      return View();
    }

    [Authorize(Roles = "Coach")]
    public ActionResult EditCoachMeeting(int coachMeetingId)
    {
      if (MeetingService.MeetingHasResults(coachMeetingId)) return RedirectToAction("Index", "Administration");
      EditableCoachMeeting erm = MeetingService.GetEditableCoachMeeting(coachMeetingId);
      if (erm == null) throw new TeamOPSchoolCupException(ErrorStrings.Meeting_CouldNotGetEditableMeeting);
      List<EventForJson> events = new List<EventForJson>();
      Sport s = EventService.GetEvent(Int32.Parse(erm.EventsStr.Split(',').First())).Sport;
      EventService.GetEventsOfSport(s.SportId).ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      return View(erm);
    }

    [Authorize(Roles = "Coach")]
    [HttpPost]
    public ActionResult EditCoachMeeting(EditableCoachMeeting meeting)
    {
      if (MeetingService.MeetingHasResults(meeting.CoachMeetingId.Value)) return RedirectToAction("Index", "Administration");
      if (ModelState.IsValid)
      {
        MeetingService.UpdateCoachMeeting(meeting);
        return RedirectToAction("Schoolcup", "Schoolcup");
      }
      List<EventForJson> events = new List<EventForJson>();
      EventService.GetEvents().ForEach(x => events.Add(new EventForJson
      {
        Id = x.EventId,
        Name = x.Name,
        SportId = x.SportId
      }));
      ViewBag.EventsJson = new JavaScriptSerializer().Serialize(events);
      ViewBag.Sports = new SelectList(SportService.GetSports(false), "SportId", "Name");
      return View();
    }
  }
}
