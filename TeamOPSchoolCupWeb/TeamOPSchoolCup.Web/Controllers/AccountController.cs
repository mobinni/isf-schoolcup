﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TeamOPSchoolCup.Web.Models;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;
//using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class AccountController : Controller
  {
    //
    // GET: /Account/LogOn

    public ActionResult LogOn(string ReturnUrl)
    {
      ViewBag.ReturnUrl = ReturnUrl;
      return PartialView("LogOn",new LogOnModel());
    }

    [HttpPost]
    public ActionResult LogOnPost(LogOnModel model, string ReturnUrl)
    {
      if (ModelState.IsValid)
      {
        try
        {
          if (Membership.GetUser(model.Email) != null)
          {
            if (Membership.ValidateUser(model.Email, model.Password))
            {
              FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);
              if (Url.IsLocalUrl(ReturnUrl) && ReturnUrl.Length > 1 && ReturnUrl.StartsWith("/")
                  && !ReturnUrl.StartsWith("//") && !ReturnUrl.StartsWith("/\\"))
              {
                return Redirect(ReturnUrl);
              }
              Member member = MemberService.GetMember(email: model.Email);
              if (member.Coach != null || member.Student != null)
              {
                return RedirectToAction("SchoolCup", "SchoolCup");
              }
              else if (member.NSSFRep != null || member.SCC != null || member.ISFAdmin != null)
              {
                return RedirectToAction("Index", "Administration");
              }
              else
              {
                return RedirectToAction("Index", "Home");
              }

            }
            else
            {
              return RedirectToAction("ExceptionReplacement", "Error", new { errorString = AccountStrings.IncorrectPassword });
            }
          }
          else
          {
            return RedirectToAction("ExceptionReplacement", "Error", new { errorString = AccountStrings.UserNotFound });
          }
        }
        catch (Exception e)
        {
          return RedirectToAction("ExceptionReplacement", "Error", new { errorString = AccountStrings.UserNotFound });
        }
      }
      return RedirectToAction("ExceptionReplacement", "Error", new { errorString = AccountStrings.UserNotFound });
    }

    public ActionResult LogOff()
    {
      FormsAuthentication.SignOut();

      return RedirectToAction("Index", "Home");
    }

    [Authorize]
    public ActionResult ChangePassword()
    {
      return View();
    }

    [Authorize]
    [HttpPost]
    public ActionResult ChangePassword(ChangePasswordModel model)
    {
      if (ModelState.IsValid)
      {
        MembershipUser user = Membership.GetUser(User.Identity.Name, true);

        if (Membership.ValidateUser(user.UserName, model.OldPassword))
        {
          if (user.ChangePassword(model.OldPassword, model.NewPassword))
          {
            return RedirectToAction("ChangePasswordSuccess");
          }
          else
          {
            ModelState.AddModelError("", TeamOPSchoolCup.Domain.Resources.AccountStrings.NewPasswordInvalid);
          }
        }
        else
        {
          ModelState.AddModelError("", TeamOPSchoolCup.Domain.Resources.AccountStrings.CurrentPasswordIncorrect);
        }
      }
      return View(model);
    }

    [Authorize]
    public ActionResult ChangePasswordSuccess()
    {
      return View();
    }
  }
}
