﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using System.Web.Security;
using TeamOPSchoolCup.Web.Helpers;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{ 
    public class RecordController : Controller
    {
        //
        // GET: /Record/

        public ViewResult Index()
        {
          List<Record> records = RecordService.GetRecords().ToList();
          ViewBag.Sports = new SelectList(SportService.GetSports(true), "SportId", "Name");
          ViewBag.Events = new SelectList(EventService.GetEvents(), "EventId", "Name");
          ViewBag.RecordHolders = new SelectList(RecordService.GetRecordHolders(),"RecordHolderId","Name");
         
          return View(records);
        }
        public ActionResult LoadRecordResult(short gender_value, int? sportId, int? eventId, int? recordHolderId, int? page, int? pageSize)
        {
          int pageSizeValue = pageSize.HasValue ? pageSize.Value : 5; // CHANGE THIS LINE IF YOU LOVE LOGIC
          // Parameters

          List<Record> mr = RecordService.GetRecords(pEventId:eventId, sportId: sportId, recordHolderId: recordHolderId, gender_value:(short?) gender_value);

          var paginatedMR = new PaginatedList<Record>(mr.ToList(), page ?? 0, pageSizeValue);

          return PartialView("Partials/_RecordResultTable", paginatedMR);
        }
        public ActionResult LoadRecordHoldersFromSport(int? id, short? gender_value)
        {
          
          

          ViewBag.RecordHolders = new SelectList(RecordService.GetRecordHoldersOfSport(id, gender_value), "RecordHolderId", "Name");

          return PartialView("Partials/_SportRecordHolders");
        }
      
        public ActionResult LoadEventsFromSport(int? id, short? gender_value)
        {
          
          

          ViewBag.Events = new SelectList(EventService.GetEventsOfSport(id, gender_value), "EventId", "Name");

          return PartialView("Partials/_SportEvents");
        }
        public ActionResult LoadRecordHoldersFromEvent(int? id, short? gender_value)
        {
          
         
            ViewBag.RecordHolders = new SelectList(RecordService.GetRecordHoldersOfEvent(id,gender_value), "RecordHolderId", "Name");
          
          return PartialView("Partials/_EventRecordHolders");
        }
        //
        // GET: /Record/Create

        public ActionResult Create()
        {
          if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
            ViewBag.EventId = new SelectList(EventService.GetEvents(), "EventId", "Name");
            ViewBag.SportId = new SelectList(SportService.GetSports(true), "SportId", "Name");
            return View();
        } 

        //
        // POST: /Record/Create

        [HttpPost]
        public ActionResult Create(CreatableRecord record)
        {
          if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
            if (ModelState.IsValid)
            {
                RecordService.AddRecord(record);
                
                return RedirectToAction("Index");  
            }

            ViewBag.EventId = new SelectList(EventService.GetEvents(), "EventId", "Name", record.EventId);
            ViewBag.SportId = new SelectList(SportService.GetSports(true), "SportId", "Name", record.SportId);
            return View(record);
        }
        public ActionResult LoadEventsFromSportAndGender(int sportId, short gender_value)
        {
          RecordSport recordSport = new RecordSport { SportGender = new SportGender { SportId = sportId, Gender_Value = gender_value }};
          List<RecordSport.EventInfo> eventInfos = new List<RecordSport.EventInfo>();
          foreach (Event e in EventService.GetEventsOfSport(sportId: sportId, gender_value: gender_value))
          {
           
           eventInfos.Add(new RecordSport.EventInfo { EventId = e.EventId, EventName = e.Name });
          }
          recordSport.EventInfos = eventInfos;
          ViewBag.RecordSport = recordSport;
          return Json(recordSport,JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /Record/Edit/5
 
        public ActionResult Edit(int id)
        {
          if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
            Record record =RecordService.GetRecord(id);
            ViewBag.EventId = new SelectList(EventService.GetEvents(), "EventId", "Name", record.EventId);
            ViewBag.SportId = new SelectList(SportService.GetSports(true), "SportId", "Name", record.SportId);  
          return View(record);
        }

        //
        // POST: /Record/Edit/5

        [HttpPost]
        public ActionResult Edit(Record record)
        {
          if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
            if (ModelState.IsValid)
            {
              RecordService.UpdateRecord(record);
                return RedirectToAction("Index");
            }
            ViewBag.EventId = new SelectList(EventService.GetEvents(), "EventId", "Name", record.EventId);
            ViewBag.SportId = new SelectList(SportService.GetSports(true), "SportId", "Name", record.SportId);
            return View(record);
        }

       
    }
}