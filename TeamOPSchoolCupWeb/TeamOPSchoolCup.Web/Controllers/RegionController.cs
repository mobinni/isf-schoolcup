﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class RegionController : Controller
  {

    //
    // GET: /Region/


    public ViewResult Index(int? regionId)
    {
      //return View(RegionService.GetRegionsByHierarchy(true));
      if (regionId == null) regionId = RegionService.GetWorld().RegionId;

      Region region = RegionService.GetRegion(regionId.Value); //.Value since it's a nullable int
      List<Region> children = RegionService.GetChildren(region, true);
      ViewBag.RegionId = region.RegionId;
      ViewBag.ParentId = region.ParentId;
      ViewBag.ParentName = region.Name;

      return View(children);
    }

    /*
     * This action returns a Json string with all the regions
     */
    public String JIndex()
    {
      //if (!Request.UserAgent.Equals("ISFphoneDroid"))
      //{
      //  return null;
      //}
      StringBuilder jsonBuilder = new StringBuilder();
      Region worldRegion = RegionService.GetRegion("World"); //World is the root of the regiontree
      JavaScriptSerializer serializer = new JavaScriptSerializer();

      jsonBuilder.Append("{\"regions\": [");
      RegionService.GenerateJson(worldRegion, jsonBuilder, serializer);
      jsonBuilder.Append("]}");

      return jsonBuilder.ToString();
    }

    //
    // GET: /Region/Create
    [Authorize]
    public ActionResult Create(int? parentId)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      ViewBag.Parent = (parentId.HasValue ? RegionService.GetRegion(parentId.Value) : null);
      return View();
    }

    //
    // POST: /Region/Create
    [Authorize]
    [HttpPost]
    public ActionResult Create(Region region)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid)
      {
        region.DateJoined = DateTime.Today;

        RegionService.AddRegion(region);
        return RedirectToAction("Index", new { regionId = region.ParentId });
      }
      ViewBag.ParentId = new SelectList(RegionService.GetAllRegions(), "RegionId", "Name", region.ParentId);
      return View(region);
    }
    //
    // GET: /Region/Edit/5

    public ActionResult Edit(int id)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Region region = RegionService.GetRegion(id);
      ViewBag.ParentId = new SelectList(RegionService.GetAllRegions().Where(x => x.RegionId!=id).ToList(), "RegionId", "Name", region.ParentId);
      return View(region);
    }

    //
    // POST: /Test/Edit/5

    [HttpPost]
    public ActionResult Edit(Region region)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Region originalRegion = RegionService.GetRegion(region.RegionId);
      region.DateJoined = originalRegion.DateJoined;
      if (ModelState.IsValid)
      {
        RegionService.UpdateRegion(region);
        return RedirectToAction("Index");
      }
      ViewBag.ParentId = new SelectList(RegionService.GetAllRegions().Where(x => x.RegionId != region.RegionId).ToList(), "RegionId", "Name", region.ParentId);
      return View(region);
    }
  }
}