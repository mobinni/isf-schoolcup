﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.DTO;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class ConversationController : Controller
  {
    //
    // GET: /Conversation/Create

    [Authorize(Roles = "Student,Coach")]
    public ActionResult Create(int teamId) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Member m = MemberService.GetMember(email: User.Identity.Name);
      Team team = TeamService.GetTeam(teamId);
      if (m.Coach != null)
      {
        if (!m.Coach.TeamCoaches.Select(x => x.TeamId).Contains(team.TeamId)) throw new TeamOPSchoolCupException(ErrorStrings.Team_Dashboard_CoachNoAccess);
        ViewBag.StudCoach = false;
      }
      else if (m.Student != null)
      {
        if (!m.Student.TeamMemberShips.Select(x => x.TeamId).Contains(team.TeamId)) throw new TeamOPSchoolCupException(ErrorStrings.Team_Dashboard_StudentNoAccess);
        ViewBag.StudCoach = true;
      }
      if (m.Coach != null) {
        ViewBag.Teams = new SelectList(TeamService.GetTeams(coach: m.Coach, historicality: TeamService.Historicality.CURRENT));
        ViewBag.StudCoach = false;
      }
      if (m.Student != null) {
        ViewBag.Teams = new SelectList(TeamService.GetTeams(student: m.Student, historicality: TeamService.Historicality.CURRENT));
        ViewBag.StudCoach = true;
        
      }
      Conversation conversation = new Conversation();
      conversation.TeamId = teamId;
      return View(conversation);
    }



    //
    // POST: /Conversation/Create


    [HttpPost]
    [Authorize(Roles = "Student,Coach")]
    public ActionResult Create(Conversation conversation) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid) {
        Member m = MemberService.GetMember(email: User.Identity.Name);
        conversation.DatePosted = DateTime.Now;
        //ConversationService.AddTopic(MemberService.GetMember(email:User.Identity.Name),conversation:conversation);
        if (m.Coach != null) ConversationService.AddTopic(coach: m.Coach, conversation: conversation);
        if (m.Student != null) ConversationService.AddTopic(student: m.Student, conversation: conversation);


        return RedirectToAction("Dashboard", "Team", new { id = conversation.TeamId });

      }

      ViewBag.TeamId = new SelectList(TeamService.GetTeams(), "TeamId", "Name");
      return RedirectToAction("Dashboard", "Team", new { id = conversation.TeamId });
    }
    //
    // GET: /Conversation/Edit/5

    public ActionResult Edit(int id) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Topic topic = ConversationService.GetTopic(id);

      //Conversation conv = new Conversation { Title = topic.Title, TopicId = topic.TopicId, TeamId = topic.TeamId };
      ViewBag.Content = ConversationService.GetFirstMessage(topic).Content;
      Message message = ConversationService.GetFirstMessage(topic);
      return View(message);
    }

    //
    // POST: /Conversation/Edit/5

    [HttpPost]
    public ActionResult Edit(Message message) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { return RedirectToAction("Index", "Home"); }
      Topic topic = ConversationService.GetTopic(message.TopicId);
      if (ModelState.IsValid) {        
        ConversationService.UpdateFirstMessageOfTopic(topic, message.Content);
        return RedirectToAction("Topic", "Team", new { id = message.TopicId });
      }
      return RedirectToAction("Dashboard", "Team", new { id = topic.TeamId }); 
    }
  }
}