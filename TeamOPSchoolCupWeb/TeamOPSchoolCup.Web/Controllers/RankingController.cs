﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Web.Helpers;


namespace TeamOPSchoolCup.Web.Controllers
{
  public class RankingController : Controller
  {
    //
    // GET: /Ranking/

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult LoadStudentRankings(int? pEventId, int? year, int? regionId,
        Gender? gender, 
        ParticipantAge? age,
        int? page, int? pageSize)
    {
        int pageSizeValue = pageSize.HasValue ? pageSize.Value : 50; // CHANGE THIS LINE IF YOU LOVE LOGIC
        // Parameters
        int eventValue = pEventId.HasValue ? pEventId.Value : 1;
        int regionValue = regionId.HasValue ? regionId.Value : 5;

        Region region = RegionService.GetRegion(regionValue);
        List<BestStudentResult> bsr = RankingService.GetStudentRanking(pEvent: EventService.GetEvent(eventValue),
        year: year, isOfficial: true, gender: gender, participantAge: age, regions: new List<Region> { region });

        var paginatedBSR = new PaginatedList<BestStudentResult>(bsr.ToList(), page ?? 0, pageSizeValue);

        return PartialView("Partials/_StudentRankingTable", paginatedBSR);
    }

    public ActionResult LoadTeamRankings(int? sportId, int? year, int? regionId, Gender? gender, int? page, int? pageSize)
    {
        // number of elements per page
        int pageSizeValue = pageSize.HasValue ? pageSize.Value : 10; // CHANGE THIS LINE IF YOU LOVE LOGIC
        // Parameters
        int sportValue = sportId.HasValue ? sportId.Value : 1;
        int regionValue = regionId.HasValue ? regionId.Value : 1;
        Region region = RegionService.GetRegion(regionValue);
        // Get BTR
        List<BestTeamResult> btr = RankingService.GetTeamRanking(sport: SportService.GetSport(sportValue), gender: gender, year: year,regions: new List<Region> { region });

        // Get Paginated list
        var paginatedBTR = new PaginatedList<BestTeamResult>(btr.ToList(), page ?? 0, pageSizeValue);

        return PartialView("Partials/_TeamRankingTable", paginatedBTR);
    }

    public ActionResult LoadEventsFromSport(int? id) 
    {
        int sportValue = id.HasValue ? id.Value : 1;
        

        ViewBag.events = new SelectList(EventService.GetEventsOfSport(sportValue), "EventId", "Name");
        return PartialView("Partials/_SportEvents");
    }

    public ActionResult Team()
    {
        // Get world subregions
        ViewBag.team_regions = RegionService.GetAllRegions();

        ViewBag.team_sports = new SelectList(SportService.GetSports(true), "SportId", "Name");
        ViewBag.team_events = new SelectList(EventService.GetEventsOfSport(SportService.GetSport(abbr:"ATH").SportId), "EventId", "Name");

       
        return View("Team");
    }

    public ActionResult Student()
    {
        ViewBag.student_regions = RegionService.GetAllRegions();
        ViewBag.student_sports = new SelectList(SportService.GetSports(true), "SportId", "Name");
        ViewBag.student_events = new SelectList(EventService.GetEventsOfSport(SportService.GetSport(abbr:"ATH").SportId), "EventId", "Name");
        return View("Student");
    }


    public ActionResult StudentPartial() {
        ViewBag.student_regions = RegionService.GetAllRegions();

        ViewBag.student_sports = new SelectList(SportService.GetSports(true), "SportId", "Name");
        ViewBag.student_events = new SelectList(EventService.GetEventsOfSport(SportService.GetSport(abbr:"ATH").SportId), "EventId", "Name");
        // Gender values
        var genders = from Gender e in Enum.GetValues(typeof(Gender))
                      select new { Id = e, Name = e };
        genders = genders.Where(x => x.Name != Gender.UNISEX);

        ViewBag.student_genders = new SelectList(genders, "Id", "Name");
        ViewBag.student_years = new SelectList(CompetitionYearService.GetCompetitionYears(), "CompetitionYearValue", "CompetitionYearValue");

        var ages = from ParticipantAge e in Enum.GetValues(typeof(ParticipantAge))
                   select new { Id = (int)e, Name = (int)e };
        ViewBag.student_ages = new SelectList(ages, "Id", "Name");

        return PartialView("Partials/_Student");
    }

    public ActionResult TeamPartial()
    {
        // Get world subregions
        ViewBag.team_regions = RegionService.GetAllRegions();
        ViewBag.team_sports = new SelectList(SportService.GetSports(true), "SportId", "Name");
        ViewBag.team_years = new SelectList(CompetitionYearService.GetCompetitionYears(), "CompetitionYearValue", "CompetitionYearValue");
        var genders = from Gender e in Enum.GetValues(typeof(Gender))
                      select new { Id = e, Name = e };
        genders = genders.Where(x => x.Name != Gender.UNISEX);

        ViewBag.team_genders = new SelectList(genders, "Id", "Name");

        return PartialView("Partials/_Team");
    }


  }
}
