﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using System.IO;
using TeamOPSchoolCup.Web.Helpers;
using System.Web.Script.Serialization;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Resources;
using TeamOPSchoolCup.Domain.Exceptions;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class TeamController : Controller
  {

    public ViewResult Profile(int id) {
      Team team = TeamService.GetTeam(id);
      ViewBag.teamRankings = RankingService.GetRankings(team);
      ViewBag.teamMembers = TeamService.GetTeamMemberships(team, null, TeamService.Historicality.CURRENT);
      string photoPath = "~/Upload/Teampicture/"+id+".jpeg";
      string photoPathTest = Server.MapPath("~/Upload/Teampicture/" + id + ".jpeg");
      string logoPath = "~/Upload/Teamlogo/" + id + ".jpeg";
      string logoPathTest = Server.MapPath("~/Upload/Teamlogo/" + id + ".jpeg");      
      ViewBag.photopath = photoPath;
      ViewBag.photopathTest = photoPathTest;
      ViewBag.logopath = logoPath;
      ViewBag.logopathTest = logoPathTest;
      return View(team);

    }
    public ActionResult TeamResults(int teamId)
    {
      int year = DateTime.Today.Year;
      Team team = TeamService.GetTeam(teamId);
      List<Event> events = EventService.GetEvents(teamId: teamId);
      List<Result> results = ResultService.GetResults(team: team);
                
      TeamComparison.StudentsInTeamsWithEvents teamData = new TeamComparison.StudentsInTeamsWithEvents();
      teamData.TeamId = team.TeamId;
      teamData.Name = team.Name;
      foreach (TeamMembership tm1 in TeamService.GetTeamMemberships(team))
      {
        TeamComparison.StudentsParticipatingEvents spe = new TeamComparison.StudentsParticipatingEvents();
        spe.Id = tm1.MemberId;
        spe.Name = tm1.Student.Member.Name;
        events.ForEach(x => spe.ParticipatingEvents.Add(x.Name, EventService.GetEvents(tm1.MemberId).Select(y => y.EventId).Contains(x.EventId)));
        teamData.Students.Add(spe);
      }

     
      List<TeamComparison.StudentsInTeamsWithEvents> data = new List<TeamComparison.StudentsInTeamsWithEvents>();
      data.Add(teamData);
      ViewBag.StudentsInTeamsWithEventsData = data;
      ViewBag.Sports = new SelectList(SportService.GetSports(true), "SportId", "Name");
      ViewBag.Meetings = new SelectList(MeetingService.GetMeetings(team: team), "MeetingId", "Name");
      ViewBag.Events = new SelectList(EventService.GetEventsOfMeeting(MeetingService.GetMeeting(4)), "EventId", "Name");
                
      ViewBag.StudentsInTeamsWithEvents = new JavaScriptSerializer().Serialize(data);
      return View(results);
    }
    public ActionResult LoadTeamResult(int teamId, bool? official, int? sportId, int? meetingId, int? pEventId, int? page, int? pageSize)
    {
      int pageSizeValue = pageSize.HasValue ? pageSize.Value : 5; // CHANGE THIS LINE IF YOU LOVE LOGIC
      // Parameters

      List<Result> mr = ResultService.GetTeamResults(teamId, official, sportId, meetingId, pEventId);

      var paginatedMR = new PaginatedList<Result>(mr.ToList(), page ?? 0, pageSizeValue);

      return PartialView("Partials/_TeamResultsTable", paginatedMR);
    }
    [Authorize(Roles="Student,Coach")]
    public ActionResult Dashboard(int id) {
      Team team = TeamService.GetTeam(id);
      Member m = MemberService.GetMember(email: User.Identity.Name);

      if (m.Coach != null)
      {
        if (!m.Coach.TeamCoaches.Select(x => x.TeamId).Contains(team.TeamId)) throw new TeamOPSchoolCupException(ErrorStrings.Team_Dashboard_CoachNoAccess);
        ViewBag.StudCoach = false;
      }
      else if (m.Student != null)
      {
        if (!m.Student.TeamMemberShips.Select(x => x.TeamId).Contains(team.TeamId)) throw new TeamOPSchoolCupException(ErrorStrings.Team_Dashboard_StudentNoAccess);
        ViewBag.StudCoach = true;
      }

      ViewBag.BestTeamResult = RankingService.GetBestTeamResult(team,DateTime.Today.Year);
      ViewBag.Topics = ConversationService.GetTopicsOfTeam(team, true);
      ViewBag.UserId = m.MemberId;
      ViewBag.TeamRankings = RankingService.GetRankings(team);
      ViewBag.LatestTopicMessage = ConversationService.GetFirstMessage(ConversationService.GetLatestSpotlightTopic(team));
      return View(team);
    }

    public ActionResult Topic(int id) {
      Topic topic = ConversationService.GetTopic(id);
      ViewBag.Topics = ConversationService.GetTopicsOfTeam(topic.Team, true);
      ViewBag.Messages = ConversationService.GetMessagesOfTopic(id);
      ViewBag.Topic = topic;
      Message message = new Message();
      message.TopicId = topic.TopicId;
      return View(message);
    }

    [HttpPost]
    public ActionResult Topic(Message message) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.Member_Inactive_NoAccess); }
      if (ModelState.IsValid) {
        if (User.Identity.IsAuthenticated) {
          Member m = MemberService.GetMember(email: User.Identity.Name);
          if (m.Student != null || m.Coach != null) {
            message.DatePosted = DateTime.Now;
            if (m.Student != null && m.Coach == null) {
              ConversationService.AddMessage(message, studentId: m.MemberId, topicId: message.TopicId);
            }
            else {
              ConversationService.AddMessage(message, coachId: m.MemberId, topicId: message.TopicId);
            }
          }
        }
      }
      return RedirectToAction("Topic", new { id = message.TopicId });
    }

    //
    // GET: /Team/Create

    public ActionResult Create() {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      ViewBag.SportId = new SelectList(SportService.GetSports(true), "SportId", "Name");
      return View();
    }

    //
    // POST: /Team/Create

    [HttpPost]
    public ActionResult Create(Team team, HttpPostedFileBase Logo, HttpPostedFileBase Photo) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid) {
        Coach coach = MemberService.GetCoach(MemberService.GetMember(email: User.Identity.Name).MemberId);
        
        team.SchoolId = coach.SchoolId;

        TeamService.AddTeam(team);

        if (Photo != null && Logo != null
          && Photo.ContentLength > 0 && Logo.ContentLength > 0) {
          new ImageHelper().Save(new System.Drawing.Bitmap(Photo.InputStream), 180, 130, 80, Path.Combine(Server.MapPath("~/Upload/Teampicture"), "" + team.TeamId + ".jpeg"));
          new ImageHelper().Save(new System.Drawing.Bitmap(Logo.InputStream), 100, 100, 80, Path.Combine(Server.MapPath("~/Upload/Teamlogo"), "" + team.TeamId + ".jpeg"));
        }
        if (Photo != null &&Photo.ContentLength > 0
                  && Logo == null) {
          new ImageHelper().Save(new System.Drawing.Bitmap(Photo.InputStream), 180, 130, 80, Path.Combine(Server.MapPath("~/Upload/Teampicture"), "" + team.TeamId + ".jpeg"));
        }
        if (Photo == null
                && Logo != null && Logo.ContentLength > 0) {
          new ImageHelper().Save(new System.Drawing.Bitmap(Logo.InputStream), 180, 130, 80, Path.Combine(Server.MapPath("~/Upload/Teamlogo"), "" + team.TeamId + ".jpeg"));
        }

        if (!MemberService.AddCoachToTeam(coach.MemberId, team.TeamId))
        {
          throw new Exception();
        }
        
      }
     return RedirectToAction("SchoolCup", "SchoolCup");
    }

    [Authorize(Roles="Coach")]
    public ActionResult AddTeamMembership(int id) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.AddTeamMembership_NoPermission); }
      Team t = TeamService.GetTeam(id: id);
      if (t.TeamCoaches.Single(tc => tc.DateLeft >= DateTime.Today).MemberId != MemberService.GetMember(email: User.Identity.Name).MemberId) { throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting); /*TODO ERROR*/ }
      List<Student> students = TeamService.GetPotentialTeamMembers(t);
      ViewBag.Team = t;
      return View(students);
    }

    //[HttpPost]
    [Authorize(Roles = "Coach")]
    public ActionResult ActualAddTeamMembership(int teamId, Guid studentId)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting); }
      if (TeamService.AddTeamMembership(teamId, studentId)) {
        return RedirectToAction("AddTeamMembership", new { id = teamId });
      }
      else { throw new TeamOPSchoolCupException(ErrorStrings.Result_CantAddResultsToMeeting); }
    }

    [Authorize(Roles="Coach")]
    public ActionResult TeamManagement(int id) {
      Team team = TeamService.GetTeam(id);
      if (User.Identity.IsAuthenticated) {
        Member m = MemberService.GetMember(email:User.Identity.Name);
        if (m.Coach != null)
        {
          if (!m.Coach.TeamCoaches.Select(x => x.TeamId).Contains(team.TeamId)) throw new TeamOPSchoolCupException(ErrorStrings.Team_Dashboard_CoachNoAccess);
          ViewBag.StudCoach = false;
        }
        if (m.Student != null || m.Coach != null) {
          if (m.Student != null && m.Coach == null) {
            ViewBag.StudCoach = true;
          }
          else {
            ViewBag.StudCoach = false;
          }
        }
      }
      EditableTeam edTeam = new EditableTeam(team);

      return View(edTeam);
    }

    [HttpPost]
    public ActionResult TeamManagement(EditableTeam eteam) {
      if (ModelState.IsValid) {
        if (TeamService.EditTeamFromEditableTeam(eteam) && eteam.Photo != null && eteam.Logo != null
          && eteam.Photo.ContentLength > 0 && eteam.Logo.ContentLength > 0) {
          new ImageHelper().Save(new System.Drawing.Bitmap(eteam.Photo.InputStream), 180, 130, 100, Path.Combine(Server.MapPath("~/Upload/Teampicture"), "" + eteam.TeamId + ".jpeg"));
          new ImageHelper().Save(new System.Drawing.Bitmap(eteam.Logo.InputStream), 100, 100, 100, Path.Combine(Server.MapPath("~/Upload/Teamlogo"), "" + eteam.TeamId + ".jpeg"));          
        }
        if (TeamService.EditTeamFromEditableTeam(eteam) && eteam.Photo != null && eteam.Photo.ContentLength > 0
                  && eteam.Logo == null) {
                    new ImageHelper().Save(new System.Drawing.Bitmap(eteam.Photo.InputStream), 180, 130, 100, Path.Combine(Server.MapPath("~/Upload/Teampicture"), "" + eteam.TeamId + ".jpeg"));
        }
        if (TeamService.EditTeamFromEditableTeam(eteam) && eteam.Photo == null
                && eteam.Logo != null && eteam.Logo.ContentLength > 0) {
                  new ImageHelper().Save(new System.Drawing.Bitmap(eteam.Logo.InputStream), 180, 130, 100, Path.Combine(Server.MapPath("~/Upload/Teamlogo"), "" + eteam.TeamId + ".jpeg"));
        }
        return RedirectToAction("SchoolCup", "SchoolCup");
      }

      return RedirectToAction("Index", "Home");
    }

  }
}