﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.DTO;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Web.Helpers;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class SchoolCupController : Controller
  {
    //
    // GET: /SchoolCup/

    public ActionResult SchoolCup()
    {
      ViewBag.StudCoach = false;

      if (User.Identity.IsAuthenticated)
      {
        Member m = MemberService.GetMember(email: User.Identity.Name);

        if (m.Student != null || m.Coach != null)
        {
          if (m.Student != null && m.Coach == null)
          {
            ViewBag.StudCoach = true;
            ViewBag.StudRankings = RankingService.GetRankings(m.Student);
            ViewBag.TeamList = TeamService.GetTeams(student: m.Student, historicality: TeamService.Historicality.CURRENT);
          }
          else
          {
            ViewBag.StudCoach = false;
            ViewBag.TeamList = TeamService.GetTeams(coach: m.Coach, historicality: TeamService.Historicality.CURRENT);
          }
          return View(m);
        }
      }
      return RedirectToAction("Index", "Home");
      //List<Team> teams;
      //if(ViewBag.StudCoach){
      //  Student s = MemberService.GetStudent(id);
      //  teams = TeamService.GetTeams(null,s,TeamService.Historicality.CURRENT,null,null);
      //}
      //else {
      //  Coach 
      //  teams = TeamService.GetTeams     
      //}         

    }

    public ActionResult MyResults(Guid? studentId = null, string query = null)
    {
      Member m = null;
      if (studentId != null)
      {
        m = MemberService.GetMember(studentId);
      }
      else
        if (User.Identity.IsAuthenticated)
        {
          m = MemberService.GetMember(email: User.Identity.Name);
        }

      if (m.Student != null || m.Coach != null)
      {



        //Juiste code List<Result> results = ResultService.GetResults(student: m.Student);
        //om te testen

        Student stud = MemberService.GetStudent(m.MemberId);
        List<Result> results = ResultService.GetResults(student: stud);
        if (query != null)
        {
          ViewBag.Query = query;
        }
        List<StudentMemberships.TeamInfo> teamInfos = new List<StudentMemberships.TeamInfo>();
        foreach (TeamMembership tm in stud.TeamMemberShips)
        {
          teamInfos.Add(new StudentMemberships.TeamInfo { TeamId = tm.TeamId, Name = tm.Team.Name });
        }




        StudentMemberships studmem = new StudentMemberships { StudentId = stud.MemberId, TeamInfos = teamInfos };
        ViewBag.StudentJson = new JavaScriptSerializer().Serialize(studmem);
        ViewBag.Sports = new SelectList(SportService.GetSports(true), "SportId", "Name");
        ViewBag.Teams = new SelectList(teamInfos, "TeamId", "Name");
        ViewBag.Meetings = new SelectList(MeetingService.GetMeetings(team: TeamService.GetTeam(teamInfos[0].TeamId)), "MeetingId", "Name");
        ViewBag.Events = new SelectList(EventService.GetEventsOfMeeting(MeetingService.GetMeeting(4)), "EventId", "Name");
        ViewBag.Student = stud;
        return View(results);
      }



      return RedirectToAction("SchoolCup");
    }
    public ActionResult LoadMyResult(Guid studentId, bool? official, int? sportId, int? teamId, int? meetingId, int? pEventId, int? page, int? pageSize)
    {
      int pageSizeValue = pageSize.HasValue ? pageSize.Value : 5; // CHANGE THIS LINE IF YOU LOVE LOGIC
      // Parameters

      List<Result> mr = ResultService.GetMyResults(studentId, official, sportId, teamId, meetingId, pEventId);

      var paginatedMR = new PaginatedList<Result>(mr.ToList(), page ?? 0, pageSizeValue);

      return PartialView("Partials/_MyResultTable", paginatedMR);
    }
    public ActionResult LoadStudentResults(string query, short gender_value, int? page, int? pageSize)
    {
      int pageSizeValue = pageSize.HasValue ? pageSize.Value : 10;
      List<Student> student_results = SearchService.SearchStudents(query,gender_value:(short?)gender_value);
      var paginatedResults = new PaginatedList<Student>(student_results.ToList(), page ?? 0, pageSizeValue);
      return PartialView("Partials/_StudentResults", paginatedResults);
    }
    public ActionResult LoadTeamsFromSport(int? id, Guid? memberId)
    {
      int sportValue = id.HasValue ? id.Value : 1;
      Sport sport = SportService.GetSport(sportValue);
      Member m = null;
      if (memberId != null)
      {
        m = MemberService.GetMember(memberId);
      }
      else
      {
        m = MemberService.GetMember(email: User.Identity.Name);
      }
      if (m.Student != null)
      {
        ViewBag.Teams = new SelectList(TeamService.GetTeams(student: MemberService.GetStudent(memberId.Value), sport: sport), "TeamId", "Name");
      }
      if (m.Coach != null)
      {
        ViewBag.Teams = new SelectList(TeamService.GetTeams(coach: MemberService.GetCoach(memberId.Value), sport: sport), "TeamId", "Name");
      }
      return PartialView("Partials/_SportTeams");
    }
    public ActionResult LoadMeetingsFromTeam(int? id)
    {
      int teamValue = id.HasValue ? id.Value : 1;
      Team team = TeamService.GetTeam(teamValue);

      ViewBag.Meetings = new SelectList(MeetingService.GetMeetings(team: team), "MeetingId", "Name");
      return PartialView("Partials/_TeamMeetings");
    }
    public ActionResult LoadEventsFromMeeting(int? id)
    {
      int meetingValue = id.HasValue ? id.Value : 1;
      Meeting meeting = MeetingService.GetMeeting(meetingValue);
      ViewBag.Events = new SelectList(EventService.GetEventsOfMeeting(meeting), "EventId", "Name");
      return PartialView("Partials/_MeetingEvents");
    }

    public ActionResult GetGraphData(string memberIds = null, Guid? memberId = null)
    {
      List<Student> studs = new List<Student>();
      if (memberIds != null && !memberIds.Equals(""))
      {
        string[] memIds = memberIds.Split(',');


        foreach (string memId in memIds)
        {
          studs.Add(MemberService.GetStudent(new Guid(memId)));
        }
      }
      if (memberId != null)
      {
        studs.Add(MemberService.GetStudent(memberId.Value));
      }
      if (studs.Count == 0) { return Json(null, JsonRequestBehavior.AllowGet); }
      StudentComparison studentComparison = ComparisonService.GetStudentComparison(studs, DateTime.Today.Year);

      //ViewBag.SCJson = new JavaScriptSerializer().Serialize(studentComparison);
      ViewBag.SC = studentComparison;
      JsonResult jr = new JsonResult();
      jr.Data = new JavaScriptSerializer().Serialize(studentComparison);
      return Json(studentComparison, JsonRequestBehavior.AllowGet);
    }
    public ActionResult GetStudentIdsOfTeam(int teamId)
    {
      List<Guid> studentIds = new List<Guid>();
      foreach (Student stud in TeamService.GetStudents(teamId))
      {
        studentIds.Add(stud.MemberId);
      }

      return Json(studentIds, JsonRequestBehavior.AllowGet);
    }
  }
}
