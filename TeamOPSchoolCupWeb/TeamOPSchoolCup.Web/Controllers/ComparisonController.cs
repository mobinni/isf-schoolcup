﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.POCO.Members;
using TeamOPSchoolCup.Domain.POCO.Ranking;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using System.Reflection;
using TeamOPSchoolCup.Domain.DTO;
using System.Web.Script.Serialization;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class ComparisonController : Controller
  {

    //
    // GET: /comparison/compare?id1=12&id2=13&sportId=1&year=2013 

    public ActionResult Compare(string memberIds, string id1, string id2, int? year = null)
    {
      Guid guid1, guid2;
      
      if (memberIds != null && !memberIds.Equals(""))
      {
        string[] memIds = memberIds.Split(',');

        if (memIds.Length == 2)
        {

          id1 = memIds[0];
          id2 = memIds[1];
        }
        else
        {
          return RedirectToAction("Index", "Home");
        }
      } 
      
      try
      {
        guid1 = (id1 == null ? new Guid("") : new Guid(id1));
      }
      catch (FormatException fme)
      {
        if (!User.Identity.IsAuthenticated) throw new TeamOPSchoolCupException("You have to be logged in to compare yourself.");
        
        guid1 = MemberService.GetMember(email: User.Identity.Name).MemberId;
      }
      try
      {
        guid2 = (id2 == null ? new Guid("") : new Guid(id2));
      }
      catch (FormatException fme)
      {
        if (!User.Identity.IsAuthenticated) throw new TeamOPSchoolCupException("You have to be logged in to compare yourself.");

        guid2 = MemberService.GetMember(email: User.Identity.Name).MemberId;
      }
      Student stud1 = MemberService.GetStudent(guid1);
      Student stud2 = MemberService.GetStudent(guid2);
      if (stud1.Member.Gender_Value != stud2.Member.Gender_Value) throw new TeamOPSchoolCupException(ErrorStrings.CantCompareStudentDifferentGenderException);
      List<Student> studs = new List<Student>();
      studs.Add(stud1);
      studs.Add(stud2);

      StudentComparison studentComparison = ComparisonService.GetStudentComparison(studs, year);
      ViewBag.SCJson = new JavaScriptSerializer().Serialize(studentComparison);

      return View(studentComparison);
    }

    public ActionResult CompareTeam(int id1, int id2)
    {
      Team team1 = TeamService.GetTeam(id1);
      Team team2 = TeamService.GetTeam(id2);
      if (team1.SportId != team2.SportId) throw new TeamOPSchoolCupException(ErrorStrings.Comparison_TeamsHaveDifferentSports);

      TeamComparison teamComparison = ComparisonService.GetTeamComparison(team1, team2);
      ViewBag.TCJson = new JavaScriptSerializer().Serialize(teamComparison);

      //Graph #1: History of points
      List<TeamComparison.PointInfo> pointHistoryT1 = team1.TeamResults.Select(x => new TeamComparison.PointInfo { Points = x.PointValue, DateAcquired = x.Meeting.Date }).ToList();
      List<TeamComparison.PointInfo> pointHistoryT2 = team2.TeamResults.Select(x => new TeamComparison.PointInfo { Points = x.PointValue, DateAcquired = x.Meeting.Date }).ToList();
      List<TeamComparison.PointEvent> pointEvents = new List<TeamComparison.PointEvent>();
      List<DateTime> commonDates = pointHistoryT1.Select(x => x.DateAcquired).Union(pointHistoryT2.Select(x => x.DateAcquired)).ToList();
      foreach (DateTime d in commonDates)
      {
        TeamComparison.PointInfo piT1 = pointHistoryT1.FirstOrDefault(x => x.DateAcquired.Equals(d));
        TeamComparison.PointInfo piT2 = pointHistoryT2.FirstOrDefault(x => x.DateAcquired.Equals(d));
        pointEvents.Add(new TeamComparison.PointEvent { Date = d, PointsT1 = (piT1 == null ? null : (double?)piT1.Points), PointsT2 = (piT2 == null ? null : (double?)piT2.Points) });
      }
      pointEvents = pointEvents.OrderBy(x => x.Date).ToList();
      ViewBag.PointEvents = new JavaScriptSerializer().Serialize(pointEvents);

      //Graph #2: History of TeamResults per event
      List<TeamComparison.TeamResultEvent> tres = new List<TeamComparison.TeamResultEvent>();
      foreach (TeamComparison.ComparisonLine cl in teamComparison.ComparisonLines)
      {
        commonDates = cl.ResultsT1.Select(x => x.DateAcquired).Union(cl.ResultsT2.Select(x => x.DateAcquired)).ToList();
        foreach (DateTime d in commonDates)
        {
          TeamComparison.ResultInfo riT1 = cl.ResultsT1.FirstOrDefault(x => x.DateAcquired.Equals(d));
          TeamComparison.ResultInfo riT2 = cl.ResultsT2.FirstOrDefault(x => x.DateAcquired.Equals(d));
          tres.Add(new TeamComparison.TeamResultEvent { Date = d, EventName = cl.EventName, ResultT1 = (riT1 == null ? null : (double?)riT1.ResultScore), ResultT2 = (riT2 == null ? null : (double?)riT2.ResultScore) });
        }
      }
      tres = tres.OrderBy(x => x.Date).ToList();
      ViewBag.TeamResultEvents = new JavaScriptSerializer().Serialize(tres);

      //Students in the teams with events
      int year = DateTime.Today.Year;
      List<Event> commonEvents = EventService.GetCommonEventsOfTeam(team1, team2, year);

      TeamComparison.StudentsInTeamsWithEvents team1Data = new TeamComparison.StudentsInTeamsWithEvents();
      team1Data.TeamId = team1.TeamId;
      team1Data.Name = team1.Name;
      foreach (TeamMembership tm1 in TeamService.GetTeamMemberships(team1))
      {
        TeamComparison.StudentsParticipatingEvents spe = new TeamComparison.StudentsParticipatingEvents();
        spe.Id = tm1.MemberId;
        spe.Name = tm1.Student.Member.Name;
        commonEvents.ForEach(x => spe.ParticipatingEvents.Add(x.Name, EventService.GetEvents(tm1.MemberId).Select(y => y.EventId).Contains(x.EventId)));
        team1Data.Students.Add(spe);
      }

      TeamComparison.StudentsInTeamsWithEvents team2Data = new TeamComparison.StudentsInTeamsWithEvents();
      team2Data.TeamId = team2.TeamId;
      team2Data.Name = team2.Name;
      foreach (TeamMembership tm2 in TeamService.GetTeamMemberships(team2))
      {
        TeamComparison.StudentsParticipatingEvents spe = new TeamComparison.StudentsParticipatingEvents();
        spe.Id = tm2.MemberId;
        spe.Name = tm2.Student.Member.Name;
        commonEvents.ForEach(x => spe.ParticipatingEvents.Add(x.Name, EventService.GetEvents(tm2.MemberId).Select(y => y.EventId).Contains(x.EventId)));
        team2Data.Students.Add(spe);
      }
      List<TeamComparison.StudentsInTeamsWithEvents> data = new List<TeamComparison.StudentsInTeamsWithEvents>();
      data.Add(team1Data);
      data.Add(team2Data);
      ViewBag.StudentsInTeamsWithEventsData = data;
      ViewBag.StudentsInTeamsWithEvents = new JavaScriptSerializer().Serialize(data);

      return View(teamComparison);
    }
  }
}
