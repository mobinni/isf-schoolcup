﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.Resources;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Exceptions;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class NSSFController : Controller
  {

    //
    // GET: /NSSF/

    public ViewResult Index() {
      var nssfs = NSSFService.GetNSSFs().ToList();
      return View(nssfs);
    }

    //
    // GET: /NSSF/Details/5

    public ViewResult Details(int nssfid) {
      NSSF nssf = NSSFService.GetNSSF(nssfid: nssfid);
      return View(nssf);
    }

    //
    // GET: /NSSF/Create
    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Create() {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      ViewBag.RegionId = new SelectList(RegionService.GetStructuredRegions(), "RegionId", "Name");
      return View();
    }
      
    //
    // POST: /NSSF/Create

    [HttpPost]
    [Authorize(Roles = "ISFAdmin")]
    public ActionResult Create(NSSFRegion nssfregion) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid) {
        Region region = RegionService.GetRegion(nssfregion.RegionId);
        NSSF nssf = new NSSF
        {
          Name = nssfregion.Name,
          City = nssfregion.City,
          Street = nssfregion.Street,
          Country = nssfregion.Country,
          PhoneNumber = nssfregion.PhoneNumber,
          FaxNumber = nssfregion.FaxNumber,
          DateAdded = DateTime.Now,
          Zipcode = nssfregion.Zipcode,
          Email = nssfregion.Email,
          Region = region
        };
        NSSFService.AddNSSF(nssf);
        string body = string.Format("{0}{1}{2}{3}{4}", MailStrings.DefaultAnonGreeting, MailStrings.DefaultAutomatedWarning, string.Format(MailStrings.NSSFAdded, "http://localhost:61513/en/Member/AddNSSFRep?nssfemail=" + nssfregion.Email /*Url.Action("AddNSSFRep", "Member")*/), MailStrings.DefaultWrongAction, MailStrings.DefaultSignoff);
        MailService.SendMail(MailStrings.SubjectNSSFAdded, new List<string>() { nssf.Email }, body, true);


        return RedirectToAction("Index");
      }
      ViewBag.RegionId = new SelectList(RegionService.GetStructuredRegions(), "RegionId", "Name");

      return View(nssfregion);
    }

    //
    // GET: /NSSF/Edit/5
    [Authorize(Roles = "NSSFRep")]
    public ActionResult Edit(int? nssfid) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      NSSF nssf = NSSFService.GetNSSF(nssfid: nssfid);
      NSSFRegion nssfregion = new NSSFRegion
      {
        Name = nssf.Name,
        City = nssf.City,
        Street = nssf.Street,
        Country = nssf.Country,
        PhoneNumber = nssf.PhoneNumber,
        FaxNumber = nssf.FaxNumber,
        Zipcode = nssf.Zipcode,
        Email = nssf.Email,
        RegionId = nssf.Region.RegionId
      };
      ViewBag.RegionId = new SelectList(RegionService.GetStructuredRegions(), "RegionId", "Name");
      return View(nssfregion);
    }

    //
    // POST: /NSSF/Edit/5

    [HttpPost]
    [Authorize(Roles = "NSSFRep")]
    public ActionResult Edit(NSSFRegion nssfregion) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid) {
        //NSSF nssf = new NSSF
        //{
        //  Name = nssfregion.Name,
        //  City = nssfregion.City,
        //  Street = nssfregion.Street,
        //  Country = nssfregion.Country,
        //  PhoneNumber = nssfregion.PhoneNumber,
        //  FaxNumber = nssfregion.FaxNumber,
        //  DateAdded = DateTime.Now,
        //  Zipcode = nssfregion.Zipcode,
        //  Email = nssfregion.Email,
        //  Region = RegionService.GetRegion(nssfregion.RegionId)
        //};
        NSSF nssf = NSSFService.GetNSSF(name: nssfregion.Name,email:nssfregion.Email);
        nssf.Name = nssfregion.Name;
        nssf.City = nssfregion.City;
        nssf.Street = nssfregion.Street;
        nssf.Country = nssfregion.Country;
        nssf.PhoneNumber = nssfregion.PhoneNumber;
        nssf.FaxNumber = nssfregion.FaxNumber;
        nssf.Zipcode = nssfregion.Zipcode;
        nssf.Email = nssfregion.Email;
        nssf.Region = NSSFService.GetRegion(nssf);

        NSSFService.UpdateNSSF(nssf);
        return RedirectToAction("Index","Administration");
      }
      ViewBag.RegionId = new SelectList(RegionService.GetStructuredRegions(), "RegionId", "Name");
      return View(nssfregion);
    }

  }
}