﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.DAL;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO.Members;
using System.Web.Security;
using TeamOPSchoolCup.Web.Models;
using System.Text;
using TeamOPSchoolCup.Domain.DTO;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;
using TeamOPSchoolCup.Web.Helpers;
using System.Globalization;
using System.IO;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class MemberController : Controller
  {
    //For testing purposes
    [Authorize]
    public string DeleteAllUsers()
    {
      foreach (MembershipUser user in Membership.GetAllUsers())
      {
        Membership.DeleteUser(user.UserName);
      }
      return "All users were successfully deleted";
    }

    [Authorize(Roles = "Coach")]
    public ActionResult AddStudent(int teamId)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.AddStudent_NoPermission); }
      Team t = TeamService.GetTeam(teamId);
      if (t.TeamCoaches.Single(x => x.DateLeft >= DateTime.Today).MemberId != MemberService.GetMember(email: User.Identity.Name).MemberId) { throw new TeamOPSchoolCupException(ErrorStrings.AddStudent_NoPermission); }
      ViewBag.Team = TeamService.GetTeam(teamId); 

      return View();
    }

    [HttpPost]
    [Authorize(Roles = "Coach")]
    public ActionResult AddStudent(BasicMemberInfo bsi, int teamId)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.AddStudent_NoPermission); }
      Team t = TeamService.GetTeam(teamId);
      String password = BCrypt.HashPassword(BCrypt.GenerateSalt(4), BCrypt.GenerateSalt(12)); // Password is required. Password will only be generated after confirmation

      bsi.Password = password;
      ModelState["Password"].Errors.Clear(); //Anders is ModelState niet geldig
      if (ModelState.IsValid)
      {
        if (MemberService.AddStudent(bsi))
        {
          string body = string.Format("{0}{1}{2}{3}{4}", string.Format(MailStrings.NamedGreeting, bsi.Name), MailStrings.DefaultAutomatedWarning,
            string.Format(MailStrings.StudentAdded, t.Name, Url.Action("ActivateStudent", "Member", new { email = bsi.Email, teamId = teamId }, Request.Url.Scheme)),
            MailStrings.DefaultWrongAction, MailStrings.DefaultSignoff);
          MailService.SendMail(MailStrings.SubjectStudentAdded, bsi.Email, body, true);

          return RedirectToAction("AddTeamMembership", "Team", new { id = teamId});
        }
      }
      
      return View();
    }


    public ActionResult ActivateStudent(string email,int teamId)
    {
      Member m = MemberService.GetMember(email: email);
      MemberService.ConfirmMember(m);
      MemberService.AddStudentToTeam(m.MemberId, teamId);

      return RedirectToAction("Profile", "Member", new { id = m.MemberId });
    }

    [Authorize(Roles = "SCC")]
    public ActionResult AddCoach(int schoolId)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      ViewBag.School = SchoolService.GetSchool(id: schoolId);
      return View();
    }

    [HttpPost]
    [Authorize(Roles = "SCC")]
    public ActionResult AddCoach(int schoolId, BasicMemberInfo bci)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      String password = BCrypt.HashPassword(BCrypt.GenerateSalt(4), BCrypt.GenerateSalt(12)); // Password is required. Password will only be generated after confirmation

      bci.Password = password;
      ModelState["Password"].Errors.Clear(); //Anders is ModelState niet geldig
      if (ModelState.IsValid)
      {
        School s = SchoolService.GetSchool(id: schoolId);
        if (MemberService.AddCoach(bci, schoolId))
        {
          string body = string.Format("{0}{1}{2}{3}{4}", string.Format(MailStrings.NamedGreeting, bci.Name), MailStrings.DefaultAutomatedWarning,
            string.Format(MailStrings.CoachAdded, s.Name, Url.Action("ActivateCoach", "Member", new { email = bci.Email }, Request.Url.Scheme)),
            MailStrings.DefaultWrongAction, MailStrings.DefaultSignoff);
          MailService.SendMail(MailStrings.SubjectCoachAdded, bci.Email, body, true);
          return RedirectToAction("Index", "Administration");
        }
      }
      return View();
    }

    public ActionResult ActivateCoach(string email)
    {
      Member m = MemberService.GetMember(email: email);
      MemberService.ConfirmMember(m);
      return RedirectToAction("EditProfile", "Member", new { id = m.MemberId });
    }

    public ActionResult AddSCC(string schoolemail)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive);  }
      string email = schoolemail.Replace("%40", "@");
      School school = SchoolService.GetSchool(email: email);
      if (school != null)
      {
        ViewBag.SchoolId = school.SchoolId;
        return View();
      }
      else
      {
        return RedirectToAction("Index", "Home");
      }
    }

    [HttpPost]
    public ActionResult AddSCC(BasicMemberInfo bmi, int schoolid)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      String password = BCrypt.HashPassword(BCrypt.GenerateSalt(4), BCrypt.GenerateSalt(12)); // Password is required. Password will only be generated after confirmation

      bmi.Password = password;
      ModelState["Password"].Errors.Clear(); //Anders is ModelState niet geldig
      if (ModelState.IsValid)
      {
        Member mem = new Member { MemberId = Guid.NewGuid(), Name = bmi.Name, Email = bmi.Email, Password = bmi.Password, Gender = bmi.Gender };
        SCC scc = new SCC { Member = mem };

        try
        {
          if (MemberService.AddSCC(scc, schoolid))
          {
            string body = string.Format("{0}{1}{2}{3}{4}", string.Format(MailStrings.NamedGreeting, scc.Member.Name), MailStrings.DefaultAutomatedWarning, string.Format(MailStrings.SCCAdded, scc.School.Name, Url.Action("ActivateSCC", "Member", new { email = bmi.Email }, Request.Url.Scheme)), MailStrings.DefaultWrongAction, MailStrings.DefaultSignoff);
            MailService.SendMail(MailStrings.SubjectSCCAdded, new List<string>() { scc.Member.Email }, body, true);
            ViewBag.SchoolId = new SelectList(SchoolService.GetSchools(region: RegionService.GetRegionOfMember(scc.Member.MemberId)), "SchoolId", "Name");
            return RedirectToAction("Index", "Home");
          }
          else
          {
            ViewBag.SchoolId = schoolid;
            return RedirectToAction("AddSCC");
          }
        }
        catch (TeamOPSchoolCupException exc)
        {

        }
      }
      ViewBag.SchoolId = schoolid;
      return View();
    }
    public ActionResult ActivateSCC(string email)
    {
      Member m = MemberService.GetMember(email: email);
      MemberService.ConfirmMember(m);
      return RedirectToAction("Index", "Home", new { id = m.MemberId });
    }
    //[Authorize]
    public ActionResult AddNSSFRep(string nssfemail)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      string email = nssfemail.Replace("%40", "@");
      NSSF nssf = NSSFService.GetNSSF(email: email);
      if (nssf != null)
      {
        ViewBag.NSSFId = nssf.NSSFId;

        return View();
      }
      else
      {
        return RedirectToAction("Index", "Home");
      }
    }
    [HttpPost]
    //[Authorize]
    public ActionResult AddNSSFRep(BasicMemberInfo bmi, int nssfid)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      String password = BCrypt.HashPassword(BCrypt.GenerateSalt(4), BCrypt.GenerateSalt(12)); // Password is required. Password will only be generated after confirmation

      bmi.Password = password;
      ModelState["Password"].Errors.Clear(); //Anders is ModelState niet geldig
      if (ModelState.IsValid)
      {
        Member mem = new Member { MemberId = Guid.NewGuid(), Name = bmi.Name, Email = bmi.Email, Password = bmi.Password, Gender = bmi.Gender };
        NSSFRep nssfrep = new NSSFRep { Member = mem };

        if (MemberService.AddNSSFRep(nssfrep, nssfid))
        {
          string body = string.Format("{0}{1}{2}{3}{4}", string.Format(MailStrings.NamedGreeting, nssfrep.Member.Name), MailStrings.DefaultAutomatedWarning, string.Format(MailStrings.NSSFRepAdded, nssfrep.NSSF.Name, Url.Action("ActivateSCC", "Member", new { email = bmi.Email }, Request.Url.Scheme)), MailStrings.DefaultWrongAction, MailStrings.DefaultSignoff);
          MailService.SendMail(MailStrings.SubjectNSSFRepAdded, new List<string>() { nssfrep.Member.Email }, body, true);

          return RedirectToAction("Index", "Home");
        }
        else
        {
          ViewBag.NSSFId = nssfid;

          return RedirectToAction("AddNSSFRep");
        }
      }

      ViewBag.NSSFId = nssfid;

      return View();
    }
    public ActionResult ActivateNSSFRep(string email)
    {
      Member m = MemberService.GetMember(email: email);
      MemberService.ConfirmMember(m);
      return RedirectToAction("Index", "Home", new { id = m.MemberId });
    }
    [Authorize]
    public ActionResult AddISFAdmin()
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      return View();
    }

    [HttpPost]
    [Authorize]
    public ActionResult AddISFAdmin(ISFAdmin isfadmin)
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid)
      {
        string password = RandomString(6, 12);
        MembershipCreateStatus createStatus;
        MembershipUser user = Membership.CreateUser(isfadmin.Member.Email, password, isfadmin.Member.Email, null, null, true, null, out createStatus);

        if (createStatus == MembershipCreateStatus.Success)
        {
          //isfadmin.Member.UserId = user.ProviderUserKey.ToString();

          try
          {
            if (MemberService.AddISFAdmin(isfadmin))
            {
              if (!Roles.RoleExists("ISFAdmin")) Roles.CreateRole("ISFAdmin");
              Roles.AddUserToRole(user.UserName, "ISFAdmin");
              //TODO: send mail to student --Thomas
              ViewBag.Password = password;

              return View();
            }
            else
            {
              Membership.DeleteUser(user.UserName, true);
              return RedirectToAction("AddISFAdmin");
            }
          }
          catch (Exception)
          {
            Membership.DeleteUser(user.UserName);
          }
        }
        else
        {
          ModelState.AddModelError("", createStatus.ToString());
        }
      }

      return View();
    }
    // [minSize, maxSize]
    private string RandomString(int minSize, int maxSize)
    {
      Random random = new Random((int)DateTime.Now.Ticks);
      StringBuilder builder = new StringBuilder();

      for (int i = 0; i < random.Next(minSize, maxSize + 1); i++)
      {
        char c;

        if (random.NextDouble() < 0.7)
        {
          c = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + (random.NextDouble() >= 0.5 ? 65 : 97))));
        }
        else //Add numeric char
        {
          c = Convert.ToChar(Convert.ToInt32(Math.Floor(10 * random.NextDouble() + 48)));
        }
        builder.Append(c);
      }

      return builder.ToString();
    }

    /* All profile related actions */

    // Anyone can view a public user profile
    // @param id The user id
    // GET: /Member/Profile/12
    public ActionResult Profile(Guid id)
    {
      Member m = MemberService.GetMember(id);
      string photoPath = "~/Upload/Memberpicture/" + id + ".jpeg";
      string photoPathTest = Server.MapPath("~/Upload/Memberpicture/" + id + ".jpeg");
      
      if (m == null) throw new TeamOPSchoolCupException(ErrorStrings.MemberDoesntExistException);
      if (m.Coach == null && m.Student == null) throw new TeamOPSchoolCupException(ErrorStrings.NoProfileException);
      ViewBag.sports = new SelectList(SportService.GetSports(true), "SportId", "Name");
      ViewBag.years = new SelectList(CompetitionYearService.GetCompetitionYears(), "CompetitionYearValue", "CompetitionYearValue");
      ViewBag.photopath = photoPath;
      ViewBag.photopathTest = photoPathTest;
      return View(m);

    }

    // Public results as part as the user profile
    public ActionResult Results(Guid id)
    {
      Member member = MemberService.GetMember(id);
      ViewBag.memberId = member.MemberId;
      ViewBag.memberName = member.Name;
      return View(member.Student.TeamMemberShips);
    }

    // Only a user can view his own profile details
    [Authorize(Roles = "Student, Coach")]
    public ActionResult ProfileDetails()
    {
      Member m = MemberService.GetMember(email: User.Identity.Name);
      return View(m);
    }

    // Only a user can edit his own profile
    [Authorize]
    public ActionResult EditProfile()
    {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      Member m = MemberService.GetMember(email: User.Identity.Name);
      EditableMember mem = new EditableMember
      {
        Name = m.Name,
        City = m.City,
        Country = m.Country,
        MemberId = m.MemberId,
        PhoneNumber = m.PhoneNumber,
        Street = m.Street,
        Zipcode = m.Zipcode
      };
      return View(mem);
    }

    [HttpPost]
    public ActionResult EditProfile(EditableMember m, HttpPostedFileBase Photo) {
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      if (ModelState.IsValid) {
        if (Photo != null && Photo.ContentLength > 0 ) {
          new ImageHelper().Save(new System.Drawing.Bitmap(Photo.InputStream), 180, 130, 80, Path.Combine(Server.MapPath("~/Upload/Memberpicture"), "" + m.MemberId + ".jpeg"));
        }
       
        MemberService.UpdateMember(m:m);       
      }
      return RedirectToAction("SchoolCup", "SchoolCup");
    }

    // Only a user can modify his own security settings (change password, etc)
    [Authorize(Roles = "Student, Coach")]
    public ActionResult Security()
    {
      return View();
    }

    [Authorize(Roles = "Student, Coach")]
    [HttpPost]
    public ActionResult Security(ChangePasswordModel model)
    {
      if (ModelState.IsValid)
      {
        MembershipUser user = Membership.GetUser(User.Identity.Name, true);

        if (Membership.ValidateUser(user.UserName, model.OldPassword))
        {
          if (user.ChangePassword(model.OldPassword, model.NewPassword))
          {
            return RedirectToAction("ChangePasswordSuccess");
          }
          else
          {
            ModelState.AddModelError("", TeamOPSchoolCup.Domain.Resources.AccountStrings.NewPasswordInvalid);
          }
        }
        else
        {
          ModelState.AddModelError("", TeamOPSchoolCup.Domain.Resources.AccountStrings.CurrentPasswordIncorrect);
        }
      }
      return View(model);
    }

    // Only a user can modify his own site preferences
    [Authorize(Roles = "Student, Coach")]
    public ActionResult Preferences()
    {
      List<KeyValuePair<int, string>> languageData = new List<KeyValuePair<int, string>>();
      foreach (string str in Enum.GetNames(typeof(Culture)))
      {
        CultureInfo ci = new CultureInfo(str);
        int cultureid = (int)Enum.Parse(typeof(Culture), str, true);
        KeyValuePair<int, string> kvp = new KeyValuePair<int, string>(cultureid, String.Format("{0} - {1}", ci.EnglishName, ci.NativeName));
        if (str == "en")
        {
          kvp = new KeyValuePair<int, string>(cultureid, String.Format("{0}", ci.NativeName));
        }
        languageData.Add(kvp);
      }

      ViewBag.languages = new SelectList(languageData, "Key", "Value");

      return View();
    }

    [Authorize(Roles = "Student, Coach")]
    [HttpPost]
    public ActionResult Preferences(int languages)
    {
      Member m = MemberService.GetMember(email: User.Identity.Name);
      EditableMember mem = new EditableMember
      {
        Name = m.Name,
        City = m.City,
        Country = m.Country,
        MemberId = m.MemberId,
        PhoneNumber = m.PhoneNumber,
        Street = m.Street,
        Zipcode = m.Zipcode,
        Language = languages
      };
      MemberService.UpdateMember(m: mem);

      List<KeyValuePair<int, string>> languageData = new List<KeyValuePair<int, string>>();
      foreach (string str in Enum.GetNames(typeof(Culture)))
      {
        CultureInfo ci = new CultureInfo(str);
        int cultureid = (int)Enum.Parse(typeof(Culture), str, true);
        KeyValuePair<int, string> kvp = new KeyValuePair<int, string>(cultureid, String.Format("{0} - {1}", ci.EnglishName, ci.NativeName));
        if (str == "en")
        {
          kvp = new KeyValuePair<int, string>(cultureid, String.Format("{0}", ci.NativeName));
        }
        languageData.Add(kvp);
      }

      ViewBag.languages = new SelectList(languageData, "Key", "Value");
      return RedirectToAction("SchoolCup", "SchoolCup");
    }
  }
}