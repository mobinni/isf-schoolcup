﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.Services;
using TeamOPSchoolCup.Domain.POCO.Members;

namespace TeamOPSchoolCup.Web.Controllers
{
    public class StudentsController : Controller
    {
        //
        // GET: /Students/

        public ActionResult Index()
        {
            return View(MemberService.GetStudents());
        }

    }
}
