﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamOPSchoolCup.Domain.POCO;
using TeamOPSchoolCup.Domain.Services;
using System.Web.Security;
using TeamOPSchoolCup.Domain.Exceptions;
using TeamOPSchoolCup.Domain.Resources;

namespace TeamOPSchoolCup.Web.Controllers
{
  public class AdministrationController : Controller
  {
    //
    // GET: /Administration/
    [Authorize]
    public ActionResult Index()
    {

      Member member = MemberService.GetMember(email: User.Identity.Name);
      if (Roles.IsUserInRole(User.Identity.Name, "Inactive")) { throw new TeamOPSchoolCupException(ErrorStrings.NoPermissionInactive); }
      
        if (member.ISFAdmin != null)
        {
          return View("ISFAdminAdministration");
        }
        else if (member.NSSFRep != null)
        {
          return View("NSSFRepAdministration");
        }
        else if (member.SCC != null)
        {
          return View("SCCAdministration");
        }
      
      return null;
    }

  }
}
