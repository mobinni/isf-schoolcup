USE [TeamOPSchoolCup]
GO
/****** Object:  StoredProcedure [dbo].[CalculateRankings]    Script Date: 04/17/2013 08:49:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tibackx Wim, Team OP
-- Create date: 2013-04-17
-- Description:	Calculates the StudentRanking and TeamRanking. To be ran once a day.
-- =============================================
-- CREATE PROCEDURE [dbo].[CalculateRankings]
CREATE PROCEDURE [dbo].[CalculateRankings]
AS
	DECLARE cur_HighEvents CURSOR READ_ONLY FOR SELECT EventId FROM Event WHERE Directionality_Value=1;
	DECLARE cur_LowEvents CURSOR READ_ONLY FOR SELECT EventId FROM Event WHERE Directionality_Value=2;
	
	DECLARE @eventId int;
	DECLARE @bsrData TABLE(RowID int not null primary key identity(1,1), MemberId uniqueidentifier, EventId int, ResultScore float, RankNr int);
	DECLARE @srData TABLE (MemberId uniqueidentifier, EventId int, BestWorldRankingThisYear int);
	DECLARE @trData TABLE (TeamId int, EventId int, BestWorldRankingThisYear int);
	DECLARE @competitionYear int;
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRAN RankingUpdate
	
	SET @competitionYear=(SELECT YEAR(GETDATE()));
	INSERT INTO @srData SELECT MemberId, EventId, BestWorldRankingThisYear FROM StudentRanking;
	TRUNCATE TABLE StudentRanking;
	INSERT INTO @trData SELECT TeamId, EventId, BestWorldRankingThisYear FROM TeamRanking;
	TRUNCATE TABLE TeamRanking;
	
	OPEN cur_HighEvents;
	FETCH NEXT FROM cur_HighEvents INTO @eventId;
	WHILE @@FETCH_STATUS=0
	BEGIN
		INSERT INTO StudentRanking
		SELECT x.MemberId as MemberId,x.EventId as EventId, x.ranknr as CurrentWorldRanking, 
			(CASE WHEN x.ranknr < 
			ISNULL((SELECT BestWorldRankingThisYear FROM @srData WHERE MemberId=x.MemberId AND EventId=x.EventId),0)
			 THEN x.ranknr
			 ELSE 
			 ISNULL((SELECT BestWorldRankingThisYear FROM @srData WHERE MemberId=x.MemberId AND EventId=x.EventId),0)
			  END) AS BestWorldRankingThisYear
		FROM (SELECT bsr.MemberId, bsr.EventId, r.ResultScore, rank() OVER (ORDER BY r.ResultScore DESC) as ranknr
			FROM BestStudentResult bsr, Result r
			WHERE bsr.CompetitionYear=@competitionYear AND bsr.EventId=@eventId AND bsr.IsOfficial=1
				AND bsr.ResultId=r.ResultId ) x;
		
		INSERT INTO TeamRanking
		SELECT x.TeamId, x.EventId, x.ranknr as CurrentWorldRanking, 
			(CASE WHEN x.ranknr > 
				ISNULL((SELECT BestWorldRankingThisYear FROM TeamRanking WHERE TeamId=x.TeamId AND EventId=X.EventId),0)
				 THEN x.ranknr
				 ELSE 
				 ISNULL((SELECT BestWorldRankingThisYear FROM TeamRanking WHERE TeamId=x.TeamId AND EventId=x.EventId),0)
				 END) as BestWorldRankingThisYear
		FROM (
			SELECT r.TeamId, r.EventId, r.ResultScore, rank() OVER (ORDER BY r.ResultScore DESC) as ranknr
			FROM Result r, Meeting m
			WHERE r.EventId=@eventId AND r.MeetingId=m.MeetingId AND YEAR(m.Date)=@competitionYear AND m.MeetingId NOT IN ( SELECT cm.MeetingId FROM CoachMeeting cm )
			GROUP BY r.TeamId, r.EventId, r.ResultScore
			HAVING r.ResultScore=(SELECT MAX(ResultScore) FROM Result WHERE TeamId=r.TeamId AND EventId=r.EventId)
		) x;
		
		FETCH NEXT FROM cur_HighEvents INTO @eventId;
	END

	CLOSE cur_HighEvents;
	DEALLOCATE cur_HighEvents;
	
	OPEN cur_LowEvents;
	FETCH NEXT FROM cur_LowEvents INTO @eventId;
	WHILE @@FETCH_STATUS=0
	BEGIN
		INSERT INTO StudentRanking
		SELECT x.MemberId as MemberId,x.EventId as EventId, x.ranknr as CurrentWorldRanking, 
			(CASE WHEN x.ranknr < 
			ISNULL((SELECT BestWorldRankingThisYear FROM @srData WHERE MemberId=x.MemberId AND EventId=x.EventId),0)
			 THEN x.ranknr
			 ELSE 
			 ISNULL((SELECT BestWorldRankingThisYear FROM @srData WHERE MemberId=x.MemberId AND EventId=x.EventId),0)
			  END) AS BestWorldRankingThisYear
		FROM (SELECT bsr.MemberId, bsr.EventId, r.ResultScore, rank() OVER (ORDER BY r.ResultScore ASC) as ranknr
			FROM BestStudentResult bsr, Result r
			WHERE bsr.CompetitionYear=@competitionYear AND bsr.EventId=@eventId AND bsr.IsOfficial=1
				AND bsr.ResultId=r.ResultId ) x;
		
		INSERT INTO TeamRanking
		SELECT x.TeamId, x.EventId, x.ranknr as CurrentWorldRanking, 
			(CASE WHEN x.ranknr > 
				ISNULL((SELECT BestWorldRankingThisYear FROM TeamRanking WHERE TeamId=x.TeamId AND EventId=X.EventId),0)
				 THEN x.ranknr
				 ELSE 
				 ISNULL((SELECT BestWorldRankingThisYear FROM TeamRanking WHERE TeamId=x.TeamId AND EventId=x.EventId),0)
				 END) as BestWorldRankingThisYear
		FROM (
			SELECT r.TeamId, r.EventId, r.ResultScore, rank() OVER (ORDER BY r.ResultScore ASC) as ranknr
			FROM Result r, Meeting m
			WHERE r.EventId=@eventId AND r.MeetingId=m.MeetingId AND YEAR(m.Date)=@competitionYear AND m.MeetingId NOT IN ( SELECT cm.MeetingId FROM CoachMeeting cm )
			GROUP BY r.TeamId, r.EventId, r.ResultScore
			HAVING r.ResultScore=(SELECT MIN(ResultScore) FROM Result WHERE TeamId=r.TeamId AND EventId=r.EventId)
		) x;
		
		FETCH NEXT FROM cur_LowEvents INTO @eventId;
	END

	CLOSE cur_LowEvents;
	DEALLOCATE cur_LowEvents;
	
	INSERT INTO TeamRanking
		SELECT x.TeamId, 0, x.ranknr as CurrentWorldRanking, 
			(CASE WHEN x.ranknr > 
				ISNULL((SELECT BestWorldRankingThisYear FROM TeamRanking WHERE TeamId=x.TeamId AND EventId=0),0)
				 THEN x.ranknr
				 ELSE 
				 ISNULL((SELECT BestWorldRankingThisYear FROM TeamRanking WHERE TeamId=x.TeamId AND EventId=0),0)
				 END) as BestWorldRankingThisYear
		FROM (
			SELECT btr.TeamId, rank() OVER (ORDER BY tr.PointValue DESC) as ranknr, tr.PointValue
			FROM BestTeamResult btr
			INNER JOIN TeamResult tr ON btr.TeamId=tr.TeamId AND btr.MeetingId=tr.MeetingId
		) x;
	
	--ROLLBACK TRAN RankingUpdate
	COMMIT TRAN RankingUpdate
END

