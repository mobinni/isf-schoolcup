package com.example.Opd_ISF_Android_Application.activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.controller.ConnectionManager;

/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 26/03/13
 * Time: 9:55
 * To change this template use File | Settings | File Templates.
 */
public class SplashActivity extends Activity {
    private boolean isBackBtnPressed;
    private boolean isDialogOpen;
    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.splash);
        this.context = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isDialogOpen) {
            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    testConnection();
                }
            }, 1000);
        }
    }

    @Override
    public void onBackPressed() {
        isBackBtnPressed = true;
        super.onBackPressed();
    }

    private void testConnection(){
        if (ConnectionManager.isConnected(this) /*&& ConnectionManager.isReachable()*/) {
            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();

                    if (!isBackBtnPressed) {
                        final Intent mainActivity = new Intent(context, MainActivity.class);
                        startActivity(mainActivity);
                        finish();
                    }
                }
            }, 1500);
        } else {
            final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setTitle(context.getString(R.string.alrtTitleNoNetwork));
            alertBuilder
                    .setMessage(context.getString(R.string.alrtNoNetwork))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                            isDialogOpen = false;
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    });
            alertBuilder.create().show();
            isDialogOpen = true;
        }
    }
}
