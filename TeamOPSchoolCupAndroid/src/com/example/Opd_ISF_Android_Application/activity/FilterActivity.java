package com.example.Opd_ISF_Android_Application.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.adapter.CustomSpinnerAdapter;
import com.example.Opd_ISF_Android_Application.controller.DataController;
import com.example.Opd_ISF_Android_Application.model.ISFData;
import com.example.Opd_ISF_Android_Application.model.Ranking;
import com.example.Opd_ISF_Android_Application.model.filter.Event;
import com.example.Opd_ISF_Android_Application.model.filter.Filter;
import com.example.Opd_ISF_Android_Application.model.filter.Region;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 27/03/13
 * Time: 21:14
 * To change this template use File | Settings | File Templates.
 */
public class FilterActivity extends Activity {
    private Button btnShowRankings;
    private Spinner spinRankingType, spinRegion, spinYear, spinMeetingType, spinSport, spinEvent, spinGender, spinAge;
    private LinearLayout studentFilters1, studentFilters2;
    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_rankings);
        context = this;

        initialiseComponents();
        initialiseEventListeners();
    }

    private void initialiseComponents() {
        btnShowRankings = (Button) findViewById(R.id.btnGo);
        spinRankingType = (Spinner) findViewById(R.id.spinRankChooser);
        spinMeetingType = (Spinner) findViewById(R.id.spinOfficialityChooser);
        spinRegion = (Spinner) findViewById(R.id.spinRegionChooser);
        spinYear = (Spinner) findViewById(R.id.spinYearChooser);
        spinSport = (Spinner) findViewById(R.id.spinSportChooser);
        spinEvent = (Spinner) findViewById(R.id.spinEventChooser);
        spinGender = (Spinner) findViewById(R.id.spinSexChooser);
        spinAge = (Spinner) findViewById(R.id.spinAgeChooser);
        studentFilters1 = (LinearLayout) findViewById(R.id.studentFilters1);
        studentFilters2 = (LinearLayout) findViewById(R.id.studentFilters2);

        spinRankingType.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.filter_rankingTypes)));
        spinMeetingType.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.filter_meetingTypes)));
        spinRegion.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.filter_regions)));
        spinYear.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.filter_competitionYears)));
        spinSport.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.filter_sports)));
        spinEvent.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.filter_events)));
        spinGender.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.filter_genders)));
        spinAge.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.filter_ages)));
        if (ISFData.getCurrentCompetitionYear() != null)
            spinYear.setSelection(ISFData.getCurrentCompetitionYear().getId());
    }

    private void initialiseEventListeners() {
        btnShowRankings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((Filter) spinRankingType.getSelectedItem()).getId() == 1) { //School
                    if (((Filter) spinSport.getSelectedItem()).getId() == 0) {
                        Toast.makeText(context, R.string.toastSportRequired, Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else { //Student
                    if (((Event) spinEvent.getSelectedItem()).getId() == 0) {
                        Toast.makeText(context, R.string.toastEventRequired, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                new AsyncTask<Void, Void, List<Ranking>>() {
                    private ProgressDialog progressDialog;

                    @Override
                    protected void onPreExecute() {
                        progressDialog = ProgressDialog.show(context, getString(R.string.prgTitleWait), getString(R.string.prgMsgLoading), true);
                        super.onPreExecute();
                    }

                    @Override
                    protected List<Ranking> doInBackground(Void... params) {
                        final List<Ranking> ranking = new ArrayList<Ranking>();

                        if (((Filter) spinRankingType.getSelectedItem()).getId() == 1) {
                            final Filter selectedSport = (Filter) spinSport.getSelectedItem();
                            final Filter selectedYear = (Filter) spinYear.getSelectedItem();
                            final Region selectedRegion = (Region) spinRegion.getSelectedItem();
                            final Filter selectedGender = (Filter) spinGender.getSelectedItem();

                            final int sportId = selectedSport.getId();
                            final String year = (selectedYear.getId() == 0 ? null : selectedYear.toString());
                            final String regionId = (selectedRegion.getId() == 0 ? null : String.valueOf(selectedRegion.getId()));
                            final String gender = (selectedGender.getId() == 0 ? null : String.valueOf(selectedGender.getId()));

                            try {
                                ranking.addAll(DataController.parseTeamRanking(sportId, year, regionId, gender));
                            } catch (IOException e) {
                            }
                        } else {
                            //int eventId, String year, String isOfficial, String gender, String regionId, String age
                            final Filter selectedYear = (Filter) spinYear.getSelectedItem();
                            final Filter selectedOfficial = (Filter) spinMeetingType.getSelectedItem();
                            final Filter selectedGender = (Filter) spinGender.getSelectedItem();
                            final Region selectedRegion = (Region) spinRegion.getSelectedItem();
                            final Filter selectedAge = (Filter) spinAge.getSelectedItem();

                            final int eventId = ((Event) spinEvent.getSelectedItem()).getId();
                            final String year = (selectedYear.getId() == 0 ? null : selectedYear.toString());
                            final String isOfficial = (selectedOfficial.getId() == 0 ? null : (selectedOfficial.getId() == 1 ? "true" : "false"));
                            final String gender = (selectedGender.getId() == 0 ? null : String.valueOf(selectedGender.getId()));
                            final String regionId = (selectedRegion.getId() == 0 ? null : String.valueOf(selectedRegion.getId()));
                            final String age = (selectedAge.getId() == 0 ? null : selectedAge.toString());

                            try {
                                ranking.addAll(DataController.parseStudentRanking(eventId, year, isOfficial, gender, regionId, age));
                            } catch (IOException e) {
                            }
                        }

                        return ranking;
                    }

                    @Override
                    protected void onPostExecute(List<Ranking> rankings) {
                        if (progressDialog.isShowing()) progressDialog.dismiss();

                        final Intent rankingIntent = new Intent(context, RankingActivity.class);
                        final Gson gson = new GsonBuilder().create();
                        final String rankingType = gson.toJson(spinRankingType.getSelectedItem(), Filter.class);
                        final String competitionYear = gson.toJson(spinYear.getSelectedItem(), Filter.class);

                        ISFData.rankings.clear();
                        ISFData.rankings.addAll(rankings);
                        rankingIntent.putExtra("RankingType", rankingType);
                        rankingIntent.putExtra("CompetitionYear", competitionYear);
                        context.startActivity(rankingIntent);
                    }
                }.execute();
            }
        });

        spinSport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final Filter selectedSport = (Filter) parent.getSelectedItem();
                final List<Filter> eventList = new ArrayList<Filter>();

                for (Filter filter : ISFData.filter_events) {
                    final Event event = (Event) filter;

                    if (event.getSport() == selectedSport.getId()) eventList.add(event);
                }

                final CustomSpinnerAdapter spinnerAdapter = new CustomSpinnerAdapter(context, new ArrayList<Object>(eventList));
                spinEvent.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinRankingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final Filter rankingType = (Filter) adapterView.getSelectedItem();

                setStudentFiltersVisibility(rankingType.getId() == 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setStudentFiltersVisibility(boolean visible) {
        int visibility = (visible ? View.GONE : View.VISIBLE);


        studentFilters1.setVisibility(visibility);
        studentFilters2.setVisibility(visibility);
    }
}
