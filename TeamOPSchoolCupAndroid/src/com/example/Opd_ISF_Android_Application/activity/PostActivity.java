package com.example.Opd_ISF_Android_Application.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.example.Opd_ISF_Android_Application.Constants;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.adapter.CustomSpinnerAdapter;
import com.example.Opd_ISF_Android_Application.controller.DataController;
import com.example.Opd_ISF_Android_Application.controller.MemberDataParser;
import com.example.Opd_ISF_Android_Application.controller.Response;
import com.example.Opd_ISF_Android_Application.model.ISFData;
import com.example.Opd_ISF_Android_Application.model.memberdata.Meeting;
import com.example.Opd_ISF_Android_Application.model.memberdata.MeetingEvent;
import com.example.Opd_ISF_Android_Application.model.memberdata.MemberData;
import com.example.Opd_ISF_Android_Application.model.memberdata.Student;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 13/05/13
 * Time: 19:41
 * To change this template use File | Settings | File Templates.
 */
public class PostActivity extends Activity {
    private Activity context;
    private EditText edtxtResult;
    private LinearLayout lnlContainer;
    private Spinner spinTeam, spinMeeting, spinEvent, spinStudent;
    private Button btnPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.post);

        new MemberDataParser(context).execute();
    }


    public void initialiseComponents() {
        lnlContainer = (LinearLayout) findViewById(R.id.lnlPostContainer);
        edtxtResult = (EditText) findViewById(R.id.edtxtResultScore);
        spinTeam = (Spinner) findViewById(R.id.spinPostTeamChooser);
        spinMeeting = (Spinner) findViewById(R.id.spinPostMeetingChooser);
        spinEvent = (Spinner) findViewById(R.id.spinPostEventChooser);
        spinStudent = (Spinner) findViewById(R.id.spinPostStudentChooser);
        btnPost = (Button) findViewById(R.id.btnPost);

        spinTeam.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.member_teams)));
        spinMeeting.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.member_meetings)));
        spinEvent.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.member_meeting_events)));

        final SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        if (!sharedPreferences.getBoolean(Constants.KEY_ROLE, false)) {
            spinStudent.setAdapter(new CustomSpinnerAdapter(context, new ArrayList<Object>(ISFData.member_team_students)));
            spinStudent.setVisibility(View.VISIBLE);
        }

        addEvents();
    }

    private void addEvents() {
        lnlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        spinTeam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final MemberData team = (MemberData) parent.getSelectedItem();
                final List<Meeting> meetings = new ArrayList<Meeting>();

                for (MemberData data : ISFData.member_meetings) {
                    Meeting meeting = (Meeting) data;

                    if (meeting.getTeamId() == team.getId()) meetings.add(meeting);
                }

                final CustomSpinnerAdapter meetingAdapter = new CustomSpinnerAdapter(context, new ArrayList<Object>(meetings));
                spinMeeting.setAdapter(meetingAdapter);

                final List<Student> students = new ArrayList<Student>();

                for (Student student : ISFData.member_team_students) {
                    if (student.getTeamId() == team.getId()) students.add(student);
                }

                final CustomSpinnerAdapter studentAdapter = new CustomSpinnerAdapter(context, new ArrayList<Object>(students));
                spinStudent.setAdapter(studentAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinMeeting.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final Meeting meeting = (Meeting) parent.getSelectedItem();
                final List<MeetingEvent> meetingEvents = new ArrayList<MeetingEvent>();

                for (MemberData data : ISFData.member_meeting_events) {
                    MeetingEvent meetingEvent = (MeetingEvent) data;

                    if (meetingEvent.getMeetingId() == meeting.getId()) meetingEvents.add(meetingEvent);
                }

                final CustomSpinnerAdapter meetingEventAdapter = new CustomSpinnerAdapter(context, new ArrayList<Object>(meetingEvents));
                spinEvent.setAdapter(meetingEventAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);

                if (edtxtResult.getText().toString().isEmpty()) {
                    Toast.makeText(context, "Please enter a result", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (spinTeam.getSelectedItem() == null || ((MemberData) spinTeam.getSelectedItem()).getId() == 0) {
                    Toast.makeText(context, "Please choose a team", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (spinMeeting.getSelectedItem() == null || ((MemberData) spinMeeting.getSelectedItem()).getId() == 0) {
                    Toast.makeText(context, "Please choose a meeting", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (spinEvent.getSelectedItem() == null || ((MemberData) spinEvent.getSelectedItem()).getId() == 0) {
                    Toast.makeText(context, "Please choose an event", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!sharedPreferences.getBoolean(Constants.KEY_ROLE, false)) {
                    if (spinStudent.getSelectedItem() == null || ((Student) spinStudent.getSelectedItem()).getId().isEmpty()) {
                        Toast.makeText(context, "Please choose a student", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                new AsyncTask<Void, Void, Response>() {
                    private ProgressDialog progressDialog;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        progressDialog = ProgressDialog.show(context, getString(R.string.prgTitleWait), getString(R.string.prgMsgLoading), true);
                    }

                    @Override
                    protected Response doInBackground(Void... params) {
                        Response response = null;

                        final SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
                        final double resultScore = Double.valueOf(edtxtResult.getText().toString());
                        final int teamId = ((MemberData) spinTeam.getSelectedItem()).getId();
                        final int meetingId = ((Meeting) spinMeeting.getSelectedItem()).getId();
                        final int eventId = ((MeetingEvent) spinEvent.getSelectedItem()).getId();
                        final String studentId = (spinStudent.getSelectedItem() == null || ((Student) spinStudent.getSelectedItem()).getId().isEmpty())
                                ? null : ((Student) spinStudent.getSelectedItem()).getId();

                        try {
                            response = DataController.postResult(sharedPreferences.getString(Constants.KEY_MAK, ""), resultScore, teamId, meetingId, eventId, studentId);
                        } catch (IOException e) {
                        }

                        return response;
                    }

                    @Override
                    protected void onPostExecute(Response response) {
                        if (progressDialog.isShowing()) progressDialog.dismiss();

                        if (response != null && response.getResult() == 0) {
                            Toast.makeText(context, getString(R.string.tstPostSuccess), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, getString(R.string.tstPostFail), Toast.LENGTH_SHORT).show();
                            Log.e("ERROR", response.getError());
                        }
                        edtxtResult.setText("");
                        spinTeam.setSelection(0);
                    }
                }.execute();


            }
        });
    }
}
