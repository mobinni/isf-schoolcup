package com.example.Opd_ISF_Android_Application.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.adapter.ComparisonAdapter;
import com.example.Opd_ISF_Android_Application.model.Comparison;
import com.example.Opd_ISF_Android_Application.model.ISFData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 2/04/13
 * Time: 18:14
 * To change this template use File | Settings | File Templates.
 */
public class ComparisonActivity extends Activity {
    private Activity context;
    private ListView comparisonList;
    private TextView name1, name2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;

        setContentView(R.layout.comparison);
        initialiseComponents();
    }

    private void initialiseComponents() {
        this.comparisonList = (ListView) findViewById(R.id.lvComparison);
        this.name1 = (TextView) findViewById(R.id.tvComparisonTitleName1);
        this.name2 = (TextView) findViewById(R.id.tvComparisonTitleName2);
        if(ISFData.comparisons.size() > 0){
            name1.setText(ISFData.comparisons.get(0).getName1());
            name2.setText(ISFData.comparisons.get(0).getName2());
        }

        final ComparisonAdapter data = new ComparisonAdapter(context);
        comparisonList.setAdapter(data);
        comparisonList.setEmptyView(findViewById(R.id.tvComparisonEmpty));
    }
}
