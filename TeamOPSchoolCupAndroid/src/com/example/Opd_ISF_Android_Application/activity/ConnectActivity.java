package com.example.Opd_ISF_Android_Application.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;
import com.example.Opd_ISF_Android_Application.Constants;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.controller.URLDataFetcher;
import com.example.Opd_ISF_Android_Application.view.LiveWebView;

/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 26/03/13
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
public class ConnectActivity extends Activity {
    private Activity context;
    private Button btnLogin;
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        setContentView(R.layout.connect);
        initialiseComponents();
        addEvents();
    }


    private void initialiseComponents() {
        btnLogin = (Button) findViewById(R.id.btnLoginConnect);
    }

    private void addEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                alertBuilder.setTitle(getString(R.string.alrtTitleSchoolCup));

                final WebView webView = new LiveWebView(context);
                webView.getSettings().setUserAgentString(URLDataFetcher.USER_AGENT);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.addJavascriptInterface(new JavaScriptWebInterface(), "JSI");
                webView.clearCache(true);
                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        if (url.contains("SetPermission")) {
                            webView.loadUrl("javascript:window.JSI.setMAK(document.getElementById(\"MobileAccessKey\") == null ? \"null\" : document.getElementById(\"MobileAccessKey\").innerHTML);");
                            webView.loadUrl("javascript:window.JSI.setRole(document.getElementById(\"IsStudent\") == null ? \"null\" : document.getElementById(\"IsStudent\").innerHTML);");
                        }
                    }
                });

                webView.loadUrl(Constants.BASE_URL + Constants.API);
                alertBuilder.setView(webView);
                alertBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog = alertBuilder.show();
            }
        });
    }

    private class JavaScriptWebInterface {
        private boolean roleSuccess = true;

        @SuppressWarnings("unused")
        @JavascriptInterface
        public void setMAK(final String mak) {
            if (!mak.equals("null") && mak.length() == 36) {
                final SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, 0);
                final SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString(Constants.KEY_MAK, mak);
                if (!editor.commit()) {
                    editor.remove(Constants.KEY_MAK);
                    editor.commit();
                    roleSuccess = false;
                }
            }
        }

        @SuppressWarnings("unused")
        @JavascriptInterface
        public void setRole(final String isStudent) {
            if (!isStudent.equals("null")) {
                final SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, 0);
                final SharedPreferences.Editor editor = sharedPreferences.edit();

                alertDialog.dismiss();
                editor.putBoolean(Constants.KEY_ROLE, Boolean.valueOf(isStudent));

                if (!editor.commit() || !roleSuccess) {
                    Toast.makeText(context, context.getString(R.string.tstErrorSavingPreferences), Toast.LENGTH_SHORT).show();
                    editor.remove(Constants.KEY_MAK);
                    editor.remove(Constants.KEY_ROLE);
                    editor.commit();

                    return;
                }

                Toast.makeText(context, R.string.tstSuccessConnect, Toast.LENGTH_SHORT).show();
                MainActivity.restart(context);
            }
        }
    }
}
