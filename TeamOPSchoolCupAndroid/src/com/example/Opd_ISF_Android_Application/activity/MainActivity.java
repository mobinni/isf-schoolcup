package com.example.Opd_ISF_Android_Application.activity;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.controller.ConnectionManager;
import com.example.Opd_ISF_Android_Application.controller.FiltersParser;

/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 26/03/13
 * Time: 12:09
 * To change this template use File | Settings | File Templates.
 */
public class MainActivity extends TabActivity {
    private static TabHost tabHost;
    private static TabHost.TabSpec postSpec;
    private static TabHost.TabSpec rankSpec;
    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        this.context = this;

        new FiltersParser(context).execute();
    }

    public void initialiseComponents() {
        tabHost = getTabHost();

        View tabView = createTabView(context, getString(R.string.tabRanking));
        rankSpec = tabHost.newTabSpec(getString(R.string.tabRanking));
        rankSpec.setIndicator(tabView);
        final Intent rankingIntent = new Intent(context, FilterActivity.class);
        rankSpec.setContent(rankingIntent);

        tabView = createTabView(context, getString(R.string.tabPostResult));
        postSpec = tabHost.newTabSpec(getString(R.string.tabPostResult));
        postSpec.setIndicator(tabView);
        final Intent connectIntent = (ConnectionManager.isLoggedIn(context) ?
                new Intent(context, PostActivity.class) : new Intent(context, ConnectActivity.class));
        postSpec.setContent(connectIntent);

        tabHost.addTab(rankSpec);
        tabHost.addTab(postSpec);
    }

    private static View createTabView(Activity context, String tabText) {
        final View view = LayoutInflater.from(context).inflate(R.layout.main_custom_tab, null, false);
        final TextView tv = (TextView) view.findViewById(R.id.tabTitleText);
        tv.setText(tabText);

        return view;
    }

    public static void restart(Activity context) {
        final Intent intent = new Intent(context, MainActivity.class);

        context.startActivity(intent);
        context.finish();
    }
}
