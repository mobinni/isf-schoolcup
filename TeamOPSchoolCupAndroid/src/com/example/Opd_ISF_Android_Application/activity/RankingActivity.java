package com.example.Opd_ISF_Android_Application.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.adapter.RankingAdapter;
import com.example.Opd_ISF_Android_Application.controller.DataController;
import com.example.Opd_ISF_Android_Application.controller.RankingScrollListener;
import com.example.Opd_ISF_Android_Application.model.Comparison;
import com.example.Opd_ISF_Android_Application.model.ISFData;
import com.example.Opd_ISF_Android_Application.model.Ranking;
import com.example.Opd_ISF_Android_Application.model.filter.*;
import com.example.Opd_ISF_Android_Application.model.filter.Filter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 26/03/13
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
public class RankingActivity extends Activity {
    private TextView tvTitleScore;
    private ListView rankingList;
    private Button btnCompare;
    private Activity context;
    private com.example.Opd_ISF_Android_Application.model.filter.Filter rankingType, competitionYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ranking);
        this.context = this;

        final Gson gson = new GsonBuilder().create();
        final Bundle extras = getIntent().getExtras();

        this.competitionYear = gson.fromJson(extras.getString("CompetitionYear"), Filter.class);
        this.rankingType = gson.fromJson(extras.getString("RankingType"), Filter.class);

        initialiseComponents();
    }

    private void initialiseComponents() {
        tvTitleScore = (TextView) findViewById(R.id.tvRankingTitleScore);
        if (rankingType.getId() != 1) tvTitleScore.setText(R.string.tvRankingTitleResult);
        rankingList = (ListView) findViewById(R.id.rankingList);
        rankingList.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        rankingList.setItemsCanFocus(true);
        btnCompare = (Button) findViewById(R.id.btnCompare);

        final RankingAdapter data = new RankingAdapter(context);
        rankingList.setAdapter(data);
        rankingList.setEmptyView(findViewById(R.id.tvRankingEmpty));
        if (data.getCount() == 0) btnCompare.setVisibility(View.GONE);

        btnCompare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ISFData.getSelectedRankings().size() < 2) {
                    Toast.makeText(context, String.format("Please select two %ss to compare.", rankingType.toString()), Toast.LENGTH_SHORT).show();
                } else if (ISFData.getSelectedRankings().size() > 2) {
                    Toast.makeText(context, String.format("You can only select 2 %ss to compare.", rankingType.toString()), Toast.LENGTH_SHORT).show();
                } else {

                    new AsyncTask<Void, Void, List<Comparison>>() {
                        private ProgressDialog progressDialog;

                        @Override
                        protected void onPreExecute() {
                            progressDialog = ProgressDialog.show(context, context.getString(R.string.prgTitleWait), context.getString(R.string.prgMsgLoading), true);
                            super.onPreExecute();
                        }

                        @Override
                        protected List<Comparison> doInBackground(Void... params) {
                            final List<Comparison> comparisons = new ArrayList<Comparison>();
                            final Ranking firstRanking = ISFData.getSelectedRankings().get(0);
                            final Ranking secondRanking = ISFData.getSelectedRankings().get(1);

                            if (rankingType.getId() == 2) {
                                try {
                                    comparisons.addAll(DataController.parseStudentComparison(firstRanking.getEntityId(),
                                            secondRanking.getEntityId(), (competitionYear.getId() == 0 ? null : competitionYear.toString())));
                                } catch (IOException e) {
                                }
                            } else {
                                try {
                                    final int firstEntityId = Integer.valueOf(firstRanking.getEntityId());
                                    final int secondEntityId = Integer.valueOf(secondRanking.getEntityId());

                                    comparisons.addAll(DataController.parseTeamComparison(firstEntityId, secondEntityId));
                                } catch (IOException e) {
                                }
                            }

                            return comparisons;
                        }

                        @Override
                        protected void onPostExecute(List<Comparison> comparisons) {
                            if (progressDialog.isShowing()) progressDialog.dismiss();
                            final Intent comparisonIntent = new Intent(context, ComparisonActivity.class);

                            ISFData.comparisons.clear();
                            ISFData.comparisons.addAll(comparisons);
                            startActivity(comparisonIntent);
                        }
                    }.execute();
                }
            }
        }
        );

        rankingList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final boolean collapsed = ISFData.rankings.get(position).switchExpanded();

                view.findViewById(R.id.lnlRankingExtras).setVisibility(collapsed ? View.VISIBLE : View.GONE);
            }
        });

        rankingList.setOnScrollListener(new RankingScrollListener(context));
    }
}