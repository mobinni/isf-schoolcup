package com.example.Opd_ISF_Android_Application;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 26/04/13
 * Time: 23:09
 * To change this template use File | Settings | File Templates.
 */
public class Constants {
    public static final String BASE_URL = "http://10.134.216.25:8013";
    public static final String API = "/en/Api";

    public static final String PATH_SPORTS = "/Sports";
    public static final String PATH_EVENTS = "/Events";
    public static final String PATH_REGIONS = "/Regions";
    public static final String PATH_COMPETITION_YEARS = "/CompetitionYears";
    public static final String PATH_STUDENT_RANKING = "/StudentRanking";
    public static final String PATH_TEAM_RANKING = "/TeamRanking";
    public static final String PATH_STUDENT_COMPARISON = "/StudentComparison";
    public static final String PATH_TEAM_COMPARISON = "/TeamComparison";
    public static final String PATH_TEAMS = "/Teams";
    public static final String PATH_MEETINGS = "/Meetings";
    public static final String PATH_MEETING_EVENTS = "/MeetingEvents";
    public static final String PATH_TEAM_STUDENTS = "/TeamStudents";
    public static final String PATH_POST_RESULT = "/PostResult";

    //Ranking parameters
    public static final String PARAM_GET_EVENT = "?eventId=";
    public static final String PARAM_YEAR = "&year=";
    public static final String PARAM_OFFICIAL = "&isOfficial=";
    public static final String PARAM_GENDER = "&gender=";
    public static final String PARAM_REGION = "&regionId=";
    public static final String PARAM_AGE = "&age=";
    public static final String PARAM_SPORT = "?sportId=";

    //Comparison parameters
    public static final String PARAM_STUDENT1 = "?studentId1=";
    public static final String PARAM_STUDENT2 = "&studentId2=";
    public static final String PARAM_TEAM1 = "?teamId1=";
    public static final String PARAM_TEAM2 = "&teamId2=";

    //Post result parameters
    public static final String PARAM_MAK = "?mobileAccessKey=";
    public static final String PARAM_RESULT = "&resultScore=";
    public static final String PARAM_TEAM = "&teamId=";
    public static final String PARAM_MEETING = "&meetingId=";
    public static final String PARAM_POST_EVENT = "&eventId=";
    public static final String PARAM_STUDENT = "&studentId=";

    //Storage
    public static final String PREFS_NAME = "ISFSchoolCupPreferences";
    public static final String KEY_MAK = "ISFSchoolCupAccessCode";
    public static final String KEY_ROLE = "ISFSchoolCupRole";
}
