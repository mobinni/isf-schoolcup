package com.example.Opd_ISF_Android_Application.controller;

import android.app.Activity;
import android.widget.AbsListView;
import android.widget.Toast;
import com.example.Opd_ISF_Android_Application.model.ISFData;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 29/04/13
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
public class RankingScrollListener implements AbsListView.OnScrollListener {
    private int firstVisibleItem;
    private int visibleItemCount;
    private int scrollState;
    private Activity context;

    public RankingScrollListener(Activity context) {
        this.context = context;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.scrollState = scrollState;
        isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.firstVisibleItem = firstVisibleItem;
        this.visibleItemCount = visibleItemCount;
    }

    private void isScrollCompleted() {
        if (this.visibleItemCount > 0 && ((this.firstVisibleItem != 0 && visibleItemCount < ISFData.rankings.size())
                || (this.firstVisibleItem == 0 && visibleItemCount >= ISFData.rankings.size())) && this.scrollState == SCROLL_STATE_IDLE) {
            Toast.makeText(context, "Bottom!" + visibleItemCount, Toast.LENGTH_SHORT).show();
        }
    }
}
