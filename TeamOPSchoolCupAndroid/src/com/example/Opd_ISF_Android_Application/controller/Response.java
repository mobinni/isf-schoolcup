package com.example.Opd_ISF_Android_Application.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 23/05/13
 * Time: 23:00
 * To change this template use File | Settings | File Templates.
 */
public class Response{
    private int result;
    private String error;

    public Response(int result, String error) {
        this.result = result;
        this.error = error;
    }

    public Response(int result){
        this.result = result;
    }

    public int getResult() {
        return result;
    }

    public String getError() {
        return error;
    }
}