package com.example.Opd_ISF_Android_Application.controller;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 10/04/13
 * Time: 18:17
 * To change this template use File | Settings | File Templates.
 */
public class URLDataFetcher {
    public static final String USER_AGENT = "ISFphoneDroid";

    public static String get(String location) throws IOException {
        final URL url = new URL(location);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);

        InputStream stream = null;
        try {
            stream = connection.getInputStream();
            return handleResponse(new BufferedInputStream(stream));
        } finally {
            connection.disconnect();
            if (stream != null) stream.close();
        }
    }

    public static String post(String location) throws IOException {
        final URL url = new URL(location);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestMethod("POST");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setUseCaches(false);

        InputStream stream = null;
        try {
            stream = connection.getInputStream();
            return handleResponse(new BufferedInputStream(stream));
        } finally {
            connection.disconnect();
            if (stream != null) stream.close();
        }
    }

    private static String handleResponse(InputStream input) throws IOException {
        return IOUtils.toString(input);
    }
}
