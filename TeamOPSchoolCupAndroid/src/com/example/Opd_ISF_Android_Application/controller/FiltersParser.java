package com.example.Opd_ISF_Android_Application.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.activity.MainActivity;
import com.example.Opd_ISF_Android_Application.model.ISFData;
import com.example.Opd_ISF_Android_Application.model.filter.Event;
import com.example.Opd_ISF_Android_Application.model.filter.Filter;
import com.example.Opd_ISF_Android_Application.model.filter.Region;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 12/05/13
 * Time: 16:55
 * To change this template use File | Settings | File Templates.
 */
public class FiltersParser extends AsyncTask {
    private Activity context;
    private ProgressDialog progressDialog;

    public FiltersParser(Activity context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(context, context.getString(R.string.prgTitleWait), context.getString(R.string.prgMsgLoading), true);
    }

    @Override
    protected Object doInBackground(Object... params) {
        boolean success = true;

        try {
            ISFData.resetFilters();

            ISFData.filter_regions.add(new Region(0, 0, "<Region>"));
            ISFData.filter_regions.addAll(DataController.parseRegions());
            ISFData.filter_sports.add(new Filter(0, "<Sport>"));
            ISFData.filter_sports.addAll(DataController.parseSports());
            ISFData.filter_events.add(new Event(0, "<Event>", 0));
            ISFData.filter_events.addAll(DataController.parseEvents());
            ISFData.filter_competitionYears.add(new Filter(0, "<Year>"));
            ISFData.filter_competitionYears.addAll(DataController.parseCompetitionYears());

            ISFData.filter_rankingTypes.add(new Filter(1, "School ranking"));
            ISFData.filter_rankingTypes.add(new Filter(2, "Student ranking"));

            ISFData.filter_genders.add(new Filter(0, "<Gender>"));
            ISFData.filter_genders.add(new Filter(1, "Male"));
            ISFData.filter_genders.add(new Filter(2, "Female"));

            ISFData.filter_ages.add(new Filter(0, "<Age>"));
            ISFData.filter_ages.add(new Filter(1, "14"));
            ISFData.filter_ages.add(new Filter(2, "15"));
            ISFData.filter_ages.add(new Filter(3, "16"));
            ISFData.filter_ages.add(new Filter(4, "17"));

            ISFData.filter_meetingTypes.add(new Filter(0, "<Meeting type>"));
            ISFData.filter_meetingTypes.add(new Filter(1, "Official"));
            ISFData.filter_meetingTypes.add(new Filter(2, "Unofficial"));
        } catch (IOException e) {
            success = false;
        } finally {
            if(success) success = (ISFData.filter_regions.size() > 0 && ISFData.filter_sports.size() > 0
                    && ISFData.filter_events.size() > 0 && ISFData.filter_competitionYears.size() > 0);
        }

        return success;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (progressDialog.isShowing()) progressDialog.dismiss();

        if (!(Boolean)o) {
            final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            alertBuilder.setTitle(context.getString(R.string.alrtTitleError));
            alertBuilder.setMessage(context.getString(R.string.alrtErrorLoading))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new FiltersParser(context).execute();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.finish();
                        }
                    });
            final AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        } else {
            ((MainActivity) context).initialiseComponents();
        }
    }
}