package com.example.Opd_ISF_Android_Application.controller;

import com.example.Opd_ISF_Android_Application.Constants;
import com.example.Opd_ISF_Android_Application.model.*;
import com.example.Opd_ISF_Android_Application.model.filter.Event;
import com.example.Opd_ISF_Android_Application.model.filter.Filter;
import com.example.Opd_ISF_Android_Application.model.filter.Region;
import com.example.Opd_ISF_Android_Application.model.memberdata.Meeting;
import com.example.Opd_ISF_Android_Application.model.memberdata.MeetingEvent;
import com.example.Opd_ISF_Android_Application.model.memberdata.MemberData;
import com.example.Opd_ISF_Android_Application.model.memberdata.Student;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 26/04/13
 * Time: 22:31
 * To change this template use File | Settings | File Templates.
 */

public class DataController {

    public static List<Filter> parseSports() throws IOException {
        final String data = URLDataFetcher.get(Constants.BASE_URL + Constants.API + Constants.PATH_SPORTS);
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<Filter>>() {
        }.getType();
        final List<Filter> sports = gson.fromJson(data, rootType);

        if (sports == null) throw new JsonSyntaxException("Could not parse filter_sports");

        return sports;
    }

    public static List<Filter> parseEvents() throws IOException {
        final String data = URLDataFetcher.get(Constants.BASE_URL + Constants.API + Constants.PATH_EVENTS);
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<Event>>() {
        }.getType();
        final List<Filter> events = gson.fromJson(data, rootType);

        if (events == null) throw new JsonSyntaxException("Could not parse filter_events");

        return events;
    }

    public static List<Filter> parseRegions() throws IOException {
        final String data = URLDataFetcher.get(Constants.BASE_URL + Constants.API + Constants.PATH_REGIONS);
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<Region>>() {
        }.getType();
        final List<Filter> regions = gson.fromJson(data, rootType);

        if (regions == null) throw new JsonSyntaxException("Could not parse filter_regions");

        return regions;
    }

    public static List<Filter> parseCompetitionYears() throws IOException {
        final String data = URLDataFetcher.get(Constants.BASE_URL + Constants.API + Constants.PATH_COMPETITION_YEARS);
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<Filter>>() {
        }.getType();
        final List<Filter> competitionYears = gson.fromJson(data, rootType);

        if (competitionYears == null) throw new JsonSyntaxException("Could not parse competition years");

        return competitionYears;
    }

    //Strings since these are optional thus easier to check if null
    public static List<Ranking> parseStudentRanking(int eventId, String year, String isOfficial, String gender, String regionId, String age) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_STUDENT_RANKING);
        locationBuilder.append(Constants.PARAM_GET_EVENT).append(eventId);
        if (year != null) locationBuilder.append(Constants.PARAM_YEAR).append(year);
        if (isOfficial != null) locationBuilder.append(Constants.PARAM_OFFICIAL).append(isOfficial);
        if (gender != null) locationBuilder.append(Constants.PARAM_GENDER).append(gender);
        if (regionId != null) locationBuilder.append(Constants.PARAM_REGION).append(regionId);
        if (age != null) locationBuilder.append(Constants.PARAM_AGE).append(age);

        final String data = URLDataFetcher.get(locationBuilder.toString());
        final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        final Type rootType = new TypeToken<List<Ranking>>() {
        }.getType();

        List<Ranking> rankings = gson.fromJson(data, rootType);
        if (rankings == null) rankings = new ArrayList<Ranking>();

        return rankings;
    }

    public static List<Ranking> parseTeamRanking(int sportId, String year, String regionId, String gender) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_TEAM_RANKING);
        locationBuilder.append(Constants.PARAM_SPORT).append(sportId);
        if (year != null) locationBuilder.append(Constants.PARAM_YEAR).append(year);
        if (regionId != null) locationBuilder.append(Constants.PARAM_REGION).append(regionId);
        if (gender != null) locationBuilder.append(Constants.PARAM_GENDER).append(gender);

        final String data = URLDataFetcher.get(locationBuilder.toString());
        final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        final Type rootType = new TypeToken<List<Ranking>>() {
        }.getType();

        List<Ranking> rankings = gson.fromJson(data, rootType);
        if (rankings == null) rankings = new ArrayList<Ranking>();

        return rankings;
    }

    public static List<Comparison> parseStudentComparison(String studentId1, String studentId2, String year) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_STUDENT_COMPARISON);
        locationBuilder.append(Constants.PARAM_STUDENT1).append(studentId1);
        locationBuilder.append(Constants.PARAM_STUDENT2).append(studentId2);
        if (year != null) locationBuilder.append(Constants.PARAM_YEAR).append(year);

        final String data = URLDataFetcher.get(locationBuilder.toString());
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<Comparison>>() {
        }.getType();

        final List<Comparison> comparisons = gson.fromJson(data, rootType);
        if (comparisons == null) throw new JsonSyntaxException("Could not parse student comparison");

        return comparisons;
    }

    public static List<Comparison> parseTeamComparison(int teamId1, int teamId2) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_TEAM_COMPARISON);
        locationBuilder.append(Constants.PARAM_TEAM1).append(teamId1);
        locationBuilder.append(Constants.PARAM_TEAM2).append(teamId2);

        final String data = URLDataFetcher.get(locationBuilder.toString());
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<Comparison>>() {
        }.getType();

        final List<Comparison> comparisons = gson.fromJson(data, rootType);
        if (comparisons == null) throw new JsonSyntaxException("Could not parse team comparison");

        return comparisons;
    }

    public static List<MemberData> parseTeams(String mak) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_TEAMS);
        locationBuilder.append(Constants.PARAM_MAK).append(mak);

        final String data = URLDataFetcher.get(locationBuilder.toString());
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<MemberData>>() {
        }.getType();

        List<MemberData> teams = gson.fromJson(data, rootType);
        if (teams == null) teams = new ArrayList<MemberData>();

        return teams;
    }

    public static List<Meeting> parseMeetings(String mak) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_MEETINGS);
        locationBuilder.append(Constants.PARAM_MAK).append(mak);

        final String data = URLDataFetcher.get(locationBuilder.toString());
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<Meeting>>() {
        }.getType();

        List<Meeting> meetings = gson.fromJson(data, rootType);
        if (meetings == null) meetings = new ArrayList<Meeting>();

        return meetings;
    }

    public static List<MeetingEvent> parseMeetingEvents(String mak) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_MEETING_EVENTS);
        locationBuilder.append(Constants.PARAM_MAK).append(mak);

        final String data = URLDataFetcher.get(locationBuilder.toString());
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<MeetingEvent>>() {
        }.getType();

        List<MeetingEvent> meetingEvents = gson.fromJson(data, rootType);
        if (meetingEvents == null) meetingEvents = new ArrayList<MeetingEvent>();

        return meetingEvents;
    }

    public static List<Student> parseTeamStudents(String mak) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_TEAM_STUDENTS);
        locationBuilder.append(Constants.PARAM_MAK).append(mak);

        final String data = URLDataFetcher.get(locationBuilder.toString());
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<List<Student>>() {
        }.getType();

        List<Student> students = gson.fromJson(data, rootType);
        if (students == null) students = new ArrayList<Student>();

        return students;
    }

    public static Response postResult(String mak, double resultScore, int teamId, int meetingId, int eventId, String studentId) throws IOException {
        final StringBuilder locationBuilder = new StringBuilder();
        locationBuilder.append(Constants.BASE_URL).append(Constants.API).append(Constants.PATH_POST_RESULT);
        locationBuilder.append(Constants.PARAM_MAK).append(mak);
        locationBuilder.append(Constants.PARAM_RESULT).append(resultScore);
        locationBuilder.append(Constants.PARAM_TEAM).append(teamId);
        locationBuilder.append(Constants.PARAM_MEETING).append(meetingId);
        locationBuilder.append(Constants.PARAM_POST_EVENT).append(eventId);
        if (studentId != null) locationBuilder.append(Constants.PARAM_STUDENT).append(studentId);

        final String data = URLDataFetcher.post(locationBuilder.toString());
        final Gson gson = new GsonBuilder().create();
        final Type rootType = new TypeToken<Response>(){}.getType();

        final Response response = gson.fromJson(data, rootType);
        if(response == null) throw new JsonSyntaxException("Could not parse response");

        return response;
    }
}
