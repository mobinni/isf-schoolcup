package com.example.Opd_ISF_Android_Application.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import com.example.Opd_ISF_Android_Application.Constants;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.activity.PostActivity;
import com.example.Opd_ISF_Android_Application.model.*;
import com.example.Opd_ISF_Android_Application.model.memberdata.Meeting;
import com.example.Opd_ISF_Android_Application.model.memberdata.MeetingEvent;
import com.example.Opd_ISF_Android_Application.model.memberdata.MemberData;
import com.example.Opd_ISF_Android_Application.model.memberdata.Student;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 22/05/13
 * Time: 20:16
 * To change this template use File | Settings | File Templates.
 */
public class MemberDataParser extends AsyncTask {
    private Activity context;
    private ProgressDialog progressDialog;

    public MemberDataParser(Activity context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(context, context.getString(R.string.prgTitleWait), context.getString(R.string.prgMsgLoading), true);
    }

    @Override
    protected Object doInBackground(Object... params) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        boolean success = true;

        ISFData.resetMemberData();

        try {
            final String mak = sharedPreferences.getString(Constants.KEY_MAK, "");

            if (!mak.isEmpty()) {
                ISFData.member_teams.add(new MemberData(0, "<Team>"));
                ISFData.member_teams.addAll(DataController.parseTeams(mak));

                ISFData.member_meetings.add(new Meeting(0, "<Meeting>", 0));
                ISFData.member_meetings.addAll(DataController.parseMeetings(mak));

                ISFData.member_meeting_events.add(new MeetingEvent(0, "<Event>", 0));
                ISFData.member_meeting_events.addAll(DataController.parseMeetingEvents(mak));

                ISFData.member_team_students.add(new Student("0", "<Student>", 0));
                ISFData.member_team_students.addAll(DataController.parseTeamStudents(mak));
            }
        } catch (IOException e) {
            success = false;
        }

        return success;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if(progressDialog.isShowing()) progressDialog.dismiss();

        if(!(Boolean)o){
            final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            alertBuilder.setTitle(context.getString(R.string.alrtTitleError));
            alertBuilder.setMessage(context.getString(R.string.alrtErrorLoading))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new MemberDataParser(context).execute();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.finish();
                        }
                    });
            final AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }else{
            ((PostActivity) context).initialiseComponents();
        }
    }
}
