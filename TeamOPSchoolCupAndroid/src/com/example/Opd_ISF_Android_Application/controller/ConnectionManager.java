package com.example.Opd_ISF_Android_Application.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.Opd_ISF_Android_Application.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 26/03/13
 * Time: 19:16
 * To change this template use File | Settings | File Templates.
 */
public class ConnectionManager {
    public static boolean isConnected(Activity context) {
        final ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = connManager.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static boolean isLoggedIn(Activity context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);

        return sharedPreferences.contains(Constants.KEY_MAK) && sharedPreferences.contains(Constants.KEY_ROLE);
    }
}
