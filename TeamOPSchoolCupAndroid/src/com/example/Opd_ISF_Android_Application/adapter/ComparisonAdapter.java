package com.example.Opd_ISF_Android_Application.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.model.Comparison;
import com.example.Opd_ISF_Android_Application.model.ISFData;
import com.example.Opd_ISF_Android_Application.model.Ranking;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 12/05/13
 * Time: 13:10
 * To change this template use File | Settings | File Templates.
 */
public class ComparisonAdapter extends ArrayAdapter<Comparison> {
    private Activity context;

    public ComparisonAdapter(Activity context) {
        super(context, R.layout.comparison_custom_list_item);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            final LayoutInflater inflater = context.getLayoutInflater();
            final ViewHolder viewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.comparison_custom_list_item, null);

            viewHolder.rank1 = (TextView) view.findViewById(R.id.tvComparisonRank1);
            viewHolder.result1 = (TextView) view.findViewById(R.id.tvComparisonResult1);
            viewHolder.event = (TextView) view.findViewById(R.id.tvComparisonEvent);
            viewHolder.result2 = (TextView) view.findViewById(R.id.tvComparisonResult2);
            viewHolder.rank2 = (TextView) view.findViewById(R.id.tvComparisonRank2);

            view.setTag(viewHolder);
        }else{
            view = convertView;
        }

        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        final Comparison comparison = ISFData.comparisons.get(position);

        viewHolder.rank1.setText(comparison.getRank1() == 0 ? "" : String.valueOf(comparison.getRank1()));
        viewHolder.result1.setText(String.valueOf(comparison.getResult1()));
        viewHolder.event.setText(comparison.getEventName());
        viewHolder.result2.setText(String.valueOf(comparison.getResult2()));
        viewHolder.rank2.setText(comparison.getRank2() == 0 ? "" : String.valueOf(comparison.getRank2()));

        view.setBackgroundResource(position % 2 == 0 ? R.color.white : R.color.light_gray);

        return view;
    }

    @Override
    public Comparison getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getCount() {
        return (ISFData.comparisons != null ? ISFData.comparisons.size() : 0);
    }

    private class ViewHolder {
        private TextView rank1, result1,
                event, result2, rank2;
    }
}
