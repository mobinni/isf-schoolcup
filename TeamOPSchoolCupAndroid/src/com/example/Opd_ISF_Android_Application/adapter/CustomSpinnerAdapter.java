package com.example.Opd_ISF_Android_Application.adapter;

import android.app.Activity;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.model.memberdata.MemberData;
import com.example.Opd_ISF_Android_Application.model.memberdata.Student;
import com.example.Opd_ISF_Android_Application.model.filter.Filter;
import com.example.Opd_ISF_Android_Application.model.filter.Region;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 12/04/13
 * Time: 14:55
 * To change this template use File | Settings | File Templates.
 */
public class CustomSpinnerAdapter implements android.widget.SpinnerAdapter {
    private Activity context;
    private List<Object> data;

    public CustomSpinnerAdapter(Activity context, List<Object> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        final View view = getView(position, convertView, parent);

        ((TextView) view).setTextSize(17);
        view.setPadding(10, 10, 0, 10);

        if (getItem(position) instanceof Region) {
            view.setPadding(10 + ((Region) getItem(position)).getLevel() * 20, 10, 0, 10);
        }

        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        final Object item = getItem(position);

        if (item instanceof MemberData) return ((MemberData) item).getId();
        if(item instanceof Student) return 0; // Type of Student id is not numerical
        return ((Filter) item).getId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TextView view = new TextView(context);

        view.setSingleLine(true);
        view.setTextSize(17);
        view.setTextColor(Color.BLACK);
        view.setText(getItem(position).toString());

        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.custom_spinner_item;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }
}
