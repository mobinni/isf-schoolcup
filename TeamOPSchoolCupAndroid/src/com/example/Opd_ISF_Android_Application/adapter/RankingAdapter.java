package com.example.Opd_ISF_Android_Application.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.Opd_ISF_Android_Application.R;
import com.example.Opd_ISF_Android_Application.model.ISFData;
import com.example.Opd_ISF_Android_Application.model.Ranking;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 27/03/13
 * Time: 20:59
 * To change this template use File | Settings | File Templates.
 */
public class RankingAdapter extends ArrayAdapter<Ranking> {
    private Activity context;

    public RankingAdapter(Activity context) {
        super(context, R.layout.ranking_custom_list_item);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            final LayoutInflater inflater = context.getLayoutInflater();
            final ViewHolder viewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.ranking_custom_list_item, null);

            viewHolder.rank = (TextView) view.findViewById(R.id.tvRankingRank);
            viewHolder.name = (TextView) view.findViewById(R.id.tvRankingName);
            viewHolder.result = (TextView) view.findViewById(R.id.tvRankingResult);
            viewHolder.extras = (LinearLayout) view.findViewById(R.id.lnlRankingExtras);
            viewHolder.region = (TextView) view.findViewById(R.id.tvRankingRegion);
            viewHolder.extra1 = (TextView) view.findViewById(R.id.tvRankingExtra1);
            viewHolder.extra2 = (TextView) view.findViewById(R.id.tvRankingExtra2);
            viewHolder.checkBox = (CheckBox) view.findViewById(R.id.chkRankingSelected);
            viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    final Ranking rank = (Ranking) viewHolder.checkBox.getTag();
                    rank.setSelected(isChecked);
                }
            });

            view.setTag(viewHolder);
            viewHolder.checkBox.setTag(ISFData.rankings.get(position));
        } else {
            view = convertView;
            ((ViewHolder) view.getTag()).checkBox.setTag(ISFData.rankings.get(position));
        }
        final ViewHolder holder = (ViewHolder) view.getTag();
        final Ranking rank = ISFData.rankings.get(position);

        holder.rank.setText(String.valueOf(rank.getRank()));
        holder.name.setText(rank.getName());
        holder.result.setText(rank.getResult() % 1.0 > 0 ? String.valueOf(rank.getResult()) : String.format("%.0f", rank.getResult()));
        holder.extras.setVisibility(rank.isExpanded() ? View.VISIBLE : View.GONE);
        holder.region.setText(String.format("Region: %s", rank.getRegion()));
        holder.extra1.setText(String.format("%s: %s", (rank.getExtra1().matches("\\d+/\\d+/\\d+") ? "Date" : "School"), rank.getExtra1()));
        holder.extra2.setText(String.format("Achieved at: %s", rank.getExtra2()));
        holder.checkBox.setChecked(rank.isSelected());

        view.setBackgroundResource(position % 2 == 0 ? R.color.white : R.color.light_gray);

        return view;
    }

    @Override
    public Ranking getItem(int position) {
        return super.getItem(position);
    }

    public void updateRankings(List<Ranking> rankings){
        ISFData.rankings.addAll(rankings);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (ISFData.rankings != null ? ISFData.rankings.size() : 0);
    }

    private class ViewHolder {
        private LinearLayout extras;
        private TextView rank, name, result, region, extra1, extra2;
        private CheckBox checkBox;
    }

}
