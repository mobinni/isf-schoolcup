package com.example.Opd_ISF_Android_Application.view;

import android.app.Activity;
import android.view.MotionEvent;
import android.webkit.WebView;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 24/04/13
 * Time: 21:52
 * To change this template use File | Settings | File Templates.
 */
public class LiveWebView extends WebView {
    public LiveWebView(Activity context) {
        super(context);
    }

    @Override
    public boolean onCheckIsTextEditor() {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP:
                if (!hasFocus())
                    requestFocus();
                break;
        }

        return super.onTouchEvent(event);
    }
}
