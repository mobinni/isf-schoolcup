package com.example.Opd_ISF_Android_Application.model;

import com.example.Opd_ISF_Android_Application.model.filter.Filter;
import com.example.Opd_ISF_Android_Application.model.memberdata.MemberData;
import com.example.Opd_ISF_Android_Application.model.memberdata.Student;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 12/04/13
 * Time: 17:56
 * To change this template use File | Settings | File Templates.
 */
public abstract class ISFData {
    /*
    Filters
     */
    public static final List<Filter> filter_regions = new ArrayList<Filter>();
    public static final List<Filter> filter_sports = new ArrayList<Filter>();
    public static final List<Filter> filter_events = new ArrayList<Filter>();
    public static final List<Filter> filter_competitionYears = new ArrayList<Filter>();
    public static final List<Filter> filter_genders = new ArrayList<Filter>();
    public static final List<Filter> filter_ages = new ArrayList<Filter>();
    public static final List<Filter> filter_meetingTypes = new ArrayList<Filter>();
    public static final List<Filter> filter_rankingTypes = new ArrayList<Filter>();

    public static final List<Ranking> rankings = new ArrayList<Ranking>();
    public static final List<Comparison> comparisons = new ArrayList<Comparison>();

    public static final List<MemberData> member_teams = new ArrayList<MemberData>();
    public static final List<MemberData> member_meetings = new ArrayList<MemberData>();
    public static final List<MemberData> member_meeting_events = new ArrayList<MemberData>();
    public static final List<Student> member_team_students = new ArrayList<Student>();

    public static void resetFilters() {
        filter_regions.clear();
        filter_sports.clear();
        filter_events.clear();
        filter_competitionYears.clear();
        filter_genders.clear();
        filter_ages.clear();
        filter_meetingTypes.clear();
        filter_rankingTypes.clear();
    }

    public static void resetMemberData(){
        member_teams.clear();
        member_meetings.clear();
        member_meeting_events.clear();
        member_team_students.clear();
    }

    public static Filter getCurrentCompetitionYear() {
        final int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        for (Filter competitionYear : filter_competitionYears) {
            if (competitionYear.toString().equalsIgnoreCase(String.valueOf(currentYear))) {
                return competitionYear;
            }
        }

        return null;
    }

    public static List<Ranking> getSelectedRankings() {
        final List<Ranking> selectedRankings = new ArrayList<Ranking>();

        for (Ranking r : rankings) if (r.isSelected()) selectedRankings.add(r);

        return selectedRankings;
    }
}
