package com.example.Opd_ISF_Android_Application.model.filter;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 26/04/13
 * Time: 22:45
 * To change this template use File | Settings | File Templates.
 */
public class Filter {
    private int id;
    private String name;

    public Filter(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
