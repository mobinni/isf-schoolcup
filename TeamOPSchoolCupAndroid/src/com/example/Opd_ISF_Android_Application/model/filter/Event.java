package com.example.Opd_ISF_Android_Application.model.filter;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 26/04/13
 * Time: 22:20
 * To change this template use File | Settings | File Templates.
 */
public class Event extends Filter{
    private int sport;

    public Event(int id, String name, int sport) {
        super(id, name);
        this.sport = sport;
    }

    public int getSport() {
        return sport;
    }
}
