package com.example.Opd_ISF_Android_Application.model.filter;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 26/04/13
 * Time: 22:21
 * To change this template use File | Settings | File Templates.
 */
public class Region  extends Filter {
    private int level;

    public Region(int id, int level, String name) {
        super(id, name);
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
