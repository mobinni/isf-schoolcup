package com.example.Opd_ISF_Android_Application.model;

import com.google.gson.annotations.Expose;

/**
 * Created with IntelliJ IDEA.
 * User: Dell
 * Date: 27/03/13
 * Time: 20:58
 * To change this template use File | Settings | File Templates.
 */
public class Ranking {
    @Expose
    private int rank;
    @Expose
    private String entityId;
    @Expose
    private String name;
    @Expose
    private double result;
    @Expose
    private String region;
    @Expose
    private String extra1;
    @Expose
    private String extra2;

    private boolean isSelected;
    private boolean isExpanded;

    public Ranking(int rank, String entityId, String name, double result, String region, String extra1, String extra2) {
        this.rank = rank;
        this.entityId = entityId;
        this.name = name;
        this.result = result;
        this.region = region;
        this.extra1 = extra1;
        this.extra2 = extra2;
        this.isSelected = false;
        this.isExpanded = false;
    }

    public int getRank() {
        return rank;
    }

    public String getEntityId() {
        return entityId;
    }

    public String getName() {
        return name;
    }

    public double getResult() {
        return result;
    }

    public String getRegion() {
        return region;
    }

    public String getExtra1() {
        return extra1;
    }

    public String getExtra2() {
        return extra2;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public boolean switchExpanded() {
        this.isExpanded = !isExpanded;

        return isExpanded;
    }
}
