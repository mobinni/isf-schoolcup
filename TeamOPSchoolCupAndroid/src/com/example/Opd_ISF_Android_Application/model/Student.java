package com.example.Opd_ISF_Android_Application.model;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 23/05/13
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public class Student {
    private String id, name;
    private int teamId;

    public Student(String id, String name, int teamId) {
        this.id = id;
        this.name = name;
        this.teamId = teamId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }
}
