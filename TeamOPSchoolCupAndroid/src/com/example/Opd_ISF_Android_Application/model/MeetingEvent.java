package com.example.Opd_ISF_Android_Application.model;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 23/05/13
 * Time: 14:48
 * To change this template use File | Settings | File Templates.
 */
public class MeetingEvent extends MemberData {
    private int meetingId;

    public MeetingEvent(int id, String name, int meetingId) {
        super(id, name);
        this.meetingId = meetingId;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }
}
