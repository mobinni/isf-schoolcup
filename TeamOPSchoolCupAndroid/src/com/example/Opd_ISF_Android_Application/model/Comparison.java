package com.example.Opd_ISF_Android_Application.model;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 1/05/13
 * Time: 21:06
 * To change this template use File | Settings | File Templates.
 */
public class Comparison {
    private String name1;
    private double result1;
    private int rank1;
    private String eventName;
    private int rank2;
    private double result2;
    private String name2;

    public Comparison(String name1, double result1, int rank1, String eventName, int rank2, double result2, String name2) {
        this.name1 = name1;
        this.result1 = result1;
        this.rank1 = rank1;
        this.eventName = eventName;
        this.rank2 = rank2;
        this.result2 = result2;
        this.name2 = name2;
    }

    public String getName1() {
        return name1;
    }

    public double getResult1() {
        return result1;
    }

    public int getRank1() {
        return rank1;
    }

    public String getEventName() {
        return eventName;
    }

    public int getRank2() {
        return rank2;
    }

    public double getResult2() {
        return result2;
    }

    public String getName2() {
        return name2;
    }
}
