package com.example.Opd_ISF_Android_Application.model;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 22/05/13
 * Time: 23:47
 * To change this template use File | Settings | File Templates.
 */
public class MemberData {
    private int id;
    private String name;

    public MemberData(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }
}
