package com.example.Opd_ISF_Android_Application.model.memberdata;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 22/05/13
 * Time: 23:46
 * To change this template use File | Settings | File Templates.
 */
public class Meeting extends MemberData {
    private int teamId;

    public Meeting(int id, String name, int teamid) {
        super(id, name);
        this.teamId = teamid;
    }

    public int getTeamId() {
        return teamId;
    }
}
