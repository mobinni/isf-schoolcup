package com.example.Opd_ISF_Android_Application.model.memberdata;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 23/05/13
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public class Student {
    private String id, name;
    private int teamId;

    public Student(String id, String name, int teamId) {
        this.id = id;
        this.name = name;
        this.teamId = teamId;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getTeamId() {
        return teamId;
    }
}
